﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Mentions légales" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-mentions_legales" -->
<!-- var(last-update)="2020-05-03 15:14:47" -->
<!-- var(lang)="en" -->
<!-- var(site:lang)="en" -->

Legal
=====


<!-- begin-include(disclaimer-trans-en) -->

**Notice about the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page has been translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page before undertaking any action.

The Enterprise Bank Service can not be held responsible for the operation of information that will be inaccurate due to a non-automatic translation faithful to the original.<!-- alert-end:warning -->

<!-- end-include -->
Latest update:<!-- begin-var(last-update) -->2020-05-03 15:14:47<!-- end-var -->

Editor's identification
-----------------------

```
Service à compétence nationale Guichet Entreprises
120 rue de Bercy – Télédoc 766
75572 Paris cedex 12


```
The Business Box Office, created by the[decree of April 22, 2015](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=A1B17F7C2CD76682B8F5E3E5CE690458.tpdila13v_1?cidTexte=JORFTEXT000030517635&dateTexte=&oldAction=rechJO&categorieLien=id&idJO=JORFCONT000030517419), is under the authority of the[Corporate Directorate](https://www.entreprises.gouv.fr/) (DGE) within the[Ministry of Economy and Finance](https://www.economie.gouv.fr/).

Editorial design, monitoring, technical maintenance and website updates[www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/) are provided by the Business Box Office.

Editor
------

Mr. Florent Tournois, Head of The Business Box Office.

Accommodation provider
----------------------

```
Cloud Temple
215 avenue Georges Clémenceau
92024 Nanterre
Tel : +33 1 41 91 77 77


```
Object of the site
------------------

The www.guichet-qualifications.fr site allows any resident of the European Union or the European Economic Area to have their professional qualifications recognised and to practise a regulated profession in France with a view to a sustainable establishment ([free establishment](https://www.guichet-entreprises.fr/en/eugo/libre-etablissement-le/)) or temporary ([free service delivery](https://www.guichet-entreprises.fr/en/eugo/libre-prestation-de-services-lps/)). It also allows residents in France to learn about the possibilities of accessing and practising a regulated profession in another Member State.

The formalities are the subject of an electronic signature, the terms and conditions of which are available on the website at the following link:[Electronic signing policy](electronic_signature_policy.md).

In addition, the www.guichet-qualifications.fr site provides contractors with a set of information on the formalities, procedures and requirements of regulated activities in accordance with Article R. 123-2 of the Code of Commerce.

Processing personal data
------------------------

Personal data contained in the single file in accordance with the General Data Protection Regulations are subject to provisions available on this site at the following link:[Data protection policy](policy_protection_gives.md).

Under this regulation and the Computer and Freedoms Act of January 6, 1978, you have the right to access, correct, modify and delete the data that concerns you.

There are a number of ways you can exercise this right:

- By contacting the Business Formalities Centre (CFE) that receives the declaration file;
- Sending an email to the support
- by sending a letter to:


```
  Service à compétence nationale Guichet Entreprises
  120 rue de Bercy – Télédoc 766
  75572 Paris cedex 12
```

Reproductive rights
-------------------

The content of this site falls under French and international copyright and intellectual property legislation.

All the graphic elements of the site are owned by the Business Box Office service. Any reproduction or adaptation of the pages of the site that would take the graphic elements is strictly forbidden.

Any use of content for commercial purposes is also prohibited.

Any citation or reprint of content from the site must have obtained the permission of the editor. The source (www.guichet-qualifications.fr) and the date of the copy will have to be indicated as well as the "Business Box" service.

Links to the site's pages
-------------------------

Any public or private site is allowed to link to the pages of the www.guichet-qualifications.fr site. There is no need to ask for prior authorization. However, the origin of the information will need to be clarified, for example in the form of: "Professional Qualification Recognition (source: www.guichet-qualifications.fr, a business box office site)". The pages of the www.guichet-qualifications.fr site should not be nested within the pages of another site. They will need to be displayed in a new window or tab.

Links to outdoor site pages
---------------------------

Links on the www.guichet-qualifications.fr site can direct the user to external sites whose content cannot in any way engage the responsibility of the Enterprise Box office.

Technical environment
---------------------

Some browsers may block windows on this site by default. In order to allow you to view certain pages, you must allow the windows to open when the browser offers you by clicking on the warning headband then displayed at the top of the page.

If your browser does not have a warning message, you need to set it up to allow windows to open for the www.guichet-qualifications.fr site.

