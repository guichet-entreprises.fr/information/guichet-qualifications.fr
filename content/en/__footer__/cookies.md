﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Utilisation des cookies sur guichet-qualifications.fr" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-cookies" -->
<!-- var(last-update)="2020-05-03 15:14:37" -->
<!-- var(lang)="en" -->
<!-- var(site:lang)="en" -->

Using cookies on guichet-qualifications.fr
==========================================


<!-- begin-include(disclaimer-trans-en) -->

**Notice about the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page has been translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page before undertaking any action.

The Enterprise Bank Service can not be held responsible for the operation of information that will be inaccurate due to a non-automatic translation faithful to the original.<!-- alert-end:warning -->

<!-- end-include -->
Latest update:<!-- begin-var(last-update) -->2020-05-03 15:14:37<!-- end-var -->

What are cookies and how do you use the Business Box Office?
------------------------------------------------------------

Box Office Can use cookies when a user browses their site. Cookies are files sent to the browser via a web server to record the user's activities during their browsing time. The use of cookies makes it possible to recognize the web browser used by the user in order to facilitate its navigation. Cookies are also used to measure the site's audience and produce consultation statistics.

Cookies used by Enterprise Box Office do not provide references to deduce users' personal data or personal information to identify a particular user. They are temporary in nature, with the sole purpose of making subsequent transmission more efficient. No cookie used on the site will have a vigour period of more than two years.

Users have the option to set up their browser to be notified of the receipt of cookies and to refuse installation. By banning cookies or disabling them, the user may not be able to access certain features of the site.

In the event of a cookie refusal or deactivation, the session should be restarted.

What types of cookies are used by guichet-qualifications.fr?
------------------------------------------------------------

### Technical cookies

They allow the user to browse the site and use some of its features.

### Analysis cookies

Box Office uses audience analytics cookies to quantify the number of visitors. These cookies are used to measure and analyze how users navigate the site.

