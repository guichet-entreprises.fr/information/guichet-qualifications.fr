﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Conditions générales d’utilisation" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-cgu" -->
<!-- var(last-update)="2020-05-03 15:14:33" -->
<!-- var(lang)="en" -->
<!-- var(site:lang)="en" -->

Terms and conditions
====================


<!-- begin-include(disclaimer-trans-en) -->

**Notice about the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page has been translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page before undertaking any action.

The Enterprise Bank Service can not be held responsible for the operation of information that will be inaccurate due to a non-automatic translation faithful to the original.<!-- alert-end:warning -->

<!-- end-include -->
Latest update:<!-- begin-var(last-update) -->2020-05-03 15:14:33<!-- end-var -->

Preamble
--------

This document outlines the terms of commitment to the use of teleservice Box Qualifications for users. It is part of the legal framework:

- Of[General Data Protection Regulation](https://www.cnil.fr/en/reglement-europeen-protection-donnees) ;
- [Directive 2005/36/EC](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036) European Parliament;
- [2013/55/EU Directive](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32013L0055) European Parliament;
- Of[Regulation 910/2014](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32014R0910) European Parliament and the Council of 23 July 2014 (e-IDAS) on electronic identification and trusted services for electronic transactions within the internal market;
- of the[December 8, 2005 order](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000636232) electronic exchanges between users and administrative authorities and between administrative authorities and the[Decree No. 2010-112 of February 2, 2010](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021779444&categorieLien=id) taken for the application of Articles 9, 10 and 12 of this ordinance;
- Of[Act 78-17 of January 6, 1978 amended](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460) computer science, files and freedoms;
- of the[Article 441-1 of the Penal Code](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006418753&cidTexte=LEGITEXT000006070719&dateTexte=20020101) ;
- of the[Article R. 123-30 of the Code of Commerce](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006256107&dateTexte=&categorieLien=cid) ;
- of the[decree of April 22, 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&categorieLien=id) creating a nationally-competent service called a "business window."

The subject of the document
---------------------------

The purpose of this document is to define the general terms and conditions of use of the Teleservice Box Qualifications, referred to as the "Service" below, between the national service Guichet Enterprises and users.

The national service Guichet Enterprises is under the authority of the Corporate Directorate of the Ministry of Economy and Finance.

Definition and purpose of the Service
-------------------------------------

The service implemented by the national service Guichet Enterprises (hereafter referred to as the "Business Box Service") helps to simplify the procedures related to the creation, changes of the situation and the cessation of a business of French and European users.

This Service has been subject to safety approval in accordance with ANSSI Interministerial Instruction 901/SGDSN/ANSSI (NOR: PRMD1503279 28/01/2015 and the PGSSI of the Economic and Financial Ministries (NOR: FCPP1622039A) of August 1, 2016.

The use of the Service is optional and free. However, as part of the formalities carried out through this Service, such as business start-up, online payments may be requested. These are secure.

All of the Service's recipients are referred to as the partner agencies below.

Under Section R.123-3 of the Code of Commerce, the data collected is transmitted for processing to the following partner organizations:

"1) Subject to the provisions of the 2nd and 3rd, the territorial chambers of commerce and industry create and manage the formalities centres of the companies responsible for:

# Traders;
# Commercial companies.

(2) The chambers of trades and crafts in the region create and manage the competent centres for individuals and companies subject to registration in the trades directory and for individuals benefiting from the registration exemption provided in Article 19 of Law 96-603 of 5 July 1996 relating to the development and promotion of trade and crafts. , excluding those mentioned in the 3rd of this article.

3. The National Chamber of Craft Boating creates and manages the centre for individuals and companies subject to registration in the register of artisanal boating companies.

(4) Commercial or commercially-ruling registries create and manage the centres responsible for:

# Civil and non-commercial companies;
# Liberal exercise societies;
# legal persons subject to registration in the register of trade and companies other than those mentioned in the 1, 2 and 3
# Industrial and commercial public establishments;
# Commercial agents;
# economic interest groups and European economic interest groups.

(5) Social security and family benefit collection unions (Urssaf) or general social security funds create and manage the centres responsible for:

# persons engaged, as a regular occupation, in a regulated or non-commercial, artisanal or agricultural independent activity;
# employers whose businesses are not registered in the register of trades and companies, in the trades directory or in the register of artisanal boating companies, and who do not report to the centres mentioned in the 6th.

(6) The Chambers of Agriculture create and manage the competent centres for individuals and corporations carrying out agricultural activities as principal[...]. »

Features
--------

The Service allows the user:

- access to documentation detailing the obligations of the Business Formality Centres (CFEs) as well as the building blocks of the declaration file and the application file;
- create a user account that provides access to personal storage space. This space allows the user to manage and use his personal data, to keep the information about him and the documents and supporting documents necessary for the completion of the administrative procedures.

From its space, the user can:

- build its unique file under Article R. 123-1 of the Code of Commerce, which includes the declaration file and, if applicable, applications for authorization;
- submit its declaration file and, if necessary, requests for authorisation to the relevant business formalities centre;
- Access the follow-up information on the processing of his file and, if necessary, his application file;
- enable CFEs to implement the treatments necessary to exploit the information received from the corporation covered in the last paragraph of Article R. 123-21 of the Code of Commerce: receipt of the single file under Article 2 of Act 94-126 of 11 February 1994 relating to individual initiative and business and forwarded by the legal person covered by Article R. 123-21 of the Code of Commerce; Receiving follow-up information on the processing of these files as provided by partner agencies and authorities; transmission of follow-up information from the processing of unique files to the legal person covered by Article R. 121-21 of the Code of Commerce.

How to register and use the Service
-----------------------------------

Access to the Service is open to anyone and free. It is optional and is not exclusive to other access channels to allow the user to complete his formalities.

For the management of access to the Service's personal space, the user shares the following information:

- user's email address.
- The password chosen by the user

The use of the Service requires the provision of personal data for the creation of the single folder.
The processing of personal data is described in the available data protection policy[Here](policy_protection_gives.md).

Using the Service requires a connection and an internet browser. The browser must be configured to allow*Cookies* session session.

To ensure an optimal browsing experience, we recommend using the following browser versions:

- Firefox version 45 and up;
- Google Chrome version 48 and up.

Indeed, other browsers may not support certain features of the Service.

The Service is optimized for a 1024-768 pixel display. It is recommended to use the latest version of the browser and update it regularly to benefit from security patches and best performance.

Specific terms of use of the signature service
----------------------------------------------

The electronic signature service is accessible directly via the Service.

Article R. 123-24 of the Code of Commerce under Regulation 910/2014 of the European Parliament and the Council of 23 July 2014 (e-IDAS) are the references applicable to the Service's electronic signature service.

Roles and commitment
--------------------

### Corporate Box Office Commitment

# The Enterprise Box Office service implements and operates the Service in accordance with the existing legal framework set out in the preamble.
# The Enterprise Box Office is committed to taking all necessary measures to ensure the security and confidentiality of the information provided by the user.
# The Business Box Office is committed to ensuring the protection of data collected under the Service, including preventing it from being distorted, damaged or accessed by unauthorized third parties, in accordance with the measures provided by the 8 December 2005 order on electronic exchanges between users and administrative authorities and between administrative authorities, Decree No. 2010-112 of 2 February 2010 for the application of Articles 9, 10 and 12 of this ordinance and Regulation 2016/679 of the European Parliament and the Council of 27 April 2016 on the processing of personal data.
# The Business Box Office and partner agencies guarantee the access, rectification and opposition rights of the Users of the Service under Law 78-17 of 6 January 1978 relating to the computer use of files and freedoms and regulation No. 2016/679 of the European Parliament and the Council of 27 April 2016 relating to the processing of personal data.
This right can be exercised in a number of ways:
(a) by contacting the Business Formalities Centre (CFE) who receives the declaration file;
b- by sending an email to the support
c- by sending a letter to:

<blockquote><p>Service à compétence nationale Guichet Entreprises</p>
<p>120 rue de Bercy – Télédoc 766</p>
<p>75572 Paris cedex 12</blockquote># The Business Box Office and partner organizations undertake not to commercialize the information and documents transmitted by the user through the Service, and not to communicate them to third parties, outside of the cases provided by law.
# The Business Box Office and partner organizations undertake not to commercialize the information and documents transmitted by the user through the Service, and not to communicate them to third parties, outside of the cases provided by law.
# The Enterprise Box Office is committed to ensuring the traceability of all actions carried out by all users of the Service, including those of partner organizations and the user.
# The Enterprise Box Office service provides users with support in the event of an incident or a defined security alert.

User engagement
---------------

# The user fills out his file online and validates it by possibly attaching the necessary parts for the treatment of the file.
# At the end of the preparation of the file, a summary of the information provided by the user is displayed on the screen so that the user can verify and confirm them. After confirmation, the file is forwarded to partner agencies. Confirmation and transmission of the form by the user is worth signing it.
# These terms and conditions are required of any user who is a user of the Service.

Availability and evolution of the Service
-----------------------------------------

The service is available 7 days a week, 24 hours a day.

The Enterprise Box Office, however, reserves the ability to evolve, modify or suspend the Service without notice for maintenance or other reasons deemed necessary. The Unavailability of the Service does not entitle you to any compensation. In the event of the Service's unavailability, an information page is then posted to the user mentioning this unavailability; he is then invited to take his step at a later date.

The terms of these terms of use may be amended at any time, without notice, depending on changes to the Service, changes in legislation or regulations, or for any other reason deemed necessary.

Responsibilities
----------------

# Responsibility for the Enterprise Box Office service cannot be incurred in the event of identity theft or any fraudulent use of the Service.
# The data transmitted to the online services of partner agencies remains the responsibility of the user, even if it is transmitted by the technical means made available in the Service.
The user can modify or delete them from partner agencies at any time.
He can choose to delete all information from his account by deleting his data from the Service.
# The user is reminded that anyone making a false statement for himself or others is liable, in particular, to the penalties provided for article 441-1 of the Penal Code, providing for penalties of up to three years' imprisonment and a fine of 45,000 euros.

