﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Qui sommes-nous ?" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-qui_sommes_nous" -->
<!-- var(last-update)="2020-05-03 15:16:16" -->
<!-- var(lang)="en" -->
<!-- var(site:lang)="en" -->

Who are we?
===========


<!-- begin-include(disclaimer-trans-en) -->

**Notice about the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page has been translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page before undertaking any action.

The Enterprise Bank Service can not be held responsible for the operation of information that will be inaccurate due to a non-automatic translation faithful to the original.<!-- alert-end:warning -->

<!-- end-include -->
Latest update:<!-- begin-var(last-update) -->2020-05-03 15:16:16<!-- end-var -->

The www.guichet-qualifications.fr site is designed and developed by the nationally-responsible "Business Box Office" service, created by the[decree of April 22, 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&dateTexte=20200109) (JORF of April 25, 2015). It is under the authority of the[Corporate Directorate](https://www.entreprises.gouv.fr/) within the[Ministry of Economy and Finance](https://www.economie.gouv.fr/).

The national "Business Box Office" service manages websites[www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/) And[www.guichet-entreprises.fr](https://www.guichet-entreprises.fr/) [1] which together constitute the electronic one-stop shop defined by the European directives[2006/123/EC](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32006L0123) And[2005/36](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036). Atms of this kind exist throughout Europe and are federated within the project " [Eugo](http://ec.europa.eu/internal_market/eu-go/index_fr.htm) European Commission.

At the Business Box Office, we encourage the professional mobility of residents of the European Union or the European Economic Area by providing access to full information on the access and practice of regulated professions in France through "professional records".

The site[www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/) is aimed at all residents of the European Union or the European Economic Area. It informs them about the possibilities of suring that their professional qualifications are recognised and that they practice a regulated profession in France with a view to a sustainable establishment ([free establishment](https://www.guichet-entreprises.fr/en/eugo/libre-etablissement-le/)) or temporary ([free service delivery](https://www.guichet-entreprises.fr/en/eugo/libre-prestation-de-services-lps/)). It also allows residents in France to learn about the possibilities of accessing and practising a regulated profession in another Member State.

