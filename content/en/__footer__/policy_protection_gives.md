﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Politique de protection des données personnelles" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-politique_protection_donnees" -->
<!-- var(last-update)="2020-05-03 15:15:19" -->
<!-- var(lang)="en" -->
<!-- var(site:lang)="en" -->

Personal data protection policy
===============================


<!-- begin-include(disclaimer-trans-en) -->

**Notice about the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page has been translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page before undertaking any action.

The Enterprise Bank Service can not be held responsible for the operation of information that will be inaccurate due to a non-automatic translation faithful to the original.<!-- alert-end:warning -->

<!-- end-include -->
Latest update:<!-- begin-var(last-update) -->2020-05-03 15:15:19<!-- end-var -->

| Version | Date       | Object                                                 |
|---------|------------|--------------------------------------------------------|
| V1      | 25/05/2018 | Initial version                                        |
| V2      | 01/09/2018 | Updated information about the data protection delegate |

The privacy policy (the "policy") informs you about how your data is collected and processed by the National Business Bank service, the security measures implemented to ensure its integrity and confidentiality, and the rights you have to control its use.

This policy complements the[terms and conditions of use](terms.md) (CGU) as well as, if necessary,[Legal](legal_mentions.md). As such, this policy is deemed to be incorporated into those documents.

As part of the development of our services and the implementation of new regulatory standards, we may have to change this policy. We invite you to read it regularly.

Who are we?
-----------

The Business Box Office, created by the[decree of April 22, 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&categorieLien=id) is under the authority of the[Corporate Directorate](https://www.entreprises.gouv.fr/) (DGE) within the[Ministry of Economy and Finance](https://www.economie.gouv.fr/).

The Business Box Office service implements a teleservice guichet-qualifcations.fr (hereafter the "Teleservice Box Office Professional Qualifications").

The Teleservice Box Office Professional Qualifications is aimed at all residents of the European Union or the European Economic Area. It informs them about the possibilities of leaving their professional qualifications recognised and practising a regulated profession in France with a view to a sustainable (free establishment) or temporary (free provision of services).

It also allows residents in France to learn about the possibilities of accessing and practising a regulated profession in another Member State.

The Teleservice Box Office Professional Qualifications is part of the legal framework:

- Of[General Data Protection Regulation](https://www.cnil.fr/en/reglement-europeen-protection-donnees) ;
- [Directive 2005/36/EC](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036) European Parliament;
- [2013/55/EU Directive](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32013L0055) European Parliament;
- Of[Regulation 910/2014](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32014R0910) European Parliament and the Council of 23 July 2014 (e-IDAS) on electronic identification and trusted services for electronic transactions within the internal market;
- of the[December 8, 2005 order](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000636232) electronic exchanges between users and administrative authorities and between administrative authorities and the[Decree No. 2010-112 of February 2, 2010](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021779444&categorieLien=id) taken for the application of Articles 9, 10 and 12 of this ordinance;
- Of[Act 78-17 of January 6, 1978 amended](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460) computer science, files and freedoms;
- of the[Article 441-1 of the Penal Code](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006418753&cidTexte=LEGITEXT000006070719&dateTexte=20020101) ;
- of the[Article R. 123-30 of the Code of Commerce](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006256107&dateTexte=&categorieLien=cid) ;
- of the[decree of April 22, 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&categorieLien=id) creating a nationally-competent service called a "business window."

What organization do we implement to protect your data?
-------------------------------------------------------

Compliance and implementation of applicable data protection provisions are monitored by the Data Protection Representative of the Business Bank Service.

Within the Ministry of Economy and Finance, on which the Corporate Directorate-General depends, the services of the senior defence official are in charge of verifying the implementation and compliance of the provisions of the General Data Protection Regulation.

How do I get in touch with us?
------------------------------

If you have any questions about the use of your personal data by the Enterprise Box Office, please read this policy and contact our data protection representative using the following contact information:

By mailing to:

```
Le Délégué à la protection des données des ministères économique et financier
Délégation aux Systèmes d’Information
139, rue de Bercy – Télédoc 322
75572 PARIS CEDEX 12


```
By email, specifying the "Data Protection" object to:[Contact](contact.md).

What is a personal data?
------------------------

Personal data is information that identifies you, either directly or indirectly. These may include your name, first name, address, phone number or email address, as well as the IP address of your computer, for example, or information related to the use of Teleservice.

When do we collect personal data?
---------------------------------

The Business Box Office collects personal data when you:

- Create your own personal space
- complete a formality via the Teleservice Business Box.

What personal data do we process?
---------------------------------

The personal data we deal with relates only to the formalities of recognition of professional qualifications in accordance with the regulations of Articles R. 123-30-10 to R. 123-30-14 of the Code of Commerce.

The data collected allow the file to be compiled as expected under Section R. 123-30-10 of the Code of Commerce.

In particular, the data collected allows the Cerfa documents associated with the formalities and procedures necessary to qualify for professional qualifications or to obtain a European professional card.

The following list summarizes the personal data we process.

For the management of access to the personal space of the service, the user shares the following information:

- The login ID chosen by the user
- The password chosen by the user
- The user's email address
- The mobile phone number
- IP login address.

For the use of personal storage space, the user shares the following information:

- surname;
- Use name/wife's name;
- Exercise name
- First name;
- date of birth;
- Country birth;
- Nationality;
- Social Security number;
- residence address (including country);
- Phone
- E-mail
- diploma, certificate, titles;
- Work experience
- a professional project;
- quality or function;
- Current employment
- place of employment (locality, country);
- titled to the position occupied.

What personal supporting documents do we deal with?
---------------------------------------------------

The personal supporting documents we deal with relate only to the formalities of recognising professional qualifications or obtaining the European professional card in accordance with the regulations related to the regulated activity carried out.

The supporting documents are used to build the file as expected under Article R. 123-30-10 of the Code of Commerce.

The list of supporting documents is approved by the competent authority in charge of investigating the case.

Personal supporting documents include documents justifying identity or residence. These supporting documents are kept on the same terms as personal data.

Their storage is provided in spaces inaccessible from the outside (Internet) and their download or transfer is secured by encryption in accordance with the provisions indicated in the chapter "How are your personal data protected?".

What is our cookie management?
------------------------------

Cookie management is described on the page:[https://www.guichet-entreprises.fr/en/cookies/](https://www.guichet-entreprises.fr/en/cookies/).

All managed cookies use secure cookie mode.

Secure cookies are a type of cookie transmitted exclusively via encrypted connections (https).

Technical cookies contain the following personal data:

- Name;
- First name;
- E-mail
- Phone.

Analysis cookies contain the login IP address.

What are your rights to protect your personal data?
---------------------------------------------------

You can access your data, correct it, delete it, limit its use or oppose the processing of it.

If you feel that the use of your personal data violates privacy rules, you have the option of making a claim to the[Cnil](https://www.CNIL.fr/).

You also have the right to set guidelines for the handling of your personal data in the event of death. Specific guidelines can be registered with the treatment manager. General guidelines can be registered with a third-party digital trust certified by the CNIL. You have the option to change or remove these guidelines at any time.

If so, you can also ask us to provide you with the personal data you have provided to us in a readable format.

You can send your various requests to:

```
Service à compétence nationale Guichet Entreprises
120 rue de Bercy – Télédoc 766
75572 Paris cedex 12


```
Your applications must be accompanied by a copy of an identity document and will be reviewed by our services.

How is your personal data protected?
------------------------------------

The Enterprise Box Office implements all industry standard practices and security procedures to prevent any breach of your personal data.

All information provided is stored on secure servers hosted by our technical providers.

Where data transmission is required and authorized, the Enterprise Box Office ensures that these third parties provide sufficient safeguards to ensure an appropriate level of protection. The exchanges are encrypted and the servers authenticated by mutual recognition.

In accordance with the data protection regulations, in the event of a breach, the Business Guichet service undertakes to communicate this violation to the relevant supervisory authority, and where required, to the persons concerned.

#### Personal space

All data transmitted to your personal space is encrypted and access is secured by the use of a personal password. You are required to keep this password confidential and not to disclose it.

#### Preservation time

Personal data processed by the Enterprise Box Office is kept for a period of twelve months in accordance with the provisions of Article R. 123-30-14 of the Code of Commerce.

#### Regarding credit card data

The Enterprise Checkout service does not store or store the banking data of users who are subject to payment at the time of their formality. Our provider manages transaction data on behalf of the Enterprise Box office in accordance with the strictest security rules applicable in the online payment industry through the use of encryption processes.

Who is your data transmitted to?
--------------------------------

The data collected allow the file to be compiled as expected under Article R.123-30-10 of the Code of Commerce.

Under Article R.123-30-12 of the Code of Commerce, the data collected is transmitted for processing to the appropriate authorities.

