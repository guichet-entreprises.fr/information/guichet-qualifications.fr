﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Politique de signature électronique" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-politique_signature_electronique" -->
<!-- var(last-update)="2020-05-03 15:16:11" -->
<!-- var(lang)="en" -->
<!-- var(site:lang)="en" -->

Electronic signing policy
=========================


<!-- begin-include(disclaimer-trans-en) -->

**Notice about the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page has been translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page before undertaking any action.

The Enterprise Bank Service can not be held responsible for the operation of information that will be inaccurate due to a non-automatic translation faithful to the original.<!-- alert-end:warning -->

<!-- end-include -->
Latest update:<!-- begin-var(last-update) -->2020-05-03 15:16:11<!-- end-var -->

| Version | Date       | Object          |
|---------|------------|-----------------|
| V1      | 27/09/2018 | Initial version |

The subject of the document
---------------------------

The electronic signature affixed to a data set ensures the integrity of the data transmitted, the non-repudiation of the signed data and the authenticity of its transmitter.

This electronic signature policy is a document describing the eligibility requirements of the receiving agencies, i.e. the relevant agencies, of a file on which an electronic signature is affixed in connection with electronic exchanges covered by Article R. 123-30-10 of the Code of Commerce.

The relevant bodies are the services responsible for hearing applications for recognition of professional qualifications.

Under this section,*"Where a signature is required, the use of an electronic signature with the guarantees set out in the first sentence of the second paragraph of Article 1367 of the Civil Code is permitted"*, and the second paragraph of Article 1367 of the Civil Code,*"When it is electronic, it consists of the use of a reliable identification process guaranteeing its connection to the act to which it attaches itself. The reliability of this process is presumed, until proven otherwise, when the electronic signature is created, the identity of the insured signatory and the integrity of the guaranteed act, under conditions set by decree in the Council of State."*,

The applicable regulations for the implementation of the electronic signature of the Teleservice Box Qualifications are derived from the following texts:

- Regulation 910/2014 of the European Parliament and the Council of 23 July 2014 (e-IDAS) on electronic identification and trusted services for electronic transactions within the internal market;
- Civil code;
- Decree No. 2017-1416 of September 28, 2017 relating to electronic signature.

In the event that electronic signature requires the provision of personal data, the applicable regulations are derived from the following texts:

- Regulation (EU) 2016/679 of the European Parliament and the Council of 27 April 2016, regulation on the protection of individuals with regard to the processing of personal data and the free movement of such data, and repealing Directive 95/46/EC (General Regulation on Data Protection);
- Law 78-17 of January 6, 1978 relating to computers, files and freedoms.

This document, "Electronic Signing Policy of Teleservice Box Qualifications," describes all the rules and provisions defining the requirements to which each of the players involved in these dematerialized exchanges complies with the transmission and receipt of streams.

This document is intended to:

- The relevant bodies;
- potential providers participating in these dematerialized exchanges on behalf of these recipient agencies.

In the rest of this document:

- The aforementioned recipient organizations are referred to as "recipients";
- the above-heated exchanges are referred to as "records";
- the national service Business Bank is referred to as the "Business Box Office service."

Scope
-----

Electronic signature is required for any formality related to a request for recognition of professional qualification within the meaning of the European Parliament and Council Directive 2005/36/EC of 7 September 2005 relating to the recognition of professional qualifications ("Professional Qualifications Directive") and its exercise, referred to in Article R. 123-30-8 of the Code of Commerce.

Article 9 of Ordinance No. 2016-1809 of December 22, 2016 provides for those affected by these formalities,

*Any national of a Member State of the European Union or party to the agreement on the European Economic Area who has acquired his professional qualifications in a Member State of the European Union or party to the agreement on the European Economic Area, and wishing to obtain recognition of these for the exercise of a regulated profession within the meaning of the European Parliament and Council's 2005 directive 2005/36/EC, can complete or monitor remotely and electronically, through a single window, all requirements, procedures or formalities relating to the recognition of one's professional qualifications under conditions determined by decree in the Council of State.*

The electronic signature has different characteristics depending on the nature of the files submitted.

**In the event that the legal provisions set out in Article R. 123-30-10 of the Code of Commerce apply**, the required electronic signature meets the provisions of the first sentence of the second paragraph of Article 1316-4 of the Civil Code:

*When it is electronic, it consists of the use of a reliable identification process guaranteeing its connection to the act to which it attaches itself.*

**In the event that more stringent legal provisions than those set out in Article R. 123-30-10 of the Code of Commerce apply** (e.g. recognition of professional qualification for the activity of a general practitioner), the electronic signature meets the provisions of Article 1 of Decree No. 2017-1416 of 28 September 2017 (reference to regulation "eIDAS" no.910/2014):

*The reliability of an electronic signature process is presumed, until proven otherwise, when this process implements a qualified electronic signature.*

*An electronic signature is an advanced electronic signature, in accordance with Article 26 of the above regulation and created using a qualified electronic signature creation device that meets the requirements of Article 29 of that regulation, which is based on a certificate qualified as an electronic signature meeting the requirements of Article 28 of that regulation.*

Article 26 of Regulation "eIDAS" 910/2014:

*An advanced electronic signature meets the following requirements:*

# *Be bound to the signatory in a unambiguous way;*
# *Allow the signatory to be identified;*
# *To have been created using electronic signature creation data that the signatory can use under their exclusive control with a high level of trust;*
# *Be linked to the data associated with this signature so that any subsequent changes to the data are detectable.*

For information, three levels of guarantee are provided by regulation "eIDAS" 910/2014:

- *Low* At this level, the goal is simply to reduce the risk of misuse or identity tampering;
- *Substantial* At this level, the aim is to substantially reduce the risk of misuse or identity tampering;
- *High* At this level, the aim is to prevent misuse or alteration of identity.

### Identification

The identification of this electronic signature policy is signified by the presence of the Company Box Service certificate used for the electronic signature of the document.

The serial number of the certificate is:

4B-51-5A-35-00-00-00-00-01-EF.

### Publication of the document

This electronic signature policy is published following its approval by the Information Systems Security Manager (RSSI) of the Enterprise Box Office.

### Update process

#### Circumstances making an update necessary

The update of this electronic signature policy may include the evolution of the law in force (see "Subject of the Document"), the emergence of new threats and new security measures, and the consideration of the observations of the various actors.

This electronic signature policy is reviewed at least every two years.

#### Taking into account remarks

All comments, or wishes for change, on this e-signature policy are to be emailed to:[Contact](mailto:contact.guichet-entreprises@finances.gouv.fr).

These comments and wishes for development are reviewed by the RSSI of the Business Box Office service, which, if necessary, initiates the process of updating this electronic signature policy.

#### Information from the actors

Information about the current version of this policy and earlier versions is available in the table of versions at the top of this document.

The publication of a new version of the signature policy consists of:

# Putting the electronic signature policy online in HTML format
# archive the previous version after the "obsolete" label is affixed to each page.

A new version comes into force and a period of validity
-------------------------------------------------------

A new version of the signature policy only comes into effect one (1) calendar month after it is posted online and remains valid until a new version comes into effect.

The one-month delay is used by recipients to take into account in their applications the changes made by the new electronic signature policy.

Actors
------

### The signatory of the file

The signatory of the file is the registrant or his agent making the formality of applying for recognition of professional qualification

#### The role of the signatory

The signatory's role is to put his electronic signature on his file incorporating the formality and supporting documents,

To put an electronic signature on his file, the signatory undertakes to use a signature tool in accordance with this electronic signature policy.

#### The signatory's obligations

These obligations are described in the chapters below.

### Signature tool used

The signatory must check the data he will sign before signing his electronic signature.

### Electronic signature procedure used

The electronic signature procedure depends on the type of formality (see "Field of Application").

**In the event that the legal provisions set out in Article R. 123-30-10 of the Code of Commerce apply**, the signatory must check a box at the end of the formality indicating that he declares on the honour the accuracy of the information of the formality and signs this statement.

It is then mentioned in the signature space of the form (Cerfa), of the conformity of this signature to the expectations of paragraph 3 of Article A. 123-4 of the Code of Commerce.

Paragraph 3 of Article A. 123-4 of the Code of Commerce:

*(3) By checking the computer box provided for this purpose, the declarant declares on the honour the accuracy of the items declared in accordance with the following formula: "I declare on the honour the accuracy of the information of the formality and sign this statement no...., made to..., the.... ».*

**In the event that more stringent legal provisions than those set out in Article R. 123-30-10 of the Code of Commerce apply**, the signatory must attach a copy of his or her declared copy in accordance with the original in accordance with the provisions of paragraph 2 of Article A. 123-4 of the Code of Commerce:

Paragraph 2 of Article A. 123-4 of the Code of Commerce:

*(2) The documents that compose it have been digitized. The copy of the proof of identity is scanned after it has been previously provided with a handwritten certificate of proof of the honour of compliance with the original, a date and the handwritten signature of the person making the declaration.*

The document to be signed electronically (Cerfa form and copy of the ID) is presented to him as well as the proof agreement listing the conditions and consequences of the electronic signature of the document.

The signatory must check a box indicating that he has read the agreement of proof and accepts it without reservation.

A code for validating the electronic signature is sent to him on his mobile phone, the number of which he indicated when he created his account.

Entering this code triggers the sealing of the document (form/Cerfa and copy of the ID), i.e. its electronic signature based on a certificate, its hashing and the time stamping of the set.

The sealed document is archived and forwarded to the recipients concerned.

The most common cases of error are:

**Case 1 - Incorrect Code**

Functional case:

*The user entered an incorrect code (for example, letters instead of numbers).*

Example of a message displayed:

*The code you have provided is incorrect. Please check the code that has been sent to you by SMS and re-enter it.*

**Case 2 - Code expired**

Functional case:

*The user entered the code received by SMS too late (the life of an OTP - One Time Password code is 20 minutes).*

Example of a message displayed:

*The validity of your code has expired. You must renew your application for an electronic signature.*

**Case 3 - Blocked Code**

Functional case:

*The user made N unsuccessful seizures of OTP codes. In order to avoid hacking attempts, the user's phone number is identified as blocked in the system.*

Example of a message displayed:

*Due to numerous typing errors on your part, your phone number is no longer allowed to sign your procedure electronically. We invite you to provide a new phone number in your user account or contact the Business Box to unlock your current phone number.*

### Protection and use of the signed file

This chapter deals only with files dealing with formalities for which more stringent legal provisions than those set out in Article R. 123-30-10 of the Code of Commerce apply.

The electronically signed document is stored in an electronic safe with a readable file bearing all traces of its sealing for audit purposes.

The electronic safe is only accessible by its administrator and the RSSI of the Business Box Service.

Access to the electronic safe is only possible with the help of a name certificate and any action on it is traced.

For the RSSI of the Enterprise Box Office service, only access for audit purposes on justified request (for example, court request) is allowed.

The signed document is forwarded to the recipient agencies in charge of investigating the associated file.

The protection of the electronically transmitted file is the responsibility of the recipient and must comply with the provisions of the General Security Regulations and the General Data Protection Regulations.

The RSSI of the Business Box Office service ensures compliance with these provisions.

### Archive and deletion of signed file

The file is archived and deleted in accordance with the provisions of Section R. 123-30-12 of the Code of Commerce.

Article R. 123-30-12 of the Code of Commerce:

*If the registrant uses a provisional data retention service proposed by the reporting service under conditions consistent with Law 78-17 of 6 January 1978 relating to computers, files and freedoms, all data and files relating to the registrant on the computer media in which they are listed are deleted at the end of the provisional retention period of up to twelve months.*

Providers of electronic signature solutions
-------------------------------------------

The solution must incorporate the expectations of this electronic signature policy into the structure of the signature data.

The Business Box Office
-----------------------

The RSSI of the Business Box Office service ensures compliance with the provisions of this electronic signature policy for the Teleservice Box Qualifications made available to users.

It verifies that the source and recipient information systems of the feeds do not pose a risk to the integrity and confidentiality of the files stored and transmitted.

Recipients
----------

Recipients monitor the integrity of the electronic signature of the file and ensure that the procedures for storing and deleting the files received are in accordance with the regulations.

### Verification data

To carry out the audits, the Enterprise Box Office uses the sealing information of the electronically signed file (see "Protection and use of the signed file").

### Protecting means

The Enterprise Box Office ensures that the necessary means are implemented to protect the equipment providing the validation services.

The measures taken concern both:

- protecting physical and logical access to equipment to only authorized persons;
- Availability of the service
- monitoring and monitoring the service.

### Assistance to signatories

Assistance with the use of the electronic signature procedure is provided by the support of the Business Box service accessible by email at:[Support](mailto:support.guichet-entreprises@helpline.fr).

Electronic signature and validation
-----------------------------------

### Signed data

The signed data consists of the form/Cerfa and the copy of the ID in the form of a single PDF file with a changeable electronic signature area.

The entire PDF file is signed and in fact no modifications to it are possible after it has been sealed.

### Signature features

The signature meets the expectations of the "XML Signature Syntax and Processing (XMLDsig)" specification developed by W3C (World Wide Web Consortium:<https://www.w3.org/>) as well as the signature format extensions specified in ETSI's European XML Advanced Electronic Signature (XADES) standard (<https://www.etsi.org>).

The deployed electronic signature implements a "substantial" level of identification (ISO/IEC 29115:2013), i.e. in accordance with the eIDAS regulation an advanced electronic signature

#### Type of signature

The electronic signature is of the type[Wrapped](https://fr.wikipedia.org/wiki/XML_Signature).

#### Signature standard

The electronic signature must meet the standard[XMLDsig, revision 1.1 of
February 2002](https://www.w3.org/TR/2002/REC-xmldsig-core-20020212).

The electronic signature must meet the XAdES-EPES (Explicit Policy based Electronic Signature) standard,[ETSI TS 101 903 v1.3.2](http://uri.etsi.org/01903/v1.3.2).

In accordance with the XadES standard, SignedProperties (SignedProperties/SignedSignatureProperties) must contain the following:

- SigningCertificate
- signing date and time (SigningTime) in UTC format.

Algorithms that can be used for signature
-----------------------------------------

#### Condensation algorithm

The calculation of the iteration condensed of the compression function on the rest of the blocks obtained by cutting out the message (iterative scheme of Merkle-Damgord).

This algorithm is accessible at the link:[Merkle-Damgord](https://fr.wikipedia.org/wiki/Construction_de_Merkle-Damgård)

#### Signature algorithm

The signature algorithm is based on RSA/[SHA 256](https://fr.wikipedia.org/wiki/SHA-2).

#### Canonicalization algorithm

The canonicalization algorithm is[c14N](https://www.w3.org/TR/xml-exc-c14n/).

### Conditions for declaring signed file valid

The conditions for declaring the signed file valid are:

- The signatory has fulfilled its obligations:- downloading a copy of his ID with a copy in accordance with the original and accepting the agreement of proof,
  + entering the code sent by SMS ("One Time Password") method;
- The Business Box Office has fulfilled its obligations:- making available a certificate generated by an AC certifying its origin (provider of the electronic signature service) and whose characteristics are:*Public Key - RSA 2048 bits, signature algorithm - RSA SHA 256* ;
- The e-signature service provider has fulfilled its obligations:- signing and sealing the PDF file according to the characteristics described in the previous chapters.

The verification of the validity of the signature can be done via Adobe Acrobat.

Legal provisions
----------------

### Nominal data

The carrier has the right to access, modify, correct and delete data about him that he can exercise by email at the following address:[Contact](mailto:contact.guichet-entreprises@finances.gouv.fr)

### Applicable law - Dispute resolution

These provisions are subject to French law.

Any dispute over the validity, interpretation or enforcement of these provisions will be referred to the jurisdiction of the Paris Administrative Court.

