﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(site:package)="https://gitlab.com/|13561843|latest|reference_md|reference/configuration.yml" -->
<!-- var(site:home)="Accueil" -->
<!-- var(site:menu)="__menu__.md" -->
<!-- var(site:footer)="__footer__/__links__.md" -->
<!-- var(site:lang)="en" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Guichet Qualifications  " -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:font_sizer_off)="off" -->
<!-- var(page:breadcrumb_off)="off" -->
<!-- var(key)="main" -->
<!-- var(translation)="Auto" -->
<!-- var(last-update)="2020-05-03 15:13:21" -->
<!-- var(lang)="en" -->

Box Office Qualifications<!-- section-banner:surf.jpg --><!-- color:dark -->
============================================================================

One address for recognition of your qualifications

[Start my walk<!-- link-model:box-trans -->](https://profiler.guichet-qualifications.fr/start/)

One address for recognition of your qualifications<!-- section-information:-120px -->
=====================================================================================

Welcome
-------

You will find on this site all the information on the recognition of professional qualification.

[First visit<!-- link-model:box -->](welcome.md)

Discover
--------

You have a degree from a Member State of the European Union or the European Economic Area

[Your possibilities<!-- link-model:box -->](understand/professional_qualification_in_france.md)

Online steps
------------

Complete your online formalities related to the recognition of your professional qualifications in France!

[Start<!-- link-model:box -->](https://profiler.guichet-qualifications.fr/start/)

Why have your professional qualifications recognized?<!-- section:courses -->
=============================================================================

A profession is called "regulated" when it is necessary to hold a specific diploma, to have passed specific examinations, to have obtained a certain authorization or to be registered with a professional body to carry it out.

Ableding one's professional qualifications is imperative when one wishes to work in another Country of the European Union in which one's profession is regulated. This means having his training and/or professional experience officially recognized by the host country.[Read more...](understand/why_have_your_professional_qualification_recognised.md)

A single address for the recognition of your professional qualifications<!-- section:welcome --><!-- color:grey -->
===================================================================================================================

The online service guichet-qualifications.fr encourages the professional mobility of residents of the European Union and the European Economic Area by providing comprehensive information on the access and practice of regulated professions in France, with a view to recognition of professional qualification.

This service is the initiative of the Ministry of Economy and Finance.

Doing business in France and Europe<!-- section-stories:drapeaux.jpg --><!-- color:dark -->
===========================================================================================

Are you a national of the European Union or the European Economic Area? We encourage business creation and professional mobility in France and Europe. Guichet-qualifications.fr is recognised as the French single window for the recognition of your professional qualifications, a member of the Eugo network created by the European Commission.

Once you have obtained recognition of your qualification, if you wish to settle permanently or temporarily to work in France, go to:

[guichet-entreprises.fr](https://guichet-entreprises.fr/)

