﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="La carte professionnelle européenne (CPE)" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(key)="fr-comprendre-carte_professionnelle_europeenne" -->
<!-- var(last-update)="2020-05-03 15:13:31" -->
<!-- var(lang)="en" -->
<!-- var(site:lang)="en" -->

The European Professional Card (CPE)
====================================


<!-- begin-include(disclaimer-trans-en) -->

**Notice about the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page has been translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page before undertaking any action.

The Enterprise Bank Service can not be held responsible for the operation of information that will be inaccurate due to a non-automatic translation faithful to the original.<!-- alert-end:warning -->

<!-- end-include -->
Latest update:<!-- begin-var(last-update) -->2020-05-03 15:13:31<!-- end-var -->

Some regulated professions benefit from the European professional card scheme. In this article you will find all the information about its operation, the formalities of obtaining it and the regulated professions it concerns.

What is the European business card?
-----------------------------------

The European Professional Card (ECC) is an electronic procedure for the recognition of professional qualifications between EU member states defined by Articles 4a to 4th in the[Directive 2005/36/EC](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036). Its aim is to facilitate the exercise of a regulated profession for European citizens in another EU Member State. This procedure is part of the professional mobility of European citizens.

###### Good to know

This is not a physical card but electronic proof that the citizen's professional qualifications have been verified and are recognised by the competent authorities of the host country.

Who can benefit?
----------------

This device can be used by any European professional as well as in the setting in the host country to practice there ([free establishment](https://www.guichet-entreprises.fr/en/eugo/libre-etablissement-le/)), only in the context of providing services on a temporary basis ([free service delivery](https://www.guichet-entreprises.fr/en/eugo/libre-prestation-de-services-lps/)).

To date, the CPE is available for five occupations:

# Real estate agent;
# Mountain guide;
# Nurse;
# Physiotherapist;
# Pharmacist.

These occupations were selected to benefit from the CPE system based on three criteria:

- significant mobility, or significant mobility potential, in the profession concerned;
- sufficient interest shown by parties with a legitimate interest;
- regulation of the profession or training associated with it in a significant number of EU Member States.

This number of occupations is expected to grow knowing that some, such as engineers or specialized nurses, are currently being evaluated.

Why ask for it?
---------------

The CPE is an electronic certificate. It aims to simplify the recognition of professional qualifications and introduce an additional level of efficiency for both citizens and the competent authorities responsible for issuing it.

When it comes to**free service delivery**, the CPE is intended to replace the declaration prior to the first benefit in the host Member State, which must be renewed every 18 months.

When it comes to**establishment**, the CPE is the decision to recognize professional qualifications by the host Member State.

###### Good to know

Professionals concerned by the CPE have a choice between its device or the traditional procedures for pre-reporting or recognition of professional qualifications.

###### To go further

[European professional card](https://europa.eu/youreurope/citizens/work/professional-qualifications/european-professional-card/index_fr.htm) (European Commission)

