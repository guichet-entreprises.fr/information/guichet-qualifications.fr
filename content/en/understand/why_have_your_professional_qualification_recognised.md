﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Pourquoi faire reconnaître sa qualification professionnelle ?" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(key)="fr-comprendre-pourquoi_faire_reconnaitre_sa_qualification_professionnelle" -->
<!-- var(last-update)="2020-05-03 15:13:52" -->
<!-- var(lang)="en" -->
<!-- var(site:lang)="en" -->

Why have your professional qualifications recognized?
=====================================================


<!-- begin-include(disclaimer-trans-en) -->

**Notice about the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page has been translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page before undertaking any action.

The Enterprise Bank Service can not be held responsible for the operation of information that will be inaccurate due to a non-automatic translation faithful to the original.<!-- alert-end:warning -->

<!-- end-include -->
Latest update:<!-- begin-var(last-update) -->2020-05-03 15:13:52<!-- end-var -->

A profession is called "regulated" when it is necessary to hold a specific diploma, to have passed specific examinations, to have obtained a certain authorization or to be registered with a professional body to carry it out.

**Getting your professional qualifications recognised** is imperative when you want to work in another EU country in which your profession is regulated. This means having his training and/or professional experience officially recognized by the host country.

- If it is a question of moving to another country to practice its profession, it is in the case of the[free establishment](https://www.guichet-entreprises.fr/en/eugo/libre-etablissement-le/) and it is essential to make the recognition of his qualifications.
- If it is a matter of providing services on a temporary basis in another country, the case is to[free service delivery](https://www.guichet-entreprises.fr/en/eugo/libre-prestation-de-services-lps/) and a pre-exercise declaration is sufficient. There is an exception for occupations with serious health or safety implications as the host Member State may require a pre-qualification check.

To find out the rules that apply to your situation, please contact the national authority responsible for access to your profession in the host country.

You can also consult the[European database of regulated professions](http://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=homepage) which lists all regulated professions in EU countries and their competent authorities.

- If your profession is regulated in your home country, you can search for it in your own language and view the English translation provided in the description; you then have to search for the English name, in order to get a list of the other countries where this occupation is regulated. If the country where you wish to settle does not appear, it may mean that the profession is not regulated.
- If you can't find your profession in the database, you can contact the National Information Points on the recognition of professional qualifications in the country where you want to work. They can help you find the appropriate authority and determine the required documents.

**Read also:** [European Professional Card (CPE)](european_business_card.md)

