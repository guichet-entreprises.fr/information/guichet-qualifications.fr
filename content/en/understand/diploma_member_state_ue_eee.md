﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Je suis titulaire d’un diplôme d’un État membre de l’Union européenne ou de l’Espace économique européen..." -->
<!-- var(key)="fr-comprendre-diplome_etat_membre_ue_eee" -->
<!-- var(last-update)="2020-05-03 15:13:38" -->
<!-- var(lang)="en" -->
<!-- var(site:lang)="en" -->

I have a degree from a Member State of the European Union or the European Economic Area...
==========================================================================================


<!-- begin-include(disclaimer-trans-en) -->

**Notice about the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page has been translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page before undertaking any action.

The Enterprise Bank Service can not be held responsible for the operation of information that will be inaccurate due to a non-automatic translation faithful to the original.<!-- alert-end:warning -->

<!-- end-include -->
Latest update:<!-- begin-var(last-update) -->2020-05-03 15:13:38<!-- end-var -->

... and I want to have my professional qualification recognised in France.
--------------------------------------------------------------------------

France has nearly 250 regulated professions that can be identified in two categories:

- The liberal professions;
- craft and commercial professions for which the recognition of professional qualification is carried out with the Chamber of Trades and Crafts on which you depend.

I want to move to France
------------------------

Two steps are needed in order to be able to practice a regulated profession in France:

# Recognition of professional qualification for foreign nationals of the European Union (EU) or the European Economic Area (EEA);
# authorisation to practice with the competent authority of the profession concerned (additional authorisation depending on the profession).

The[fact sheets on regulated professions](regulated_professions.md) identify these steps and direct you through existing regulations.

Note that occupations recognised by the[European professional card](european_business_card.md) (CPE) may thus deviate from the traditional procedure:

- General care nurses;
- Pharmacists;
- physiotherapists;
- mountain guides;
- real estate agents.

I want to issue a temporary benefit in France
---------------------------------------------

There is no need to use a professional qualification recognition for a temporary benefit, however you must be established in your home country. In some cases, you will have to apply for permission in advance of your benefit.

Steps
-----

Contact with the relevant authority is most often to be taken in the area in which you decide to settle. A number of supporting documents are to be provided, some of which sometimes require a sworn translation.

**Good to know**

If your profession is regulated in your country of origin but not in the host country (France), you can practice without having to do a professional qualification, such as nationals of the host country.

### List of EU Member States

Germany, Austria, Belgium, Bulgaria, Cyprus, Croatia, Denmark, Spain, Estonia, Finland, France, Greece, Hungary, Ireland, Italy, Latvia, Lithuania, Luxembourg, Malta, Netherlands, Poland, Portugal, Romania, United Kingdom, Slovakia, Slovenia, Sweden, Czech Republic.

### List of European Economic Area States

These are the 28 member countries of the European Union (EU) listed above and the following 3 countries: Norway, Iceland, Liechtenstein.

