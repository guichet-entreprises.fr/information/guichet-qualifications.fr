﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Je suis titulaire d'un diplôme français..." -->
<!-- var(key)="fr-comprendre-diplome_francais" -->
<!-- var(last-update)="2020-05-03 15:13:47" -->
<!-- var(lang)="en" -->
<!-- var(site:lang)="en" -->

I have a French degree...
=========================


<!-- begin-include(disclaimer-trans-en) -->

**Notice about the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page has been translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page before undertaking any action.

The Enterprise Bank Service can not be held responsible for the operation of information that will be inaccurate due to a non-automatic translation faithful to the original.<!-- alert-end:warning -->

<!-- end-include -->
Latest update:<!-- begin-var(last-update) -->2020-05-03 15:13:47<!-- end-var -->

... and I would like to have my professional qualification recognised in a Member State of the European Union or the European Economic Area
-------------------------------------------------------------------------------------------------------------------------------------------

If you are a national of a Member State of the European Union (EU) or the European Economic Area (EEA), holding a French diploma or a diploma issued by a third country and recognised by France, you must have your professional qualification recognised before you start practising in an EU or EEA Member State.

I want to move to an EU or EEA country where my profession is regulated
-----------------------------------------------------------------------

Before you start working in a country, find out about the regulated professions in that country. If your profession happens to be regulated, you need to have your professional qualification recognized. It's the[competent authority](https://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=homepage) the country in which you are establishing yourself to carry out your activity which is the reference as to the steps to follow.

Once you have obtained recognition, you can practice your profession as the nationals who have graduated there.

I want to deliver a one-time benefit in a Member State where my profession is regulated
---------------------------------------------------------------------------------------

To provide a temporary service abroad, you must be established in your country of origin but without the obligation to practice and recognition of professional qualifications is not necessary, only a written advance declaration is sufficient. Depending on the degree of impact of the profession on health or safety, the member country may ask to verify your professional qualification. A renewal of the declaration may be required annually if you continue to work temporarily in that country.

**Good to know**

If the profession is not regulated in the host state, the assessment of the diploma and the professional level belongs to the employer.

Steps
-----

The authorities in the country in which you apply for recognition of your professional qualifications have one month to acknowledge your application and ask for other supporting documents.

Once the full file has been received, the authorities will decide within a time frame:

- 3 months if you are a doctor, general care nurse, midwife, veterinarian, dentist, pharmacist or architect and you benefit from a[automatic recognition](https://europa.eu/youreurope/citizens/work/professional-qualifications/european-professional-card-documents/index_fr.htm#103720) ;
- 4 months for all other occupations.

Therefore, allow 5 months during which the qualification recognition takes place, an essential prerequisite before practising your profession legally in the Member State where you provide your service. After that time,[contact the helpline](https://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=yec_pq) or talk to[national information points on the recognition of professional qualifications](https://ec.europa.eu/growth/single-market/services/free-movement-professionals_fr/#contacts).

Going further
-------------

Useful links (European Commission website):

- [Regulated Occupations Database lists occupations regulated by EU country and by authorities](https://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=homepage)
- [List of national websites for regulated professions](https://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=sites)
- To authenticate the relevant national authority, you can contact a[National information point on the recognition of professional qualifications](https://ec.europa.eu/growth/single-market/services/free-movement-professionals_fr/#contacts).

List of EU Member States
------------------------

Germany, Austria, Belgium, Bulgaria, Cyprus, Croatia, Denmark, Spain, Estonia, Finland, France, Greece, Hungary, Ireland, Italy, Latvia, Lithuania, Luxembourg, Malta, Netherlands, Poland, Portugal, Romania, United Kingdom, Slovakia, Slovenia, Sweden, Czech Republic.

List of European Economic Area States
-------------------------------------

These are the 28 member countries of the European Union (EU) listed above and the following 3 countries: Norway, Iceland, Liechtenstein.

