﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt) -->
<!-- include-file(generated.txt) -->

<!-- var(collapsable)="close" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Getting my professional qualification recognised in France" -->
<!-- var(key)="fr-comprendre-qualification_professionnelle_en_france" -->
<!-- var(last-update)="2020-07-15 14:48:47" -->
<!-- var(lang)="en" -->
<!-- var(site:lang)="en" -->

Getting my professional qualification recognised in France
==========================================================


<!-- begin-include(disclaimer-trans-en) -->
<!-- end-include -->
Latest update:<!-- begin-var(last-update) -->2020-05-03 11:32:08<!-- end-var -->

Are you a national of the European Union or the European Economic Area and would like to have your professional qualifications recognised in your country of origin?<!-- collapsable:off -->
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

The guichet-qualifications.fr website encourages the professional mobility of residents of the European Union and the European Economic Area. Online services guichet-entreprises.fr and guichet-qualifications.fr are the one-stop shop recognised by the European Commission. They allow you to explore new opportunities and develop an activity on French territory by completing your online formalities.

On this website, you can make a recognition of your professional qualification with the competent authority:

- I have a degree from an EU or EEA member state and I would like [to have my professional qualification recognised in France](diploma_member_state_ue_eee.md)
- I have a French diploma and I would like [to have my professional qualification recognised in an EU or EEA member state](french_diploma.md)

Create an activity in France with guichet-entreprises.fr
--------------------------------------------------------

Once you have recognized your professional qualification, you have the opportunity to set up your online business on[guichet-entreprises.fr](https://www.guichet-entreprises.fr) ! You will also find useful information about regulated activities in France and installation conditions.

Regulated activities<!-- collapsable:close -->
----------------------------------------------

Find out what conditions you must follow in order to carry out a regulated activity in France.

[Check out the fact sheets](https://www.guichet-entreprises.fr/en/activites-reglementees/)

Free establishment<!-- collapsable:close -->
--------------------------------------------

You can now settle in France and carry out your business freely, and this, permanently.

[Learn more about free establishment](https://www.guichet-entreprises.fr/en/eugo/libre-etablissement-le/)

Free provision of services<!-- collapsable:close -->
----------------------------------------------------

Delivering temporary or occasional services in France has never been easier !

[Learn more about free service delivery](https://www.guichet-entreprises.fr/en/eugo/libre-prestation-de-services-lps/)

