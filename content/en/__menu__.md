﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="[Accueil](index.md)" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__menu__" -->
<!-- var(last-update)="2020-05-03 15:13:22" -->
<!-- var(lang)="en" -->
<!-- var(site:lang)="en" -->

[Home](index.md)
================

Understand
==========

[](welcome.md)
----------------

[](understand/why_have_your_professional_qualification_recognised.md)
-----------------------------------------------------------------------------

[](understand/professional_qualification_in_france.md)
---------------------------------------------------------

[](understand/diploma_member_state_ue_eee.md)
--------------------------------------------

[](understand/french_diploma.md)
----------------------------------

[](understand/regulated_professions.md)
------------------------------------------

[](understand/european_business_card.md)
--------------------------------------------------

Regulated professions
=====================

# <!-- include(../reference/en/directive-qualification-professionnelle/_list_menu.md) -->

[Services](https://welcome.guichet-qualifications.fr/)
======================================================

[My files](https://dashboard.guichet-qualifications.fr/) @authenticated
=======================================================================

