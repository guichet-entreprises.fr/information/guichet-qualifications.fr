﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Bienvenue" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(key)="welcome" -->
<!-- var(translation)="Auto" -->
<!-- var(last-update)="2020-05-03 15:12:51" -->
<!-- var(lang)="en" -->
<!-- var(site:lang)="en" -->

Welcome
=======


<!-- begin-include(disclaimer-trans-en) -->

**Notice about the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page has been translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page before undertaking any action.

The Enterprise Bank Service can not be held responsible for the operation of information that will be inaccurate due to a non-automatic translation faithful to the original.<!-- alert-end:warning -->

<!-- end-include -->
Latest update:<!-- begin-var(last-update) -->2020-05-03 15:12:51<!-- end-var -->

You will find on this site all the information on the procedure for recognition of professional qualification.<!-- alert-start:info -->
<!-- alert-end:info -->

First visit to www.guichet-qualifications.fr
--------------------------------------------

Guichet-qualifications.fr is the one-stop shop for recognition of professional qualifications. Free and accessible to all, the online service allows you to build a folder without having to travel. You will find on the website[the fact sheets of the professions concerned](understand/regulated_professions.md) recognition of qualification.

**Did you know that?** The professional cards are put online as soon as they are written by the Business Box Office in collaboration with the relevant administrations. We always show the date of their latest updates.

Recognition of professional qualification: formalities
------------------------------------------------------

Ableding one's professional qualifications is imperative when one wishes to work in another Country of the European Union in which one's profession is regulated. To find out the rules that apply to your situation, you need to contact the national authority responsible for access to your profession in the host country. Thanks to the www.guichet-qualifications.fr website, you can complete these formalities online without having to travel.

**Read also** :[Why have your professional qualifications recognized?](https://www.guichet-qualifications.fr/en/pourquoi-faire-reconnaitre-sa-qualification-professionnelle/)

Create an account and identify yourself
---------------------------------------

To use the online service, you are invited to[create a personal space](https://account.guichet-qualifications.fr/session/new) on www.guichet-qualifications.fr. It will allow you to create and manage your file and change your personal information. You will be redirected to[www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/en/) but your steps will remain visible in the "My steps on guichet-qualifications.fr" tab on your dashboard.

With authentication[FranceConnect](https://franceconnect.gouv.fr/), if you already have an account on Impots.gouv.fr, Ameli.fr or the Post Office website, you can log in to www.guichet-qualifications.fr using one of these three accounts.

Complete and validate your file online
--------------------------------------

The site www.guichet-qualifications.fr allows you to build an online file, attach any supporting documents and pay any costs related to the formality. Once the file has been compiled and validated, it is sent to the appropriate body for processing. To get your file tracked, you can contact the relevant authority receiving your file at any time.

**Did you know that?** In France, there are 250 regulated professions (under the Qualifications Directive) listed in the[Regulated professions](understand/regulated_professions.md). If, however, you do not find your profession, go to the[European database of regulated professions](http://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=sites).

