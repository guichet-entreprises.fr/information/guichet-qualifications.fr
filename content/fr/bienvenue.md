﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Bienvenue" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(key)="welcome" -->
<!-- var(translation)="None" -->
<!-- var(last-update)="2020-05-03 11:30:54" -->
<!-- var(lang)="fr" -->

Bienvenue
========

Dernière mise à jour : <!-- begin-var(last-update) -->2020-05-03 11:30:54<!-- end-var -->


Vous trouverez sur ce site toute l’information sur la procédure de reconnaissance de qualification professionnelle.<!-- alert-start:info -->
<!-- alert-end:info -->

## Première visite sur www.guichet-qualifications.fr

Guichet-qualifications.fr est le guichet unique de la reconnaissance de qualifications professionnelles. Gratuit et accessible à tous, le service en ligne permet de constituer un dossier sans avoir à se déplacer. Vous trouverez sur le site [les fiches d’information des professions concernées](comprendre/professions_reglementees.md) par la reconnaissance de qualification.

**Le saviez-vous ?** Les fiches professions sont mises en ligne dès lors qu’elles sont rédigées par le service Guichet Entreprises en collaboration avec les administrations concernées. Nous affichons systématiquement la date de leurs dernières mises à jour.

## Reconnaissance de qualification professionnelle : les formalités

Faire reconnaître ses qualifications professionnelles est impératif lorsque l’on souhaite travailler dans un autre pays de l’Union européenne dans lequel sa profession est réglementée. Pour connaître les règles qui s’appliquent à votre situation, il vous faut contacter l’autorité nationale responsable de l’accès à votre profession dans le pays d’accueil. Grâce au site www.guichet-qualifications.fr, vous pouvez réaliser ces formalités en ligne sans avoir à vous déplacer.

**Lire aussi** : [Pourquoi faire reconnaitre sa qualification professionnelle ?](https://www.guichet-qualifications.fr/fr/pourquoi-faire-reconnaitre-sa-qualification-professionnelle/)

## Créer un compte et s’identifier

Pour utiliser le service en ligne, vous êtes invité à [créer un espace personnel](https://account.guichet-qualifications.fr/session/new) sur www.guichet-qualifications.fr. Ce dernier vous permettra de créer et gérer par la suite votre/vos dossier(s) et modifier vos informations personnelles. Vous serez redirigé vers [www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/fr/) mais vos démarches resteront visibles dans l’onglet « Mes démarches sur guichet-qualifications.fr » de votre tableau de bord.

Avec l’authentification [FranceConnect](https://franceconnect.gouv.fr/), si vous disposez déjà d’un compte sur Impots.gouv.fr, Ameli.fr ou le site de La Poste, vous pouvez vous connecter sur www.guichet-qualifications.fr en utilisant l’un de ces trois comptes.

## Compléter et valider votre dossier en ligne

Le site www.guichet-qualifications.fr permet de constituer un dossier en ligne, d’y joindre les éventuelles pièces justificatives et de régler les éventuels frais liés à la formalité. Une fois le dossier constitué et validé, ce dernier est envoyé à l’organisme compétent pour traitement. Pour obtenir le suivi de votre dossier, vous pourrez à tout moment contacter l’autorité compétente destinataire de votre dossier.

**Le saviez-vous ?** En France, il existe 250 professions réglementées (au titre de la directive « Qualifications ») dont vous trouverez la liste dans la rubrique [Professions réglementées](comprendre/professions_reglementees.md). Si toutefois vous ne trouvez pas votre profession, rendez-vous sur la [base de données européenne des professions réglementées](http://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=sites).