﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Mentions légales" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="None" -->
<!-- var(key)="fr-__footer__-mentions_legales" -->
<!-- var(last-update)="2020-05-03 11:33:00" -->
<!-- var(lang)="fr" -->

Mentions légales
================

Dernière mise à jour : <!-- begin-var(last-update) -->2020-05-03 11:33:00<!-- end-var -->


## Identification de l’éditeur

    Service à compétence nationale Guichet Entreprises
    120 rue de Bercy – Télédoc 766
    75572 Paris cedex 12

Le service Guichet Entreprises, créé par l’[arrêté du 22 avril 2015](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=A1B17F7C2CD76682B8F5E3E5CE690458.tpdila13v_1?cidTexte=JORFTEXT000030517635&dateTexte=&oldAction=rechJO&categorieLien=id&idJO=JORFCONT000030517419), est placé sous l’autorité de la [direction générale des entreprises](https://www.entreprises.gouv.fr/) (DGE) au sein du [ministère de l’Économie et des Finances](https://www.economie.gouv.fr/).

La conception éditoriale, le suivi, la maintenance technique et les mises à jour du site internet [www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/) sont assurés par le service Guichet Entreprises.

## Directeur de la publication

Monsieur Florent Tournois, chef du service Guichet Entreprises.

## Prestataire d’hébergement

    Cloud Temple
    215 avenue Georges Clémenceau
    92024 Nanterre
    Tel : +33 1 41 91 77 77

## Objet du site

Le site www.guichet-qualifications.fr permet à tout résident de l’Union européenne ou de l’Espace économique européen de faire reconnaître ses qualifications professionnelles et d’exercer une profession réglementée en France en vue d’une implantation durable ([libre établissement](https://www.guichet-entreprises.fr/fr/eugo/libre-etablissement-le/)) ou temporaire ([libre prestation de services](https://www.guichet-entreprises.fr/fr/eugo/libre-prestation-de-services-lps/)). Il permet également aux résidents en France de s’informer sur les possibilités d’accéder et d’exercer une profession réglementée dans un autre État membre.

Les formalités font l’objet d’une signature électronique dont les modalités sont disponibles sur le site au lien suivant : [Politique de signature électronique](politique_signature_electronique.md).

En outre, le site www.guichet-qualifications.fr met à disposition des entrepreneurs un ensemble d’informations concernant les formalités, procédures et exigences liées aux activités réglementées conformément à l’article R. 123-2 du Code de commerce.

## Traitement des données personnelles

Les données personnelles contenues dans le dossier unique conformément au règlement général sur la protection des données font l’objet de dispositions disponibles sur ce site au lien suivant : [Politique de protection des données](politique_protection_donnees.md).

En application de ce règlement et de la loi Informatique et Libertés du 6 janvier 1978, vous disposez d’un droit d’accès, de rectification, de modification et de suppression des données qui vous concernent.

Vous pouvez exercer ce droit de plusieurs façons :

- en vous adressant au centre de formalités des entreprises (CFE) destinataire du dossier de déclaration ;
- en envoyant un courriel au support ;
- en envoyant un courrier à l’adresse suivante :

        Service à compétence nationale Guichet Entreprises
        120 rue de Bercy – Télédoc 766
        75572 Paris cedex 12

## Droits de reproduction

Le contenu de ce site relève de la législation française et internationale sur le droit d’auteur et la propriété intellectuelle.

L’ensemble des éléments graphiques du site est la propriété du service Guichet Entreprises. Toute reproduction ou adaptation des pages du site qui en reprendrait les éléments graphiques est strictement interdite.

Toute utilisation des contenus à des fins commerciales est également interdite.

Toute citation ou reprise de contenus du site doit avoir obtenu l’autorisation du directeur de la publication. La source (www.guichet-qualifications.fr) et la date de la copie devront être indiquées ainsi que la mention du service « Guichet Entreprises ».

## Liens vers les pages du site

Tout site public ou privé est autorisé à établir des liens vers les pages du site www.guichet-qualifications.fr. Il n’y a pas à demander d’autorisation préalable. Cependant, l’origine des informations devra être précisée, par exemple sous la forme : « Reconnaissance de qualification professionnelle (source : www.guichet-qualifications.fr, un site du service Guichet Entreprises) ». Les pages du site www.guichet-qualifications.fr ne devront pas être imbriquées à l’intérieur des pages d’un autre site. Elles devront être affichées dans une nouvelle fenêtre ou un nouvel onglet.

## Liens vers les pages de sites extérieurs

Les liens présents sur le site www.guichet-qualifications.fr peuvent orienter l’utilisateur sur des sites extérieurs dont le contenu ne peut en aucune manière engager la responsabilité du service Guichet Entreprises.

## Environnement technique

Certains navigateurs peuvent bloquer par défaut l’ouverture de fenêtres sur ce site. Afin de vous permettre d’afficher certaines pages, vous devez autoriser l’ouverture des fenêtres lorsque le navigateur vous le propose en cliquant sur le bandeau d’avertissement alors affiché en haut de la page.

En cas d’absence de message d’avertissement de la part de votre navigateur, vous devez configurer celui-ci afin qu’il autorise l’ouverture des fenêtres pour le site www.guichet-qualifications.fr.