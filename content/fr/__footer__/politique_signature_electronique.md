﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Politique de signature électronique" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="None" -->
<!-- var(key)="fr-__footer__-politique_signature_electronique" -->
<!-- var(last-update)="2020-05-03 11:34:36" -->
<!-- var(lang)="fr" -->

Politique de signature électronique
===================================

Dernière mise à jour : <!-- begin-var(last-update) -->2020-05-03 11:34:36<!-- end-var -->


|    Version  |  Date    |   Objet            |
|:-----------:|:--------:|--------------------|
| V1          |27/09/2018| Version initiale   |

## Objet du document

La signature électronique apposée sur un ensemble de données permet de garantir l’intégrité des données transmises, la non-répudiation des données signées et l’authenticité de leur émetteur.

La présente politique de signature électronique est un document décrivant les conditions de recevabilité par les organismes destinataires, à savoir les organismes compétents, d’un fichier sur lequel est apposé une signature électronique dans le cadre d’échanges électroniques visés à l’article R. 123-30-10 du Code de commerce.

Les organismes compétents sont les services en charge d'instruire les demandes de reconnaissance de qualification professionnelle.

En vertu de cet article, *« lorsqu'une signature est requise, le recours à une signature électronique présentant les garanties énoncées à la première phrase du second alinéa de l'article 1367 du Code civil est autorisé »*, et du second alinéa de l'article 1367 du Code civil, *« lorsqu'elle est électronique, elle consiste en l'usage d'un procédé fiable d'identification garantissant son lien avec l'acte auquel elle s'attache. La fiabilité de ce procédé est présumée, jusqu'à preuve contraire, lorsque la signature électronique est créée, l'identité du signataire assurée et l'intégrité de l'acte garantie, dans des conditions fixées par décret en Conseil d'État.»*,

Les dispositions réglementaires applicables pour la mise en œuvre de la signature électronique du téléservice Guichet Qualifications sont issues des textes suivants :
- Règlement n° 910/2014 du Parlement européen et du Conseil du 23 juillet 2014 (e-IDAS) sur l’identification électronique et les services de confiance pour les transactions électroniques au sein du marché intérieur ;
- Code civil ;
- Décret n° 2017-1416 du 28 septembre 2017 relatif à la signature électronique.

Dans le cas où la signature électronique requiert la fourniture de données personnelles les dispositions réglementaires applicables sont issues des textes suivants :
- Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016, règlement relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données, et abrogeant la directive 95/46/CE (règlement général sur la protection des données) ;
- Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés.

Le présent document, « Politique de signature électronique du téléservice Guichet Qualifications », décrit l’ensemble des règles et des dispositions définissant les exigences auxquelles chacun des acteurs impliqués dans ces échanges dématérialisés se conforme pour la transmission et la réception des flux.

Ce document est destiné :
- aux organismes compétents ;
- aux éventuels prestataires participant à ces échanges dématérialisés pour le compte de ces organismes destinataires.

Dans la suite de ce document :
- les organismes destinataires susvisés sont désignés par le terme « destinataires » ;
- les échanges dématérialisés susvisés sont désignés par le terme « dossiers » ;
- le service à compétence nationale Guichet Entreprises est désigné par le terme « service Guichet Entreprises ».

## Champ d’application

La signature électronique est requise pour toute formalité liée à une demande de reconnaissance de qualification professionnelle au sens de la directive 2005/36/CE du Parlement européen et du Conseil du 7 septembre 2005 relative à la reconnaissance de qualification professionnelle (« directive qualifications professionnelles ») et à son exercice, mentionnées à l’article R. 123-30-8 du Code de commerce.

L'article 9 de l'ordonnance n° 2016-1809 du 22 décembre 2016 dispose des personnes concernées par ces formalités,

*Tout ressortissant d'un État membre de l'Union européenne ou partie à l'accord sur l'Espace économique européen ayant acquis ses qualifications professionnelles dans un État membre de l'Union européenne ou partie à l'accord sur l'Espace économique européen, et souhaitant obtenir une reconnaissance de celles-ci pour l'exercice d'une profession réglementée au sens de la directive 2005/36/CE du Parlement européen et du Conseil du 7 septembre 2005 susvisée, peut remplir ou suivre à distance et par voie électronique, par l'intermédiaire d'un guichet unique, l'ensemble des exigences, procédures ou formalités relatives à la reconnaissance de ses qualifications professionnelles dans des conditions déterminées par décret en Conseil d'État.*

La signature électronique revêt des caractéristiques différenciées selon la nature des dossiers transmis.

**Dans le cas où les dispositions légales énoncées à l'article R. 123-30-10 du Code de commerce s'appliquent**, la signature électronique requise répond aux dispositions de la première phrase du second alinéa de l’article 1316-4 du Code civil :

*Lorsqu'elle est électronique, elle consiste en l'usage d'un procédé fiable d'identification garantissant son lien avec l'acte auquel elle s'attache.*

**Dans le cas où des dispositions légales plus contraignantes que celles énoncées à l'article R. 123-30-10 du Code de commerce s'appliquent** (par exemple la reconnaissance de qualification professionnelle pour l'activité de médecin généraliste), la signature électronique répond aux dispositions de l’article 1 du décret n° 2017-1416 du 28 septembre 2017 (référence au règlement « eIDAS » n°910/2014) :

*La fiabilité d'un procédé de signature électronique est présumée, jusqu'à preuve du contraire, lorsque ce procédé met en œuvre une signature électronique qualifiée.*

*Est une signature électronique qualifiée une signature électronique avancée, conforme à l'article 26 du règlement susvisé et créée à l'aide d'un dispositif de création de signature électronique qualifié répondant aux exigences de l'article 29 dudit règlement, qui repose sur un certificat qualifié de signature électronique répondant aux exigences de l'article 28 de ce règlement.*

Article 26 du règlement « eIDAS » n° 910/2014 :

*Une signature électronique avancée satisfait aux exigences suivantes :*
1. *Être liée au signataire de manière univoque ;*
2. *Permettre d’identifier le signataire ;*
3. *Avoir été créée à l’aide de données de création de signature électronique que le signataire peut, avec un niveau de confiance élevé, utiliser sous son contrôle exclusif ;*
4. *Être liée aux données associées à cette signature de telle sorte que toute modification ultérieure des données soit détectable.*

Pour information, trois niveaux de garantie sont prévus par le règlement « eIDAS » n° 910/2014 :
- *Faible* : à ce niveau, l’objectif est simplement de réduire le risque d’utilisation abusive ou d’altération d’identité ;
- *Substantiel* : à ce niveau, l’objectif est de réduire substantiellement le risque d’utilisation abusive ou d’altération d’identité ;
- *Élevé* : à ce niveau, l’objectif est d’empêcher l’utilisation abusive ou l’altération de l’identité.

### Identification

L’identification de cette politique de signature électronique est signifiée par la présence du certificat du service Guichet Entreprises utilisé pour la signature électronique du document.

Le numéro de série du certificat a pour valeur :

4B-51-5A-35-00-00-00-00-01-EF.

### Publication du document

La présente politique de signature électronique est publiée suite à son approbation par le responsable sécurité des systèmes d’information (RSSI) du service Guichet Entreprises.

### Processus de mise à jour

#### Circonstances rendant une mise à jour nécessaire

La mise à jour de la présente politique de signature électronique peut avoir pour origines notamment, l’évolution du droit en vigueur (cf. « Objet du Document »), l’apparition de nouvelles menaces et de nouvelles mesures de sécurité, la prise en compte des observations des différents acteurs.

La présente politique de signature électronique est réexaminée au moins tous les deux ans.

#### Prise en compte des remarques

Toutes les remarques, ou souhaits d’évolution, sur la présente politique de signature électronique sont à adresser par messagerie électronique à l’adresse suivante : [contact](mailto:contact.guichet-entreprises@finances.gouv.fr).

Ces remarques et souhaits d’évolution sont examinés par le RSSI du service Guichet Entreprises qui engage si nécessaire le processus de mise à jour de la présente politique de signature électronique.

#### Information des acteurs

Les informations relatives à la version courante de cette politique et aux versions antérieures sont disponibles dans le tableau des versions situé en tête du présent document.

La publication d’une nouvelle version de la politique de signature consiste à :
1. mettre en ligne la politique de signature électronique au format HTML ;
2. archiver la version précédente après apposition de la mention « obsolète » sur chaque page.

## Entrée en vigueur d’une nouvelle version et période de validité

Une nouvelle version de la politique de signature n’entre en vigueur qu’un (1) mois calendaire après sa mise en ligne et reste valide jusqu’à l’entrée en vigueur d’une nouvelle version.

Le délai d’un (1) mois est mis à profit par les destinataires pour prendre en compte dans leurs applications, les changements apportés par la nouvelle politique de signature électronique.

## Acteurs

### Le signataire du dossier

Le signataire du dossier est le déclarant ou son mandataire effectuant la formalité de demande de reconnaissance de qualification professionnelle

#### Le rôle du signataire

Le signataire a pour rôle d’apposer sa signature électronique sur son dossier intégrant la formalité et les pièces justificatives afférentes,

Pour apposer une signature électronique sur son dossier, le signataire s’engage à utiliser un outil de signature respectant la présente politique de signature électronique.

#### Les obligations du signataire

Ces obligations sont décrites dans les chapitres ci-dessous.

### Outil de signature utilisé

Le signataire doit contrôler les données qu’il va signer avant d’y apposer sa signature électronique.

### Procédure de signature électronique utilisée

La procédure de signature électronique est fonction du type de formalité (cf. « Champ d'application »).

**Dans le cas où les dispositions légales énoncées à l'article R. 123-30-10 du Code de commerce s'appliquent**, le signataire doit cocher une case en fin de formalité indiquant qu’il déclare sur l'honneur l'exactitude des informations de la formalité et signe la présente déclaration.

Il est alors fait mention dans l’espace de signature du formulaire (Cerfa), de la conformité de cette signature aux attendus de l’alinéa 3 de l’article A. 123-4 du Code de commerce.

Alinéa 3 de l’article A. 123-4 du Code de commerce :

*3° En cochant la case informatique prévue à cet effet, le déclarant déclare sur l'honneur l'exactitude des éléments déclarés conformément à la formule suivante : « Je déclare sur l'honneur l'exactitude des informations de la formalité et signe la présente déclaration n°..., faite à..., le.... ».*

**Dans le cas où des dispositions légales plus contraignantes que celles énoncées à l'article R. 123-30-10 du Code de commerce s'appliquent**, le signataire doit joindre une copie de sa pièce d’identité déclarée copie conforme à l’original conformément aux dispositions de l’alinéa 2 de l’article A. 123-4 du Code de commerce :

Alinéa 2 de l’article A. 123-4 du Code de commerce :

*2° Les documents qui la composent ont fait l'objet d'une numérisation. La copie du justificatif d'identité est numérisée après avoir été préalablement revêtue d'une mention manuscrite d'attestation sur l'honneur de conformité à l'original, d'une date et de la signature manuscrite de la personne qui effectue la déclaration.*

Le document à signer électroniquement (formulaire Cerfa et copie de la pièce d’identité) lui est présenté ainsi que la convention de preuve listant les conditions et les conséquences de la signature électronique du document.

Le signataire doit cocher une case indiquant qu’il a pris connaissance de la convention de preuve et l’accepte sans réserve.

Un code de validation de la signature électronique lui est transmis sur son téléphone portable dont il a indiqué le numéro lors de la création de son compte.

La saisie de ce code permet de déclencher le scellement du document (formulaire/Cerfa et copie de la pièce d’identité), c’est-à-dire sa signature électronique basée sur un certificat, de son hachage de et l’horodatage de l’ensemble.

Le document scellé est archivé et transmis à ou aux destinataire(s) concerné(s).

Les cas d'erreur les plus fréquents sont les suivants :

**Cas 1 = Code incorrect**

Cas fonctionnel :

*L’utilisateur a saisi un code incorrect (par exemple des lettres au lieu de chiffres).*

Exemple de message affiché :

*Le code que vous avez renseigné est incorrect. Veuillez vérifier le code qui vous a été transmis par SMS et ressaisissez-le.*

**Cas 2 = Code expiré**

Cas fonctionnel :

*L’utilisateur a saisi trop tardivement le code reçu par SMS (la durée de vie d’un code OTP = One Time Password est de 20 minutes).*

Exemple de message affiché :

*Le délai de validité de votre code a expiré. Vous devez renouveler votre demande de signature électronique.*

**Cas 3 = Code bloqué**

Cas fonctionnel :

*L’utilisateur a effectué N saisies infructueuses de codes OTP. Afin d’éviter des tentatives de piratage, le numéro de téléphone de l’utilisateur est identifié comme bloqué dans le système.*

Exemple de message affiché :

*Suite à de nombreuses erreurs de saisies de votre part, votre numéro de téléphone +33600000000 ne vous permet plus de signer électroniquement votre démarche. Nous vous invitons à renseigner un nouveau numéro de téléphone dans votre compte utilisateur ou à contacter le Guichet Entreprises pour débloquer votre numéro de téléphone actuel.*

### Protection et usage du dossier signé

Ce chapitre ne traite que des dossiers portant sur des formalités pour lesquels des dispositions légales plus contraignantes que celles énoncées à l'article R. 123-30-10 du Code de commerce s'appliquent.

Le document signé électroniquement est conservé dans un coffre-fort électronique accompagné d’un fichier lisible portant toutes les traces de son scellement à des fins d’audit.

Le coffre-fort électronique n’est accessible que par son administrateur et le RSSI du service Guichet Entreprises.

L’accès au coffre-fort électronique n’est possible qu’à l’aide d’un certificat nominatif et toute action sur celui-ci est tracée.

Pour le RSSI du service Guichet Entreprises, seuls les accès à des fins d’audit sur demande justifiée (par exemple requête judiciaire) ne sont autorisés.

Le document signée est transmis à ou aux organisme(s) destinataire(s) en charge d’instruire le dossier associé.

Les modalités de protection du dossier transmis signé électroniquement sont à la charge du destinataire et doivent être conformes aux dispositions du Règlement général de sécurité et du Règlement général de protection des données.

Le RSSI du service Guichet Entreprises s’assure du respect de ces dispositions.

### Archivage et suppression du dossier signé

Le dossier est archivé et supprimé conformément aux dispositions de l’article R. 123-30-12 du Code de commerce.

Article R. 123-30-12 du Code de commerce :

*Si le déclarant utilise un service de conservation provisoire des données proposé par le service de déclaration dans des conditions conformes à la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, il est procédé, à l'issue de la période de conservation provisoire d'une durée maximale de douze mois, à l'effacement de toutes les données et de tous les fichiers concernant le déclarant sur les supports informatiques où ils figurent.*

Les fournisseurs de solutions de signature électronique
-------------------------------------------------------

La solution doit incorporer, dans la structure des données de signature, les attendus de la présente politique de signature électronique.

Le service Guichet Entreprises
-----------------------------------------------------

Le RSSI du service Guichet Entreprises s’assure du respect des dispositions de la présente politique de signature électronique pour le téléservice Guichet Qualifications mis à disposition des usagers.

Il vérifie que les systèmes d’information sources et destinataires des flux ne présentent pas de risques quant à l’intégrité et la confidentialité des dossiers stockés et transmis.

Les destinataires
-----------------

Les destinataires contrôlent l’intégrité de la signature électronique du dossier et s’assure que les modalités de conservation et de suppression des dossiers reçus sont conformes aux dispositions réglementaires.

### Données de vérification

Pour effectuer les vérifications, le service Guichet Entreprises utilise les informations de scellement du dossier signé électroniquement (cf. « Protection et usage du dossier signé »).

### Protection des moyens

Le service Guichet Entreprises s’assure de la mise en œuvre des moyens nécessaires à la protection des équipements fournissant les services de validation.

Les mesures prises concernent à la fois :
- la protection des accès physiques et logiques aux équipements aux seules personnes habilitées ;
- la disponibilité du service ;
- la surveillance et le suivi du service.

### Assistance aux signataires 

L’assistance à l’utilisation de la procédure de signature électronique est assurée par le support du service Guichet Entreprises accessible par courriel à l’adresse suivante : [support](mailto:support.guichet-entreprises@helpline.fr).

## Signature électronique et validation

### Données signées

Les données signées sont composées du formulaire/Cerfa et de la copie de la pièce d’identité sous forme d’un seul fichier PDF avec une zone de signature électronique modifiable.

L’ensemble du fichier PDF est signé et de fait aucune modification de celui-ci n’est possible après son scellement.

### Caractéristiques des signatures

La signature respecte les attendus de la spécification « XML Signature Syntax and Processing (XMLDsig) » élaborée par W3C (World Wide Web Consortium : <https://www.w3.org/>) ainsi que les extensions de format de signatures spécifiées dans le standard européen XML Advanced Electronic Signature (XADES) de l’ETSI (<https://www.etsi.org>).

La signature électronique déployée implémente un niveau d’identification « substantiel » (norme ISO/IEC 29115:2013), soit conformément au règlement eIDAS une signature électronique avancée

#### Type de signature

La signature électronique est de type
[enveloppée](https://fr.wikipedia.org/wiki/XML_Signature).

#### Norme de signature

La signature électronique doit respecter la norme [XMLDsig, révision 1.1 de
février 2002](https://www.w3.org/TR/2002/REC-xmldsig-core-20020212).

La signature électronique doit respecter la norme XAdES-EPES (Explicit Policy based Electronic Signature), [ETSI TS 101 903 version v1.3.2](http://uri.etsi.org/01903/v1.3.2).

Conformément à la norme XadES, les propriétés signées (SignedProperties/SignedSignatureProperties) doivent contenir les éléments suivants :

- le certificat du signataire (SigningCertificate) ;
- la date et l’heure de signature (SigningTime) au format UTC.

Algorithmes utilisables pour la signature
-----------------------------------------

#### Algorithme de condensation

Le calcul du condensé par itération de la fonction de compression sur la suite des blocs obtenus en découpant le message (schéma itératif de Merkle-Damgård).

Cet algorithme est accessible au lien : [Merkle-Damgård](https://fr.wikipedia.org/wiki/Construction_de_Merkle-Damgård)

#### Algorithme de signature

L’algorithme de signature est basé sur RSA/[SHA 256](https://fr.wikipedia.org/wiki/SHA-2).

#### Algorithme de canonicalisation

L’algorithme de canonicalisation est [c14N](https://www.w3.org/TR/xml-exc-c14n/).

### Conditions pour déclarer valide le fichier signé

Les conditions pour déclarer valide le fichier signé sont les suivantes :
- le signataire a rempli ses obligations :
    - téléchargement d’une copie de sa pièce d’identité avec mention de copie conforme à l’original et acceptation de la convention de preuve,
    - saisie du code envoyé par SMS (méthode « One Time Password ») ;
- le service Guichet Entreprises a rempli ses obligations :
    - mise à disposition d’un certificat généré par une AC certifiant son origine (fournisseur du service de signature électronique) et dont les caractéristiques sont les suivantes : *Clef publique = RSA 2048 bits, algorithme de signature = RSA SHA 256* ;
- le fournisseur du service de signature électronique a rempli ses obligations :
    - signature et scellement du fichier PDF selon les caractéristiques décrites dans les chapitres précédents.

La vérification de validité de la signature peut être effectuée via Adobe Acrobat.

## Dispositions juridiques

### Données nominatives

Le porteur dispose d’un droit d’accès, de modification, de rectification et de suppression des données le concernant qu’il peut exercer par courriel à l’adresse suivante : [contact](mailto:contact.guichet-entreprises@finances.gouv.fr)

### Droit applicable – Résolution des litiges

Les présentes dispositions sont soumises au droit français.

Tout litige relatif à la validité, l’interprétation ou l’exécution des présentes dispositions sera soumis à la juridiction du tribunal administratif de Paris.