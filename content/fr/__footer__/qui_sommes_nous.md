﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Qui sommes-nous ?" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="None" -->
<!-- var(key)="fr-__footer__-qui_sommes_nous" -->
<!-- var(last-update)="2020-05-03 11:34:42" -->
<!-- var(lang)="fr" -->

Qui sommes-nous ?
================

Dernière mise à jour : <!-- begin-var(last-update) -->2020-05-03 11:34:42<!-- end-var -->


Le site www.guichet-qualifications.fr est conçu et développé par le service à compétence nationale « Guichet Entreprises », créé par l’[arrêté du 22 avril 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&dateTexte=20200109) (JORF du 25 avril 2015). Il est placé sous l’autorité de la [direction générale des entreprises](https://www.entreprises.gouv.fr/) au sein du [ministère de l’Économie et des Finances](https://www.economie.gouv.fr/).

Le service à compétence nationale « Guichet Entreprises » gère les sites Internet [www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/) et [www.guichet-entreprises.fr](https://www.guichet-entreprises.fr/) [1] qui, à eux deux, constituent le guichet unique électronique défini par les directives européennes [2006/123/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32006L0123) et [2005/36/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036). Des guichets de ce type existent dans toute l’Europe et sont fédérés au sein du projet « [Eugo](http://ec.europa.eu/internal_market/eu-go/index_fr.htm) » de la Commission européenne.

Au service Guichet Entreprises, nous encourageons la mobilité professionnelle des résidents de l’Union européenne ou de l’Espace économique européen en donnant l’accès à une information complète sur l’accès et l’exercice des professions réglementées en France par le biais des « fiches professions ».

Le site [www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/) s’adresse à tous les résidents de l’Union européenne ou de l’Espace économique européen. Il les informe sur les possibilités de faire reconnaître leurs qualifications professionnelles et d’exercer une profession réglementée en France en vue d’une implantation durable ([libre établissement](https://www.guichet-entreprises.fr/fr/eugo/libre-etablissement-le/)) ou temporaire ([libre prestation de services](https://www.guichet-entreprises.fr/fr/eugo/libre-prestation-de-services-lps/)). Il permet également aux résidents en France de s’informer sur les possibilités d’accéder et d’exercer une profession réglementée dans un autre État membre.