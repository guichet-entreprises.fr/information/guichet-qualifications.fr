﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Politique de protection des données personnelles" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="None" -->
<!-- var(key)="fr-__footer__-politique_protection_donnees" -->
<!-- var(last-update)="2020-05-03 11:33:35" -->
<!-- var(lang)="fr" -->

Politique de protection des données personnelles
===============================================

Dernière mise à jour : <!-- begin-var(last-update) -->2020-05-03 11:33:35<!-- end-var -->


|    Version  |  Date    |   Objet                                                                      |
|:-----------:|:--------:|------------------------------------------------------------------------------|
| V1          |25/05/2018|Version initiale                                                              |
| V2          |01/09/2018|Mise à jour des informations concernant le délégué à la protection des données|

La politique de protection des données personnelles (ci-après la « politique ») vous informe sur la manière dont vos données sont collectées et traitées par le service à compétence nationale Guichet Entreprises, sur les mesures de sécurité mises en œuvre pour en garantir l’intégrité et la confidentialité et sur les droits dont vous disposez pour en contrôler l’usage.

Cette politique vient compléter les [conditions générales d’utilisation](cgu.md) (CGU) ainsi que, le cas échéant, les [mentions légales](mentions_legales.md). À ce titre, la présente politique est réputée être intégrée auxdits documents.

Dans le cadre du développement de nos services et de la mise en œuvre de nouvelles normes réglementaires, nous pouvons être amenés à modifier la présente politique. Nous vous invitons donc à en prendre connaissance régulièrement.

## Qui sommes-nous ?

Le service Guichet Entreprises, créé par l’[arrêté du 22 avril 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&categorieLien=id) est placé sous l’autorité de la [direction générale des entreprises](https://www.entreprises.gouv.fr/) (DGE) au sein du [ministère de l’Économie et des Finances](https://www.economie.gouv.fr/).

Le service Guichet Entreprises met en œuvre un téléservice guichet-qualifcations.fr (ci-après le « Téléservice Guichet Qualifications Professionnelles »).

Le Téléservice Guichet Qualifications Professionnelles s’adresse à tous les résidents de l’Union européenne ou de l’Espace économique européen. Il les informe sur les possibilités de faire reconnaître leurs qualifications professionnelles et d’exercer une profession réglementée en France en vue d’une implantation durable (libre établissement) ou temporaire (libre prestation de services).

Il permet également aux résidents en France de s’informer sur les possibilités d’accéder et d’exercer une profession réglementée dans un autre État membre.

Le Téléservice Guichet Qualifications Professionnelles s’inscrit dans le cadre juridique :

-   du [règlement général sur la protection des données](https://www.cnil.fr/fr/reglement-europeen-protection-donnees) ;
-   de la [directive 2005/36/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036) du Parlement européen ;
-   de la [directive 2013/55/UE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32013L0055) du Parlement européen ;
-   du [règlement n° 910/2014](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32014R0910) du Parlement européen et du Conseil du 23 juillet 2014 (e-IDAS) sur l’identification électronique et les services de confiance pour les transactions électroniques au sein du marché intérieur ;
-   de l’[ordonnance du 8 décembre 2005](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000636232) relative aux échanges électroniques entre les usagers et les autorités administratives et entre les autorités administratives et le [décret n° 2010-112 du 2 février 2010](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021779444&categorieLien=id) pris pour l’application des articles 9, 10 et 12 de cette ordonnance ;
-   du [dispositif de la loi n° 78-17 du 6 janvier 1978 modifiée](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460) relative à l’informatique, aux fichiers et aux libertés ;
-   de l’[article 441-1 du Code pénal](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006418753&cidTexte=LEGITEXT000006070719&dateTexte=20020101) ;
-   de l’[article R. 123-30 du Code de commerce](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006256107&dateTexte=&categorieLien=cid) ;
-   de l’[arrêté du 22 avril 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&categorieLien=id) portant création d’un service à compétence nationale dénommé « guichet entreprises ».

## Quelle organisation mettons-nous en œuvre pour protéger vos données ?

Le respect et la mise en œuvre des dispositions applicables en matière de protection des données personnelles sont suivis par le délégué à la protection des données du service Guichet Entreprises.

Au sein du ministère de l’Économie et des Finances, dont dépend la direction générale des entreprises, les services du haut fonctionnaire de défense sont en charge de vérifier la mise en œuvre et le respect des dispositions du règlement général sur la protection des données.

## Comment nous contacter ?

Pour toute question concernant l’utilisation de vos données personnelles par le service Guichet Entreprises, nous vous invitons à lire la présente politique et à contacter notre délégué à la protection des données en utilisant les coordonnées suivantes :

Par courrier à l’attention de :

    Le Délégué à la protection des données des ministères économique et financier
    Délégation aux Systèmes d’Information
    139, rue de Bercy – Télédoc 322
    75572 PARIS CEDEX 12

Par courriel, en précisant l’objet « Protection des données » à : [contact](contact.md).

## Qu’est-ce qu’une donnée personnelle ?

Les données personnelles sont des informations qui permettent de vous identifier, que ce soit directement ou indirectement. Il peut notamment s’agir de votre nom, votre prénom, votre adresse, votre numéro de téléphone ou votre adresse électronique, mais également l’adresse IP de votre ordinateur, par exemple, ou encore les informations liées à l’utilisation du Téléservice.

## Quand collectons-nous des données personnelles ?

Le service Guichet Entreprises collecte des données personnelles lorsque vous :

-   créez votre espace personnel ;
-   complétez une formalité via le Téléservice Guichet Entreprises.

## Quelles sont les données personnelles que nous traitons ?

Les données personnelles que nous traitons ne concernent que les formalités de reconnaissance de qualifications professionnelles conformément aux dispositions réglementaires des articles R. 123-30-10 à R. 123-30-14 du Code de commerce.

Les données recueillies permettent de constituer le dossier tel qu’attendu au titre de l’article R. 123-30-10 du Code de commerce.

Les données recueillies permettent notamment de compléter les documents Cerfa associés aux formalités et procédures nécessaires à la reconnaissance de qualifications professionnelles ou l’obtention de la carte professionnelle européenne.

La liste suivante résume les données à caractère personnel que nous traitons.

Pour la gestion de l’accès à l’espace personnel du service, l’usager partage les informations suivantes :

-   l’identifiant de connexion choisi par l’usager ;
-   le mot de passe choisi par l’usager ;
-   l’adresse électronique de l’usager ;
-   le numéro de téléphone portable ;
-   l’adresse IP de connexion.

Pour l’utilisation de l’espace de stockage personnel, l’usager partage les informations suivantes :

-   nom patronymique ;
-   nom d’usage/nom d’épouse ;
-   nom d’exercice ;
-   prénom ;
-   date naissance ;
-   pays naissance ;
-   nationalité ;
-   numéro de sécurité sociale ;
-   adresse de résidence (pays inclus) ;
-   téléphone ;
-   courriel ;
-   diplôme, certificat, titres ;
-   expérience professionnelle ;
-   projet professionnel ;
-   qualité ou fonction exercée ;
-   emploi actuel ;
-   lieu d’emploi (localité, pays) ;
-   intitulé du poste occupé.

## Quelles sont les pièces justificatives personnelles que nous traitons ?

Les pièces justificatives personnelles que nous traitons ne concernent que les formalités de reconnaissance de qualifications professionnelles ou obtention de la carte professionnelle européenne conformément aux dispositions réglementaires liées à l’activité réglementée exercée.

Les pièces justificatives permettent de constituer le dossier tel qu’attendu au titre de l’article R. 123-30-10 du Code de commerce.

La liste des pièces justificatives fait l’objet d’une homologation par l’autorité compétente en charge d’instruire le dossier.

Parmi les pièces justificatives personnelles figurent les pièces justifiant l’identité ou le domicile. Ces pièces justificatives sont conservées selon les mêmes modalités que les données personnelles.

Leur stockage est assuré sur des espaces inaccessibles depuis l’extérieur (Internet) et leur téléchargement ou transfert est sécurisé par chiffrement conformément aux dispositions indiquées dans le chapitre « Comment sont protégées vos données personnelles ? ».

## Quelle est notre gestion des cookies ?

La gestion des cookies est décrite dans la page : [https://www.guichet-entreprises.fr/fr/cookies/](https://www.guichet-entreprises.fr/fr/cookies/).

Tous les cookies gérés utilisent le mode « Secure cookie » (cookies sécurisés).

Les cookies sécurisés sont un type de cookie transmis exclusivement via des connexions chiffrées (https).

Les cookies techniques contiennent les données personnelles suivantes :

-   nom ;
-   prénom ;
-   courriel ;
-   téléphone.

Les cookies d’analyse contiennent l’adresse IP de connexion.

## Quels sont vos droits pour la protection de vos données personnelles ?

Vous pouvez accéder à vos données, les rectifier, les effacer, limiter leur utilisation ou encore vous opposer au traitement de celles-ci.

Si vous estimez que l’utilisation de vos données personnelles contrevient aux règles de protection des données personnelles, vous avez la possibilité d’adresser une réclamation à la [CNIL](https://www.CNIL.fr/).

Vous disposez également du droit de définir des directives relatives au traitement de vos données à caractère personnel en cas de décès. Les directives particulières peuvent être enregistrées auprès du responsable du traitement. Les directives générales peuvent être enregistrées auprès d’un tiers de confiance numérique certifié par la CNIL. Vous avez la possibilité de modifier ou supprimer ces directives à tout moment.

Le cas échéant, vous avez également la possibilité de nous demander de vous communiquer les données personnelles que vous nous avez fournies dans un format lisible.

Vous pouvez adresser vos différentes demandes à l’adresse suivante :

    Service à compétence nationale Guichet Entreprises
    120 rue de Bercy – Télédoc 766
    75572 Paris cedex 12

Vos demandes devront être accompagnées de la copie d’un justificatif d’identité et seront examinées par nos services.

## Comment sont protégées vos données personnelles ?

Le service Guichet Entreprises met en œuvre l’ensemble des pratiques standard de l’industrie et des procédures de sécurité afin de prévenir toute violation de vos données personnelles.

Toutes les informations communiquées sont stockées sur des serveurs sécurisés hébergés par nos prestataires techniques.

Lorsque la transmission de données est nécessaire et autorisée, le service Guichet Entreprises s’assure que ces tiers présentent les garanties suffisantes pour assurer un niveau de protection approprié. Les échanges sont chiffrés et les serveurs authentifiés par reconnaissance mutuelle.

Conformément à la règlementation sur la protection des données, en cas de violation, le service Guichet Entreprises s’engage à communiquer cette violation à l’autorité de contrôle compétente, et lorsque cela est exigé, aux personnes concernées.

#### Espace Personnel

Toutes les données transmises sur votre espace personnel sont chiffrées et leur accès est sécurisé par l’utilisation d’un mot de passe personnel. Vous êtes tenu de garder ce mot de passe confidentiel et de ne pas le communiquer.

#### Durée de conservation

Les données personnelles traitées par le service Guichet Entreprises sont conservées pour une durée de douze mois conformément aux dispositions de l’article R. 123-30-14 du Code de commerce.

#### Concernant les données de cartes bancaires

Le service Guichet Entreprises ne stocke ni ne conserve les données bancaires des usagers soumis à paiement lors de leur formalité. Notre prestataire gère pour le compte du service Guichet Entreprises les données de transactions conformément aux règles de sécurité les plus strictes applicables dans le secteur du paiement en ligne via l’utilisation de procédés de chiffrement.

## A qui sont transmises vos données ?

Les données recueillies permettent de constituer le dossier tel qu’attendu au titre de l’article R.123-30-10 du Code de commerce.

En vertu de l’article R.123-30-12 du Code de commerce, les données recueillies sont transmises aux fins de traitement aux autorités compétentes.