﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(translation)="None" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Professions réglementées" -->
<!-- var(key)="fr-comprendre-professions_reglementees" -->
<!-- var(last-update)="2020-05-03 11:32:02" -->
<!-- var(lang)="fr" -->

# Professions réglementées

Dernière mise à jour : <!-- begin-var(last-update) -->2020-05-03 11:32:02<!-- end-var -->


Architecte, infirmier, vétérinaire, boulanger, etc., avant de faire reconnaître votre qualification professionnelle, informez-vous sur les conditions d’accès à une profession réglementée en France.

Consultez nos fiches d’information classées par groupes de professions.

## Réglementation spécifique en France

En France, certaines professions nécessitent pour leur exercice l’obtention d’un titre professionnel, d’une qualification professionnelle voire d’une autorisation préalable, sanctionnant un certain niveau de formation ou d’expérience. Les fiches professions permettent de faire le point sur les modalités de reconnaissance des qualifications professionnelles ainsi que sur la législation applicable à ces différentes professions. À ce jour près de 250 activités sont concernées et font l’objet de nos fiches professions.

**Le saviez-vous ?** Les fiches professions sont mises en ligne dès lors qu’elles sont rédigées par le service Guichet Entreprises en collaboration avec les administrations concernées.

Si la fiche d’information relative à votre qualification professionnelle n’est pas disponible, rendez-vous sur la [base de données européenne des professions réglementées](https://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=homepage).