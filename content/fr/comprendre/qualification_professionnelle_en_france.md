﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(translation)="None" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Faire reconnaître ma qualification professionnelle en France" -->
<!-- var(key)="fr-comprendre-qualification_professionnelle_en_france" -->
<!-- var(last-update)="2020-07-15 11:32:08" -->
<!-- var(lang)="fr" -->

Faire reconnaître ma qualification professionnelle en France
=====================================================

Dernière mise à jour : <!-- begin-var(last-update) -->2020-05-03 11:32:08<!-- end-var -->


## Vous êtes ressortissant de l’Union européenne ou de l’Espace économique européen et vous souhaitez faire reconnaître votre qualification professionnelle acquise dans votre pays d’origine ? <!-- collapsable:off -->

Le site guichet-qualifications.fr encourage la mobilité professionnelle des résidents de l’Union européenne et de l’Espace économique européen. Les services en ligne guichet-entreprises.fr et guichet-qualifications.fr constituent à eux deux le guichet unique reconnu par la Commission européenne. Ils vous permettent d’explorer de nouveaux débouchés et de développer une activité sur le territoire français en accomplissant vos formalités en ligne.

Sur ce site, vous pouvez effectuer une reconnaissance de votre qualification professionnelle auprès de l’autorité compétente :

- Je suis titulaire d’un diplôme d’un État membre de l’UE ou de l’EEE et je souhaite [faire reconnaître ma qualification professionnelle en France](diplome_etat_membre_ue_eee.md)
- J’ai un diplôme français et je souhaite [faire reconnaître ma qualification professionnelle dans un État membre de l’UE ou de l’EEE](diplome_francais.md)

## Créer une activité en France avec guichet-entreprises.fr

Une fois votre qualification professionnelle reconnue, vous avez la possibilité de créer votre entreprise en ligne sur [guichet-entreprises.fr](https://www.guichet-entreprises.fr) ! Vous y trouverez également des informations utiles concernant les activités réglementées en France et les conditions d’installation.

## Activités règlementées <!-- collapsable:close -->

Informez-vous sur les conditions à respecter pour exercer une activité règlementée en France.

[> Consultez les fiches d'informations](https://www.guichet-entreprises.fr/fr/activites-reglementees/)

## Libre établissement <!-- collapsable:close -->

Vous pouvez désormais vous installer en France et exercer votre activité librement, et ce, de façon permanente.

[> En savoir plus sur le libre établissement](https://www.guichet-entreprises.fr/fr/eugo/libre-etablissement-le/)

## Libre prestation de services <!-- collapsable:close -->

Délivrer une prestation de services temporaire ou occasionnelle en France n'a jamais été aussi facile !

[> En savoir plus sur la libre prestation de services](https://www.guichet-entreprises.fr/fr/eugo/libre-prestation-de-services-lps/)