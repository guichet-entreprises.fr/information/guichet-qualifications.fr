﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="La carte professionnelle européenne (CPE)" -->
<!-- var(translation)="None" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(key)="fr-comprendre-carte_professionnelle_europeenne" -->
<!-- var(last-update)="2020-05-03 11:31:35" -->
<!-- var(lang)="fr" -->

# La carte professionnelle européenne (CPE)

Dernière mise à jour : <!-- begin-var(last-update) -->2020-05-03 11:31:35<!-- end-var -->


Certaines professions réglementées bénéficient du dispositif de la carte professionnelle européenne. Vous trouverez dans cet article toutes les informations relatives à son fonctionnement, aux formalités liées à son obtention et aux professions réglementées qu’elle concerne.

## Qu’est-ce que la carte professionnelle européenne?

La carte professionnelle européenne (CPE) est une procédure électronique pour la reconnaissance des qualifications professionnelles entre les pays membres de l’Union européenne définie par les articles 4a à 4e de la [directive 2005/36/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036). Elle a pour but de faciliter, pour les citoyens européens, l’exercice d’une profession réglementée dans un autre État membre de l’Union européenne. Cette procédure s’inscrit dans le cadre de la mobilité professionnelle des citoyens européens.

###### Bon à savoir

Il ne s’agit pas d’une carte physique mais d’une preuve électronique que les qualifications professionnelles du citoyen ont été vérifiées et sont reconnues par les autorités compétentes du pays d’accueil.

## Qui peut en bénéficier ?

Ce dispositif peut être utilisé par tout professionnel européen aussi bien dans le cadre de l’établissement dans le pays d’accueil pour y exercer ([libre établissement](https://www.guichet-entreprises.fr/fr/eugo/libre-etablissement-le/)), que dans le cadre d’une fourniture de services de manière temporaire ([libre prestation de services](https://www.guichet-entreprises.fr/fr/eugo/libre-prestation-de-services-lps/)).

À ce jour, la CPE est disponible pour cinq professions :

1. Agent immobilier ;
2. Guide de montagne ;
3. Infirmier ;
4. Kinésithérapeute ;
5. Pharmacien.

Ces professions ont été choisies pour bénéficier du système de la CPE selon trois critères :

- une mobilité significative, ou un potentiel de mobilité important, dans la profession concernée ;
- un intérêt suffisant manifesté par les parties ayant un intérêt légitime ;
- une réglementation de la profession ou de la formation qui lui est associée dans un nombre significatif d’États membres de l’UE.

Ce nombre de professions est amené à croître sachant que certaines, comme celles d’ingénieur ou d’infirmier spécialisé, sont à ce jour en cours d’évaluation.

## Pourquoi la demander?

La CPE est un certificat électronique. Elle a pour but de simplifier la reconnaissance des qualifications professionnelles et d’introduire un degré supplémentaire d’efficacité aussi bien pour les citoyens que pour les autorités compétentes chargées de la délivrer.

En matière de **libre prestation de services**, la CPE a vocation à remplacer la déclaration préalable à la première prestation dans l’État membre d’accueil qui doit être renouvelée tous les 18 mois.

En matière d’**établissement**, la CPE a valeur de décision de reconnaissance des qualifications professionnelles par l’État membre d’accueil.

###### Bon à savoir

Les professionnels concernés par la CPE ont le choix entre son dispositif ou les procédures classiques de déclaration préalable ou de reconnaissance de qualifications professionnelles.

###### Pour aller plus loin

[Carte professionnelle européenne](https://europa.eu/youreurope/citizens/work/professional-qualifications/european-professional-card/index_fr.htm) (Commission européenne)