﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Pourquoi faire reconnaître sa qualification professionnelle ?" -->
<!-- var(translation)="None" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(key)="fr-comprendre-pourquoi_faire_reconnaitre_sa_qualification_professionnelle" -->
<!-- var(last-update)="2020-05-03 11:31:59" -->
<!-- var(lang)="fr" -->

# Pourquoi faire reconnaître sa qualification professionnelle ?

Dernière mise à jour : <!-- begin-var(last-update) -->2020-05-03 11:31:59<!-- end-var -->


Une profession est dite « réglementée » lorsqu’il est nécessaire de détenir un diplôme spécifique, d’avoir passé des examens particuliers, d’avoir obtenu une certaine autorisation ou d’être enregistré auprès d’un organisme professionnel pour l’exercer.

**Faire reconnaître ses qualifications professionnelles** est impératif lorsque l’on souhaite travailler dans un autre pays de l’Union européenne dans lequel sa profession est réglementée. Cela revient à faire reconnaître officiellement sa formation et/ou son expérience professionnelle par le pays d’accueil.

- S’il s’agit de s’installer dans un autre pays pour y pratiquer sa profession, on rentre dans le cas de figure du [libre établissement](https://www.guichet-entreprises.fr/fr/eugo/libre-etablissement-le/) et il est  indispensable de procéder à la reconnaissance de ses qualifications.
- S’il s’agit de fournir des services à titre temporaire dans un autre pays, on se place dans le cas de la [libre prestation de services](https://www.guichet-entreprises.fr/fr/eugo/libre-prestation-de-services-lps/) et une déclaration préalable au premier exercice suffit. Il existe une exception pour les professions ayant des incidences graves pour la sécurité ou la santé car l’État membre d’accueil peut exiger de procéder à une vérification préalable des qualifications.

Pour connaître les règles qui s’appliquent à votre situation, veuillez contacter l’autorité nationale responsable de l’accès à votre profession dans le pays d’accueil.

Il vous est aussi possible de consulter la [base de données européenne des professions réglementées](http://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=homepage) qui liste l’ensemble des professions réglementées dans les pays de l’UE ainsi que leurs autorités compétentes.

- Si votre profession est réglementée dans votre pays d’origine, vous pouvez la rechercher dans votre propre langue et consulter la traduction anglaise fournie dans la description ; vous devez ensuite effectuer une recherche sur le nom anglais, afin d’obtenir la liste des autres pays où cette profession est réglementée. Si le pays où vous souhaitez vous installer n’apparaît pas, cela peut signifier que la profession n’y est pas réglementée.
- Si vous ne trouvez pas votre profession dans la base de données, vous pouvez vous adresser aux points nationaux d’information sur la reconnaissance des qualifications professionnelles dans le pays où vous souhaitez travailler. Ils pourront vous aider à trouver l’autorité compétente et à déterminer les documents requis.

**Lire aussi :** [la carte professionnelle européenne (CPE)](carte_professionnelle_europeenne.md)