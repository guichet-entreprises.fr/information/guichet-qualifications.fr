﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(translation)="None" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Je suis titulaire d’un diplôme d’un État membre de l’Union européenne ou de l’Espace économique européen..." -->
<!-- var(key)="fr-comprendre-diplome_etat_membre_ue_eee" -->
<!-- var(last-update)="2020-05-03 11:31:44" -->
<!-- var(lang)="fr" -->

# Je suis titulaire d’un diplôme d’un État membre de l’Union européenne ou de l’Espace économique européen...

Dernière mise à jour : <!-- begin-var(last-update) -->2020-05-03 11:31:44<!-- end-var -->


## ...et je souhaite faire reconnaître ma qualification professionnelle en France.

La France comporte près de 250 professions réglementées que l’on peut recenser en deux catégories :

* les professions libérales ;
* les professions artisanales et commerciales pour lesquelles la reconnaissance de qualification professionnelle s’effectue auprès de la chambre des métiers et de l’artisanat dont vous dépendez.

## Je souhaite m’installer en France

Deux étapes sont nécessaires afin de pouvoir exercer une profession réglementée en France :

1. la reconnaissance de qualification professionnelle pour les ressortissants étrangers de l’Union européenne (UE) ou de l’Espace économique européen (EEE) ;
2. l’autorisation de pratiquer auprès de l’autorité compétente de la profession concernée (autorisation complémentaire selon les professions).

Les [fiches sur les professions réglementées](professions_reglementees.md) recensent ces démarches et vous dirigent à travers la réglementation en vigueur.

Notez que les professions reconnues par la procédure de la [carte professionnelle européenne](carte_professionnelle_europeenne.md) (CPE) peuvent ainsi déroger à la procédure classique :

* infirmiers responsables de soins généraux ;
* pharmaciens ;
* kinésithérapeutes ;
* guides de montagne ;
* agents immobiliers.

## Je souhaite délivrer une prestation temporaire en France

Il n’est pas nécessaire de recourir à une reconnaissance de qualification professionnelle pour une prestation temporaire, cependant vous devez être établi dans votre pays d’origine. Dans certains cas, vous devrez au préalable à l’exercice de votre prestation, demander une autorisation.

## Démarches

Le contact avec l’autorité compétente est le plus souvent à prendre dans la région dans laquelle vous décidez de vous installer. Un certain nombre de pièces justificatives sont à fournir dont certaines nécessites parfois une traduction assermentée.

**Bon à savoir**

Si votre profession est réglementée dans votre pays d’origine mais pas dans le pays d’accueil (France), vous pouvez exercer sans avoir à faire de reconnaissance de qualification professionnelle, comme les ressortissants du pays d’accueil.

### Liste des États membres de l’Union européenne

Allemagne, Autriche, Belgique, Bulgarie, Chypre, Croatie, Danemark, Espagne, Estonie, Finlande, France, Grèce, Hongrie, Irlande, Italie, Lettonie, Lituanie, Luxembourg, Malte, Pays-Bas, Pologne, Portugal, Roumanie, Royaume-Uni, Slovaquie, Slovénie, Suède, Tchéquie.

### Liste des États de l’Espace économique européen

Il s’agit des 28 pays membres de l’Union européenne (UE) dont la liste figure ci-dessus et des 3 pays suivants : Norvège, Islande, Liechtenstein.