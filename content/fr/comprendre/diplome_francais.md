﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(translation)="None" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Je suis titulaire d'un diplôme français..." -->
<!-- var(key)="fr-comprendre-diplome_francais" -->
<!-- var(last-update)="2020-05-03 11:31:54" -->
<!-- var(lang)="fr" -->

Je suis titulaire d'un diplôme français...
======================

Dernière mise à jour : <!-- begin-var(last-update) -->2020-05-03 11:31:54<!-- end-var -->


## ...et je souhaite faire reconnaître ma qualification professionnelle dans un État membre de l’Union européenne ou de l’Espace économique européen

Si vous êtes ressortissant d’un État membre de l’Union européenne (UE) ou de l’Espace économique européen (EEE), titulaire d’un diplôme français ou d’un diplôme délivré par un pays tiers et reconnu par la France, vous devez faire reconnaître votre qualification professionnelle avant de commencer à exercer dans un État membre de l’UE ou de l’EEE.

## Je souhaite m’installer dans un pays de l’UE ou de l’EEE où ma profession est réglementée

Avant de commencer à travailler dans un pays, renseignez-vous sur les professions réglementées dudit pays. Si votre profession se trouve être réglementée, il vous faut faire reconnaître votre qualification professionnelle. C’est l’[autorité compétente](https://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=homepage) du pays dans lequel vous vous établissez pour exercer votre activité qui est la référente quant aux démarches à suivre.

Une fois la reconnaissance obtenue, vous pouvez exercer votre profession comme les ressortissants y ayant obtenu leur diplôme.

## Je souhaite délivrer une prestation ponctuelle dans un État membre où ma profession est réglementée

Pour fournir temporairement un service à l’étranger, vous devez être établi dans votre pays d’origine mais sans obligation d’exercer et la reconnaissance des qualifications professionnelles n’est pas nécessaire, seule une déclaration préalable écrite suffit. Selon le degré d’incidence de la profession sur la santé ou la sécurité, le pays membre peut demander à y vérifier votre qualification professionnelle. Un renouvellement de la déclaration peut être nécessaire tous les ans si vous continuez d’exercer temporairement votre activité dans ce pays.

**Bon à savoir**

Si la profession n’est pas soumise à une réglementation dans l’État d’accueil, l’appréciation du diplôme et du niveau professionnel appartient à l’employeur.

## Démarches

Les autorités du pays dans lequel vous sollicitez la reconnaissance de vos qualifications professionnelles disposent d’un mois pour accuser réception de votre demande et vous demander d’autres pièces justificatives.

Une fois le dossier complet reçu, les autorités se prononcent dans un délai :

* de 3 mois si vous êtes médecin, infirmier en soins généraux, sage-femme, vétérinaire, dentiste, pharmacien ou architecte et que vous bénéficiez à ce titre d’une [reconnaissance automatique](https://europa.eu/youreurope/citizens/work/professional-qualifications/european-professional-card-documents/index_fr.htm#103720) ;
* de 4 mois pour toutes les autres professions.

Prévoyez donc 5 mois au cours desquels la reconnaissance de qualification a lieu, préalable indispensable avant d’exercer votre profession en toute légalité dans l’État membre où vous fournissez votre service. Passé ce délai, [contactez le service d’assistance](https://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=yec_pq) ou adressez-vous aux [points nationaux d’information sur la reconnaissance des qualifications professionnelles](https://ec.europa.eu/growth/single-market/services/free-movement-professionals_fr/#contacts).

## Aller plus loin

Liens utiles (site de la Commission européenne) :

* [Base de données des professions réglementées recense les professions règlementées par pays de l’UE et par autorités](https://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=homepage)
* [Liste des sites web nationaux concernant les professions réglementées](https://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=sites)
* Pour authentifier l’autorité nationale compétente, vous pouvez vous adresser à un [point national d’information sur la reconnaissance des qualifications professionnelles](https://ec.europa.eu/growth/single-market/services/free-movement-professionals_fr/#contacts).

## Liste des États membres de l’Union européenne

Allemagne, Autriche, Belgique, Bulgarie, Chypre, Croatie, Danemark, Espagne, Estonie, Finlande, France, Grèce, Hongrie, Irlande, Italie, Lettonie, Lituanie, Luxembourg, Malte, Pays-Bas, Pologne, Portugal, Roumanie, Royaume-Uni, Slovaquie, Slovénie, Suède, Tchéquie.

## Liste des États de l’Espace économique européen

Il s’agit des 28 pays membres de l’Union européenne (UE) dont la liste figure ci-dessus et des 3 pays suivants : Norvège, Islande, Liechtenstein.