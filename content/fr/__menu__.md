﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="[Accueil](index.md)" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="None" -->
<!-- var(key)="fr-__menu__" -->
<!-- var(last-update)="2020-05-03 13:02:33" -->
<!-- var(lang)="fr" -->

# [Accueil](index.md)

# Comprendre
## [](bienvenue.md)
## [](comprendre/pourquoi_faire_reconnaitre_sa_qualification_professionnelle.md)
## [](comprendre/qualification_professionnelle_en_france.md)
## [](comprendre/diplome_etat_membre_ue_eee.md)
## [](comprendre/diplome_francais.md)
## [](comprendre/professions_reglementees.md)
## [](comprendre/carte_professionnelle_europeenne.md)
# Professions reglementées
# <!-- include(../reference/fr/directive-qualification-professionnelle/_list_menu.md) -->
# [Services @toggle.env](https://welcome.guichet-qualifications.fr/)
# [Mes dossiers @visibility.authenticated @toggle.env](https://dashboard.guichet-qualifications.fr/)
