﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(site:package)="https://gitlab.com/|13561843|latest|reference_md|reference/configuration.yml" -->
<!-- var(site:home)="Accueil" -->
<!-- var(site:menu)="__menu__.md" -->
<!-- var(site:footer)="__footer__/__links__.md" -->
<!-- var(site:lang)="fr" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Guichet Qualifications  " -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:font_sizer_off)="off" -->
<!-- var(page:breadcrumb_off)="off" -->
<!-- var(translation)="None" -->
<!-- var(last-update)="2020-05-03 11:31:25" -->
<!-- var(lang)="fr" -->

Guichet Qualifications  <!-- section-banner:surf.jpg --><!-- color:dark -->
===================================================

Une seule adresse pour la reconnaissance de vos qualifications

[Commencer ma demarche  🡺 <!-- link-model:box-trans -->](https://profiler.guichet-qualifications.fr/start/)

Une seule adresse pour la reconnaissance de vos qualifications  <!-- section-information:-120px -->
===================================================

Bienvenue
------------
Vous trouverez sur ce site toute l’information sur la reconnaissance de qualification professionnelle.

[Première visite <!-- link-model:box -->](bienvenue.md)

Découvrir
----------------
Vous êtes titulaire d’un diplôme d’un État membre de l’Union européenne ou de l’Espace économique européen

[Vos possibilités <!-- link-model:box -->](comprendre/qualification_professionnelle_en_france.md)

Démarches en ligne
------------------
Réalisez vos formalités en ligne liées à la reconnaissance de vos qualifications professionnelles en France !

[Commencer <!-- link-model:box -->](https://profiler.guichet-qualifications.fr/start/)

Pourquoi faire reconnaître sa qualification professionnelle ?<!-- section:courses -->
=============================================================

Une profession est dite « réglementée » lorsqu’il est nécessaire de détenir un diplôme spécifique, d’avoir passé des examens particuliers, d’avoir obtenu une certaine autorisation ou d’être enregistré auprès d’un organisme professionnel pour l’exercer.

Faire reconnaître ses qualifications professionnelles est impératif lorsque l’on souhaite travailler dans un autre pays de l’Union européenne dans lequel sa profession est réglementée. Cela revient à faire reconnaître officiellement sa formation et/ou son expérience professionnelle par le pays d’accueil. [Lire la suite…](comprendre/pourquoi_faire_reconnaitre_sa_qualification_professionnelle.md)

Une seule adresse pour la reconnaissance de vos qualifications professionnelles<!-- section:welcome --><!-- color:grey -->
===============================================================================

Le service en ligne guichet-qualifications.fr encourage la mobilité professionnelle des résidents de l’Union européenne et de l’Espace économique européen en offrant une information complète sur l’accès et l’exercice des professions réglementées en France, en vue d’une reconnaissance de qualification professionnelle.

Ce service est l’initiative du ministère de l’Économie et des Finances.

Entreprendre en France et en Europe <!-- section-stories:drapeaux.jpg --><!-- color:dark -->
===================================

Vous êtes ressortissant de l’Union européenne ou de l’Espace économique européen ? Nous encourageons la création d’entreprise et la mobilité professionnelle en France et en Europe. Guichet-qualifications.fr est reconnu comme le guichet unique français de la reconnaissance de vos qualifications professionnelles, membre du réseau Eugo créé par la Commission européenne.

Dès lors que vous avez obtenu la reconnaissance de votre qualification, si vous souhaitez vous installer durablement ou temporairement pour exercer une activité en France, rendez-vous sur : 

[guichet-entreprises.fr](https://guichet-entreprises.fr/)