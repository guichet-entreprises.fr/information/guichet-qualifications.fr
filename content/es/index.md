﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(site:package)="https://gitlab.com/|13561843|latest|reference_md|reference/configuration.yml" -->
<!-- var(site:home)="Accueil" -->
<!-- var(site:menu)="__menu__.md" -->
<!-- var(site:footer)="__footer__/__links__.md" -->
<!-- var(site:lang)="es" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Guichet Qualifications  " -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:font_sizer_off)="off" -->
<!-- var(page:breadcrumb_off)="off" -->
<!-- var(key)="main" -->
<!-- var(translation)="Auto" -->
<!-- var(last-update)="2020-05-03 14:58:59" -->
<!-- var(lang)="es" -->

Calificaciones de taquilla<!-- section-banner:surf.jpg --><!-- color:dark -->
=============================================================================

Una dirección para el reconocimiento de sus cualificaciones

[Comienza mi caminata<!-- link-model:box-trans -->](https://profiler.guichet-qualifications.fr/start/)

Una dirección para el reconocimiento de sus cualificaciones<!-- section-information:-120px -->
==============================================================================================

Bienvenido
----------

Encontrará sin encontrar en este sitio toda la información sobre el reconocimiento de la cualificación profesional.

[Primera visita<!-- link-model:box -->](bienvenido.md)

Descubrir
---------

Usted tiene un título de un Estado miembro de la Unión Europea o del Espacio Económico Europeo

[Tus posibilidades<!-- link-model:box -->](entender/cualificacion_profesional_en_francia.md)

Pasos en línea
--------------

Completa tus trámites online relacionados con el reconocimiento de tus cualificaciones profesionales en Francia!

[Empezar<!-- link-model:box -->](https://profiler.guichet-qualifications.fr/start/)

¿Por qué han reconocido sus cualificaciones profesionales?<!-- section:courses -->
==================================================================================

Una profesión se denomina "regulada" cuando es necesario poseer un diploma específico, haber superado exámenes específicos, haber obtenido una determinada autorización o estar inscrita en un organismo profesional para llevarla a cabo.

La cualificación profesional es imprescindible cuando se desea trabajar en otro país de la Unión Europea en el que se regula la profesión. Esto significa que su formación y/o experiencia profesional sea reconocida oficialmente por el país anfitrión.[Leer más...](entender/por_que_su_cualificacion_profesional_ha_reconocido.md)

Una única dirección para el reconocimiento de sus cualificaciones profesionales<!-- section:welcome --><!-- color:grey -->
==========================================================================================================================

El servicio en línea guichet-qualifications.fr fomenta la movilidad profesional de los residentes de la Unión Europea y del Espacio Económico Europeo proporcionando información completa sobre el acceso y la práctica de las profesiones reguladas en Francia, con vistas al reconocimiento de la cualificación profesional.

Este servicio es iniciativa del Ministerio de Economía y Hacienda.

Hacer negocios en Francia y Europa<!-- section-stories:drapeaux.jpg --><!-- color:dark -->
==========================================================================================

¿Es usted nacional de la Unión Europea o del Espacio Económico Europeo? Fomentamos la creación de empresas y la movilidad profesional en Francia y Europa. Guichet-qualifications.fr es reconocida como la ventanilla única francesa para el reconocimiento de sus cualificaciones profesionales, miembro de la red Eugo creada por la Comisión Europea.

Una vez que haya obtenido el reconocimiento de su cualificación, si desea establecerse de forma permanente o temporal para trabajar en Francia, vaya a:

[guichet-entreprises.fr](https://guichet-entreprises.fr/)

