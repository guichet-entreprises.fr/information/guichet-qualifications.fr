﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt) -->
<!-- include-file(generated.txt) -->

<!-- var(collapsable)="close" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Conseguir que mi cualificación profesional sea reconocida en Francia" -->
<!-- var(key)="fr-comprendre-qualification_professionnelle_en_france" -->
<!-- var(last-update)="2020-07-15 14:48:22" -->
<!-- var(lang)="es" -->
<!-- var(site:lang)="es" -->

Conseguir que mi cualificación profesional sea reconocida en Francia
====================================================================


<!-- begin-include(disclaimer-trans-es) -->
<!-- end-include -->
La última actualización:<!-- begin-var(last-update) -->2020-05-03 11:32:08<!-- end-var -->

¿Es usted nacional de la Unión Europea o del Espacio Económico Europeo y le gustaría que sus cualificaciones profesionales fueran reconocidas en su país de origen?<!-- collapsable:off -->
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

El sitio web de guichet-qualifications.fr fomenta la movilidad profesional de los residentes de la Unión Europea y del Espacio Económico Europeo. Los servicios en línea guichet-entreprises.fr y guichet-qualifications.fr son la ventanilla única reconocida por la Comisión Europea. Le permiten explorar nuevas oportunidades y desarrollar una actividad en territorio francés completando sus formalidades en línea.

En este sitio web, puede hacer un reconocimiento a su cualificación profesional con la autoridad competente:

- Tengo un título de un Estado miembro de la UE o del EEE y me gustaría[para que mi cualificación profesional sea reconocida en Francia](diploma_miembro_estado_ue_eee.md)
- Tengo un diploma francés y me gustaría[para que mi cualificación profesional sea reconocida en un estado miembro de la UE o del EEE](diploma_frances.md)

Crea una actividad en Francia con guichet-entreprises.fr
--------------------------------------------------------

Una vez que haya reconocido su cualificación profesional, tendrá la oportunidad de establecer su negocio en línea en[guichet-entreprises.fr](https://www.guichet-entreprises.fr) ! También encontrará información útil sobre las actividades reguladas en Francia y las condiciones de instalación.

Actividades reguladas<!-- collapsable:close -->
-----------------------------------------------

Descubra qué condiciones debe seguir para llevar a cabo una actividad regulada en Francia.

[Echa un vistazo a las hojas informativas](https://www.guichet-entreprises.fr/es/activites-reglementees/)

Establecimiento gratuito<!-- collapsable:close -->
--------------------------------------------------

Ahora puede establecerse en Francia y llevar a cabo su negocio libremente, y esto, permanentemente.

[Más información sobre el establecimiento gratuito](https://www.guichet-entreprises.fr/es/eugo/libre-etablissement-le/)

Prestación gratuita de servicios<!-- collapsable:close -->
----------------------------------------------------------

Ofrecer servicios temporales u ocasionales en Francia nunca ha sido tan fácil !

[Más información sobre la prestación gratuita de servicios](https://www.guichet-entreprises.fr/es/eugo/libre-prestation-de-services-lps/)

