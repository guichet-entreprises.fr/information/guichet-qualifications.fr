﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Pourquoi faire reconnaître sa qualification professionnelle ?" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(key)="fr-comprendre-pourquoi_faire_reconnaitre_sa_qualification_professionnelle" -->
<!-- var(last-update)="2020-05-03 14:59:40" -->
<!-- var(lang)="es" -->
<!-- var(site:lang)="es" -->

¿Por qué han reconocido sus cualificaciones profesionales?
==========================================================


<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
Actualización más reciente:<!-- begin-var(last-update) -->2020-05-03 14:59:40<!-- end-var -->

Una profesión se denomina "regulada" cuando es necesario poseer un diploma específico, haber superado exámenes específicos, haber obtenido una determinada autorización o estar inscrita en un organismo profesional para llevarla a cabo.

**Obtener la reconocida cualificación profesional** es imprescindible cuando se desea trabajar en otro país de la UE en el que se regula su profesión. Esto significa que su formación y/o experiencia profesional sea reconocida oficialmente por el país anfitrión.

- Si se trata de trasladarse a otro país para ejercer su profesión, es en el caso de la[establecimiento libre](https://www.guichet-entreprises.fr/es/eugo/libre-etablissement-le/) y es esencial hacer el reconocimiento de sus cualificaciones.
- Si se trata de prestar servicios de forma temporal en otro país, el caso es[entrega de servicio gratuita](https://www.guichet-entreprises.fr/es/eugo/libre-prestation-de-services-lps/) y una declaración previa al ejercicio es suficiente. Existe una excepción para las ocupaciones con graves implicaciones sanitarias o de seguridad, ya que el Estado miembro de acogida puede exigir un control previo a la calificación.

Para conocer las normas que se aplican a su situación, póngase en contacto con la autoridad nacional responsable del acceso a su profesión en el país de acogida.

También puede consultar el[Base de datos europea de profesiones reguladas](http://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=homepage) que enumera todas las profesiones reguladas en los países de la UE y sus autoridades competentes.

- Si su profesión está regulada en su país de origen, puede buscarla en su propio idioma y ver la traducción al inglés proporcionada en la descripción; a continuación, hay que buscar el nombre en inglés, con el fin de obtener una lista de los otros países donde esta ocupación está regulada. Si el país en el que desea establecerse no aparece, puede significar que la profesión no está regulada.
- Si no encuentra su profesión en la base de datos, puede ponerse en contacto con los Puntos Nacionales de Información sobre el reconocimiento de cualificaciones profesionales en el país donde desea trabajar. Pueden ayudarle a encontrar la autoridad adecuada y determinar los documentos requeridos.

**Lea también:** [Tarjeta Profesional Europea (CPE)](tarjeta_de_visita_europea.md)

