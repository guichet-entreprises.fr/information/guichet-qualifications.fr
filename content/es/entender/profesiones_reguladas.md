﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Professions réglementées" -->
<!-- var(key)="fr-comprendre-professions_reglementees" -->
<!-- var(last-update)="2020-05-03 14:59:43" -->
<!-- var(lang)="es" -->
<!-- var(site:lang)="es" -->

Profesiones reguladas
=====================


<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
Actualización más reciente:<!-- begin-var(last-update) -->2020-05-03 14:59:43<!-- end-var -->

Arquitecto, enfermero, veterinario, panadero, etc., antes de que se reconozca su cualificación profesional, conozca las condiciones de acceso a una profesión regulada en Francia.

Echa un vistazo a nuestras fichas técnicas, categorizadas por grupos ocupacionales.

Normativa específica en Francia
-------------------------------

En Francia, algunas profesiones requieren una designación profesional, una cualificación profesional o incluso una autorización previa para su práctica, sancionando un cierto nivel de formación o experiencia. Los registros profesionales proporcionan una actualización sobre cómo reconocer las cualificaciones profesionales y la legislación aplicable a estas diferentes profesiones. Hasta la fecha, cerca de 250 actividades están involucradas y son objeto de nuestros registros profesionales.

**¿Sabías eso?** Las tarjetas profesionales se ponen en línea tan pronto como son escritas por la Oficina de Business Box en colaboración con las administraciones pertinentes.

Si la hoja informativa sobre su cualificación profesional no está disponible, vaya a la[Base de datos europea de profesiones reguladas](https://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=homepage).

