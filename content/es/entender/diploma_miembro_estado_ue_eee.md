﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Je suis titulaire d’un diplôme d’un État membre de l’Union européenne ou de l’Espace économique européen..." -->
<!-- var(key)="fr-comprendre-diplome_etat_membre_ue_eee" -->
<!-- var(last-update)="2020-05-03 14:59:20" -->
<!-- var(lang)="es" -->
<!-- var(site:lang)="es" -->

Tengo un título de un Estado miembro de la Unión Europea o del Espacio Económico Europeo...
===========================================================================================


<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
Actualización más reciente:<!-- begin-var(last-update) -->2020-05-03 14:59:20<!-- end-var -->

... y quiero que mi cualificación profesional sea reconocida en Francia.
------------------------------------------------------------------------

Francia tiene casi 250 profesiones reguladas que se pueden identificar en dos categorías:

- Las profesiones liberales;
- profesiones artesanales y comerciales de las que se lleva a cabo el reconocimiento de la cualificación profesional con la Cámara de Comercio y Artesanía de la que depende.

Quiero mudarme a Francia
------------------------

Se necesitan dos pasos para poder ejercer una profesión regulada en Francia:

# Reconocimiento de la cualificación profesional para los extranjeros de la Unión Europea (UE) o del Espacio Económico Europeo (EEE);
# autorización para ejercer con la autoridad competente de la profesión de que se trate (autorización adicional en función de la profesión).

el[fichas informativas sobre las profesiones reguladas](profesiones_reguladas.md) identificar estos pasos y dirigirle a través de las regulaciones existentes.

Tenga en cuenta que las ocupaciones reconocidas por el[Tarjeta profesional europea](tarjeta_de_visita_europea.md) (CPE) podrá, por tanto, desviarse del procedimiento tradicional:

- Enfermeras de atención general;
- Farmacéuticos;
- fisioterapeutas;
- guías de montaña;
- agentes inmobiliarios.

Quiero emitir un beneficio temporal en Francia
----------------------------------------------

No hay necesidad de utilizar un reconocimiento de calificación profesional para un beneficio temporal, sin embargo, debe establecerse en su país de origen. En algunos casos, usted tendrá que solicitar permiso antes de su beneficio.

Pasos
-----

El contacto con la autoridad pertinente suele tomarse en el área en la que decida establecerse. Se proporcionarán varios documentos justificativos, algunos de los cuales a veces requieren una traducción jurada.

**Es bueno saber**

Si su profesión está regulada en su país de origen pero no en el país de acogida (Francia), puede ejercer sin tener que hacer una cualificación profesional, como los nacionales del país de acogida.

### Lista de Estados miembros de la UE

Alemania, Austria, Bélgica, Bulgaria, Chipre, Croacia, Dinamarca, España, Estonia, Finlandia, Francia, Grecia, Hungría, Irlanda, Italia, Letonia, Lituania, Luxemburgo, Malta, Países Bajos, Polonia, Portugal, Rumanía, Reino Unido, Eslovaquia, Eslovenia, Suecia, República Checa.

### Lista de Estados del Espacio Económico Europeo

Estos son los 28 países miembros de la Unión Europea (UE) mencionados anteriormente y los siguientes 3 países: Noruega, Islandia, Liechtenstein.

