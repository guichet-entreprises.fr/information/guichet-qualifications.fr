﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Je suis titulaire d'un diplôme français..." -->
<!-- var(key)="fr-comprendre-diplome_francais" -->
<!-- var(last-update)="2020-05-03 14:59:32" -->
<!-- var(lang)="es" -->
<!-- var(site:lang)="es" -->

Tengo un título en francés...
=============================


<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
Actualización más reciente:<!-- begin-var(last-update) -->2020-05-03 14:59:32<!-- end-var -->

... y me gustaría que se reconociera mi cualificación profesional en un Estado miembro de la Unión Europea o en el Espacio Económico Europeo
--------------------------------------------------------------------------------------------------------------------------------------------

Si usted es nacional de un Estado miembro de la Unión Europea (UE) o del Espacio Económico Europeo (EEE), que posee un título francés o un diploma emitido por un tercer país y reconocido por Francia, debe tener su cualificación profesional reconocida antes de empezar a ejercer en un Estado miembro de la UE o del EEE.

Quiero trasladarme a un país de la UE o del EEE donde mi profesión esté regulada
--------------------------------------------------------------------------------

Antes de empezar a trabajar en un país, infórmese sobre las profesiones reguladas en ese país. Si su profesión está regulada, necesita que se reconozca su cualificación profesional. Es el[autoridad competente](https://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=homepage) el país en el que se está estableciendo para llevar a cabo su actividad, que es la referencia en cuanto a los pasos a seguir.

Una vez que haya obtenido el reconocimiento, puede ejercer su profesión como los nacionales que se han graduado allí.

Quiero ofrecer una prestación única en un Estado miembro en el que mi profesión esté regulada
---------------------------------------------------------------------------------------------

Para prestar un servicio temporal en el extranjero, debe estar establecido en su país de origen, pero sin la obligación de ejercer y el reconocimiento de cualificaciones profesionales no es necesario, sólo una declaración anticipada por escrito es suficiente. Dependiendo del grado de impacto de la profesión en la salud o la seguridad, el país miembro puede solicitar verificar su cualificación profesional. Una renovación de la declaración puede ser requerida anualmente si usted continúa trabajando temporalmente en ese país.

**Es bueno saber**

Si la profesión no está regulada en el estado de acogida, la evaluación del diploma y el nivel profesional pertenece al empleador.

Pasos
-----

Las autoridades del país en el que solicita el reconocimiento de sus cualificaciones profesionales tienen un mes para confirmar su solicitud y solicitar otros documentos justificativos.

Una vez recibido el archivo completo, las autoridades decidirán en un plazo:

- 3 meses si usted es médico, enfermero de cuidados generales, partera, veterinario, dentista, farmacéutico o arquitecto y se beneficia de un[reconocimiento automático](https://europa.eu/youreurope/citizens/work/professional-qualifications/european-professional-card-documents/index_fr.htm#103720) ;
- 4 meses para todas las demás ocupaciones.

Por lo tanto, espere 5 meses durante los cuales se lleve a cabo el reconocimiento de cualificación, un requisito previo esencial antes de ejercer su profesión legalmente en el Estado miembro en el que preste su servicio. Después de ese tiempo,[póngase en contacto con la línea de ayuda](https://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=yec_pq) o hablar con[puntos de información nacionales sobre el reconocimiento de cualificaciones profesionales](https://ec.europa.eu/growth/single-market/services/free-movement-professionals_fr/#contacts).

Ir más lejos
------------

Enlaces útiles (sitio web de la Comisión Europea):

- [La base de datos de ocupaciones reguladas enumera las ocupaciones reguladas por el país de la UE y por las autoridades](https://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=homepage)
- [Lista de sitios web nacionales para profesiones reguladas](https://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=sites)
- Para autenticar a la autoridad nacional pertinente, puede ponerse en contacto con un[Punto de información nacional sobre el reconocimiento de cualificaciones profesionales](https://ec.europa.eu/growth/single-market/services/free-movement-professionals_fr/#contacts).

Lista de Estados miembros de la UE
----------------------------------

Alemania, Austria, Bélgica, Bulgaria, Chipre, Croacia, Dinamarca, España, Estonia, Finlandia, Francia, Grecia, Hungría, Irlanda, Italia, Letonia, Lituania, Luxemburgo, Malta, Países Bajos, Polonia, Portugal, Rumanía, Reino Unido, Eslovaquia, Eslovenia, Suecia, República Checa.

Lista de Estados del Espacio Económico Europeo
----------------------------------------------

Estos son los 28 países miembros de la Unión Europea (UE) mencionados anteriormente y los siguientes 3 países: Noruega, Islandia, Liechtenstein.

