﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="La carte professionnelle européenne (CPE)" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(key)="fr-comprendre-carte_professionnelle_europeenne" -->
<!-- var(last-update)="2020-05-03 14:59:11" -->
<!-- var(lang)="es" -->
<!-- var(site:lang)="es" -->

La tarjeta profesional europea (CPE)
====================================


<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
Actualización más reciente:<!-- begin-var(last-update) -->2020-05-03 14:59:11<!-- end-var -->

Algunas profesiones reguladas se benefician del sistema europeo de tarjetas profesionales. En este artículo encontrarás toda la información sobre su funcionamiento, los trámites de obtención y las profesiones reguladas a las que se refiere.

¿Qué es la tarjeta de visita europea?
-------------------------------------

La Tarjeta Profesional Europea (CEC) es un procedimiento electrónico para el reconocimiento de cualificaciones profesionales entre los Estados miembros de la UE definidos por los artículos 4 bis a 4o en el[Directiva 2005/36/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036). Su objetivo es facilitar el ejercicio de una profesión regulada para los ciudadanos europeos en otro Estado miembro de la UE. Este procedimiento forma parte de la movilidad profesional de los ciudadanos europeos.

###### Es bueno saber

No se trata de una tarjeta física, sino de una prueba electrónica de que las cualificaciones profesionales del ciudadano han sido verificadas y son reconocidas por las autoridades competentes del país de acogida.

¿Quién puede beneficiarse?
--------------------------

Este dispositivo puede ser utilizado por cualquier profesional europeo, así como en la configuración en el país de acogida para practicar allí ([establecimiento libre](https://www.guichet-entreprises.fr/es/eugo/libre-etablissement-le/)), sólo en el contexto de la prestación temporal de servicios ([entrega de servicio gratuita](https://www.guichet-entreprises.fr/es/eugo/libre-prestation-de-services-lps/)).

Hasta la fecha, el CPE está disponible para cinco ocupaciones:

# Agente inmobiliario;
# Guía de montaña;
# Enfermera;
# Fisioterapeuta;
# Farmacéutico.

Estas ocupaciones fueron seleccionadas para beneficiarse del sistema CPE sobre la base de tres criterios:

- movilidad significativa, o potencial de movilidad significativo, en la profesión de que se trate;
- interés suficiente mostrado por las partes con un interés legítimo;
- regulación de la profesión o formación asociada a ella en un número significativo de Estados miembros de la UE.

Se espera que este número de ocupaciones crezca sabiendo que algunas, como ingenieros o enfermeras especializadas, están siendo evaluadas actualmente.

¿Por qué pedirlo?
-----------------

El CPE es un certificado electrónico. Su objetivo es simplificar el reconocimiento de las cualificaciones profesionales e introducir un nivel adicional de eficiencia tanto para los ciudadanos como para las autoridades competentes responsables de su expedición.

Cuando se trata de**entrega de servicio gratuita**, el CPE tiene por objeto sustituir la declaración anterior a la primera prestación en el Estado miembro de acogida, que debe renovarse cada 18 meses.

Cuando se trata de**Establecimiento**, el CPE es la decisión de reconocer las cualificaciones profesionales del Estado miembro de acogida.

###### Es bueno saber

Los profesionales afectados por el CPE pueden elegir entre su dispositivo o los procedimientos tradicionales de preinforme o reconocimiento de cualificaciones profesionales.

###### Para ir más allá

[Tarjeta profesional europea](https://europa.eu/youreurope/citizens/work/professional-qualifications/european-professional-card/index_fr.htm) (Comisión Europea)

