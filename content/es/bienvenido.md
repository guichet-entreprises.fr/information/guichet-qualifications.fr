﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Bienvenue" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(key)="welcome" -->
<!-- var(translation)="Auto" -->
<!-- var(last-update)="2020-05-03 14:58:25" -->
<!-- var(lang)="es" -->
<!-- var(site:lang)="es" -->

Bienvenido
==========


<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
Actualización más reciente:<!-- begin-var(last-update) -->2020-05-03 14:58:25<!-- end-var -->

Encontrará en este sitio toda la información sobre el procedimiento de reconocimiento de la cualificación profesional.<!-- alert-start:info -->
<!-- alert-end:info -->

Primera visita a www.guichet-qualifications.fr
----------------------------------------------

Guichet-qualifications.fr es la ventanilla única para el reconocimiento de las cualificaciones profesionales. Gratuito y accesible para todos, el servicio en línea le permite construir una carpeta sin tener que viajar. Usted encontrará en el sitio web[las fichas técnicas de las profesiones afectadas](entender/profesiones_reguladas.md) reconocimiento de la cualificación.

**¿Sabías eso?** Las tarjetas profesionales se ponen en línea tan pronto como son escritas por la Oficina de Business Box en colaboración con las administraciones pertinentes. Siempre mostramos la fecha de sus últimas actualizaciones.

Reconocimiento de la cualificación profesional: formalidades
------------------------------------------------------------

La cualificación profesional es imprescindible cuando se desea trabajar en otro país de la Unión Europea en el que se regula la profesión. Para conocer las normas que se aplican a su situación, debe ponerse en contacto con la autoridad nacional responsable del acceso a su profesión en el país de acogida. Gracias a la www.guichet-qualifications.fr sitio web, puede completar estos trámites en línea sin tener que viajar.

**Leer también** :[¿Por qué han reconocido sus cualificaciones profesionales?](https://www.guichet-qualifications.fr/es/pourquoi-faire-reconnaitre-sa-qualification-professionnelle/)

Crea una cuenta e identifícate
------------------------------

Para utilizar el servicio en línea, se le invita a[crear un espacio personal](https://account.guichet-qualifications.fr/session/new) en www.guichet-qualifications.fr. Le permitirá crear y administrar su archivo y cambiar su información personal. Será redirigido a[www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/es/) pero sus pasos permanecerán visibles en la pestaña "Mis pasos en guichet-qualifications.fr" en su panel de control.

Con autenticación[FranceConnect](https://franceconnect.gouv.fr/), si ya tiene una cuenta en Impots.gouv.fr, Ameli.fr o en el sitio web de Correos, puede iniciar sesión en www.guichet-qualifications.fr con una de estas tres cuentas.

Complete y valide su archivo en línea
-------------------------------------

El sitio www.guichet-qualifications.fr le permite construir un archivo en línea, adjuntar cualquier documento de apoyo y pagar cualquier costo relacionado con la formalidad. Una vez que el archivo se ha compilado y validado, se envía al cuerpo adecuado para su procesamiento. Para obtener un seguimiento de su archivo, puede ponerse en contacto con la autoridad correspondiente que recibe su archivo en cualquier momento.

**¿Sabías eso?** En Francia, hay 250 profesiones reguladas (en virtud de la Directiva sobre cualificaciones) enumeradas en la[Profesiones reguladas](entender/profesiones_reguladas.md). Si, sin embargo, no encuentra su profesión, vaya a la[Base de datos europea de profesiones reguladas](http://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=sites).

