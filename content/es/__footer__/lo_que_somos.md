﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Qui sommes-nous ?" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-qui_sommes_nous" -->
<!-- var(last-update)="2020-05-03 15:03:47" -->
<!-- var(lang)="es" -->
<!-- var(site:lang)="es" -->

¿Quiénes somos?
===============


<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
Actualización más reciente:<!-- begin-var(last-update) -->2020-05-03 15:03:47<!-- end-var -->

El sitio de www.guichet-qualifications.fr está diseñado y desarrollado por el servicio "Business Box Office" responsable a nivel nacional, creado por el[decreto del 22 de abril de 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&dateTexte=20200109) (JORF de 25 de abril de 2015). Está bajo la autoridad de la[Dirección Corporativa](https://www.entreprises.gouv.fr/) dentro de la[Ministerio de Economía y Finanzas](https://www.economie.gouv.fr/).

El servicio nacional "Business Box Office" gestiona sitios web[www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/) Y[www.guichet-entreprises.fr](https://www.guichet-entreprises.fr/) [1] que en conjunto constituyen la tienda electrónica definida por las directivas europeas[2006/123/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32006L0123) Y[2005/36](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036). Atms de este tipo existen en toda Europa y están federados dentro del proyecto " [Eugo](http://ec.europa.eu/internal_market/eu-go/index_fr.htm) Comisión Europea.

En business Box Office, fomentamos la movilidad profesional de los residentes de la Unión Europea o del Espacio Económico Europeo proporcionando acceso a información completa sobre el acceso y la práctica de las profesiones reguladas en Francia a través de "registros profesionales".

El sitio[www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/) está dirigido a todos los residentes de la Unión Europea o del Espacio Económico Europeo. Les informa sobre las posibilidades de garantizar el reconocimiento de sus cualificaciones profesionales y de que ejercen una profesión regulada en Francia con vistas a un establecimiento sostenible ([establecimiento libre](https://www.guichet-entreprises.fr/es/eugo/libre-etablissement-le/)) o temporal ([entrega de servicio gratuita](https://www.guichet-entreprises.fr/es/eugo/libre-prestation-de-services-lps/)). También permite a los residentes en Francia conocer las posibilidades de acceder y ejercer una profesión regulada en otro Estado miembro.

