﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Conditions générales d’utilisation" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-cgu" -->
<!-- var(last-update)="2020-05-03 15:00:36" -->
<!-- var(lang)="es" -->
<!-- var(site:lang)="es" -->

Términos y condiciones
======================


<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
Actualización más reciente:<!-- begin-var(last-update) -->2020-05-03 15:00:36<!-- end-var -->

Preámbulo
---------

Este documento describe los términos del compromiso con el uso de las cualificaciones de caja de teleservicio para los usuarios. Forma parte del marco jurídico:

- De[Reglamento General de Protección de Datos](https://www.cnil.fr/es/reglement-europeen-protection-donnees) ;
- [Directiva 2005/36/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036) Parlamento Europeo;
- [Directiva 2013/55/UE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32013L0055) Parlamento Europeo;
- De[Reglamento 910/2014](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32014R0910) Parlamento Europeo y el Consejo, de 23 de julio de 2014 (e-IDAS) sobre identificación electrónica y servicios de confianza para las transacciones electrónicas en el mercado interior;
- Dela[Orden del 8 de diciembre de 2005](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000636232) intercambios electrónicos entre los usuarios y las autoridades administrativas y entre las autoridades administrativas y la[Decreto No 2010-112 de 2 de febrero de 2010](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021779444&categorieLien=id) para la aplicación de los artículos 9, 10 y 12 de esta ordenanza;
- De[Ley 78-17, de 6 de enero de 1978, enmendada](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460) ciencias de la computación, archivos y libertades;
- Dela[Artículo 441-1 del Código Penal](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006418753&cidTexte=LEGITEXT000006070719&dateTexte=20020101) ;
- Dela[Artículo R. 123-30 del Código de Comercio](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006256107&dateTexte=&categorieLien=cid) ;
- Dela[decreto del 22 de abril de 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&categorieLien=id) la creación de un servicio nacional competente llamado "ventana de negocios".

El tema del documento
---------------------

El propósito de este documento es definir las condiciones generales de uso de las Cualificaciones de Cajas de Teleservicio, denominadas el "Servicio" a continuación, entre el servicio nacional Guichet Enterprises y los usuarios.

El servicio nacional Guichet Enterprises está bajo la autoridad de la Dirección Corporativa del Ministerio de Economía y Hacienda.

Definición y propósito del Servicio
-----------------------------------

El servicio implementado por el servicio nacional Guichet Enterprises (en adelante, el "Servicio de Caja De Negocio") ayuda a simplificar los procedimientos relacionados con la creación, los cambios de la situación y el cese de una empresa de usuarios franceses y europeos.

Este Servicio ha sido sujeto a la aprobación de seguridad de acuerdo con la Instrucción Interministerial 901/SGDSN/ANSSI de ANSSI (NOR: PRMD1503279 28/01/2015 y la PGSSI de los Ministerios Económicos y Financieros (NOR: FCPP1622039A) de 1 de agosto de 2016.

El uso del Servicio es opcional y gratuito. Sin embargo, como parte de los trámites realizados a través de este Servicio, como la puesta en marcha de negocios, se pueden solicitar pagos en línea. Estos son seguros.

Todos los destinatarios del Servicio se conocen como las agencias asociadas a continuación.

En virtud de la sección R.123-3 del Código de Comercio, los datos recopilados se transmiten para su tratamiento a las siguientes organizaciones asociadas:

"1) Sin perjuicio de lo dispuesto en los años 2 y 3, las cámaras territoriales de comercio e industria crean y gestionan los centros de formalidad de las empresas responsables de:

# Comerciantes;
# Empresas comerciales.

(2) Las cámaras de comercio y artesanía de la región crean y gestionan los centros competentes para las personas físicas y empresas sujetas a registro en el directorio de oficios y para las personas que se benefician de la exención de registro prevista en el artículo 19 de la Ley 96-603, de 5 de julio de 1996, relativa al desarrollo y la promoción del comercio y la artesanía. , excluyendo los mencionados en el 3o de este artículo.

3. La Cámara Nacional de Embarcación Artesanal crea y gestiona el centro para particulares y empresas sujetas a inscripción en el registro de empresas de navegación artesanal.

(4) Los registros comerciales o mercantiles crean y gestionan los centros responsables de:

# Empresas civiles y no comerciales;
# Sociedades liberales de ejercicio;
# personas jurídicas sujetas a inscripción en el registro de comercio y empresas distintas de las mencionadas en los puntos 1, 2 y 3
# Establecimientos públicos industriales y comerciales;
# Agentes comerciales;
# grupos de interés económico y grupos de interés económico europeos.

(5) Los sindicatos de la seguridad social y de la prestación familiar (Urssaf) o los fondos generales de la seguridad social crean y gestionan los centros responsables de:

# personas que se dedican, como ocupación regular, a una actividad regulada o no comercial, artesanal o independiente de la agricultura;
# empresarios cuyas empresas no estén inscritas en el registro de comercios y empresas, en el directorio de comercios o en el registro de empresas de navegación artesanal, y que no informen a los centros mencionados en el día 6.

(6) Las Cámaras de Agricultura crean y gestionan los centros competentes para particulares y corporaciones que realizan actividades agrícolas como[...]. »

Funciones
---------

El Servicio permite al usuario:

- acceso a documentación que detalle las obligaciones de los Centros de Formalidad Empresarial (CTE), así como los elementos básicos del expediente de declaración y del expediente de solicitud;
- crear una cuenta de usuario que proporcione acceso al espacio de almacenamiento personal. Este espacio permite al usuario gestionar y utilizar sus datos personales, conservar la información sobre él y los documentos y documentos justificativos necesarios para la realización de los procedimientos administrativos.

Desde su espacio, el usuario puede:

- construir su expediente único en virtud del artículo R. 123-1 del Código de Comercio, que incluye el expediente de declaración y, en su caso, las solicitudes de autorización;
- presentar su expediente de declaración y, en caso necesario, las solicitudes de autorización al centro de formalidades comerciales pertinente;
- Acceder a la información de seguimiento sobre el procesamiento de su expediente y, en su caso, su expediente de solicitud;
- permitir a las CFE aplicar los tratamientos necesarios para explotar la información recibida de la sociedad a que se encuentra en el último párrafo del artículo R. 123-21 del Código de Comercio: recepción del expediente único en virtud del artículo 2 de la Ley 94-126, de 11 de febrero de 1994, relativa a la iniciativa y las actividades individuales y remitida por la persona jurídica a que se cubierta el artículo R. 123-21 del Código de Comercio; Recibir información de seguimiento sobre el procesamiento de estos archivos proporcionada por las agencias asociadas y las autoridades; transmisión de información de seguimiento desde la tramitación de expedientes únicos a la persona jurídica a que se contempla el artículo R. 121-21 del Código de Comercio.

Cómo registrarse y utilizar el Servicio
---------------------------------------

El acceso al Servicio está abierto a cualquier persona y es gratuito. Es opcional y no es exclusivo de otros canales de acceso para permitir al usuario completar sus trámites.

Para la gestión del acceso al espacio personal del Servicio, el usuario comparte la siguiente información:

- dirección de correo electrónico del usuario.
- La contraseña elegida por el usuario

El uso del Servicio requiere el suministro de datos personales para la creación de la única carpeta.
El tratamiento de datos personales se describe en la política de protección de datos disponible[Aquí](proteccion_de_las_politicas_da.md).

El uso del Servicio requiere una conexión y un navegador de Internet. El navegador debe estar configurado para permitir*Galletas* sesión.

Para garantizar una experiencia de navegación óptima, recomendamos utilizar las siguientes versiones del navegador:

- Firefox versión 45 y versiones anteriores;
- Google Chrome versión 48 y versiones anteriores.

De hecho, es posible que otros navegadores no admitan ciertas características del Servicio.

El servicio está optimizado para una pantalla de 1024-768 píxeles. Se recomienda utilizar la última versión del navegador y actualizarlo regularmente para beneficiarse de los parches de seguridad y el mejor rendimiento.

Condiciones específicas de uso del servicio de firma
----------------------------------------------------

El servicio de firma electrónica es accesible directamente a través del Servicio.

El artículo R. 123-24 del Código de Comercio en virtud del Reglamento 910/2014 del Parlamento Europeo y del Consejo, de 23 de julio de 2014 (e-IDAS), son las referencias aplicables al servicio de firma electrónica del Servicio.

Funciones y compromiso
----------------------

### Compromiso de la taquilla corporativa

# El servicio Enterprise Box Office implementa y opera el Servicio de acuerdo con el marco jurídico existente establecido en el preámbulo.
# Enterprise Box Office se compromete a tomar todas las medidas necesarias para garantizar la seguridad y confidencialidad de la información proporcionada por el usuario.
# La Oficina de Business Box se compromete a garantizar la protección de los datos recopilados en el marco del Servicio, incluida la prevención de su distorsión, dañados o a los que acceden terceros no autorizados, de conformidad con las medidas previstas en la orden de 8 de diciembre de 2005 sobre intercambios electrónicos entre usuarios y autoridades administrativas y entre autoridades administrativas, el Decreto No 2010-112, de 2 de febrero de 2010, relativo a la aplicación de los artículos 9, 10 y 12 de la presente Ordenanza y del Reglamento 2016/679 del Parlamento Europeo y del Consejo, de 27 de abril de 2016, relativo al tratamiento de datos personales.
# La Oficina de Empresa y los organismos asociados garantizan los derechos de acceso, rectificación y oposición de los Usuarios del Servicio en virtud de la Ley 78-17, de 6 de enero de 1978, relativa al uso informático de archivos y libertades y al Reglamento No 2016/679 del Parlamento Europeo y del Consejo, de 27 de abril de 2016, relativo al tratamiento de datos personales.
Este derecho puede ejercerse de varias maneras:
(a) poniéndose en contacto con el Centro de Formalidades Comerciales (CFE) que recibe el expediente de declaración;
b- enviando un correo electrónico al soporte
c- enviando una carta a:

<blockquote><p>Service à compétence nationale Guichet Entreprises</p>
<p>120 rue de Bercy – Télédoc 766</p>
<p>75572 Paris cedex 12</blockquote># La Oficina de La Empresa y las organizaciones asociadas se comprometen a no comercializar la información y los documentos transmitidos por el usuario a través del Servicio, y a no comunicarlos a terceros, fuera de los casos previstos por la ley.
# La Oficina de La Empresa y las organizaciones asociadas se comprometen a no comercializar la información y los documentos transmitidos por el usuario a través del Servicio, y a no comunicarlos a terceros, fuera de los casos previstos por la ley.
# Enterprise Box Office se compromete a garantizar la trazabilidad de todas las acciones llevadas a cabo por todos los usuarios del Servicio, incluidas las de las organizaciones asociadas y el usuario.
# El servicio Enterprise Box Office proporciona a los usuarios compatibilidad en caso de un incidente o una alerta de seguridad definida.

Participación de los usuarios
-----------------------------

# El usuario rellena su archivo en línea y lo valida posiblemente adjuntando las partes necesarias para el tratamiento del archivo.
# Al final de la preparación del fichero, se muestra un resumen de la información proporcionada por el usuario en la pantalla para que el usuario pueda verificarlos y confirmarlos. Después de la confirmación, el archivo se reenvía a las agencias asociadas. La confirmación y transmisión del formulario por parte del usuario vale la pena firmarlo.
# Estos términos y condiciones son requeridos por cualquier usuario que sea usuario del Servicio.

Disponibilidad y evolución del Servicio
---------------------------------------

El servicio está disponible los 7 días de la semana, las 24 horas del día.

Sin embargo, Enterprise Box Office se reserva la posibilidad de evolucionar, modificar o suspender el Servicio sin previo aviso por mantenimiento u otras razones que se consideren necesarias. La indisponibilidad del Servicio no le da derecho a ninguna compensación. En caso de que el Servicio no esté disponible, se publica una página de información al usuario que menciona esta falta de disponibilidad; luego es invitado a dar su paso en una fecha posterior.

Los términos de estos términos de uso pueden ser modificados en cualquier momento, sin previo aviso, dependiendo de los cambios en el Servicio, cambios en la legislación o regulaciones, o por cualquier otra razón que se considere necesario.

Responsabilidades
-----------------

# La responsabilidad por el servicio de Enterprise Box Office no puede incurrir en caso de robo de identidad o cualquier uso fraudulento del Servicio.
# Los datos transmitidos a los servicios en línea de las agencias asociadas siguen siendo responsabilidad del usuario, incluso si se transmiten por los medios técnicos puestos a disposición en el Servicio.
El usuario puede modificarlos o eliminarlos de las agencias asociadas en cualquier momento.
Puede optar por eliminar toda la información de su cuenta eliminando sus datos del Servicio.
# Se recuerda al usuario que toda persona que haga una declaración falsa por sí mismo o por parte de los demás es responsable, en particular, de las sanciones previstas en el artículo 441-1 del Código Penal, que prevé sanciones de hasta tres años de prisión y una multa de 45.000 euros.

