﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Mentions légales" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-mentions_legales" -->
<!-- var(last-update)="2020-05-03 15:00:54" -->
<!-- var(lang)="es" -->
<!-- var(site:lang)="es" -->

Legal
=====


<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
Actualización más reciente:<!-- begin-var(last-update) -->2020-05-03 15:00:54<!-- end-var -->

Identificación del editor
-------------------------

```
Service à compétence nationale Guichet Entreprises
120 rue de Bercy – Télédoc 766
75572 Paris cedex 12


```
La Oficina de Negocios, creada por el[decreto del 22 de abril de 2015](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=A1B17F7C2CD76682B8F5E3E5CE690458.tpdila13v_1?cidTexte=JORFTEXT000030517635&dateTexte=&oldAction=rechJO&categorieLien=id&idJO=JORFCONT000030517419), está bajo la autoridad de la[Dirección Corporativa](https://www.entreprises.gouv.fr/) (DGE) dentro del[Ministerio de Economía y Finanzas](https://www.economie.gouv.fr/).

Diseño editorial, monitoreo, mantenimiento técnico y actualizaciones del sitio web[www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/) son proporcionados por la Oficina de Business Box.

Editor
------

Florent Tournois, Jefe de la Taquilla de Negocios.

Proveedor de alojamiento
------------------------

```
Cloud Temple
215 avenue Georges Clémenceau
92024 Nanterre
Tel : +33 1 41 91 77 77


```
Objeto del sitio
----------------

El sitio www.guichet-qualifications.fr permite a cualquier residente de la Unión Europea o del Espacio Económico Europeo tener sus cualificaciones profesionales reconocidas y ejercer una profesión regulada en Francia con vistas a un establecimiento sostenible ([establecimiento libre](https://www.guichet-entreprises.fr/es/eugo/libre-etablissement-le/)) o temporal ([entrega de servicio gratuita](https://www.guichet-entreprises.fr/es/eugo/libre-prestation-de-services-lps/)). También permite a los residentes en Francia conocer las posibilidades de acceder y ejercer una profesión regulada en otro Estado miembro.

Los trámites son objeto de una firma electrónica, cuyos términos y condiciones están disponibles en el sitio web en el siguiente enlace:[Política de firma electrónica](politica_de_firma_electronica.md).

Además, el sitio de www.guichet-qualifications.fr proporciona a los contratistas un conjunto de información sobre las formalidades, procedimientos y requisitos de las actividades reguladas de conformidad con el artículo R. 123-2 del Código de Comercio.

Procesamiento de datos personales
---------------------------------

Los datos personales contenidos en el único fichero de conformidad con el Reglamento General de Protección de Datos están sujetos a las disposiciones disponibles en este sitio en el siguiente enlace:[Política de protección de datos](proteccion_de_las_politicas_da.md).

En virtud de este reglamento y de la Ley de Computación y Libertades de 6 de enero de 1978, usted tiene derecho a acceder, corregir, modificar y eliminar los datos que le conciernen.

Hay varias maneras de ejercer este derecho:

- Al ponerse en contacto con el Centro de Formalidades Comerciales (CFE) que recibe el expediente de declaración;
- Envío de un correo electrónico al soporte
- enviando una carta a:


```
  Service à compétence nationale Guichet Entreprises
  120 rue de Bercy – Télédoc 766
  75572 Paris cedex 12
```

Derechos reproductivos
----------------------

El contenido de este sitio está comprendido en la legislación francesa e internacional en materia de derechos de autor y propiedad intelectual.

Todos los elementos gráficos del sitio son propiedad del servicio Business Box Office. Queda terminantemente prohibida cualquier reproducción o adaptación de las páginas del sitio que tome los elementos gráficos.

Cualquier uso de contenido con fines comerciales también está prohibido.

Cualquier cita o reimpresión de contenido del sitio debe haber obtenido el permiso del editor. Deberá indicarse la fuente (www.guichet-qualifications.fr) y la fecha de la copia, así como el servicio "Business Box".

Enlaces a las páginas del sitio
-------------------------------

Cualquier sitio público o privado puede enlazar a las páginas del sitio de www.guichet-qualifications.fr. No es necesario solicitar autorización previa. Sin embargo, el origen de la información deberá aclararse, por ejemplo en forma de: "Reconocimiento de Cualificación Profesional (fuente: www.guichet-qualifications.fr, un sitio de taquilla empresarial)". Las páginas del sitio www.guichet-qualifications.fr no deben anidarse dentro de las páginas de otro sitio. Tendrán que mostrarse en una nueva ventana o pestaña.

Enlaces a páginas de sitios al aire libre
-----------------------------------------

Los enlaces del sitio www.guichet-qualifications.fr pueden dirigir al usuario a sitios externos cuyo contenido no puede de ninguna manera asumir la responsabilidad de la oficina de Enterprise Box.

Entorno técnico
---------------

Algunos navegadores pueden bloquear ventanas en este sitio de forma predeterminada. Con el fin de permitirle ver ciertas páginas, debe permitir que las ventanas se abran cuando el navegador le ofrece haciendo clic en la diadema de advertencia y luego se muestra en la parte superior de la página.

Si su navegador no tiene un mensaje de advertencia, debe configurarlo para permitir que se abran ventanas para el sitio www.guichet-qualifications.fr.

