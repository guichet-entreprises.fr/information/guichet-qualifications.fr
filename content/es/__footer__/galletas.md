﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Utilisation des cookies sur guichet-qualifications.fr" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-cookies" -->
<!-- var(last-update)="2020-05-03 15:00:41" -->
<!-- var(lang)="es" -->
<!-- var(site:lang)="es" -->

Uso de cookies en guichet-qualifications.fr
===========================================


<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
Actualización más reciente:<!-- begin-var(last-update) -->2020-05-03 15:00:41<!-- end-var -->

¿Qué son las cookies y cómo utiliza Business Box Office?
--------------------------------------------------------

Box Office Puede utilizar cookies cuando un usuario navega por su sitio. Las cookies son archivos enviados al navegador a través de un servidor web para registrar las actividades del usuario durante su tiempo de navegación. El uso de cookies permite reconocer el navegador web utilizado por el usuario con el fin de facilitar su navegación. Las cookies también se utilizan para medir la audiencia del sitio y producir estadísticas de consulta.

Las cookies utilizadas por Enterprise Box Office no proporcionan referencias para deducir los datos personales o la información personal de los usuarios para identificar a un usuario en particular. Son de carácter temporal, con el único propósito de hacer más eficiente la transmisión posterior. Ninguna cookie utilizada en el sitio tendrá un período de vigor de más de dos años.

Los usuarios tienen la opción de configurar su navegador para ser notificado de la recepción de cookies y para rechazar la instalación. Al prohibir las cookies o deshabilitarlas, es posible que el usuario no pueda acceder a determinadas funciones del sitio.

En caso de una denegación o desactivación de cookies, la sesión debe reiniciarse.

¿Qué tipos de cookies utiliza guichet-qualifications.fr?
--------------------------------------------------------

### Cookies técnicas

Permiten al usuario navegar por el sitio y utilizar algunas de sus características.

### Cookies de análisis

Box Office utiliza cookies de análisis de audiencia para cuantificar el número de visitantes. Estas cookies se utilizan para medir y analizar cómo los usuarios navegan por el sitio.

