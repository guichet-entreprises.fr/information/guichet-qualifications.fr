﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Politique de signature électronique" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-politique_signature_electronique" -->
<!-- var(last-update)="2020-05-03 15:03:32" -->
<!-- var(lang)="es" -->
<!-- var(site:lang)="es" -->

Política de firma electrónica
=============================


<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
Actualización más reciente:<!-- begin-var(last-update) -->2020-05-03 15:03:32<!-- end-var -->

| Versión | Fecha      | Objeto          |
|---------|------------|-----------------|
| V1      | 27/09/2018 | Versión inicial |

El tema del documento
---------------------

La firma electrónica adherido a un conjunto de datos garantiza la integridad de los datos transmitidos, la no repudio de los datos firmados y la autenticidad de su transmisor.

Esta política de firma electrónica es un documento que describe los requisitos de admisibilidad de los organismos receptores, es decir, los organismos pertinentes, de un expediente en el que se coloca una firma electrónica en relación con los intercambios electrónicos cubiertos por el artículo R. 123-30-10 del Código de Comercio.

Los organismos pertinentes son los servicios responsables de escuchar las solicitudes de reconocimiento de cualificaciones profesionales.

En esta sección,*"Cuando se requiera una firma, se permitirá el uso de una firma electrónica con las garantías establecidas en el artículo 1367, párrafo segundo, primera frase, del Código Civil"*, y el artículo 1367, párrafo segundo, del Código Civil,*"Cuando es electrónico, consiste en el uso de un proceso de identificación fiable que garantiza su conexión con el acto al que se acopla. La fiabilidad de este proceso se presume, hasta que se demuestre lo contrario, cuando se crea la firma electrónica, la identidad del firmante asegurado y la integridad del acto garantizado, en las condiciones establecidas por decreto en el Consejo de Estado."*,

La normativa aplicable para la aplicación de la firma electrónica de las cualificaciones de las cajas de teleservicio se deriva de los siguientes textos:

- Reglamento 910/2014 del Parlamento Europeo y del Consejo, de 23 de julio de 2014 (e-IDAS) relativo a la identificación electrónica y a los servicios de confianza para las transacciones electrónicas en el mercado interior;
- Código civil;
- Decreto No 2017-1416 de 28 de septiembre de 2017 relativo a la firma electrónica.

En el caso de que la firma electrónica requiera el suministro de datos personales, la normativa aplicable se deriva de los siguientes textos:

- Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo, de 27 de abril de 2016, reglamento relativo a la protección de las personas físicas en lo que respecta al tratamiento de datos personales y a la libre circulación de estos datos, y por el que se deroga la Directiva 95/46/CE (Reglamento General de Protección de Datos);
- Ley 78-17, de 6 de enero de 1978, relativa a ordenadores, archivos y libertades.

Este documento, "Política de Firma Electrónica de Cualificaciones de Cajas de Teleservicio", describe todas las reglas y disposiciones que definen los requisitos a los que cada uno de los actores involucrados en estos intercambios desmaterializados cumple con la transmisión y recepción de corrientes.

Este documento está destinado a:

- Los órganos pertinentes;
- posibles proveedores que participan en estos intercambios desmaterializados en nombre de estas agencias receptoras.

En el resto de este documento:

- Las organizaciones receptoras antes mencionadas se conocen como "receptores";
- los intercambios calentados anteriormente se denominan "registros";
- el servicio nacional Business Bank se conoce como el "servicio de Business Box Office".

Alcance
-------

La firma electrónica es necesaria para cualquier formalidad relacionada con una solicitud de reconocimiento de la cualificación profesional en el sentido de la Directiva 2005/36/CE del Parlamento Europeo y del Consejo, de 7 de septiembre de 2005, relativa al reconocimiento de cualificaciones profesionales ("Directiva sobre cualificaciones profesionales") y su ejercicio, contemplado en el artículo R. 123-30-8 del Código de Comercio.

El artículo 9 de la Ordenanza No 2016-1809, de 22 de diciembre de 2016, prevé a los afectados por estas formalidades,

*Todo nacional de un Estado miembro de la Unión Europea o parte en el acuerdo sobre el Espacio Económico Europeo que haya adquirido sus cualificaciones profesionales en un Estado miembro de la Unión Europea o parte en el acuerdo sobre el Espacio Económico Europeo, y deseando obtener su reconocimiento para el ejercicio de una profesión regulada en el sentido de la Directiva 2005/36/CE del Parlamento Europeo y del Consejo, puede completar o supervisar de forma remota y electrónica, a través de una sola ventana, todos los requisitos, procedimientos o formalidades relativos al reconocimiento de las cualificaciones profesionales en las condiciones determinadas por decreto en el Consejo de Estado.*

La firma electrónica tiene diferentes características dependiendo de la naturaleza de los expedientes presentados.

**En caso de que se apliquen las disposiciones legales establecidas en el artículo R. 123-30-10 del Código de Comercio**, la firma electrónica requerida cumple las disposiciones del artículo 1316-4, párrafo segundo, primera frase, del Código Civil:

*Cuando es electrónico, consiste en el uso de un proceso de identificación fiable que garantiza su conexión con el acto al que se adhiere.*

**En caso de que se apliquen disposiciones jurídicas más estrictas que las establecidas en el artículo R. 123-30-10 del Código de Comercio** (por ejemplo, el reconocimiento de la cualificación profesional para la actividad de un médico general), la firma electrónica cumple con las disposiciones del artículo 1 del Decreto No 2017-1416, de 28 de septiembre de 2017 (referencia al Reglamento "eIDAS" no 910/2014):

*La fiabilidad de un proceso de firma electrónica se presume, hasta que se demuestre lo contrario, cuando este proceso implementa una firma electrónica calificada.*

*Una firma electrónica es una firma electrónica avanzada, de conformidad con el artículo 26 del Reglamento anterior y creada mediante un dispositivo cualificado de creación de firma electrónica que cumple los requisitos del artículo 29 de dicho Reglamento, que se basa en un certificado calificado como firma electrónica que cumple los requisitos del artículo 28 de dicho Reglamento.*

Artículo 26 del Reglamento "eIDAS" 910/2014:

*Una firma electrónica avanzada cumple con los siguientes requisitos:*

# *Estar vinculados al firmante de una manera inequívoca;*
# *Permitir que se identifique al firmante;*
# *Haber sido creado utilizando datos de creación de firma electrónica que el signatario puede utilizar bajo su control exclusivo con un alto nivel de confianza;*
# *Estar vinculado a los datos asociados a esta firma para que se detecten los cambios posteriores en los datos.*

A para obtener información, el Reglamento "eIDAS" 910/2014 proporciona tres niveles de garantía:

- *Bajo* En este nivel, el objetivo es simplemente reducir el riesgo de uso indebido o manipulación de identidad;
- *Substancial* A este nivel, el objetivo es reducir sustancialmente el riesgo de uso indebido o manipulación de identidad;
- *Alto* A este nivel, el objetivo es evitar el uso indebido o la alteración de la identidad.

### Identificación

La identificación de esta política de firma electrónica se indica con la presencia del certificado de Servicio de Caja de la Empresa utilizado para la firma electrónica del documento.

El número de serie del certificado es:

4B-51-5A-35-00-00-00-00-01-EF.

### Publicación del documento

Esta política de firma electrónica se publica tras su aprobación por el Administrador de Seguridad de Sistemas de Información (RSSI) de Enterprise Box Office.

### Proceso de actualización

#### Circunstancias que hacen necesaria una actualización

La actualización de esta política de firma electrónica puede incluir la evolución de la ley en vigor (véase "Asunto del documento"), la aparición de nuevas amenazas y nuevas medidas de seguridad, y el examen de las observaciones de los diversos actores.

Esta política de firma electrónica se revisa al menos cada dos años.

#### Teniendo en cuenta las observaciones

Todos los comentarios, o deseos de cambio, sobre esta política de firma electrónica deben enviarse por correo electrónico a:[Contacto](mailto:contact.guichet-entreprises@finances.gouv.fr).

Estos comentarios y deseos de desarrollo son revisados por el RSSI del servicio Business Box Office, que, en caso necesario, inicia el proceso de actualización de esta política de firma electrónica.

#### Información de los actores

La información sobre la versión actual de esta directiva y las versiones anteriores está disponible en la tabla de versiones en la parte superior de este documento.

La publicación de una nueva versión de la política de firmas consta de:

# Poner la política de firma electrónica en línea en formato HTML
# archivar la versión anterior después de que la etiqueta "obsoleta" se fija a cada página.

Entra en vigor una nueva versión y un período de validez
--------------------------------------------------------

Una nueva versión de la directiva de firma solo entra en vigor un (1) mes calendario después de que se publique en línea y siga siendo válida hasta que entre en vigor una nueva versión.

Los destinatarios utilizan el retraso de un mes para tener en cuenta en sus solicitudes los cambios realizados por la nueva política de firma electrónica.

Actores
-------

### El firmante del expediente

El firmante del expediente es el solicitante de registro o su agente que hace la formalidad de solicitar el reconocimiento de la cualificación profesional

#### El papel del signatario

La función del firmante es poner su firma electrónica en su expediente incorporando la formalidad y los documentos justificativos,

Para poner una firma electrónica en su expediente, el firmante se compromete a utilizar una herramienta de firma de conformidad con esta política de firma electrónica.

#### Obligaciones del firmante

Estas obligaciones se describen en los capítulos siguientes.

### Herramienta de firma utilizada

El firmante debe comprobar los datos que firmará antes de firmar su firma electrónica.

### Procedimiento de firma electrónica utilizado

El procedimiento de firma electrónica depende del tipo de formalidad (véase "Campo de solicitud").

**En caso de que se apliquen las disposiciones legales establecidas en el artículo R. 123-30-10 del Código de Comercio**, el firmante deberá marcar una casilla al final de la formalidad indicando que declara en honor la exactitud de la información de la formalidad y firma esta declaración.

A continuación, se menciona en el espacio de firma de la forma (Cerfa), de la conformidad de esta firma con las expectativas del párrafo 3 del artículo A. 123-4 del Código de Comercio.

Párrafo 3 del artículo A. 123-4 del Código de Comercio:

*(3) Al marcar la casilla de ordenador prevista a tal efecto, el declarante declara en honor la exactitud de los artículos declarados de acuerdo con la siguiente fórmula: "Declaro en honor la exactitud de la información de la formalidad y firmo esta declaración no...., hecha a..., el.... ».*

**En caso de que se apliquen disposiciones jurídicas más estrictas que las establecidas en el artículo R. 123-30-10 del Código de Comercio**, el signatario deberá adjuntar una copia de su copia declarada de conformidad con el original de conformidad con lo dispuesto en el párrafo 2 del artículo A. 123-4 del Código de Comercio:

Párrafo 2 del artículo A. 123-4 del Código de Comercio:

*(2) Los documentos que lo componen han sido digitalizados. La copia de la prueba de identidad se escanea después de que se haya proporcionado previamente un certificado manuscrita de prueba del honor de cumplimiento del original, una fecha y la firma manuscrita de la persona que realiza la declaración.*

El documento que se firma electrónicamente (formulario Cerfa y copia del documento de identidad) se le presenta, así como el acuerdo de prueba que enumera las condiciones y consecuencias de la firma electrónica del documento.

El firmante debe marcar una casilla que indique que ha leído el acuerdo de prueba y lo acepta sin reservas.

Se le envía un código para validar la firma electrónica en su teléfono móvil, cuyo número indicó cuando creó su cuenta.

La introducción de este código desencadena el sellado del documento (formulario/Cerfa y copia del ID), es decir, su firma electrónica basada en un certificado, su hash y la marca de tiempo del conjunto.

El documento sellado se archiva y se reenvía a los destinatarios interesados.

Los casos de error más comunes son:

**Caso 1 - Código incorrecto**

Caso funcional:

*El usuario ha introducido un código incorrecto (por ejemplo, letras en lugar de números).*

Ejemplo de un mensaje mostrado:

*El código que ha proporcionado es incorrecto. Compruebe el código que se le ha enviado por SMS y vuelva a introducirlo.*

**Caso 2 - Código caducado**

Caso funcional:

*El usuario ingresó el código recibido por SMS demasiado tarde (la vida de un código OTP - One Time Password es de 20 minutos).*

Ejemplo de un mensaje mostrado:

*La validez del código ha expirado. Debe renovar su solicitud de firma electrónica.*

**Caso 3 - Código bloqueado**

Caso funcional:

*El usuario hizo N incautaciones sin éxito de códigos OTP. Con el fin de evitar intentos de piratería, el número de teléfono del usuario se identifica como bloqueado en el sistema.*

Ejemplo de un mensaje mostrado:

*Debido a numerosos errores de escritura en su parte, su número de teléfono ya no está permitido firmar su procedimiento electrónicamente. Le invitamos a proporcionar un nuevo número de teléfono en su cuenta de usuario o a ponerse en contacto con el Business Box para desbloquear su número de teléfono actual.*

### Protección y uso del archivo firmado

Este capítulo se refiere únicamente a los expedientes relativos a las formalidades para las que se aplican disposiciones jurídicas más estrictas que las establecidas en el artículo R. 123-30-10 del Código de Comercio.

El documento firmado electrónicamente se almacena en una caja fuerte electrónica con un archivo legible con todos los rastros de su sellado para fines de auditoría.

La caja fuerte electrónica solo es accesible por su administrador y el RSSI del servicio Business Box.

El acceso a la caja fuerte electrónica sólo es posible con la ayuda de un certificado de nombre y cualquier acción en él se rastrea.

Para el RSSI del servicio Enterprise Box Office, solo se permite el acceso para fines de auditoría a petición justificada (por ejemplo, solicitud judicial).

El documento firmado se envía a las agencias receptoras encargadas de investigar el archivo asociado.

La protección del fichero transmitido electrónicamente es responsabilidad del destinatario y debe cumplir con las disposiciones del Reglamento General de Seguridad y del Reglamento General de Protección de Datos.

El RSSI del servicio Business Box Office garantiza el cumplimiento de estas disposiciones.

### Archivo y eliminación del archivo firmado

El expediente se archiva y suprime de conformidad con las disposiciones de la Sección R. 123-30-12 del Código de Comercio.

Artículo R. 123-30-12 del Código de Comercio:

*Si el solicitante de registro utiliza un servicio provisional de conservación de datos propuesto por el servicio de notificación en condiciones compatibles con la Ley 78-17, de 6 de enero de 1978, relativa a los ordenadores, archivos y libertades, todos los datos y archivos relativos al solicitante de registro en los soportes informáticos en los que se enumeran se suprimen al final del período de retención provisional de hasta doce meses.*

Proveedores de soluciones de firma electrónica
----------------------------------------------

La solución debe incorporar las expectativas de esta política de firma electrónica en la estructura de los datos de firma.

La taquilla de negocios
-----------------------

El RSSI del servicio Business Box Office garantiza el cumplimiento de las disposiciones de esta política de firma electrónica para las cualificaciones de Teleservice Box puestos a disposición de los usuarios.

Comprueba que los sistemas de información de origen y destinatario de los feeds no suponen un riesgo para la integridad y confidencialidad de los archivos almacenados y transmitidos.

Destinatarios
-------------

Los destinatarios supervisan la integridad de la firma electrónica del archivo y se aseguran de que los procedimientos para almacenar y eliminar los archivos recibidos estén de acuerdo con la normativa.

### Datos de verificación

Para llevar a cabo las auditorías, Enterprise Box Office utiliza la información de sellado del archivo firmado electrónicamente (consulte "Protección y uso del archivo firmado").

### Medios de protección

Enterprise Box Office garantiza que se implementan los medios necesarios para proteger el equipo que proporciona los servicios de validación.

Las medidas adoptadas se refieren tanto a:

- proteger el acceso físico y lógico al equipo únicamente a personas autorizadas;
- Disponibilidad del servicio
- supervisión y supervisión del servicio.

### Asistencia a los signatarios

La asistencia con el uso del procedimiento de firma electrónica es proporcionada por el apoyo del servicio Business Box accesible por correo electrónico en:[Apoyo](mailto:support.guichet-entreprises@helpline.fr).

Firma y validación electrónica
------------------------------

### Datos firmados

Los datos firmados consisten en el formulario/Cerfa y la copia del ID en forma de un único archivo PDF con un área de firma electrónica modificable.

Todo el archivo PDF está firmado y de hecho no hay modificaciones en él son posibles después de que se ha sellado.

### Características de la firma

La firma cumple con las expectativas de la especificación "XML Signature Syntax and Processing (XMLDsig)" desarrollada por W3C (World Wide Web Consortium:<https://www.w3.org/>) así como las extensiones de formato de firma especificadas en el estándar europeo de firma electrónica avanzada XML (XADES) de ETSI (<https://www.etsi.org>).

La firma electrónica desplegada implementa un nivel "sustancial" de identificación (ISO/IEC 29115:2013), es decir, de acuerdo con el reglamento eIDAS una firma electrónica avanzada

#### Tipo de firma

La firma electrónica es del tipo[Envuelto](https://fr.wikipedia.org/wiki/XML_Signature).

#### Estándar de firma

La firma electrónica debe cumplir con el estándar[XMLDsig, revisión 1.1 de
Febrero de 2002](https://www.w3.org/TR/2002/REC-xmldsig-core-20020212).

La firma electrónica debe cumplir con el estándar XAdES-EPES (Explicit Policy based Electronic Signature),[ETSI TS 101 903 v1.3.2](http://uri.etsi.org/01903/v1.3.2).

De acuerdo con el estándar XadES, SignedProperties (SignedProperties/SignedSignatureProperties) debe contener lo siguiente:

- SigningCertificate
- fecha y hora de firma (SigningTime) en formato UTC.

Algoritmos que se pueden utilizar para la firma
-----------------------------------------------

#### Algoritmo de condensación

El cálculo de la iteración condensada de la función de compresión en el resto de los bloques obtenidos cortando el mensaje (esquema iterativo de Merkle-Damgord).

Este algoritmo es accesible en el link:[Merkle-Damgord](https://fr.wikipedia.org/wiki/Construction_de_Merkle-Damgård)

#### Algoritmo de firma

El algoritmo de firma se basa en RSA/[SHA 256](https://fr.wikipedia.org/wiki/SHA-2).

#### Algoritmo de canonización

El algoritmo de canonicalización es[c14N](https://www.w3.org/TR/xml-exc-c14n/).

### Condiciones para declarar válido el archivo firmado

Las condiciones para declarar válido el archivo firmado son:

- El firmante ha cumplido con sus obligaciones:- descargando una copia de su Documento de Identidad con una copia de acuerdo con el original y aceptando el acuerdo de prueba,
  + introducir el código enviado por el método SMS ("Contraseña de una sola vez"),;
- La Oficina de La Empresa ha cumplido con sus obligaciones:- poniendo a disposición un certificado generado por una AC que certifica su origen (proveedor del servicio de firma electrónica) y cuyas características son:*Clave pública - RSA 2048 bits, algoritmo de firma - RSA SHA 256* ;
- El proveedor de servicios de firma electrónica ha cumplido con sus obligaciones:- firma y sellado del archivo PDF de acuerdo con las características descritas en los capítulos anteriores.

La verificación de la validez de la firma se puede realizar a través de Adobe Acrobat.

Disposiciones legales
---------------------

### Datos nominales

El transportista tiene derecho a acceder, modificar, corregir y eliminar datos sobre él que pueda ejercer por correo electrónico a la siguiente dirección:[Contacto](mailto:contact.guichet-entreprises@finances.gouv.fr)

### Ley aplicable - Resolución de disputas

Estas disposiciones están sujetas a la legislación francesa.

Cualquier disputa sobre la validez, interpretación o ejecución de estas disposiciones será remitida a la jurisdicción del Tribunal Administrativo de París.

