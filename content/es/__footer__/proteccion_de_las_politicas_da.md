﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Politique de protection des données personnelles" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-politique_protection_donnees" -->
<!-- var(last-update)="2020-05-03 15:01:39" -->
<!-- var(lang)="es" -->
<!-- var(site:lang)="es" -->

Política de protección de datos personales
==========================================


<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
Actualización más reciente:<!-- begin-var(last-update) -->2020-05-03 15:01:39<!-- end-var -->

| Versión | Fecha      | Objeto                                                           |
|---------|------------|------------------------------------------------------------------|
| V1      | 25/05/2018 | Versión inicial                                                  |
| V2      | 01/09/2018 | Información actualizada sobre el delegado de protección de datos |

La política de privacidad (la "política") le informa sobre cómo el servicio de National Business Bank recopila y procesa sus datos, las medidas de seguridad implementadas para garantizar su integridad y confidencialidad, y los derechos que tiene para controlar su uso.

Esta política complementa la[términos y condiciones de uso](terminos.md) (CGU), así como, si es necesario,[Legal](menciones_legales.md). Como tal, esta política se considera incorporada a esos documentos.

Como parte del desarrollo de nuestros servicios y la implementación de nuevas normas regulatorias, es posible que tengamos que cambiar esta política. Te invitamos a leerlo regularmente.

¿Quiénes somos?
---------------

La Oficina de Negocios, creada por el[decreto del 22 de abril de 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&categorieLien=id) está bajo la autoridad de la[Dirección Corporativa](https://www.entreprises.gouv.fr/) (DGE) dentro del[Ministerio de Economía y Finanzas](https://www.economie.gouv.fr/).

El servicio Business Box Office implementa un servicio de teleservicio guichet-qualifcations.fr (en adelante, las "Cualificaciones profesionales de Teleservice Box Office").

Las cualificaciones profesionales de Teleservice Box Office están dirigidas a todos los residentes de la Unión Europea o del Espacio Económico Europeo. Les informa sobre las posibilidades de dejar reconocidas sus cualificaciones profesionales y ejercer una profesión regulada en Francia con vistas a una sintenciones sostenibles (establecimientos libres) o temporales (prestación gratuita de servicios).

También permite a los residentes en Francia conocer las posibilidades de acceder y ejercer una profesión regulada en otro Estado miembro.

Las cualificaciones profesionales de Teleservice Box Office forman parte del marco jurídico:

- De[Reglamento General de Protección de Datos](https://www.cnil.fr/es/reglement-europeen-protection-donnees) ;
- [Directiva 2005/36/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036) Parlamento Europeo;
- [Directiva 2013/55/UE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32013L0055) Parlamento Europeo;
- De[Reglamento 910/2014](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32014R0910) Parlamento Europeo y el Consejo, de 23 de julio de 2014 (e-IDAS) sobre identificación electrónica y servicios de confianza para las transacciones electrónicas en el mercado interior;
- Dela[Orden del 8 de diciembre de 2005](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000636232) intercambios electrónicos entre los usuarios y las autoridades administrativas y entre las autoridades administrativas y la[Decreto No 2010-112 de 2 de febrero de 2010](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021779444&categorieLien=id) para la aplicación de los artículos 9, 10 y 12 de esta ordenanza;
- De[Ley 78-17, de 6 de enero de 1978, enmendada](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460) ciencias de la computación, archivos y libertades;
- Dela[Artículo 441-1 del Código Penal](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006418753&cidTexte=LEGITEXT000006070719&dateTexte=20020101) ;
- Dela[Artículo R. 123-30 del Código de Comercio](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006256107&dateTexte=&categorieLien=cid) ;
- Dela[decreto del 22 de abril de 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&categorieLien=id) la creación de un servicio nacional competente llamado "ventana de negocios".

¿Qué organización implementamos para proteger sus datos?
--------------------------------------------------------

El cumplimiento y la aplicación de las disposiciones de protección de datos aplicables son supervisados por el Representante de Protección de Datos del Servicio del Banco Comercial.

Dentro del Ministerio de Economía y Hacienda, del que depende la Dirección General de Empresas, los servicios del alto funcionario de defensa son los encargados de verificar la aplicación y el cumplimiento de las disposiciones del Reglamento General de Protección de Datos.

¿Cómo me comunico con nosotros?
-------------------------------

Si tiene alguna pregunta sobre el uso de sus datos personales por parte de Enterprise Box Office, lea esta política y póngase en contacto con nuestro representante de protección de datos utilizando la siguiente información de contacto:

Por correo a:

```
Le Délégué à la protection des données des ministères économique et financier
Délégation aux Systèmes d’Information
139, rue de Bercy – Télédoc 322
75572 PARIS CEDEX 12


```
Por correo electrónico, especificando el objeto "Protección de datos" para:[Contacto](contacto.md).

¿Qué son los datos personales?
------------------------------

Los datos personales son información que lo identifica, ya sea directa o indirectamente. Estos pueden incluir su nombre, nombre, dirección, número de teléfono o dirección de correo electrónico, así como la dirección IP de su computadora, por ejemplo, o información relacionada con el uso de Teleservice.

¿Cuándo recopilamos datos personales?
-------------------------------------

Business Box Office recopila datos personales cuando:

- Crea tu propio espacio personal
- formalidad a través de Teleservice Business Box.

¿Qué datos personales tratamos?
-------------------------------

Los datos personales con los que tratamos se refieren únicamente a las formalidades de reconocimiento de cualificaciones profesionales de conformidad con el reglamento de los artículos R. 123-30-10 a R. 123-30-14 del Código de Comercio.

Los datos recopilados permiten que el archivo se recopile según lo previsto en la Sección R. 123-30-10 del Código de Comercio.

En particular, los datos recogidos permiten a los Cerfa documentos asociados a los trámites y procedimientos necesarios para optar a las cualificaciones profesionales o obtener una tarjeta profesional europea.

La siguiente lista resume los datos personales que procesamos.

Para la gestión del acceso al espacio personal del servicio, el usuario comparte la siguiente información:

- El ID de inicio de sesión elegido por el usuario
- La contraseña elegida por el usuario
- Dirección de correo electrónico del usuario
- El número de teléfono móvil
- Dirección de inicio de sesión IP.

Para el uso del espacio de almacenamiento personal, el usuario comparte la siguiente información:

- apellido;
- Utilice el nombre del nombre/esposa;
- Nombre del ejercicio
- Nombre;
- fecha de nacimiento;
- Nacimiento del país;
- Nacionalidad;
- Número de Seguro Social;
- dirección de residencia (incluido el país);
- Teléfono
- Correo electrónico
- diploma, certificado, títulos;
- Experiencia laboral
- un proyecto profesional;
- calidad o función;
- Empleo actual
- lugar de empleo (localidad, país);
- titulado a la posición ocupada.

¿Con qué documentos de apoyo personales tratamos?
-------------------------------------------------

Los documentos de apoyo personales que tratamos se refieren únicamente a los trámites de reconocimiento de cualificaciones profesionales u obtención de la tarjeta profesional europea de acuerdo con la normativa relativa a la actividad regulada realizada.

Los documentos justificativos se utilizan para crear el expediente según lo previsto en el artículo R. 123-30-10 del Código de Comercio.

La lista de documentos justificativos es aprobada por la autoridad competente encargada de investigar el caso.

Los documentos de apoyo personal incluyen documentos que justifican su identidad o residencia. Estos documentos justificativos se mantienen en los mismos términos que los datos personales.

Su almacenamiento se proporciona en espacios inaccesibles desde el exterior (Internet) y su descarga o transferencia está asegurada por cifrado de acuerdo con las disposiciones indicadas en el capítulo "¿Cómo están protegidos sus datos personales?".

¿Cuál es nuestra gestión de cookies?
------------------------------------

La gestión de cookies se describe en la página:[https://www.guichet-entreprises.fr/es/cookies/](https://www.guichet-entreprises.fr/es/cookies/).

Todas las cookies gestionadas utilizan el modo cookie seguro.

Las cookies seguras son un tipo de cookie transmitida exclusivamente a través de conexiones cifradas (https).

Las cookies técnicas contienen los siguientes datos personales:

- Nombre;
- Nombre;
- Correo electrónico
- Teléfono.

Las cookies de análisis contienen la dirección IP de inicio de sesión.

¿Cuáles son sus derechos para proteger sus datos personales?
------------------------------------------------------------

Puede acceder a sus datos, corregirlos, eliminarlos, limitar su uso u oponerse al tratamiento de los mismos.

Si usted siente que el uso de sus datos personales viola las reglas de privacidad, tiene la opción de hacer una[Cnil](https://www.CNIL.fr/).

También tiene derecho a establecer pautas para el tratamiento de sus datos personales en caso de fallecimiento. Se pueden registrar directrices específicas con el administrador de tratamiento. Las directrices generales se pueden registrar con un fideicomiso digital de terceros certificado por la CNIL. Usted tiene la opción de cambiar o eliminar estas pautas en cualquier momento.

Si es así, también puede solicitarnos que le proporcionemos los datos personales que nos ha proporcionado en un formato legible.

Puede enviar sus diversas solicitudes a:

```
Service à compétence nationale Guichet Entreprises
120 rue de Bercy – Télédoc 766
75572 Paris cedex 12


```
Sus solicitudes deben ir acompañadas de una copia de un documento de identidad y serán revisadas por nuestros servicios.

¿Cómo se protegen sus datos personales?
---------------------------------------

Enterprise Box Office implementa todas las prácticas estándar del sector y los procedimientos de seguridad para evitar cualquier violación de sus datos personales.

Toda la información proporcionada se almacena en servidores seguros alojados por nuestros proveedores técnicos.

Cuando se requiere y autoriza la transmisión de datos, La Enterprise Box Office garantiza que estos terceros proporcionen garantías suficientes para garantizar un nivel adecuado de protección. Los intercambios se cifran y los servidores se autentican mediante el reconocimiento mutuo.

De conformidad con la normativa de protección de datos, en caso de incumplimiento, el servicio De Guichet se compromete a comunicar la misma infracción a la autoridad de control pertinente y, cuando sea necesario, a las personas afectadas.

#### Espacio personal

Todos los datos transmitidos a su espacio personal están encriptados y el acceso está protegido por el uso de una contraseña personal. Usted está obligado a mantener esta contraseña confidencial y no revelarla.

#### Tiempo de conservación

Los datos personales tratados por la Enterprise Box Office se conservan durante un período de doce meses de conformidad con lo dispuesto en el artículo R. 123-30-14 del Código de Comercio.

#### En cuanto a los datos de la tarjeta de crédito

El servicio Enterprise Checkout no almacena ni almacena los datos bancarios de los usuarios que están sujetos al pago en el momento de su formalidad. Nuestro proveedor gestiona los datos de transacciones en nombre de la oficina de Enterprise Box de acuerdo con las normas de seguridad más estrictas aplicables en la industria de pagos en línea a través del uso de procesos de cifrado.

¿A quién se transmiten sus datos?
---------------------------------

Los datos recopilados permiten que el archivo se recopile según lo previsto en el artículo R.123-30-10 del Código de Comercio.

De conformidad con el artículo R.123-30-12 del Código de Comercio, los datos recogidos se transmiten para su tratamiento a las autoridades competentes.

