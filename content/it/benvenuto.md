﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Bienvenue" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(key)="welcome" -->
<!-- var(translation)="Auto" -->
<!-- var(last-update)="2020-05-03 15:04:05" -->
<!-- var(lang)="it" -->
<!-- var(site:lang)="it" -->

Benvenuto
=========


<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
Ultimo aggiornamento:<!-- begin-var(last-update) -->2020-05-03 15:04:05<!-- end-var -->

Troverete su questo sito tutte le informazioni sulla procedura per il riconoscimento della qualifica professionale.<!-- alert-start:info -->
<!-- alert-end:info -->

Prima visita a www.guichet-qualifications.fr
--------------------------------------------

Guichet-qualifications.fr è lo sportello unico per il riconoscimento delle qualifiche professionali. Gratuito e accessibile a tutti, il servizio online consente di costruire una cartella senza dover viaggiare. Troverete sul sito[le schede informative delle professioni interessate](capire/professioni_regolamentate.md) riconoscimento della qualifica.

**Lo sapevi?** Le carte professionali vengono messe online non appena sono scritte dal Business Box Office in collaborazione con le amministrazioni competenti. Mostriamo sempre la data dei loro ultimi aggiornamenti.

Riconoscimento della qualifica professionale: formalità
-------------------------------------------------------

Potererie professionali è imperativo quando si desidera lavorare in un altro paese dell'Unione europea in cui è regolamentata la propria professione. Per scoprire le regole che si applicano alla tua situazione, è necessario contattare l'autorità nazionale responsabile per l'accesso alla vostra professione nel paese ospitante. Grazie al sito web www.guichet-qualifications.fr, è possibile completare queste formalità online senza dover viaggiare.

**Leggi anche** :[Perché le vostre qualifiche professionali sono state riconosciute?](https://www.guichet-qualifications.fr/it/pourquoi-faire-reconnaitre-sa-qualification-professionnelle/)

Crea un account e identifica te stesso
--------------------------------------

Per utilizzare il servizio online, si è invitati a[creare uno spazio personale](https://account.guichet-qualifications.fr/session/new) su www.guichet-qualifications.fr. Ti permetterà di creare e gestire il tuo file e modificare le tue informazioni personali. Sarete reindirizzati a[www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/it/) ma i tuoi passi rimarranno visibili nella scheda "I miei passi guichet-qualifications.fr" sul tuo cruscotto.

Con autenticazione[FranceConnect](https://franceconnect.gouv.fr/), se si dispone già di un account su Impots.gouv.fr, Ameli.fr o sul sito Web delle Poste, è possibile accedere a www.guichet-qualifications.fr utilizzando uno di questi tre account.

Completa e convalida il tuo file online
---------------------------------------

Il sito www.guichet-qualifications.fr consente di costruire un file online, allegare eventuali documenti giustificativi e pagare eventuali costi relativi alla formalità. Una volta che il file è stato compilato e convalidato, viene inviato al corpo appropriato per l'elaborazione. Per tenere traccia del file, è possibile contattare l'autorità competente che riceve il file in qualsiasi momento.

**Lo sapevi?** In Francia, vi sono 250 professioni regolamentate (ai sensi della direttiva sulle qualifiche) elencate nella[Professioni regolamentate](capire/professioni_regolamentate.md). Se, tuttavia, non trovi la tua professione, vai al[Banca dati europea delle professioni regolamentate](http://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=sites).

