﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="[Accueil](index.md)" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__menu__" -->
<!-- var(last-update)="2020-05-03 15:04:51" -->
<!-- var(lang)="it" -->
<!-- var(site:lang)="it" -->

[Casa](index.md)
================

Capire
======

[](benvenuto.md)
----------------

[](capire/perche_la_vostra_qualifica_professionale_riconosciuto.md)
-----------------------------------------------------------------------------

[](capire/qualifica_professionale_in_francia.md)
---------------------------------------------------------

[](capire/diploma_stato_membro_ue_eee.md)
--------------------------------------------

[](capire/diploma_francese.md)
----------------------------------

[](capire/professioni_regolamentate.md)
------------------------------------------

[](capire/biglietto_da_visita_europeo.md)
--------------------------------------------------

Professioni regolamentate
=========================

# <!-- include(../reference/it/directive-qualification-professionnelle/_list_menu.md) -->

[Servizi](https://welcome.guichet-qualifications.fr/)
=====================================================

[I miei file](https://dashboard.guichet-qualifications.fr/) @authenticated
==========================================================================

