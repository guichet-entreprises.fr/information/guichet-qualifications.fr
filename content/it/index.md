﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(site:package)="https://gitlab.com/|13561843|latest|reference_md|reference/configuration.yml" -->
<!-- var(site:home)="Accueil" -->
<!-- var(site:menu)="__menu__.md" -->
<!-- var(site:footer)="__footer__/__links__.md" -->
<!-- var(site:lang)="it" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Guichet Qualifications  " -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:font_sizer_off)="off" -->
<!-- var(page:breadcrumb_off)="off" -->
<!-- var(key)="main" -->
<!-- var(translation)="Auto" -->
<!-- var(last-update)="2020-05-03 15:04:50" -->
<!-- var(lang)="it" -->

Qualifiche Box Office<!-- section-banner:surf.jpg --><!-- color:dark -->
========================================================================

Un indirizzo per il riconoscimento delle tue qualifiche

[Inizia la mia passeggiata<!-- link-model:box-trans -->](https://profiler.guichet-qualifications.fr/start/)

Un indirizzo per il riconoscimento delle tue qualifiche<!-- section-information:-120px -->
==========================================================================================

Benvenuto
---------

Troverete su questo sito tutte le informazioni sul riconoscimento della qualifica professionale.

[Prima visita<!-- link-model:box -->](benvenuto.md)

Scoprire
--------

Ha una laurea in uno Stato membro dell'Unione europea o dello Spazio economico europeo

[Le tue possibilità<!-- link-model:box -->](capire/qualifica_professionale_in_francia.md)

Passaggi online
---------------

Completa le tue formalità online relative al riconoscimento delle tue qualifiche professionali in Francia!

[Iniziare<!-- link-model:box -->](https://profiler.guichet-qualifications.fr/start/)

Perché le vostre qualifiche professionali sono state riconosciute?<!-- section:courses -->
==========================================================================================

Una professione è chiamata "regolata" quando è necessario tenere un diploma specifico, aver superato esami specifici, aver ottenuto una certa autorizzazione o essere registrato presso un organismo professionale per eseguirlo.

Potererie professionali è imperativo quando si desidera lavorare in un altro paese dell'Unione europea in cui è regolamentata la propria professione. Ciò significa che la sua formazione e/o esperienza professionale sono ufficialmente riconosciute dal paese ospitante.[Per saperne di più...](capire/perche_la_vostra_qualifica_professionale_riconosciuto.md)

Un unico indirizzo per il riconoscimento delle tue qualifiche professionali<!-- section:welcome --><!-- color:grey -->
======================================================================================================================

Il servizio online guichet-qualifications.fr incoraggia la mobilità professionale dei residenti dell'Unione europea e dello Spazio economico europeo fornendo informazioni complete sull'accesso e la pratica delle professioni regolamentate in Francia, al fine del riconoscimento della qualifica professionale.

Questo servizio è l'iniziativa del Ministero dell'Economia e delle Finanze.

Fare affari in Francia e in Europa<!-- section-stories:drapeaux.jpg --><!-- color:dark -->
==========================================================================================

Sei un cittadino dell'Unione europea o dello Spazio economico europeo? Incoraggiamo la creazione di imprese e la mobilità professionale in Francia e in Europa. Guichet-qualifications.fr è riconosciuta come la finestra unica francese per il riconoscimento delle vostre qualifiche professionali, un membro della rete Eugo creata dalla Commissione europea.

Una volta ottenuto il riconoscimento della tua qualifica, se desideri stabilirti definitivamente o temporaneamente per lavorare in Francia, vai a:

[guichet-entreprises.fr](https://guichet-entreprises.fr/)

