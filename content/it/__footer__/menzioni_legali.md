﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Mentions légales" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-mentions_legales" -->
<!-- var(last-update)="2020-05-03 15:06:32" -->
<!-- var(lang)="it" -->
<!-- var(site:lang)="it" -->

Legale
======


<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
Ultimo aggiornamento:<!-- begin-var(last-update) -->2020-05-03 15:06:32<!-- end-var -->

Identificazione del redattore
-----------------------------

```
Service à compétence nationale Guichet Entreprises
120 rue de Bercy – Télédoc 766
75572 Paris cedex 12


```
Il Business Box Office, creato dal[decreto del 22 aprile 2015](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=A1B17F7C2CD76682B8F5E3E5CE690458.tpdila13v_1?cidTexte=JORFTEXT000030517635&dateTexte=&oldAction=rechJO&categorieLien=id&idJO=JORFCONT000030517419), è sotto l'autorità del[Direzione Aziendale](https://www.entreprises.gouv.fr/) (DGE) all'interno del[Ministero dell'Economia e delle Finanze](https://www.economie.gouv.fr/).

Progettazione editoriale, monitoraggio, manutenzione tecnica e aggiornamenti del sito web[www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/) sono forniti dal Business Box Office.

Editore
-------

Florent Tournois, responsabile del Business Box Office.

Fornitore di alloggi
--------------------

```
Cloud Temple
215 avenue Georges Clémenceau
92024 Nanterre
Tel : +33 1 41 91 77 77


```
Oggetto del sito
----------------

Il sito www.guichet-qualifications.fr consente a qualsiasi residente dell'Unione europea o dello Spazio economico europeo di far riconoscere le proprie qualifiche professionali e di esercitare una professione regolamentata in Francia in vista di uno stabilimento sostenibile ([stabilimento libero](https://www.guichet-entreprises.fr/it/eugo/libre-etablissement-le/)) o temporaneo ([fornitura gratuita del servizio](https://www.guichet-entreprises.fr/it/eugo/libre-prestation-de-services-lps/)). Consente inoltre ai residenti in Francia di conoscere le possibilità di accesso e di pratica di una professione regolamentata in un altro Stato membro.

Le formalità sono oggetto di una firma elettronica, i cui termini e condizioni sono disponibili sul sito web al seguente link:[Politica di firma elettronica](politica_di_firma_elettronica.md).

Inoltre, il sito www.guichet-qualifications.fr fornisce agli appaltatori una serie di informazioni sulle formalità, le procedure e i requisiti delle attività regolamentate in conformità con l'articolo R. 123-2 del Codice di Commercio.

Trattamento dei dati personali
------------------------------

I dati personali contenuti nel singolo fascicolo in conformità con il regolamento generale sulla protezione dei dati sono soggetti alle disposizioni disponibili su questo sito al seguente link:[Politica di protezione dei dati](la_protezione_delle_politiche_da.md).

In base al presente regolamento e al Computer and Freedoms Act del 6 gennaio 1978, hai il diritto di accedere, correggere, modificare ed eliminare i dati che ti riguardano.

Ci sono diversi modi per esercitare questo diritto:

- Contattando il Business Formalities Centre (CFE) che riceve il file di dichiarazione;
- Invio di un messaggio di posta elettronica al supporto tecnico
- inviando una lettera a:


```
  Service à compétence nationale Guichet Entreprises
  120 rue de Bercy – Télédoc 766
  75572 Paris cedex 12
```

Diritti riproduttivi
--------------------

Il contenuto di questo sito rientra nella legislazione francese e internazionale sul diritto d'autore e sulla proprietà intellettuale.

Tutti gli elementi grafici del sito sono di proprietà del servizio Business Box Office. È severamente vietato riprodurre o adattare le pagine del sito che prenderebbero gli elementi grafici.

È inoltre vietato qualsiasi utilizzo di contenuti a fini commerciali.

Qualsiasi citazione o ristampa del contenuto dal sito deve aver ottenuto l'autorizzazione dell'editore. La fonte (www.guichet-qualifications.fr) e la data della copia dovranno essere indicate, nonché il servizio "Business Box".

Link alle pagine del sito
-------------------------

Qualsiasi sito pubblico o privato può collegarsi alle pagine del sito www.guichet-qualifications.fr. Non è necessario richiedere l'autorizzazione preventiva. Tuttavia, l'origine delle informazioni dovrà essere chiarita, ad esempio sotto forma di: "Riconoscimento delle qualifiche professionali (fonte: www.guichet-qualifications.fr, un sito di business box office)". Le pagine del sito www.guichet-qualifications.fr non devono essere nidificate all'interno delle pagine di un altro sito. Dovranno essere visualizzati in una nuova finestra o scheda.

Collegamenti a pagine di siti esterni
-------------------------------------

I link sul sito www.guichet-qualifications.fr possono indirizzare l'utente a siti esterni il cui contenuto non può in alcun modo assumere la responsabilità dell'Enterprise Box office.

Ambiente tecnico
----------------

Alcuni browser potrebbero bloccare le finestre di questo sito per impostazione predefinita. Per consentire la visualizzazione di determinate pagine, è necessario consentire l'apertura delle finestre quando il browser offre facendo clic sull'archetto di avviso e quindi visualizzato nella parte superiore della pagina.

Se il browser non dispone di un messaggio di avviso, è necessario configurarlo in modo da consentire l'apertura delle finestre per il sito www.guichet-qualifications.fr.

