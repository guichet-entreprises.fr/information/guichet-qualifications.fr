﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Utilisation des cookies sur guichet-qualifications.fr" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-cookies" -->
<!-- var(last-update)="2020-05-03 15:06:19" -->
<!-- var(lang)="it" -->
<!-- var(site:lang)="it" -->

Utilizzo dei cookie su guichet-qualifications.fr
================================================


<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
Ultimo aggiornamento:<!-- begin-var(last-update) -->2020-05-03 15:06:19<!-- end-var -->

Cosa sono i cookie e come si utilizza il Business Box Office?
-------------------------------------------------------------

Box Office Può utilizzare i cookie quando un utente visita il proprio sito. I cookie sono file inviati al browser tramite un server web per registrare le attività dell'utente durante il periodo di navigazione. L'utilizzo dei cookie consente di riconoscere il browser web utilizzato dall'utente al fine di facilitarne la navigazione. I cookie vengono utilizzati anche per misurare il pubblico del sito e produrre statistiche di consultazione.

I cookie utilizzati da Enterprise Box Office non forniscono riferimenti per dedurre i dati personali o le informazioni personali degli utenti per identificare un particolare utente. Sono di natura temporanea, con il solo scopo di rendere più efficiente la successiva trasmissione. Nessun cookie utilizzato sul sito avrà un periodo di vigore di più di due anni.

Gli utenti hanno la possibilità di impostare il proprio browser per essere informati della ricezione dei cookie e di rifiutare l'installazione. Vietando i cookie o disabilitandoli, l'utente potrebbe non essere in grado di accedere a determinate funzionalità del sito.

In caso di rifiuto o disattivazione di un cookie, la sessione deve essere riavviata.

Quali tipi di cookie vengono utilizzati da guichet-qualifications.fr?
---------------------------------------------------------------------

### Cookie tecnici

Consentono all'utente di esplorare il sito e utilizzare alcune delle sue caratteristiche.

### Cookie di analisi

Box Office utilizza i cookie di analisi del pubblico per quantificare il numero di visitatori. Questi cookie vengono utilizzati per misurare e analizzare il modo in cui gli utenti navigano nel sito.

