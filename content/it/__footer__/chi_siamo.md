﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Qui sommes-nous ?" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-qui_sommes_nous" -->
<!-- var(last-update)="2020-05-03 15:08:11" -->
<!-- var(lang)="it" -->
<!-- var(site:lang)="it" -->

Chi siamo?
==========


<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
Ultimo aggiornamento:<!-- begin-var(last-update) -->2020-05-03 15:08:11<!-- end-var -->

Il sito www.guichet-qualifications.fr è progettato e sviluppato dal servizio "Business Box Office" a livello nazionale, creato dal[decreto del 22 aprile 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&dateTexte=20200109) (JORF del 25 aprile 2015). È sotto l'autorità del[Direzione Aziendale](https://www.entreprises.gouv.fr/) all'interno del[Ministero dell'Economia e delle Finanze](https://www.economie.gouv.fr/).

Il servizio nazionale "Business Box Office" gestisce i siti web[www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/) E[www.guichet-entreprises.fr](https://www.guichet-entreprises.fr/) [1] che insieme costituiscono lo sportello unico elettronico definito dalle direttive europee[2006/123/EC](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32006L0123) E[2005/36](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036). Banchi di questo tipo esistono in tutta Europa e sono federati all'interno del progetto " [Eugo](http://ec.europa.eu/internal_market/eu-go/index_fr.htm) Commissione europea.

Al Business Box Office, incoraggiamo la mobilità professionale dei residenti dell'Unione Europea o dello Spazio Economico Europeo fornendo l'accesso a tutte le informazioni sull'accesso e la pratica delle professioni regolamentate in Francia attraverso "record professionali".

Il sito[www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/) è rivolto a tutti i residenti dell'Unione europea o dello Spazio economico europeo. Essa li informa sulle possibilità di sogliere che le loro qualifiche professionali sono riconosciute e che praticano una professione regolamentata in Francia in vista di uno stabilimento sostenibile ([stabilimento libero](https://www.guichet-entreprises.fr/it/eugo/libre-etablissement-le/)) o temporaneo ([fornitura gratuita del servizio](https://www.guichet-entreprises.fr/it/eugo/libre-prestation-de-services-lps/)). Consente inoltre ai residenti in Francia di conoscere le possibilità di accesso e di pratica di una professione regolamentata in un altro Stato membro.

