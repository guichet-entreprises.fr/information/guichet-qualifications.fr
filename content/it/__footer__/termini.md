﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Conditions générales d’utilisation" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-cgu" -->
<!-- var(last-update)="2020-05-03 15:06:14" -->
<!-- var(lang)="it" -->
<!-- var(site:lang)="it" -->

Termini e condizioni
====================


<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
Ultimo aggiornamento:<!-- begin-var(last-update) -->2020-05-03 15:06:14<!-- end-var -->

Preambolo
---------

Questo documento descrive i termini di impegno per l'uso delle qualifiche di teleservice Box per gli utenti. Fa parte del quadro giuridico:

- Di[Regolamento generale sulla protezione dei dati](https://www.cnil.fr/it/reglement-europeen-protection-donnees) ;
- [Direttiva 2005/36/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036) Parlamento europeo;
- [direttiva 2013/55/UE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32013L0055) Parlamento europeo;
- Di[Regolamento 910/2014](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32014R0910) Parlamento europeo e il Consiglio del 23 luglio 2014 (e-IDAS) sull'identificazione elettronica e sui servizi di fiducia per le transazioni elettroniche all'interno del mercato interno;
- della[8 dicembre 2005 ordina](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000636232) scambi elettronici tra utenti e autorità amministrative e tra le autorità amministrative e il[Decreto n. 2010-112 del 2 febbraio 2010](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021779444&categorieLien=id) per l'applicazione degli articoli 9, 10 e 12 di questa ordinanza;
- Di[Atto 78-17 del 6 gennaio 1978 modificato](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460) informatica, file e libertà;
- della[Articolo 441-1 del codice penale](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006418753&cidTexte=LEGITEXT000006070719&dateTexte=20020101) ;
- della[Articolo R. 123-30 del Codice di Commercio](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006256107&dateTexte=&categorieLien=cid) ;
- della[decreto del 22 aprile 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&categorieLien=id) la creazione di un servizio competente a livello nazionale chiamato "finestra di business".

L'oggetto del documento
-----------------------

Lo scopo del presente documento è quello di definire i termini e le condizioni generali di utilizzo delle qualifiche Teleservice Box, denominate "Servizio" di seguito, tra il servizio nazionale Guichet Enterprises e gli utenti.

Il servizio nazionale Guichet Enterprises è sotto l'autorità della Direzione aziendale del Ministero dell'Economia e delle Finanze.

Definizione e scopo del Servizio
--------------------------------

Il servizio implementato dal servizio nazionale Guichet Enterprises (in seguito denominato "Business Box Service") contribuisce a semplificare le procedure relative alla creazione, ai cambiamenti della situazione e alla cessazione di un'attività di utenti francesi ed europei.

Il servizio è stato oggetto di approvazione per la sicurezza in conformità con anSSI Interministerial Instruction 901/SGDSN/ANSSI (NOR: PRMD1503279 28/01/2015 e la PGSSI dei ministeri economico e finanziario (NOR: FCPP1622039A) del 1 agosto 2016.

L'utilizzo del Servizio è facoltativo e gratuito. Tuttavia, nell'ambito delle formalità svolte attraverso questo Servizio, come l'avvio di un'impresa, possono essere richiesti pagamenti online. Questi sono sicuri.

Tutti i destinatari del Servizio sono indicati come le agenzie partner di seguito.

Ai sensi della Sezione R.123-3 del Codice di Commercio, i dati raccolti vengono trasmessi per l'elaborazione alle seguenti organizzazioni partner:

"1) Fatte salve le disposizioni della seconda e terza, le camere territoriali di commercio e industria creano e gestiscono i centri formalidelle delle imprese responsabili:

# Commercianti;
# Aziende commerciali.

(2) Le camere di commercio e artigianato della regione creano e gestiscono i centri competenti per le persone fisiche e le imprese soggette a registrazione nell'elenco dei mestieri e per le persone che beneficiano dell'esenzione di registrazione prevista dall'articolo 19 della legge 96-603 del 5 luglio 1996 in relazione allo sviluppo e alla promozione del commercio e dell'artigianato. , esclusi quelli menzionati nel terzo di questo articolo.

3. La Camera Nazionale di Nautica Nautica Crea e gestisce il centro per privati e aziende soggette a registrazione nel registro delle società nautiche artigianali.

(4) I registri commerciali o commerciali creano e gestiscono i centri responsabili di:

# Aziende civili e non commerciali;
# Società di eserciziliberali;
# persone giuridiche soggette a registrazione nel registro delle imprese e delle società diverse da quelle menzionate nei
# Enti pubblici industriali e commerciali;
# Agenti commerciali;
# gruppi di interesse economico e i gruppi di interesse economico europei.

(5) I sindacati della sicurezza sociale e delle prestazioni familiari (Urssaf) o i fondi generali di previdenza sociale creano e gestiscono i centri responsabili di:

# persone impegnate, come occupazione regolare, in un'attività regolamentata o non commerciale, artigianale o agricola indipendente;
# datori di lavoro le cui imprese non sono registrate nel registro dei mestieri e delle imprese, nell'elenco dei mestieri o nel registro delle società di nautica artigianale e che non fanno rapporto ai centri menzionati nel sesto.

(6) Le Camere dell'Agricoltura creano e gestiscono i centri competenti per privati e imprese che svolgono attività agricole come[...]. »

Caratteristiche
---------------

Il Servizio consente all'utente di:

- l'accesso alla documentazione che descrive in dettaglio gli obblighi dei Business Formality Centres (CFE) nonché gli elementi costitutivi del fascicolo di dichiarazione e del file della domanda;
- creare un account utente che fornisce l'accesso allo spazio di archiviazione personale. Questo spazio permette all'utente di gestire e utilizzare i suoi dati personali, di conservare le informazioni su di lui e i documenti e i documenti giustificativi necessari per il completamento delle procedure amministrative.

Dal suo spazio, l'utente può:

- compilare il suo file unico ai sensi dell'articolo R. 123-1 del Codice di Commercio, che include il file di dichiarazione e, se del caso, le domande di autorizzazione;
- presentare il suo fascicolo di dichiarazione e, se necessario, le richieste di autorizzazione al centro di formalità commerciali competente;
- Accedere alle informazioni di follow-up sull'elaborazione del suo fascicolo e, se necessario, il suo file di domanda;
- consentire ai CFE di attuare i trattamenti necessari per sfruttare le informazioni ricevute dalla società oggetto nell'ultimo paragrafo dell'articolo 123-21 del Codice di commercio: ricevuta del fascicolo unico ai sensi dell'articolo 2 della legge 94-126 dell'11 febbraio 1994 in relazione all'iniziativa individuale e alle imprese e inoltrata dalla persona giuridica contrattata dall'articolo R. 123-21 del codice di commercio; ricevere informazioni di follow-up sul trattamento di questi fascicoli fornite dalle agenzie e dalle autorità partner; trasmissione di informazioni di follow-up dal trattamento di fascicoli univoci alla persona giuridica contrattata dall'articolo 121-21 del codice di commercio.

Come registrarsi e utilizzare il Servizio
-----------------------------------------

L'accesso al Servizio è aperto a chiunque e gratuito. È facoltativo e non è esclusivo di altri canali di accesso per consentire all'utente di completare le sue formalità.

Per la gestione dell'accesso allo spazio personale del Servizio, l'utente condivide le seguenti informazioni:

- l'indirizzo e-mail dell'utente.
- La password scelta dall'utente

L'utilizzo del Servizio richiede la fornitura di dati personali per la creazione della singola cartella.
Il trattamento dei dati personali è descritto nella politica di protezione dei dati disponibile[Qui](la_protezione_delle_politiche_da.md).

L'utilizzo del Servizio richiede una connessione e un browser Internet. Il browser deve essere configurato per*Biscotti* sessione di sessione.

Per garantire un'esperienza di navigazione ottimale, ti consigliamo di utilizzare le seguenti versioni del browser:

- Firefox versione 45 e versioni 20;
- Google Chrome versione 48 e versioni 20.

Infatti, altri browser potrebbero non supportare alcune funzionalità del Servizio.

Il servizio è ottimizzato per un display da 1024-768 pixel. Si consiglia di utilizzare la versione più recente del browser e aggiornarlo regolarmente per beneficiare di patch di sicurezza e le migliori prestazioni.

Condizioni specifiche per l'utilizzo del servizio di firma
----------------------------------------------------------

Il servizio di firma elettronica è accessibile direttamente tramite il Servizio.

Articolo R. 123-24 del Codice di Commercio ai sensi del regolamento 910/2014 del Parlamento europeo e del Consiglio del 23 luglio 2014 (e-IDAS) sono i riferimenti applicabili al servizio di firma elettronica del Servizio.

Ruoli e impegno
---------------

### Impegno di Corporate Box Office

# Il servizio Enterprise Box Office implementa e gestisce il Servizio in conformità con il quadro giuridico esistente stabilito nel preambolo.
# L'Enterprise Box Office si impegna ad adottare tutte le misure necessarie per garantire la sicurezza e la riservatezza delle informazioni fornite dall'utente.
# Il Business Box Office si impegna a garantire la protezione dei dati raccolti nell'ambito del Servizio, impedendone anche la distorsione, danneggiato o accessibile da terzi non autorizzati, conformemente alle misure previste dall'ordinanza dell'8 dicembre 2005 sugli scambi elettronici tra utenti e autorità amministrative e tra autorità amministrative, Decreto n. 2010-112 del 2 febbraio 2010 per l'applicazione degli articoli 9, 10 e 12 della presente ordinanza e regolamento 2016/679 del Parlamento europeo e del Consiglio del 27 aprile 2016 sull'elaborazione dei dati personali dei dati.
# Il Business Box Office e le agenzie partner garantiscono i diritti di accesso, rettifica e opposizione degli Utenti del Servizio ai sensi della legge 78-17 del 6 gennaio 1978 relativi all'uso informatico di file e libertà e regolamento n. 2016/679 del Parlamento europeo e del Consiglio del 27 aprile 2016 relativi all'elaborazione dei dati personali.
Questo diritto può essere esercitato in diversi modi:
a) contattando il Business Formalities Centre (CFE) che riceve il file di dichiarazione;
b- inviando un'e-mail al supporto
c- inviando una lettera a:

<blockquote><p>Service à compétence nationale Guichet Entreprises</p>
<p>120 rue de Bercy – Télédoc 766</p>
<p>75572 Paris cedex 12</blockquote># Il Business Box Office e le organizzazioni partner si impegnano a non commercializzare le informazioni e i documenti trasmessi dall'utente tramite il Servizio e a non comunicarli a terzi, al di fuori dei casi previsti dalla legge.
# Il Business Box Office e le organizzazioni partner si impegnano a non commercializzare le informazioni e i documenti trasmessi dall'utente tramite il Servizio e a non comunicarli a terzi, al di fuori dei casi previsti dalla legge.
# L'Enterprise Box Office si impegna a garantire la tracciabilità di tutte le azioni eseguite da tutti gli utenti del Servizio, comprese quelle delle organizzazioni partner e dell'utente.
# Il servizio Enterprise Box Office fornisce agli utenti il supporto in caso di incidente o di un avviso di sicurezza definito.

Coinvolgimento degli utenti
---------------------------

# L'utente compila il suo file online e lo convalida eventualmente allegando le parti necessarie per il trattamento del file.
# Al termine della preparazione del file, sullo schermo viene visualizzato un riepilogo delle informazioni fornite dall'utente in modo che l'utente possa verificarle e confermarle. Dopo la conferma, il file viene inoltrato alle agenzie partner. La conferma e la trasmissione del modulo da parte dell'utente vale la pena di firmarlo.
# Questi termini e condizioni sono richiesti da qualsiasi utente che sia un utente del Servizio.

Disponibilità ed evoluzione del Servizio
----------------------------------------

Il servizio è disponibile 7 giorni su 7, 24 ore su 24.

L'Enterprise Box Office, tuttavia, si riserva la possibilità di evolvere, modificare o sospendere il Servizio senza preavviso per manutenzione o altri motivi ritenuti necessari. L'Indisponibilità del Servizio non dà diritto ad alcun risarcimento. In caso di indisponibilità del Servizio, una pagina informativa viene poi inviata all'utente che menziona questa indisponibilità; viene quindi invitato a fare il suo passo in un secondo momento.

I termini di utilizzo possono essere modificati in qualsiasi momento, senza preavviso, a seconda delle modifiche al Servizio, delle modifiche alla legislazione o dei regolamenti o per qualsiasi altro motivo ritenuto necessario.

Responsabilità
--------------

# La responsabilità per il servizio Enterprise Box Office non può essere sostenuta in caso di furto di identità o qualsiasi uso fraudolento del Servizio.
# I dati trasmessi ai servizi online delle agenzie partner rimangono di competenza dell'utente, anche se trasmessi con i mezzi tecnici messi a disposizione nel Servizio.
L'utente può modificarli o eliminarli dalle agenzie partner in qualsiasi momento.
Può scegliere di eliminare tutte le informazioni dal suo account cancellando i suoi dati dal Servizio.
# Si ricorda all'utente che chiunque faccia una falsa dichiarazione per se stesso o per gli altri è responsabile, in particolare, delle sanzioni previste per l'articolo 441-1 del codice penale, prevedendo sanzioni fino a tre anni di reclusione e una multa di 45.000 euro.

