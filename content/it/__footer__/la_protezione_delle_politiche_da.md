﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Politique de protection des données personnelles" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-politique_protection_donnees" -->
<!-- var(last-update)="2020-05-03 15:07:09" -->
<!-- var(lang)="it" -->
<!-- var(site:lang)="it" -->

Politica sulla protezione dei dati personali
============================================


<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
Ultimo aggiornamento:<!-- begin-var(last-update) -->2020-05-03 15:07:09<!-- end-var -->

| Versione      | Data       | Oggetto                                                         |
|---------------|------------|-----------------------------------------------------------------|
| V1 (in via di | 25/05/2018 | Versione iniziale                                               |
| V2            | 01/09/2018 | Informazioni aggiornate sul delegato per la protezione dei dati |

L'informativa sulla privacy (la "politica") ti informa su come i tuoi dati vengono raccolti ed elaborati dal servizio National Business Bank, le misure di sicurezza implementate per garantirne l'integrità e la riservatezza e i diritti di cui hai bisogno per controllarne l'uso.

Questa politica integra la[termini e condizioni d'uso](termini.md) (CGU) e, se necessario,[Legale](menzioni_legali.md). In quanto tale, tale politica è considerata incorporata in tali documenti.

Nell'ambito dello sviluppo dei nostri servizi e dell'attuazione di nuovi standard normativi, potremmo dover modificare questa politica. Vi invitiamo a leggerlo regolarmente.

Chi siamo?
----------

Il Business Box Office, creato dal[decreto del 22 aprile 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&categorieLien=id) è sotto l'autorità del[Direzione Aziendale](https://www.entreprises.gouv.fr/) (DGE) all'interno del[Ministero dell'Economia e delle Finanze](https://www.economie.gouv.fr/).

Il servizio Business Box Office implementa un guichet-qualifcations.fr di teleservizio (in seguito le "Teleservice Box Office Professional Qualifications").

Il Teleservice Box Office Professional Qualifications è rivolto a tutti i residenti dell'Unione Europea o dello Spazio Economico Europeo. Li informa sulle possibilità di lasciare riconosciute le loro qualifiche professionali e di esercitare una professione regolamentata in Francia in vista di un sostenibile (istituzione libera) o temporaneo (fornitura gratuita di servizi).

Consente inoltre ai residenti in Francia di conoscere le possibilità di accesso e di pratica di una professione regolamentata in un altro Stato membro.

Il Teleservice Box Office Professional Qualifications fa parte del quadro giuridico:

- Di[Regolamento generale sulla protezione dei dati](https://www.cnil.fr/it/reglement-europeen-protection-donnees) ;
- [Direttiva 2005/36/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036) Parlamento europeo;
- [direttiva 2013/55/UE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32013L0055) Parlamento europeo;
- Di[Regolamento 910/2014](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32014R0910) Parlamento europeo e il Consiglio del 23 luglio 2014 (e-IDAS) sull'identificazione elettronica e sui servizi di fiducia per le transazioni elettroniche all'interno del mercato interno;
- della[8 dicembre 2005 ordina](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000636232) scambi elettronici tra utenti e autorità amministrative e tra le autorità amministrative e il[Decreto n. 2010-112 del 2 febbraio 2010](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021779444&categorieLien=id) per l'applicazione degli articoli 9, 10 e 12 di questa ordinanza;
- Di[Atto 78-17 del 6 gennaio 1978 modificato](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460) informatica, file e libertà;
- della[Articolo 441-1 del codice penale](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006418753&cidTexte=LEGITEXT000006070719&dateTexte=20020101) ;
- della[Articolo R. 123-30 del Codice di Commercio](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006256107&dateTexte=&categorieLien=cid) ;
- della[decreto del 22 aprile 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&categorieLien=id) la creazione di un servizio competente a livello nazionale chiamato "finestra di business".

Quale organizzazione implementiamo per proteggere i tuoi dati?
--------------------------------------------------------------

La conformità e l'attuazione delle disposizioni applicabili in materia di protezione dei dati sono monitorate dal rappresentante per la protezione dei dati del Business Bank Service.

All'interno del Ministero dell'Economia e delle Finanze, da cui dipende la Direzione generale delle imprese, i servizi dell'alto funzionario della difesa sono incaricati di verificare l'attuazione e il rispetto delle disposizioni del regolamento generale sulla protezione dei dati.

Come posso contattarci?
-----------------------

In caso di domande sull'utilizzo dei tuoi dati personali da parte dell'Enterprise Box Office, leggi questa informativa e contatta il nostro rappresentante per la protezione dei dati utilizzando le seguenti informazioni di contatto:

Inviando a:

```
Le Délégué à la protection des données des ministères économique et financier
Délégation aux Systèmes d’Information
139, rue de Bercy – Télédoc 322
75572 PARIS CEDEX 12


```
Tramite e-mail, specificando l'oggetto "Protezione dati" per:[Contattare](contattare.md).

Che cos'è un dato personale?
----------------------------

I dati personali sono informazioni che identificano l'utente, direttamente o indirettamente. Questi possono includere il tuo nome, nome, indirizzo, numero di telefono o indirizzo e-mail, nonché l'indirizzo IP del tuo computer, ad esempio, o informazioni relative all'uso di Teleservice.

Quando raccogliamo dati personali?
----------------------------------

Il Business Box Office raccoglie dati personali quando:

- Crea il tuo spazio personale
- completare una formalità tramite il Teleservice Business Box.

Quali dati personali elaboriamo?
--------------------------------

I dati personali di cui ci occupiamo si riferiscono solo alle formalità di riconoscimento delle qualifiche professionali in conformità con le norme degli articoli da 123-30-10 a R. 123-30-14 del Codice di Commercio.

I dati raccolti consentono di compilare il file come previsto nella sezione R. 123-30-10 del Codice di Commercio.

In particolare, i dati raccolti consentono ai documenti Cerfa associati alle formalità e alle procedure necessarie per qualificarsi per le qualifiche professionali o per ottenere una tessera professionale europea.

L'elenco seguente riepiloga i dati personali che elaboriamo.

Per la gestione dell'accesso allo spazio personale del servizio, l'utente condivide le seguenti informazioni:

- L'ID di accesso scelto dall'utente
- La password scelta dall'utente
- Indirizzo di posta elettronica dell'utente
- Il numero di cellulare
- indirizzo di accesso IP.

Per l'utilizzo dello spazio di archiviazione personale, l'utente condivide le seguenti informazioni:

- cognome;
- Usa il nome/moglie;
- Nome dell'esercizio
- Nome;
- data di nascita;
- Nascita del paese;
- Nazionalità;
- Numero di previdenza sociale;
- indirizzo di residenza (compreso il paese);
- Telefono
- Posta elettronica
- diploma, certificato, titoli;
- Esperienza lavorativa
- un progetto professionale;
- qualità o funzione;
- Occupazione attuale
- luogo di lavoro (località, paese);
- titoli alla posizione occupata.

Quali documenti di supporto personali abbiamo a che fare?
---------------------------------------------------------

I documenti di supporto personali di cui ci occupiamo riguardano solo le formalità di riconoscere le qualifiche professionali o di ottenere la carta professionale europea in conformità con le normative relative all'attività regolamentata svolta.

I documenti giustificativi vengono utilizzati per compilare il file come previsto ai sensi dell'articolo 123-30-10 del Codice di Commercio.

L'elenco dei documenti giustificativi è approvato dall'autorità competente incaricata di indagare sul caso.

I documenti giustificativi personali includono documenti che giustificano l'identità o la residenza. Questi documenti giustificativi sono conservati alle stesse condizioni dei dati personali.

La loro conservazione è fornita in spazi inaccessibili dall'esterno (Internet) e il loro download o trasferimento è protetto dalla crittografia in conformità con le disposizioni indicate nel capitolo "Come vengono protetti i tuoi dati personali?".

Qual è la nostra gestione dei cookie?
-------------------------------------

La gestione dei cookie è descritta nella pagina:[https://www.guichet-entreprises.fr/it/cookies/](https://www.guichet-entreprises.fr/it/cookies/).

Tutti i cookie gestiti utilizzano la modalità cookie sicura.

I cookie sicuri sono un tipo di cookie trasmesso esclusivamente tramite connessioni crittografate (https).

I cookie tecnici contengono i seguenti dati personali:

- Nome;
- Nome;
- Posta elettronica
- Telefono.

I cookie di analisi contengono l'indirizzo IP di accesso.

Quali sono i tuoi diritti per proteggere i tuoi dati personali?
---------------------------------------------------------------

Puoi accedere ai tuoi dati, correggerli, eliminarli, limitarne l'uso o opporti al trattamento.

Se ritieni che l'uso dei tuoi dati personali violi le norme sulla privacy, hai la possibilità di rivendicare il[Cnil](https://www.CNIL.fr/).

Hai anche il diritto di stabilire linee guida per la gestione dei tuoi dati personali in caso di decesso. Linee guida specifiche possono essere registrate con il responsabile del trattamento. Le linee guida generali possono essere registrate con un trust digitale di terze parti certificato dal CNIL. Hai la possibilità di modificare o rimuovere queste linee guida in qualsiasi momento.

In tal caso, puoi anche chiederci di fornirti i dati personali che ci hai fornito in un formato leggibile.

Puoi inviare le tue varie richieste a:

```
Service à compétence nationale Guichet Entreprises
120 rue de Bercy – Télédoc 766
75572 Paris cedex 12


```
Le vostre domande devono essere accompagnate da una copia di un documento d'identità e saranno esaminate dai nostri servizi.

Come vengono protetti i tuoi dati personali?
--------------------------------------------

L'Enterprise Box Office implementa tutte le procedure standard del settore e le procedure di sicurezza per prevenire qualsiasi violazione dei dati personali.

Tutte le informazioni fornite sono memorizzate su server sicuri ospitati dai nostri fornitori tecnici.

Quando la trasmissione dei dati è richiesta e autorizzata, l'Enterprise Box Office garantisce che queste terze parti forniscano garanzie sufficienti per garantire un adeguato livello di protezione. Gli scambi vengono crittografati e i server autenticati mediante riconoscimento reciproco.

In conformità con le norme sulla protezione dei dati, in caso di violazione, il servizio Business Guichet si impegna a comunicare tale violazione all'autorità di vigilanza competente e, ove necessario, alle persone interessate.

#### Spazio personale

Tutti i dati trasmessi al tuo spazio personale sono criptati e l'accesso è protetto dall'uso di una password personale. L'utente è tenuto a mantenere riservata questa password e a non divulgarla.

#### Tempo di conservazione

I dati personali trattati dall'Enterprise Box Office sono conservati per un periodo di dodici mesi secondo le disposizioni dell'articolo 123-30-14 del Codice di Commercio.

#### Per quanto riguarda i dati della carta di credito

Il servizio Enterprise Checkout non memorizza né memorizza i dati bancari degli utenti soggetti a pagamento al momento della loro formalità. Il nostro fornitore gestisce i dati delle transazioni per conto di Enterprise Box office in conformità con le più severe regole di sicurezza applicabili nel settore dei pagamenti online attraverso l'uso di processi di crittografia.

A chi vengono trasmessi i tuoi dati?
------------------------------------

I dati raccolti consentono di compilare il file come previsto ai sensi dell'articolo R.123-30-10 del Codice di Commercio.

Ai sensi dell'articolo R.123-30-12 del Codice di commercio, i dati raccolti vengono trasmessi per il trattamento alle autorità competenti.

