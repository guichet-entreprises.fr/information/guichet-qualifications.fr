﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Politique de signature électronique" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-politique_signature_electronique" -->
<!-- var(last-update)="2020-05-03 15:08:04" -->
<!-- var(lang)="it" -->
<!-- var(site:lang)="it" -->

Politica di firma elettronica
=============================


<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
Ultimo aggiornamento:<!-- begin-var(last-update) -->2020-05-03 15:08:04<!-- end-var -->

| Versione      | Data       | Oggetto           |
|---------------|------------|-------------------|
| V1 (in via di | 27/09/2018 | Versione iniziale |

L'oggetto del documento
-----------------------

La firma elettronica affissa su un set di dati garantisce l'integrità dei dati trasmessi, il non ripudio dei dati firmati e l'autenticità del trasmettitore.

Questa politica di firma elettronica è un documento che descrive i requisiti di ammissibilità delle agenzie riceventi, vale a dire le agenzie competenti, di un file sul quale è apposto una firma elettronica in relazione agli scambi elettronici coperti dall'articolo 123-30-10 del codice di commercio.

Gli organi competenti sono i servizi responsabili delle domande di audizione per il riconoscimento delle qualifiche professionali.

In questa sezione,*"Quando è richiesta una firma, è consentito l'uso di una firma elettronica con le garanzie stabilite nella prima frase del secondo paragrafo dell'articolo 1367 del codice civile"*, e il secondo comma dell'articolo 1367 del codice civile,*"Quando è elettronico, consiste nell'uso di un processo di identificazione affidabile che ne garantisca il collegamento con l'atto a cui si collega. L'affidabilità di questo processo è presunta, fino a prova contraria, quando viene creata la firma elettronica, l'identità del firmatario assicurato e l'integrità dell'atto garantito, a condizioni stabilite per decreto dal Consiglio di Stato."*,

I regolamenti applicabili per l'attuazione della firma elettronica delle qualifiche Teleservice Box derivano dai seguenti testi:

- regolamento 910/2014 del Parlamento europeo e del Consiglio del 23 luglio 2014 (e-IDAS) sull'identificazione elettronica e sui servizi di fiducia per le transazioni elettroniche all'interno del mercato interno;
- Codice civile;
- Decreto n. 2017-1416 del 28 settembre 2017 relativo alla firma elettronica.

Nel caso in cui la firma elettronica richieda la fornitura di dati personali, le normative applicabili derivano dai seguenti testi:

- regolamento (UE) 2016/679 del Parlamento europeo e del Consiglio del 27 aprile 2016, regolamento sulla protezione delle persone per quanto riguarda il trattamento dei dati personali e la libera circolazione di tali dati, e l'abrocciamento della direttiva 95/46/EC (regolamento generale sulla protezione dei dati);
- Legge 78-17 del 6 gennaio 1978 relativa a computer, fascicoli e libertà.

Questo documento, "Electronic Signing Policy of Teleservice Box Qualifications", descrive tutte le regole e le disposizioni che definiscono i requisiti a cui ciascuno degli attori coinvolti in questi scambi smaterializzati è conforme alla trasmissione e alla ricezione di flussi.

Il presente documento ha lo scopo di:

- Gli organi competenti;
- potenziali fornitori che partecipano a questi scambi smaterializzati per conto di queste agenzie di beneficiaria.

Nel resto di questo documento:

- Le suddette organizzazioni dei destinatari sono denominate "destinatari";
- gli scambi sopra riscaldati sono indicati come "record";
- il servizio nazionale Business Bank è indicato come il "servizio Business Box Office".

Ambito
------

La firma elettronica è richiesta per qualsiasi formalità relativa a una richiesta di riconoscimento della qualifica professionale ai fini del Tema europeo e della direttiva del Consiglio 2005/36/EC del 7 settembre 2005 relativa al riconoscimento delle qualifiche professionali ("direttiva sulle qualifiche professionali") e al suo esercizio, citato nell'articolo R. 123-30-8 del codice di commercio.

L'articolo 9 dell'ordinanza n. 2016-1809 del 22 dicembre 2016 prevede che le persone interessate da tali formalità,

*Qualsiasi cittadino di uno Stato membro dell'Unione europea o parte dell'accordo sullo Spazio economico europeo che abbia acquisito le sue qualifiche professionali in uno Stato membro dell'Unione europea o parte dell'accordo sullo Spazio economico europeo, e che desiderano ottenere il riconoscimento di tali professioni per l'esercizio di una professione regolamentata ai sensi della direttiva 2005/36/EC del Parlamento europeo e del Consiglio, può completare o controllare in modo remoto e elettronico, attraverso un'unica finestra, tutti i requisiti, le procedure o le formalità relative al riconoscimento delle proprie qualifiche professionali a condizioni stabilite dal Decreto in seno al Consiglio di Stato.*

La firma elettronica ha caratteristiche diverse a seconda della natura dei file inviati.

**Nel caso in cui si applichino le disposizioni giuridiche stabilite nell'articolo 123-30-10 del Codice di**, la firma elettronica richiesta soddisfa le disposizioni della prima frase del secondo paragrafo dell'articolo 1316-4 del codice civile:

*Quando è elettronico, consiste nell'uso di un processo di identificazione affidabile che ne garantisca il collegamento con l'atto a cui si collega.*

**Nel caso in cui si applichino disposizioni giuridiche più rigorose di quelle stabilite nell'articolo R. 123-30-10 del Codice di** (ad esempio il riconoscimento della qualifica professionale per l'attività di un medico generico), la firma elettronica soddisfa le disposizioni dell'articolo 1 del decreto n. 2017-1416 del 28 settembre 2017 (riferimento al regolamento "eIDAS" n.910/2014):

*L'affidabilità di un processo di firma elettronica è presunta, fino a prova contraria, quando questo processo implementa una firma elettronica qualificata.*

*Una firma elettronica è una firma elettronica avanzata, conformemente all'articolo 26 del suddetto regolamento e creata utilizzando un dispositivo di creazione della firma elettronica qualificato che soddisfi i requisiti dell'articolo 29 di tale regolamento, che si basa su un certificato qualificato come firma elettronica conforme ai requisiti dell'articolo 28 di tale regolamento.*

Articolo 26 del regolamento "eIDAS" 910/2014:

*Una firma elettronica avanzata soddisfa i seguenti requisiti:*

# *Siate legati al firmatario in modo inequivocabile;*
# *consentire l'imposizione del firmatario;*
# *Essere stati creati utilizzando dati di creazione di firme elettroniche che il firmatario può utilizzare sotto il loro controllo esclusivo con un alto livello di attendibilità;*
# *Essere collegati ai dati associati a questa firma in modo che eventuali modifiche successive ai dati siano rilevabili.*

Per informazioni, tre livelli di garanzia sono forniti dal regolamento "eIDAS" 910/2014:

- *Basso* A questo livello, l'obiettivo è semplicemente quello di ridurre il rischio di uso improprio o manomissione dell'identità;
- *Sostanziale* A questo livello, l'obiettivo è quello di ridurre sostanzialmente il rischio di uso improprio o manomissione dell'identità;
- *alto* A questo livello, l'obiettivo è quello di prevenire l'uso improprio o l'alterazione dell'identità.

### Identificazione

L'identificazione di questo criterio di firma elettronica è ciò indica la presenza del certificato Company Box Service utilizzato per la firma elettronica del documento.

Il numero di serie del certificato è:

4B-51-5A-35-00-00-00-00-01-EF.

### Pubblicazione del documento

Questa politica di firma elettronica è pubblicata in seguito alla sua approvazione da parte di Information Systems Security Manager (RSSI) dell'Enterprise Box Office.

### Processo di aggiornamento

#### Circostanze che rendono necessario un aggiornamento

L'aggiornamento di questa politica di firma elettronica può includere l'evoluzione della legge in vigore (vedi "Oggetto del documento"), l'emergere di nuove minacce e nuove misure di sicurezza, e la considerazione delle osservazioni dei vari attori.

Questa politica di firma elettronica viene riesaminata almeno ogni due anni.

#### Tenendo conto delle osservazioni

Tutti i commenti, o desideri di modifica, su questa politica di firma elettronica devono essere mandati a:[Contattare](mailto:contact.guichet-entreprises@finances.gouv.fr).

Questi commenti e desideri di sviluppo sono esaminati dal RSSI del servizio Business Box Office, che, se necessario, avvia il processo di aggiornamento di questa politica di firma elettronica.

#### Informazioni degli attori

Informazioni sulla versione corrente di questo criterio e sulle versioni precedenti sono disponibili nella tabella delle versioni all'inizio di questo documento.

La pubblicazione di una nuova versione della politica di firma è costituita da:

# Inserimento della politica di firma elettronica online in formato HTML
# archiviare la versione precedente dopo che l'etichetta "obsoleta" è stata apposto per ogni pagina.

Entra in vigore una nuova versione e un periodo di validità
-----------------------------------------------------------

Una nuova versione del criterio di firma entra in vigore solo un (1) mese di calendario dopo che è pubblicato online e rimane valido fino a quando non entra in vigore una nuova versione.

Il ritardo di un mese viene utilizzato dai destinatari per tener conto nelle loro applicazioni delle modifiche apportate dal nuovo criterio di firma elettronica.

Attori
------

### Il firmatario del file

Il firmatario del fascicolo è il dichiarante o il suo agente che fa la formalità di richiedere il riconoscimento della qualifica professionale

#### Il ruolo del firmatario

Il ruolo del firmatario è quello di mettere la sua firma elettronica sul suo fascicolo incorporando la formalità e i documenti giustificativi,

Per inserire una firma elettronica sul suo file, il firmatario si impegna a utilizzare uno strumento di firma in conformità con la presente politica di firma elettronica.

#### Obblighi del firmatario

Tali obblighi sono descritti nei capitoli seguenti.

### Strumento di firma utilizzato

Il firmatario deve controllare i dati che firmerà prima di firmare la firma elettronica.

### Procedura di firma elettronica utilizzata

La procedura di firma elettronica dipende dal tipo di formalità (vedere "Campo di applicazione").

**Nel caso in cui si applichino le disposizioni giuridiche stabilite nell'articolo 123-30-10 del Codice di**, il firmatario deve selezionare una casella alla fine della formalità indicando che dichiara in onore l'accuratezza delle informazioni della formalità e firma questa affermazione.

È poi menzionato nello spazio di firma del modulo (Cerfa), della conformità di questa firma alle aspettative del paragrafo 3 dell'articolo A. 123-4 del codice di commercio.

Paragrafo 3 dell'articolo A. 123-4 del Codice di Commercio:

*(3) Selezionando la scatola del computer prevista a questo scopo, il dichiarante dichiara in onore l'accuratezza degli elementi dichiarati secondo la seguente formula: "Dichiaro in onore l'accuratezza delle informazioni della formalità e firmo questa affermazione no...., fatta a..., il.... ».*

**Nel caso in cui si applichino disposizioni giuridiche più rigorose di quelle stabilite nell'articolo R. 123-30-10 del Codice di**, il firmatario deve allegare una copia della sua copia dichiarata in conformità con l'originale conformemente alle disposizioni del paragrafo 2 dell'articolo A. 123-4 del codice di commercio:

Paragrafo 2 dell'articolo A. 123-4 del Codice di Commercio:

*(2) I documenti che lo compongono sono stati digitalizzati. La copia della prova d'identità viene scansionata dopo che è stata precedentemente fornita con un certificato scritto a mano di prova dell'onore di conformità con l'originale, una data e la firma scritta a mano della persona che effettua la dichiarazione.*

Gli viene presentato il documento da firmare elettronicamente (modulo Cerfa e copia dell'ID), nonché l'accordo di prova che elenca le condizioni e le conseguenze della firma elettronica del documento.

Il firmatario deve selezionare una casella che indichi di aver letto l'accordo di prova e di averlo accettato senza riserve.

Un codice per convalidare la firma elettronica gli viene inviato sul suo telefono cellulare, il cui numero ha indicato quando ha creato il suo account.

L'immissione di questo codice attiva il sigillo del documento (form/Cerfa e copia dell'ID), ovvero la firma elettronica basata su un certificato, il relativo hash e l'indicazione dell'ora del set.

Il documento sigillato viene archiviato e inoltrato ai destinatari interessati.

I casi più comuni di errore sono:

**Caso 1 - Codice errato**

Custodia funzionale:

*L'utente ha immesso un codice non corretto (ad esempio, lettere anziché numeri).*

Esempio di messaggio visualizzato:

*Il codice fornito non è corretto. Si prega di controllare il codice che è stato inviato via SMS e reinserirlo.*

**Caso 2 - Codice scaduto**

Custodia funzionale:

*L'utente ha inserito il codice ricevuto via SMS troppo tardi (la durata di un OTP - One Time Password code è di 20 minuti).*

Esempio di messaggio visualizzato:

*La validità del codice è scaduta. È necessario rinnovare la domanda per una firma elettronica.*

**Caso 3 - Codice bloccato**

Custodia funzionale:

*L'utente ha effettuato N sequestri di codici OTP senza successo. Al fine di evitare tentativi di hacking, il numero di telefono dell'utente viene identificato come bloccato nel sistema.*

Esempio di messaggio visualizzato:

*A causa di numerosi errori di digitazione da parte tua, il tuo numero di telefono non è più autorizzato a firmare la tua procedura elettronicamente. Ti invitiamo a fornire un nuovo numero di telefono nel tuo account utente o a contattare la Business Box per sbloccare il numero di telefono corrente.*

### Protezione e utilizzo del file firmato

Questo capitolo si occupa solo di fascicoli relativi a formalità per le quali si applicano disposizioni giuridiche più rigorose di quelle stabilite nell'articolo 123-30-10 del codice di commercio.

Il documento firmato elettronicamente è conservato in una cassaforte elettronica con un file leggibile recante tutte le tracce della sua guarnizione a scopo di audit.

La cassaforte elettronica è accessibile solo dal suo amministratore e dall'RSSI del Servizio Business Box.

L'accesso alla cassaforte elettronica è possibile solo con l'aiuto di un certificato di nome e qualsiasi azione su di esso è tracciata.

Per l'RSSI del servizio Enterprise Box Office, è consentito l'accesso solo a scopo di audit su richiesta giustificata (ad esempio, richiesta del tribunale).

Il documento firmato viene inoltrato alle agenzie destinatari incaricate di analizzare il file associato.

La protezione del fascicolo trasmesso elettronicamente è di competenza del destinatario e deve essere conforme alle disposizioni del regolamento di sicurezza generale e del regolamento generale sulla protezione dei dati.

Il RSSI del servizio Business Box Office garantisce la conformità a queste disposizioni.

### Archiviazione ed eliminazione dei file firmati

Il fascicolo viene archiviato ed eliminato in conformità con le disposizioni della sezione R. 123-30-12 del Codice di Commercio.

Articolo R. 123-30-12 del Codice di Commercio:

*Se il dichiarante utilizza un servizio provvisorio di conservazione dei dati proposto dal servizio di segnalazione a condizioni coerenti con la legge 78-17 del 6 gennaio 1978 relativa a computer, fascicoli e libertà, tutti i dati e i file relativi al registrante sui media informatici in cui sono elencati vengono cancellati al termine del periodo di conservazione provvisorio fino a dodici mesi.*

Fornitori di soluzioni per la firma elettronica
-----------------------------------------------

La soluzione deve incorporare le aspettative di questa politica di firma elettronica nella struttura dei dati della firma.

Il Business Box Office
----------------------

Il RSSI del servizio Business Box Office garantisce la conformità con le disposizioni di questa politica di firma elettronica per le qualifiche Teleservice Box messe a disposizione degli utenti.

Verifica che i sistemi di informazione di origine e destinatario dei feed non costituiscano un rischio per l'integrità e la riservatezza dei file memorizzati e trasmessi.

Destinatari
-----------

I destinatari monitorano l'integrità della firma elettronica del file e assicurano che le procedure per l'archiviazione e l'eliminazione dei file ricevuti siano conformi alle normative.

### Dati di verifica

Per effettuare gli audit, l'Enterprise Box Office utilizza le informazioni di sifatura del file firmato elettronicamente (vedere "Protezione e utilizzo del file firmato").

### Proteggere i mezzi

L'Enterprise Box Office assicura che siano implementati i mezzi necessari per proteggere le apparecchiature che forniscono i servizi di convalida.

Le misure adottate riguardano sia:

- proteggere l'accesso fisico e logico alle apparecchiature solo alle persone autorizzate;
- Disponibilità del servizio
- monitoraggio e monitoraggio del servizio.

### Assistenza ai firmatari

L'assistenza nell'uso della procedura di firma elettronica è fornita dal supporto del servizio Business Box accessibile via e-mail all'indirizzo:[Supporto](mailto:support.guichet-entreprises@helpline.fr).

Firma elettronica e convalida
-----------------------------

### Dati firmati

I dati firmati sono costituiti dal modulo/Cerfa e dalla copia dell'ID sotto forma di un singolo file PDF con un'area modificabile per la firma elettronica.

L'intero file PDF è firmato e infatti nessuna modifica ad esso sono possibili dopo che è stato sigillato.

### Caratteristiche della firma

La firma soddisfa le aspettative della specifica "XML Signature Syntax and Processing (XMLDsig)" sviluppata da W3C (World Wide Web Consortium:<https://www.w3.org/>) nonché le estensioni di formato di firma specificate nello standard XADES (European XML Electronic Signature) dell'ETSI (<https://www.etsi.org>).

La firma elettronica distribuita implementa un "sostanziale" livello di identificazione (ISO/IEC 29115:2013), vale a dire in conformità con il regolamento eIDAS una firma elettronica avanzata

#### Tipo di firma

La firma elettronica è di tipo[Avvolto](https://fr.wikipedia.org/wiki/XML_Signature).

#### Standard di firma

La firma elettronica deve essere conforme allo standard[XMLDsig, revisione 1.1 di
Febbraio 2002](https://www.w3.org/TR/2002/REC-xmldsig-core-20020212).

La firma elettronica deve soddisfare lo standard XAdES-EPES (Explicit Policy based Electronic Signature),[ETSI TS 101 903 v1.3.2](http://uri.etsi.org/01903/v1.3.2).

In conformità con lo standard XadES, SignedProperties (SignedProperties/SignedSignatureProperties) deve contenere quanto segue:

- SigningCertificate
- data e ora di firma (SigningTime) in formato UTC.

Algoritmi che possono essere utilizzati per la firma
----------------------------------------------------

#### Algoritmo di condensazione

Il calcolo dell'iterazione condensato della funzione di compressione sul resto dei blocchi ottenuti tagliando il messaggio (schema iterativo di Merkle-Damgord).

Questo algoritmo è accessibile al link:[Merkle-Damgord](https://fr.wikipedia.org/wiki/Construction_de_Merkle-Damgård)

#### Algoritmo di firma

L'algoritmo di firma si basa su RSA/[SHA 256](https://fr.wikipedia.org/wiki/SHA-2).

#### Algoritmo di conversione in formato canonico

L'algoritmo di conversione in formato canonico è[c14N](https://www.w3.org/TR/xml-exc-c14n/).

### Condizioni per la dichiarazione di file firmato valido

Le condizioni per dichiarare valido il file firmato sono:

- Il firmatario ha adempiuto ai suoi obblighi:- scaricando una copia del suo documento d'identità con una copia conforme all'originale e accettando l'accordo di prova,
  + inserendo il codice inviato dal metodo SMS ("One Time Password");
- Il Business Box Office ha adempiuto ai suoi obblighi:- mettendo a disposizione un certificato generato da un'autorità di certificazione che ne certifica l'origine (fornitore del servizio di firma elettronica) e le cui caratteristiche sono:*Chiave pubblica - RSA 2048 bit, algoritmo di firma - RSA SHA 256* ;
- Il fornitore di servizi di e-signature ha adempiuto ai suoi obblighi:- la firma e il sigillamento del file PDF in base alle caratteristiche descritte nei capitoli precedenti.

La verifica della validità della firma può essere effettuata tramite Adobe Acrobat.

Disposizioni
------------

### Dati nominali

Il vettore ha il diritto di accedere, modificare, correggere ed eliminare i dati su di lui che può esercitare via e-mail al seguente indirizzo:[Contattare](mailto:contact.guichet-entreprises@finances.gouv.fr)

### Diritto applicabile - Risoluzione delle controversie

Tali disposizioni sono soggette al diritto francese.

Qualsiasi controversia sulla validità, l'interpretazione o l'esecuzione di tali disposizioni sarà deferita alla giurisdizione del tribunale amministrativo di Parigi.

