﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="La carte professionnelle européenne (CPE)" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(key)="fr-comprendre-carte_professionnelle_europeenne" -->
<!-- var(last-update)="2020-05-03 15:05:01" -->
<!-- var(lang)="it" -->
<!-- var(site:lang)="it" -->

La carta professionale europea (CPE)
====================================


<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
Ultimo aggiornamento:<!-- begin-var(last-update) -->2020-05-03 15:05:01<!-- end-var -->

Alcune professioni regolamentate beneficiano del sistema europeo delle carte professionali. In questo articolo troverete tutte le informazioni sul suo funzionamento, le formalità di ottenerlo e le professioni regolamentate che riguarda.

Cos'è il biglietto da visita europeo?
-------------------------------------

La carta professionale europea (ECC) è una procedura elettronica per il riconoscimento delle qualifiche professionali tra gli Stati membri dell'UE definita dagli articoli da 4 a 4[Direttiva 2005/36/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036). Il suo obiettivo è quello di facilitare l'esercizio di una professione regolamentata per i cittadini europei in un altro Stato membro dell'UE. Questa procedura fa parte della mobilità professionale dei cittadini europei.

###### Buono a sapersi

Non si tratta di una carta fisica, ma di una prova elettronica che le qualifiche professionali del cittadino sono state verificate e sono riconosciute dalle autorità competenti del paese ospitante.

Chi può beneficiarne?
---------------------

Questo dispositivo può essere utilizzato da qualsiasi professionista europeo e nell'ambiente nel paese ospitante per esercitarsi lì ([stabilimento libero](https://www.guichet-entreprises.fr/it/eugo/libre-etablissement-le/)), solo nel contesto della fornitura di servizi su base temporanea ([fornitura gratuita del servizio](https://www.guichet-entreprises.fr/it/eugo/libre-prestation-de-services-lps/)).

Ad oggi, il CPE è disponibile per cinque occupazioni:

# Agente immobiliare;
# Guida alpina;
# Infermiera;
# Fisioterapista;
# Farmacista.

Queste professioni sono state selezionate per beneficiare del sistema CPE sulla base di tre criteri:

- mobilità significativa, o un potenziale di mobilità significativo, nella professione interessata;
- un interesse sufficiente dimostrato da parti con un interesse legittimo;
- regolamentazione della professione o della formazione ad essa associata in un numero significativo di Stati membri dell'UE.

Si prevede che questo numero di professioni crescerà sapendo che alcuni, come ingegneri o infermieri specializzati, sono attualmente in fase di valutazione.

Perché chiederlo?
-----------------

Il CPE è un certificato elettronico. Esso mira a semplificare il riconoscimento delle qualifiche professionali e a introdurre un ulteriore livello di efficienza sia per i cittadini che per le autorità competenti responsabili della sua emissione.

Quando si tratta di**fornitura gratuita del servizio**, il CPE è inteso a sostituire la dichiarazione prima del primo beneficio nello Stato membro ospitante, che deve essere rinnovata ogni 18 mesi.

Quando si tratta di**Istituzione**, il CPE è la decisione di riconoscere le qualifiche professionali da parte dello Stato membro ospitante.

###### Buono a sapersi

I professionisti interessati dal CPE possono scegliere tra il suo dispositivo o le procedure tradizionali per la pre-segnalazione o il riconoscimento delle qualifiche professionali.

###### Per andare oltre

[Carta professionale europea](https://europa.eu/youreurope/citizens/work/professional-qualifications/european-professional-card/index_fr.htm) (Commissione europea)

