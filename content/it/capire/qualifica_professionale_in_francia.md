﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt) -->
<!-- include-file(generated.txt) -->

<!-- var(collapsable)="close" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Ottenere il riconoscimento della mia qualifica professionale in Francia" -->
<!-- var(key)="fr-comprendre-qualification_professionnelle_en_france" -->
<!-- var(last-update)="2020-07-15 14:48:31" -->
<!-- var(lang)="it" -->
<!-- var(site:lang)="it" -->

Ottenere il riconoscimento della mia qualifica professionale in Francia
=======================================================================


<!-- begin-include(disclaimer-trans-it) -->
<!-- end-include -->
Ultimo aggiornamento:<!-- begin-var(last-update) -->2020-05-03 11:32:08<!-- end-var -->

Sei un cittadino dell'Unione europea o dello Spazio economico europeo e vorresti che le tue qualifiche professionali riconoscessero nel tuo paese d'origine?<!-- collapsable:off -->
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Il sito web guichet-qualifications.fr incoraggia la mobilità professionale dei residenti dell'Unione europea e dello Spazio economico europeo. I servizi online guichet-entreprises.fr e guichet-qualifications.fr sono lo sportello unico riconosciuto dalla Commissione europea. Ti permettono di esplorare nuove opportunità e sviluppare un'attività sul territorio francese completando le tue formalità online.

Su questo sito web, puoi fare un riconoscimento della tua qualifica professionale con l'autorità competente:

- Ho una laurea presso uno Stato membro dell'UE o del CEA e vorrei[di avere la mia qualifica professionale riconosciuta in Francia](diploma_stato_membro_ue_eee.md)
- Ho un diploma francese e vorrei[di avere la mia qualifica professionale riconosciuta in uno Stato membro dell'UE o del CEA](diploma_francese.md)

Creare un'attività in Francia con guichet-entreprises.fr
--------------------------------------------------------

Una volta riconosciuta la tua qualifica professionale, hai l'opportunità di impostare la tua attività online su[guichet-entreprises.fr](https://www.guichet-entreprises.fr) ! Troverete anche informazioni utili sulle attività regolamentate in Francia e le condizioni di installazione.

Attività regolamentate<!-- collapsable:close -->
------------------------------------------------

Scopri quali condizioni devi seguire per svolgere un'attività regolamentata in Francia.

[Scopri le schede informative](https://www.guichet-entreprises.fr/it/activites-reglementees/)

Stabilimento gratuito<!-- collapsable:close -->
-----------------------------------------------

Ora è possibile stabilirsi in Francia e svolgere la vostra attività liberamente, e questo, in modo permanente.

[Ulteriori informazioni sull'istituzione gratuita](https://www.guichet-entreprises.fr/it/eugo/libre-etablissement-le/)

Fornitura gratuita di servizi<!-- collapsable:close -->
-------------------------------------------------------

Offrire servizi temporanei o occasionali in Francia non è mai stato così facile !

[Ulteriori informazioni sull'erogazione gratuita dei servizi](https://www.guichet-entreprises.fr/it/eugo/libre-prestation-de-services-lps/)

