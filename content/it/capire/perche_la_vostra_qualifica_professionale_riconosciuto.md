﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Pourquoi faire reconnaître sa qualification professionnelle ?" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(key)="fr-comprendre-pourquoi_faire_reconnaitre_sa_qualification_professionnelle" -->
<!-- var(last-update)="2020-05-03 15:05:24" -->
<!-- var(lang)="it" -->
<!-- var(site:lang)="it" -->

Perché le vostre qualifiche professionali sono state riconosciute?
==================================================================


<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
Ultimo aggiornamento:<!-- begin-var(last-update) -->2020-05-03 15:05:24<!-- end-var -->

Una professione è chiamata "regolata" quando è necessario tenere un diploma specifico, aver superato esami specifici, aver ottenuto una certa autorizzazione o essere registrato presso un organismo professionale per eseguirlo.

**Ottenere il riconoscimento delle qualifiche professionali** è imperativo quando si desidera lavorare in un altro paese dell'UE in cui la vostra professione è regolamentata. Ciò significa che la sua formazione e/o esperienza professionale sono ufficialmente riconosciute dal paese ospitante.

- Se si tratta di trasferirsi in un altro paese per praticare la sua professione, è nel caso della[stabilimento libero](https://www.guichet-entreprises.fr/it/eugo/libre-etablissement-le/) ed è essenziale rendere il riconoscimento delle sue qualifiche.
- Se si tratta di fornire servizi su base temporanea in un altro paese, il caso è quello di[fornitura gratuita del servizio](https://www.guichet-entreprises.fr/it/eugo/libre-prestation-de-services-lps/) ed è sufficiente una dichiarazione di pre-esercizio. Esiste un'eccezione per le professioni con gravi implicazioni per la salute o la sicurezza, in quanto lo Stato membro ospitante può richiedere un controllo pre-qualifica.

Per conoscere le regole che si applicano alla tua situazione, contatta l'autorità nazionale responsabile dell'accesso alla tua professione nel paese ospitante.

È inoltre possibile consultare il[Banca dati europea delle professioni regolamentate](http://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=homepage) che elenca tutte le professioni regolamentate nei paesi dell'UE e le loro autorità competenti.

- Se la tua professione è regolamentata nel tuo paese, puoi cercarla nella tua lingua e visualizzare la traduzione inglese fornita nella descrizione; è quindi necessario cercare il nome inglese, al fine di ottenere un elenco degli altri paesi in cui questa occupazione è regolata. Se il paese in cui si desidera stabilirsi non appare, può significare che la professione non è regolamentata.
- Se non riesci a trovare la tua professione nel database, puoi contattare i National Information Points per il riconoscimento delle qualifiche professionali nel paese in cui desideri lavorare. Possono aiutarti a trovare l'autorità appropriata e a determinare i documenti richiesti.

**Leggi anche:** [Carta Professionale Europea (CPE)](biglietto_da_visita_europeo.md)

