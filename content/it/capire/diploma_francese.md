﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Je suis titulaire d'un diplôme français..." -->
<!-- var(key)="fr-comprendre-diplome_francais" -->
<!-- var(last-update)="2020-05-03 15:05:19" -->
<!-- var(lang)="it" -->
<!-- var(site:lang)="it" -->

Ho una laurea in francese...
============================


<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
Ultimo aggiornamento:<!-- begin-var(last-update) -->2020-05-03 15:05:19<!-- end-var -->

... e vorrei che la mia qualifica professionale riconoscesse in uno Stato membro dell'Unione europea o nello Spazio economico europeo
-------------------------------------------------------------------------------------------------------------------------------------

Se sei un cittadino di uno Stato membro dell'Unione europea (UE) o dello Spazio economico europeo (AEA), che ha conseguito un diploma francese o un diploma rilasciato da un paese terzo e riconosciuto dalla Francia, devi avere riconosciuto la tua qualifica professionale prima di iniziare a praticare in uno Stato membro dell'UE o del SEA.

Voglio trasferirmi in un paese dell'UE o del SEA in cui la mia professione è regolamentata
------------------------------------------------------------------------------------------

Prima di iniziare a lavorare in un paese, scopri le professioni regolamentate in quel paese. Se la tua professione è regolamentata, devi farti riconoscere la tua qualifica professionale. E 'il[autorità competente](https://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=homepage) il paese in cui vi state instaurando per svolgere la vostra attività, che è il riferimento per quanto riguarda i passi da seguire.

Una volta ottenuto il riconoscimento, puoi praticare la tua professione di cittadini che vi si sono laureati.

Desidero erogare un assegno una tantera in uno Stato membro in cui la mia professione è regolamentata
-----------------------------------------------------------------------------------------------------

Per fornire un servizio temporaneo all'estero, è necessario essere stabiliti nel proprio paese di origine, ma senza l'obbligo di praticare e il riconoscimento delle qualifiche professionali non è necessario, solo una dichiarazione anticipata scritta è sufficiente. A seconda del grado di impatto della professione sulla salute o sulla sicurezza, il paese membro può chiedere di verificare la propria qualifica professionale. Un rinnovo della dichiarazione può essere richiesto annualmente se si continua a lavorare temporaneamente in quel paese.

**Buono a sapersi**

Se la professione non è regolamentata nello Stato ospitante, la valutazione del diploma e del livello professionale appartiene al datore di lavoro.

Passi
-----

Le autorità del paese in cui si fa domanda per il riconoscimento delle proprie qualifiche professionali hanno un mese per confermare la domanda e chiedere altri documenti giustificativi.

Una volta ricevuto il file completo, le autorità decideranno entro un lasso di tempo:

- 3 mesi se sei un medico, un infermiere di assistenza generale, un'ostetrica, un veterinario, un dentista, un farmacista o un architetto e benefici di un[riconoscimento automatico](https://europa.eu/youreurope/citizens/work/professional-qualifications/european-professional-card-documents/index_fr.htm#103720) ;
- 4 mesi per tutte le altre occupazioni.

Pertanto, concedete 5 mesi durante i quali avviene il riconoscimento della qualifica, un prerequisito essenziale prima di esercitare legalmente la professione nello Stato membro in cui offrite il vostro servizio. Dopo quel tempo,[contattare la linea di assistenza](https://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=yec_pq) o parlare con[informazioni nazionali sul riconoscimento delle qualifiche professionali](https://ec.europa.eu/growth/single-market/services/free-movement-professionals_fr/#contacts).

Andare oltre
------------

Link utili (sito web della Commissione europea):

- [La banca dati sulle occupazioni regolamentate elenca le occupazioni regolate dal paese dell'UE e dalle autorità](https://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=homepage)
- [Elenco dei siti web nazionali per le professioni regolamentate](https://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=sites)
- Per autenticare l'autorità nazionale competente, è possibile contattare un[Punto di informazione nazionale sul riconoscimento delle qualifiche professionali](https://ec.europa.eu/growth/single-market/services/free-movement-professionals_fr/#contacts).

Elenco degli Stati membri dell'UE
---------------------------------

Germania, Austria, Belgio, Bulgaria, Cipro, Croazia, Danimarca, Spagna, Estonia, Finlandia, Francia, Grecia, Ungheria, Irlanda, Italia, Lettonia, Lituania, Lussemburgo, Malta, Paesi Bassi, Polonia, Portogallo, Romania, Regno Unito, Slovacchia, Slovenia, Svezia, Repubblica Ceca.

Stati dello Spazio economico europeo
------------------------------------

Questi sono i 28 paesi membri dell'Unione europea (UE) sopra elencati e i seguenti 3 paesi: Norvegia, Islanda, Liechtenstein.

