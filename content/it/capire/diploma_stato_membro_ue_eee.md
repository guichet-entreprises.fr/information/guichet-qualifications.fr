﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Je suis titulaire d’un diplôme d’un État membre de l’Union européenne ou de l’Espace économique européen..." -->
<!-- var(key)="fr-comprendre-diplome_etat_membre_ue_eee" -->
<!-- var(last-update)="2020-05-03 15:05:10" -->
<!-- var(lang)="it" -->
<!-- var(site:lang)="it" -->

Ho una laurea in uno Stato membro dell'Unione europea o dello Spazio economico europeo...
=========================================================================================


<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
Ultimo aggiornamento:<!-- begin-var(last-update) -->2020-05-03 15:05:10<!-- end-var -->

... e voglio che la mia qualifica professionale venga riconosciuta in Francia.
------------------------------------------------------------------------------

La Francia ha quasi 250 professioni regolamentate che possono essere identificate in due categorie:

- Le professioni liberali;
- professioni artigianali e commerciali per le quali il riconoscimento della qualificazione professionale viene effettuato con la Camera dei mestieri e dell'artigianato da cui dipendete.

Voglio trasferirmi in Francia
-----------------------------

Sono necessari due passi per poter praticare una professione regolamentata in Francia:

# Riconoscimento della qualificazione professionale per i cittadini stranieri dell'Unione europea (UE) o dello Spazio economico europeo (AEA);
# autorizzazione a praticare con l'autorità competente della professione interessata (autorizzazione aggiuntiva a seconda della professione).

Le[schede informative sulle professioni regolamentate](professioni_regolamentate.md) identificare questi passaggi e indirizzarvi attraverso le normative esistenti.

Si noti che le professioni riconosciute dal[Carta professionale europea](biglietto_da_visita_europeo.md) (CPE) può quindi deviare dalla procedura tradizionale:

- Infermiere di cura generale;
- Farmacisti;
- fisioterapisti;
- guide alpine;
- agenti immobiliari.

Voglio emettere un beneficio temporaneo in Francia
--------------------------------------------------

Non è necessario utilizzare un riconoscimento di qualifica professionale per un'indennità temporanea, tuttavia è necessario essere stabiliti nel proprio paese. In alcuni casi, dovrai richiedere il permesso prima del tuo beneficio.

Passi
-----

Il contatto con l'autorità competente è più spesso da prendere nella zona in cui si decide di stabilirsi. Devono essere forniti diversi documenti giustificativi, alcuni dei quali a volte richiedono una traduzione giurata.

**Buono a sapersi**

Se la tua professione è regolamentata nel tuo paese di origine ma non nel paese ospitante (Francia), puoi esercitarti senza dover fare una qualifica professionale, come i cittadini del paese ospitante.

### Elenco degli Stati membri dell'UE

Germania, Austria, Belgio, Bulgaria, Cipro, Croazia, Danimarca, Spagna, Estonia, Finlandia, Francia, Grecia, Ungheria, Irlanda, Italia, Lettonia, Lituania, Lussemburgo, Malta, Paesi Bassi, Polonia, Portogallo, Romania, Regno Unito, Slovacchia, Slovenia, Svezia, Repubblica Ceca.

### Stati dello Spazio economico europeo

Questi sono i 28 paesi membri dell'Unione europea (UE) sopra elencati e i seguenti 3 paesi: Norvegia, Islanda, Liechtenstein.

