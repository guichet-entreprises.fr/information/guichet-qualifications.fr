﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Professions réglementées" -->
<!-- var(key)="fr-comprendre-professions_reglementees" -->
<!-- var(last-update)="2020-05-03 15:05:27" -->
<!-- var(lang)="it" -->
<!-- var(site:lang)="it" -->

Professioni regolamentate
=========================


<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
Ultimo aggiornamento:<!-- begin-var(last-update) -->2020-05-03 15:05:27<!-- end-var -->

Architetto, infermiere, veterinario, panettiere, ecc., prima di ottenere il riconoscimento della qualifica professionale, scopri le condizioni di accesso a una professione regolamentata in Francia.

Dai un'occhiata alle nostre schede informative, suddivise per gruppi professionali.

Norme specifiche in Francia
---------------------------

In Francia, alcune professioni richiedono una designazione professionale, una qualifica professionale o addirittura un'autorizzazione preventiva per la loro pratica, sanzionando un certo livello di formazione o esperienza. I registri professionali forniscono un aggiornamento su come riconoscere le qualifiche professionali e la legislazione applicabile a queste diverse professioni. Ad oggi, quasi 250 attività sono coinvolte e sono oggetto dei nostri registri professionali.

**Lo sapevi?** Le carte professionali vengono messe online non appena sono scritte dal Business Box Office in collaborazione con le amministrazioni competenti.

Se la scheda informativa sulla qualifica professionale non è disponibile,[Banca dati europea delle professioni regolamentate](https://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=homepage).

