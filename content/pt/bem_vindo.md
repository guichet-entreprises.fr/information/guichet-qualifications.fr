﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Bienvenue" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(key)="welcome" -->
<!-- var(translation)="Auto" -->
<!-- var(last-update)="2020-05-03 14:54:33" -->
<!-- var(lang)="pt" -->
<!-- var(site:lang)="pt" -->

Bem-vindo
=========


<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
Última atualização:<!-- begin-var(last-update) -->2020-05-03 14:54:33<!-- end-var -->

Você encontrará neste site todas as informações sobre o procedimento de reconhecimento da qualificação profissional.<!-- alert-start:info -->
<!-- alert-end:info -->

Primeira visita a www.guichet-qualifications.fr
-----------------------------------------------

Guichet-qualifications.fr é a loja única para o reconhecimento de qualificações profissionais. Gratuito e acessível a todos, o serviço online permite construir uma pasta sem ter que viajar. Você encontrará no site[as folhas de fatos das profissões em causa](entender/profissoes_regulamentadas.md) reconhecimento da qualificação.

**Você sabia disso?** Os cartões profissionais são colocados on-line assim que são escritos pela Business Box Office em colaboração com as administrações relevantes. Sempre mostramos a data de suas últimas atualizações.

Reconhecimento da qualificação profissional: formalidades
---------------------------------------------------------

A qualificação profissional é imperativa quando se deseja trabalhar em outro país da União Europeia, no qual a profissão é regulamentada. Para saber as regras aplicáveis à sua situação, você precisa entrar em contato com a autoridade nacional responsável pelo acesso à sua profissão no país de acolhimento. Graças ao site www.guichet-qualifications.fr, você pode completar essas formalidades online sem ter que viajar.

**Leia também** :[Por que suas qualificações profissionais são reconhecidas?](https://www.guichet-qualifications.fr/pt/pourquoi-faire-reconnaitre-sa-qualification-professionnelle/)

Crie uma conta e identifique-se
-------------------------------

Para usar o serviço online, você está convidado a[criar um espaço pessoal](https://account.guichet-qualifications.fr/session/new) em www.guichet-qualifications.fr. Ele permitirá que você crie e gerencie seu arquivo e altere suas informações pessoais. Você será redirecionado para[www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/pt/) mas seus passos permanecerão visíveis na aba "Meus passos em guichet-qualifications.fr" no seu painel.

Com autenticação[FranceConnect](https://franceconnect.gouv.fr/), se você já tem uma conta em Impots.gouv.fr, Ameli.fr ou no site dos Correios, você pode fazer login em www.guichet-qualifications.fr usando uma dessas três contas.

Complete e valide seu arquivo on-line
-------------------------------------

O site www.guichet-qualifications.fr permite que você construa um arquivo on-line, anexe quaisquer documentos de suporte e pague quaisquer custos relacionados à formalidade. Uma vez que o arquivo tenha sido compilado e validado, ele é enviado ao órgão apropriado para processamento. Para rastrear seu arquivo, você pode entrar em contato com a autoridade relevante que recebe seu arquivo a qualquer momento.

**Você sabia disso?** Na França, existem 250 profissões regulamentadas (sob a Diretiva de Qualificação) listadas no[Profissões regulamentadas](entender/profissoes_regulamentadas.md). Se, no entanto, você não encontrar a sua profissão, ir para o[Banco de dados europeu de profissões regulamentadas](http://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=sites).

