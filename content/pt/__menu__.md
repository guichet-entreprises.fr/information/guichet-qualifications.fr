﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="[Accueil](index.md)" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__menu__" -->
<!-- var(last-update)="2020-05-03 14:55:02" -->
<!-- var(lang)="pt" -->
<!-- var(site:lang)="pt" -->

[Casa](index.md)
================

Entender
========

[](bem_vindo.md)
----------------

[](entender/por_que_ter_sua_qualificacao_profissional_reconhecida.md)
-----------------------------------------------------------------------------

[](entender/qualificacao_profissional_na_franca.md)
---------------------------------------------------------

[](entender/diploma_estado_membro_ue_eee.md)
--------------------------------------------

[](entender/diploma_frances.md)
----------------------------------

[](entender/profissoes_regulamentadas.md)
------------------------------------------

[](entender/cartao_de_visita_europeu.md)
--------------------------------------------------

Profissões regulamentadas
=========================

# <!-- include(../reference/pt/directive-qualification-professionnelle/_list_menu.md) -->

[Serviços](https://welcome.guichet-qualifications.fr/)
======================================================

[Meus arquivos](https://dashboard.guichet-qualifications.fr/) @authenticated
============================================================================

