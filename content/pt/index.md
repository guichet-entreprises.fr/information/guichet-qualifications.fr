﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(site:package)="https://gitlab.com/|13561843|latest|reference_md|reference/configuration.yml" -->
<!-- var(site:home)="Accueil" -->
<!-- var(site:menu)="__menu__.md" -->
<!-- var(site:footer)="__footer__/__links__.md" -->
<!-- var(site:lang)="pt" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Guichet Qualifications  " -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:font_sizer_off)="off" -->
<!-- var(page:breadcrumb_off)="off" -->
<!-- var(key)="main" -->
<!-- var(translation)="Auto" -->
<!-- var(last-update)="2020-05-03 14:55:01" -->
<!-- var(lang)="pt" -->

Qualificações de Bilheteria<!-- section-banner:surf.jpg --><!-- color:dark -->
==============================================================================

Um endereço para reconhecimento de suas qualificações

[Comece minha caminhada<!-- link-model:box-trans -->](https://profiler.guichet-qualifications.fr/start/)

Um endereço para reconhecimento de suas qualificações<!-- section-information:-120px -->
========================================================================================

Bem-vindo
---------

Você encontrará neste site todas as informações sobre o reconhecimento da qualificação profissional.

[Primeira visita<!-- link-model:box -->](bem_vindo.md)

Descobrir
---------

Você tem um diploma de um Estado-Membro da União Europeia ou da Área Econômica Europeia

[Suas possibilidades<!-- link-model:box -->](entender/qualificacao_profissional_na_franca.md)

Etapas online
-------------

Complete suas formalidades online relacionadas ao reconhecimento de suas qualificações profissionais na França!

[Começar<!-- link-model:box -->](https://profiler.guichet-qualifications.fr/start/)

Por que suas qualificações profissionais são reconhecidas?<!-- section:courses -->
==================================================================================

Uma profissão é chamada de "regulamentada" quando é necessário ter um diploma específico, ter passado por exames específicos, ter obtido uma determinada autorização ou ser registrado em um órgão profissional para realizá-lo.

A qualificação profissional é imperativa quando se deseja trabalhar em outro país da União Europeia, no qual a profissão é regulamentada. Isso significa ter sua formação e/ou experiência profissional oficialmente reconhecida pelo país anfitrião.[Leia mais...](entender/por_que_ter_sua_qualificacao_profissional_reconhecida.md)

Um único endereço para o reconhecimento de suas qualificações profissionais<!-- section:welcome --><!-- color:grey -->
======================================================================================================================

O serviço online guichet-qualifications.fr incentiva a mobilidade profissional dos residentes da União Europeia e da Área Econômica Europeia, fornecendo informações abrangentes sobre o acesso e o desprovido de profissões regulamentadas na França, com vistas ao reconhecimento da qualificação profissional.

Esse serviço é de iniciativa do Ministério da Economia e Finanças.

Fazendo negócios na França e europa<!-- section-stories:drapeaux.jpg --><!-- color:dark -->
===========================================================================================

Você é nacional da União Europeia ou da Área Econômica Europeia? Incentivamos a criação de negócios e a mobilidade profissional na França e na Europa. Guichet-qualifications.fr é reconhecido como a janela única francesa para o reconhecimento de suas qualificações profissionais, um membro da rede Eugo criada pela Comissão Europeia.

Uma vez que você tenha obtido o reconhecimento de sua qualificação, se você deseja se estabelecer permanente ou temporariamente para trabalhar na França, vá para:

[guichet-entreprises.fr](https://guichet-entreprises.fr/)

