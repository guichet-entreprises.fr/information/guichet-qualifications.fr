﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Je suis titulaire d'un diplôme français..." -->
<!-- var(key)="fr-comprendre-diplome_francais" -->
<!-- var(last-update)="2020-05-03 14:55:26" -->
<!-- var(lang)="pt" -->
<!-- var(site:lang)="pt" -->

Eu tenho um diploma de francês...
=================================


<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
Última atualização:<!-- begin-var(last-update) -->2020-05-03 14:55:26<!-- end-var -->

... e gostaria de ter minha qualificação profissional reconhecida em um Estado-Membro da União Europeia ou na Área Econômica Europeia
-------------------------------------------------------------------------------------------------------------------------------------

Se você é um cidadão de um Estado-Membro da União Europeia (UE) ou da Área Econômica Europeia (EEE), detentor de um diploma francês ou um diploma emitido por um país terceiro e reconhecido pela França, você deve ter sua qualificação profissional reconhecida antes de começar a praticar em um Estado-Membro da UE ou da EEE.

Eu quero me mudar para um país da UE ou EEE onde minha profissão é regulamentada
--------------------------------------------------------------------------------

Antes de começar a trabalhar em um país, descubra sobre as profissões regulamentadas naquele país. Se sua profissão for regulamentada, você precisa ter sua qualificação profissional reconhecida. É o[autoridade competente](https://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=homepage) o país em que você está se estabelecendo para realizar sua atividade que é a referência quanto aos passos a seguir.

Uma vez que você tenha obtido o reconhecimento, você pode exercer sua profissão como os nacionais que se formaram lá.

Eu quero entregar um benefício único em um Estado-Membro onde minha profissão é regulamentada
---------------------------------------------------------------------------------------------

Para prestar um serviço temporário no exterior, você deve ser estabelecido em seu país de origem, mas sem a obrigação de praticar e reconhecer qualificações profissionais não é necessário, apenas uma declaração prévia por escrito é suficiente. Dependendo do grau de impacto da profissão na saúde ou segurança, o país membro pode solicitar a verificação de sua qualificação profissional. Uma renovação da declaração pode ser necessária anualmente se você continuar a trabalhar temporariamente naquele país.

**É bom saber**

Se a profissão não for regulamentada no estado de acolhimento, a avaliação do diploma e do nível profissional pertence ao empregador.

Passos
------

As autoridades do país em que você solicita o reconhecimento de suas qualificações profissionais têm um mês para reconhecer sua solicitação e solicitar outros documentos de apoio.

Uma vez que o arquivo completo tenha sido recebido, as autoridades decidirão dentro de um prazo:

- 3 meses se você é um médico, enfermeira de cuidados gerais, parteira, veterinário, dentista, farmacêutico ou arquiteto e você se beneficia de um[reconhecimento automático](https://europa.eu/youreurope/citizens/work/professional-qualifications/european-professional-card-documents/index_fr.htm#103720) ;
- 4 meses para todas as outras ocupações.

Portanto, permita 5 meses durante os quais o reconhecimento de qualificação ocorre, um pré-requisito essencial antes de exercer sua profissão legalmente no Estado-Membro onde você presta o seu serviço. Depois desse tempo,[entrar em contato com a linha de ajuda](https://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=yec_pq) ou falar com[informações nacionais apontam sobre o reconhecimento de qualificações profissionais](https://ec.europa.eu/growth/single-market/services/free-movement-professionals_fr/#contacts).

Indo mais longe
---------------

Links úteis (site da Comissão Europeia):

- [Banco de Dados de Ocupações Regulamentadas lista ocupações reguladas por país da UE e por autoridades](https://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=homepage)
- [Lista de sites nacionais para profissões regulamentadas](https://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=sites)
- Para autenticar a autoridade nacional relevante, você pode entrar em contato com um[Informações nacionais apontam sobre o reconhecimento de qualificações profissionais](https://ec.europa.eu/growth/single-market/services/free-movement-professionals_fr/#contacts).

Lista dos Estados-Membros da UE
-------------------------------

Alemanha, Áustria, Bélgica, Bulgária, Chipre, Croácia, Dinamarca, Espanha, Estônia, Finlândia, França, Grécia, Hungria, Irlanda, Itália, Letônia, Lituânia, Luxemburgo, Malta, Holanda, Polônia, Portugal, Romênia, Reino Unido, Eslováquia, Eslovênia, Suécia, República Checa.

Lista de Estados da Área Econômica Europeia
-------------------------------------------

Estes são os 28 países membros da União Europeia (UE) listados acima e os seguintes 3 países: Noruega, Islândia, Liechtenstein.

