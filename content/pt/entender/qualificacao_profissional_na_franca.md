﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt) -->
<!-- include-file(generated.txt) -->

<!-- var(collapsable)="close" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Tendo minha qualificação profissional reconhecida na França" -->
<!-- var(key)="fr-comprendre-qualification_professionnelle_en_france" -->
<!-- var(last-update)="2020-07-15 14:48:12" -->
<!-- var(lang)="pt" -->
<!-- var(site:lang)="pt" -->

Tendo minha qualificação profissional reconhecida na França
===========================================================


<!-- begin-include(disclaimer-trans-pt) -->
<!-- end-include -->
Última atualização:<!-- begin-var(last-update) -->2020-05-03 11:32:08<!-- end-var -->

Você é nacional da União Europeia ou da Área Econômica Europeia e gostaria de ter suas qualificações profissionais reconhecidas em seu país de origem?<!-- collapsable:off -->
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

O site guichet-qualifications.fr incentiva a mobilidade profissional dos residentes da União Europeia e da Área Econômica Europeia. Serviços on-line guichet-entreprises.fr e guichet-qualifications.fr são a loja única reconhecida pela Comissão Europeia. Eles permitem que você explore novas oportunidades e desenvolva uma atividade em território francês, completando suas formalidades online.

Neste site, você pode fazer um reconhecimento de sua qualificação profissional com a autoridade competente:

- Eu tenho um diploma de um Estado membro da UE ou EEE e eu gostaria de[ter minha qualificação profissional reconhecida na França](diploma_estado_membro_ue_eee.md)
- Eu tenho um diploma francês e eu gostaria de[ter minha qualificação profissional reconhecida em um Estado membro da UE ou EEE](diploma_frances.md)

Crie uma atividade na França com guichet-entreprises.fr
-------------------------------------------------------

Uma vez que você tenha reconhecido sua qualificação profissional, você tem a oportunidade de configurar seu negócio on-line em[guichet-entreprises.fr](https://www.guichet-entreprises.fr) ! Você também encontrará informações úteis sobre atividades regulamentadas na França e condições de instalação.

Atividades regulamentadas<!-- collapsable:close -->
---------------------------------------------------

Descubra quais condições você deve seguir para realizar uma atividade regulamentada na França.

[Confira as fichas de fatos](https://www.guichet-entreprises.fr/pt/activites-reglementees/)

Estabelecimento gratuito<!-- collapsable:close -->
--------------------------------------------------

Agora você pode se estabelecer na França e realizar seu negócio livremente, e isso, permanentemente.

[Saiba mais sobre estabelecimento gratuito](https://www.guichet-entreprises.fr/pt/eugo/libre-etablissement-le/)

Prestação gratuita de serviços<!-- collapsable:close -->
--------------------------------------------------------

Entregar serviços temporários ou ocasionais na França nunca foi tão fácil !

[Saiba mais sobre entrega gratuita de serviços](https://www.guichet-entreprises.fr/pt/eugo/libre-prestation-de-services-lps/)

