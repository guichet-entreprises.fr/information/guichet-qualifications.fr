﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Pourquoi faire reconnaître sa qualification professionnelle ?" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(key)="fr-comprendre-pourquoi_faire_reconnaitre_sa_qualification_professionnelle" -->
<!-- var(last-update)="2020-05-03 14:55:32" -->
<!-- var(lang)="pt" -->
<!-- var(site:lang)="pt" -->

Por que suas qualificações profissionais são reconhecidas?
==========================================================


<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
Última atualização:<!-- begin-var(last-update) -->2020-05-03 14:55:32<!-- end-var -->

Uma profissão é chamada de "regulamentada" quando é necessário ter um diploma específico, ter passado por exames específicos, ter obtido uma determinada autorização ou ser registrado em um órgão profissional para realizá-lo.

**Obtendo suas qualificações profissionais reconhecidas** é imperativo quando você quer trabalhar em outro país da UE em que sua profissão é regulamentada. Isso significa ter sua formação e/ou experiência profissional oficialmente reconhecida pelo país anfitrião.

- Se é uma questão de se mudar para outro país para exercer sua profissão, é no caso do[estabelecimento livre](https://www.guichet-entreprises.fr/pt/eugo/libre-etablissement-le/) e é essencial fazer o reconhecimento de suas qualificações.
- Se é uma questão de prestar serviços em uma base temporária em outro país, o caso é[entrega de serviço gratuito](https://www.guichet-entreprises.fr/pt/eugo/libre-prestation-de-services-lps/) e uma declaração pré-exercício é suficiente. Há uma exceção para ocupações com sérias implicações de saúde ou segurança, uma vez que o Estado-Membro anfitrião pode exigir uma verificação de pré-qualificação.

Para saber as regras aplicáveis à sua situação, entre em contato com a autoridade nacional responsável pelo acesso à sua profissão no país de acolhimento.

Você também pode consultar o[Banco de dados europeu de profissões regulamentadas](http://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=homepage) que lista todas as profissões regulamentadas nos países da UE e suas autoridades competentes.

- Se sua profissão é regulamentada em seu país de origem, você pode procurá-la em seu próprio idioma e ver a tradução em inglês fornecida na descrição; em seguida, você tem que procurar o nome em inglês, a fim de obter uma lista dos outros países onde essa ocupação é regulamentada. Se o país onde você deseja se estabelecer não aparecer, pode significar que a profissão não é regulamentada.
- Se você não conseguir encontrar sua profissão no banco de dados, você pode entrar em contato com os Pontos de Informação Nacionais sobre o reconhecimento de qualificações profissionais no país onde você quer trabalhar. Eles podem ajudá-lo a encontrar a autoridade apropriada e determinar os documentos necessários.

**Leia também:** [Cartão Profissional Europeu (CPE)](cartao_de_visita_europeu.md)

