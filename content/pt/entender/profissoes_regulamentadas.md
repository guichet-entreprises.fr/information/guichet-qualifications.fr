﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Professions réglementées" -->
<!-- var(key)="fr-comprendre-professions_reglementees" -->
<!-- var(last-update)="2020-05-03 14:55:35" -->
<!-- var(lang)="pt" -->
<!-- var(site:lang)="pt" -->

Profissões regulamentadas
=========================


<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
Última atualização:<!-- begin-var(last-update) -->2020-05-03 14:55:35<!-- end-var -->

Arquiteto, enfermeiro, veterinário, padeiro, etc., antes de ter sua qualificação profissional reconhecida, descubra sobre as condições de acesso a uma profissão regulamentada na França.

Confira nossas fichas técnicas, categorizadas por grupos ocupacionais.

Regulamentos específicos na França
----------------------------------

Na França, algumas profissões exigem uma designação profissional, uma qualificação profissional ou mesmo autorização prévia para sua prática, sancionando um certo nível de formação ou experiência. Os registros profissionais fornecem uma atualização sobre como reconhecer as qualificações profissionais e a legislação aplicável a essas diferentes profissões. Até o momento, cerca de 250 atividades estão envolvidas e são objeto de nossos registros profissionais.

**Você sabia disso?** Os cartões profissionais são colocados on-line assim que são escritos pela Business Box Office em colaboração com as administrações relevantes.

Se a ficha técnica da sua qualificação profissional não estiver disponível, vá para o[Banco de dados europeu de profissões regulamentadas](https://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=homepage).

