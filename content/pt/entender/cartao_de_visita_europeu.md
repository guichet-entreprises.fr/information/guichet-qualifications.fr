﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="La carte professionnelle européenne (CPE)" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(key)="fr-comprendre-carte_professionnelle_europeenne" -->
<!-- var(last-update)="2020-05-03 14:55:10" -->
<!-- var(lang)="pt" -->
<!-- var(site:lang)="pt" -->

O Cartão Profissional Europeu (CPE)
===================================


<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
Última atualização:<!-- begin-var(last-update) -->2020-05-03 14:55:10<!-- end-var -->

Algumas profissões regulamentadas se beneficiam do regime europeu de cartões profissionais. Neste artigo você encontrará todas as informações sobre seu funcionamento, as formalidades de sua obtenção e as profissões regulamentadas que ele diz respeito.

O que é o cartão de visita europeu?
-----------------------------------

O European Professional Card (ECC) é um procedimento eletrônico para o reconhecimento de qualificações profissionais entre estados membros da UE definidos pelos artigos 4º a 4º do regulamento[Diretiva 2005/36/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036). Seu objetivo é facilitar o exercício de uma profissão regulamentada para os cidadãos europeus em outro Estado-Membro da UE. Este procedimento faz parte da mobilidade profissional dos cidadãos europeus.

###### É bom saber

Este não é um cartão físico, mas uma prova eletrônica de que as qualificações profissionais do cidadão foram verificadas e são reconhecidas pelas autoridades competentes do país anfitrião.

Quem pode se beneficiar?
------------------------

Este dispositivo pode ser usado por qualquer profissional europeu, bem como na configuração no país anfitrião para praticar lá ([estabelecimento livre](https://www.guichet-entreprises.fr/pt/eugo/libre-etablissement-le/)), apenas no contexto da prestação de serviços de forma temporária ([entrega de serviço gratuito](https://www.guichet-entreprises.fr/pt/eugo/libre-prestation-de-services-lps/)).

Até o momento, o CPE está disponível para cinco ocupações:

# Agente imobiliário;
# Guia da montanha;
# Enfermeira;
# Fisioterapeuta;
# Farmacêutico.

Essas ocupações foram selecionadas para beneficiar do sistema CPE com base em três critérios:

- mobilidade significativa, ou potencial de mobilidade significativo, na profissão em questão;
- interesse suficiente demonstrado pelas partes com interesse legítimo;
- regulação da profissão ou formação associada a ela em um número significativo de Estados-Membros da UE.

Espera-se que esse número de ocupações cresça sabendo que algumas, como engenheiros ou enfermeiros especializados, estão sendo avaliadas.

Por que pedir?
--------------

O CPE é um certificado eletrônico. Tem como objetivo simplificar o reconhecimento das qualificações profissionais e introduzir um nível adicional de eficiência tanto para os cidadãos quanto para as autoridades competentes responsáveis pela emissão.

Quando se trata de**entrega de serviço gratuito**, o CPE destina-se a substituir a declaração anterior ao primeiro benefício no Estado-Membro anfitrião, que deve ser renovada a cada 18 meses.

Quando se trata de**Estabelecimento**, a CPE é a decisão de reconhecer as qualificações profissionais pelo Estado-Membro anfitrião.

###### É bom saber

Os profissionais interessados no CPE têm uma escolha entre seu dispositivo ou os procedimentos tradicionais de pré-emissão ou reconhecimento de qualificações profissionais.

###### Para ir mais longe

[Cartão profissional europeu](https://europa.eu/youreurope/citizens/work/professional-qualifications/european-professional-card/index_fr.htm) (Comissão Europeia)

