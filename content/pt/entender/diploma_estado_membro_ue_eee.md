﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Je suis titulaire d’un diplôme d’un État membre de l’Union européenne ou de l’Espace économique européen..." -->
<!-- var(key)="fr-comprendre-diplome_etat_membre_ue_eee" -->
<!-- var(last-update)="2020-05-03 14:55:18" -->
<!-- var(lang)="pt" -->
<!-- var(site:lang)="pt" -->

Sou formado por um Estado-Membro da União Europeia ou pela Área Econômica Europeia...
=====================================================================================


<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
Última atualização:<!-- begin-var(last-update) -->2020-05-03 14:55:18<!-- end-var -->

... e eu quero ter minha qualificação profissional reconhecida na França.
-------------------------------------------------------------------------

A França tem cerca de 250 profissões regulamentadas que podem ser identificadas em duas categorias:

- As profissões liberais;
- artesanato e profissões comerciais para as quais o reconhecimento da qualificação profissional é realizado com a Câmara de Comércio e Artesanato da qual você depende.

Quero me mudar para a França.
-----------------------------

São necessários dois passos para poder exercer uma profissão regulamentada na França:

# Reconhecimento da qualificação profissional para estrangeiros da União Europeia (UE) ou da Área Econômica Europeia (EEE);
# autorização para exercer com a autoridade competente da profissão em causa (autorização adicional dependendo da profissão).

O[folhas de fatos em profissões regulamentadas](profissoes_regulamentadas.md) identificar essas etapas e direcioná-lo através de regulamentos existentes.

Note que as ocupações reconhecidas pelo[Cartão profissional europeu](cartao_de_visita_europeu.md) (CPE) pode, assim, desviar-se do procedimento tradicional:

- Enfermeiras de cuidados gerais;
- Farmacêuticos;
- fisioterapeutas;
- guias de montanha;
- agentes imobiliários.

Eu quero emitir um benefício temporário na França
-------------------------------------------------

Não há necessidade de usar um reconhecimento de qualificação profissional para um benefício temporário, no entanto, você deve ser estabelecido em seu país de origem. Em alguns casos, você terá que solicitar permissão antes do seu benefício.

Passos
------

O contato com a autoridade competente é, na maioria das vezes, levado na área em que você decide se estabelecer. Uma série de documentos de suporte devem ser fornecidos, alguns dos quais às vezes requerem uma tradução juramentada.

**É bom saber**

Se sua profissão é regulamentada em seu país de origem, mas não no país de origem (França), você pode praticar sem ter que fazer uma qualificação profissional, como nacionais do país anfitrião.

### Lista dos Estados-Membros da UE

Alemanha, Áustria, Bélgica, Bulgária, Chipre, Croácia, Dinamarca, Espanha, Estônia, Finlândia, França, Grécia, Hungria, Irlanda, Itália, Letônia, Lituânia, Luxemburgo, Malta, Holanda, Polônia, Portugal, Romênia, Reino Unido, Eslováquia, Eslovênia, Suécia, República Checa.

### Lista de Estados da Área Econômica Europeia

Estes são os 28 países membros da União Europeia (UE) listados acima e os seguintes 3 países: Noruega, Islândia, Liechtenstein.

