﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Politique de protection des données personnelles" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-politique_protection_donnees" -->
<!-- var(last-update)="2020-05-03 14:57:09" -->
<!-- var(lang)="pt" -->
<!-- var(site:lang)="pt" -->

Política de proteção de dados pessoais
======================================


<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
Última atualização:<!-- begin-var(last-update) -->2020-05-03 14:57:09<!-- end-var -->

| Versão | Data       | Objeto                                                        |
|--------|------------|---------------------------------------------------------------|
| V1     | 25/05/2018 | Versão inicial                                                |
| V2     | 01/09/2018 | Informações atualizadas sobre o delegado de proteção de dados |

A política de privacidade (a "política") informa sobre como seus dados são coletados e processados pelo serviço do Banco Nacional de Negócios, as medidas de segurança implementadas para garantir sua integridade e confidencialidade e os direitos que você tem para controlar seu uso.

Esta política complementa o[termos e condições de uso](termos.md) (CGU) bem como, se necessário,[Legal](mencoes_legais.md). Como tal, considera-se que esta política é incorporada a esses documentos.

Como parte do desenvolvimento de nossos serviços e da implementação de novas normas regulatórias, talvez tenhamos que mudar essa política. Convidamos você a lê-lo regularmente.

Quem somos nós?
---------------

A Bilheteria de Negócios, criada pelo[decreto de 22 de abril de 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&categorieLien=id) está sob a autoridade do[Diretoria Corporativa](https://www.entreprises.gouv.fr/) (DGE) dentro do[Ministério da Economia e Finanças](https://www.economie.gouv.fr/).

O serviço de Bilheteria Empresarial implementa um guichet-qualifcations.fr de teleserviço (doravante a "Qualificações Profissionais de Bilheteria de Teleserviço").

As Qualificações Profissionais de Bilheteria de Teleserviço são destinadas a todos os residentes da União Europeia ou da Área Econômica Europeia. Informa-os sobre as possibilidades de deixar suas qualificações profissionais reconhecidas e exercer uma profissão regulamentada na França com vistas a uma profissão sustentável (livre) ou temporária (prestação gratuita de serviços).

Também permite que os residentes na França conheçam as possibilidades de acesso e exercício de uma profissão regulamentada em outro Estado-Membro.

As Qualificações Profissionais de Bilheteria de Teleservice fazem parte do arcabouço legal:

- De[Regulamento Geral de Proteção de Dados](https://www.cnil.fr/pt/reglement-europeen-protection-donnees) ;
- [Diretiva 2005/36/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036) Parlamento Europeu;
- [Diretiva 2013/55/UE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32013L0055) Parlamento Europeu;
- De[Regulamento 910/2014](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32014R0910) Parlamento Europeu e o Conselho de 23 de Julho de 2014 (e-IDAS) sobre identificação eletrônica e serviços confiáveis para transações eletrônicas no mercado interno;
- do[Ordem de 8 de dezembro de 2005](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000636232) trocas eletrônicas entre usuários e autoridades administrativas e entre autoridades administrativas e os[Decreto nº 2010-112 de 2 de fevereiro de 2010](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021779444&categorieLien=id) tomadas para a aplicação dos artigos 9º, 10º e 12º da portaria;
- De[Ato 78-17 de 6 de janeiro de 1978 alterado](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460) ciência da computação, arquivos e liberdades;
- do[441-1 do Código Penal](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006418753&cidTexte=LEGITEXT000006070719&dateTexte=20020101) ;
- do[Artigo R. 123-30 do Código de Comércio](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006256107&dateTexte=&categorieLien=cid) ;
- do[decreto de 22 de abril de 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&categorieLien=id) criando um serviço nacionalmente competente chamado "janela de negócios".

Que organização implementamos para proteger seus dados?
-------------------------------------------------------

A conformidade e a implementação das disposições aplicáveis de proteção de dados são monitoradas pelo Representante de Proteção de Dados do Business Bank Service.

No âmbito do Ministério da Economia e Finanças, do qual depende a Diretoria-Geral de Empresas, os serviços do alto funcionário da defesa são responsáveis por verificar a implementação e o cumprimento das disposições do Regulamento Geral de Proteção de Dados.

Como faço para entrar em contato conosco?
-----------------------------------------

Se você tiver alguma dúvida sobre o uso de seus dados pessoais pela Enterprise Box Office, leia esta política e entre em contato com nosso representante de proteção de dados usando as seguintes informações de contato:

Enviando para:

```
Le Délégué à la protection des données des ministères économique et financier
Délégation aux Systèmes d’Information
139, rue de Bercy – Télédoc 322
75572 PARIS CEDEX 12


```
Por e-mail, especificando o objeto "Proteção de Dados" para:[Contato](contato.md).

O que é um dado pessoal?
------------------------

Dados pessoais são informações que o identificam, direta ou indiretamente. Estes podem incluir seu nome, primeiro nome, endereço, número de telefone ou endereço de e-mail, bem como o endereço IP do seu computador, por exemplo, ou informações relacionadas ao uso do Telesserviço.

Quando coletamos dados pessoais?
--------------------------------

A Business Box Office coleta dados pessoais quando você:

- Crie seu próprio espaço pessoal
- completar uma formalidade através da Caixa de Negócios de Telesserviço.

Que dados pessoais processamos?
-------------------------------

Os dados pessoais que tratamos referem-se apenas às formalidades de reconhecimento das qualificações profissionais de acordo com as normas dos artigos R. 123-30-10 a R. 123-30-14 do Código de Comércio.

Os dados coletados permitem que o arquivo seja compilado conforme esperado na Seção R. 123-30-10 do Código de Comércio.

Em particular, os dados coletados permitem que os documentos do Cerfa associados às formalidades e procedimentos necessários se qualifiquem para qualificações profissionais ou para obter um cartão profissional europeu.

A lista a seguir resume os dados pessoais que processamos.

Para a gestão do acesso ao espaço pessoal do serviço, o usuário compartilha as seguintes informações:

- O ID de login escolhido pelo usuário
- A senha escolhida pelo usuário
- O endereço de e-mail do usuário
- O número do telefone celular
- Endereço de login IP.

Para o uso do espaço de armazenamento pessoal, o usuário compartilha as seguintes informações:

- sobrenome;
- Use o nome/nome da esposa;
- Nome do exercício
- Primeiro nome;
- data de nascimento;
- Nascimento do país;
- Nacionalidade;
- Número da Previdência Social;
- endereço de residência (incluindo país);
- Telefone
- E-mail
- diploma, certificado, títulos;
- Experiência de trabalho
- um projeto profissional;
- qualidade ou função;
- Emprego atual
- local de trabalho (localidade, país);
- intitulado para a posição ocupada.

Com quais documentos pessoais de apoio lidamos?
-----------------------------------------------

Os documentos pessoais de apoio que tratamos referem-se apenas às formalidades de reconhecimento de qualificações profissionais ou obtenção do cartão profissional europeu de acordo com as normas relativas à atividade regulamentada realizada.

Os documentos de apoio são usados para construir o arquivo conforme previsto no artigo R. 123-30-10 do Código de Comércio.

A lista de documentos comprobatórias é aprovada pela autoridade competente responsável pela investigação do caso.

Documentos pessoais com probação incluem documentos que justifiquem identidade ou residência. Esses documentos de suporte são mantidos nos mesmos termos que os dados pessoais.

Seu armazenamento é fornecido em espaços inacessíveis de fora (Internet) e seu download ou transferência é protegido por criptografia de acordo com as disposições indicadas no capítulo "Como seus dados pessoais estão protegidos?".

Qual é a nossa gestão de biscoitos?
-----------------------------------

O gerenciamento de cookies é descrito na página:[https://www.guichet-entreprises.fr/pt/cookies/](https://www.guichet-entreprises.fr/pt/cookies/).

Todos os cookies gerenciados usam o modo de cookie seguro.

Cookies seguros são um tipo de cookie transmitido exclusivamente através de conexões criptografadas (https).

Os cookies técnicos contêm os seguintes dados pessoais:

- Nome;
- Primeiro nome;
- E-mail
- Telefone.

Os cookies de análise contêm o endereço IP de login.

Quais são seus direitos de proteger seus dados pessoais?
--------------------------------------------------------

Você pode acessar seus dados, corrigi-los, excluí-los, limitar seu uso ou se opor ao processamento deles.

Se você acha que o uso de seus dados pessoais viola as regras de privacidade, você tem a opção de fazer uma reivindicação para o[Cnil](https://www.CNIL.fr/).

Você também tem o direito de definir diretrizes para o manuseio de seus dados pessoais em caso de morte. Orientações específicas podem ser registradas com o gerente de tratamento. As diretrizes gerais podem ser registradas em um fundo digital de terceiros certificado pelo CNIL. Você tem a opção de alterar ou remover essas diretrizes a qualquer momento.

Se assim for, você também pode nos pedir para fornecer-lhe os dados pessoais que você nos forneceu em um formato legível.

Você pode enviar suas várias solicitações para:

```
Service à compétence nationale Guichet Entreprises
120 rue de Bercy – Télédoc 766
75572 Paris cedex 12


```
Suas solicitações devem ser acompanhadas de uma cópia de um documento de identidade e serão revisadas por nossos serviços.

Como seus dados pessoais são protegidos?
----------------------------------------

O Enterprise Box Office implementa todas as práticas e procedimentos de segurança padrão do setor para evitar qualquer violação de seus dados pessoais.

Todas as informações fornecidas são armazenadas em servidores seguros hospedados por nossos provedores técnicos.

Quando a transmissão de dados é necessária e autorizada, a Enterprise Box Office garante que esses terceiros forneçam salvaguardas suficientes para garantir um nível adequado de proteção. As trocas são criptografadas e os servidores autenticados por reconhecimento mútuo.

De acordo com as normas de proteção de dados, em caso de violação, o serviço Business Guichet compromete-se a comunicar essa violação à autoridade fiscalizadora competente e, quando necessário, às pessoas em causa.

#### Espaço pessoal

Todos os dados transmitidos ao seu espaço pessoal são criptografados e o acesso é protegido pelo uso de uma senha pessoal. Você é obrigado a manter esta senha confidencial e não divulgá-la.

#### Tempo de preservação

Os dados pessoais processados pela Enterprise Box Office são mantidos por um período de doze meses, de acordo com as disposições do artigo 123-30-14 do Código de Comércio.

#### Em relação aos dados do cartão de crédito

O serviço enterprise checkout não armazena ou armazena os dados bancários de usuários que estão sujeitos ao pagamento no momento de sua formalidade. Nosso provedor gerencia dados de transações em nome da Enterprise Box office de acordo com as regras de segurança mais rigorosas aplicáveis no setor de pagamentos on-line através do uso de processos de criptografia.

Para quem seus dados são transmitidos?
--------------------------------------

Os dados coletados permitem que o arquivo seja compilado conforme previsto no artigo R.123-30-10 do Código de Comércio.

De acordo com o artigo R.123-30-12 do Código de Comércio, os dados coletados são transmitidos para processamento às autoridades competentes.

