﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Conditions générales d’utilisation" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-cgu" -->
<!-- var(last-update)="2020-05-03 14:56:20" -->
<!-- var(lang)="pt" -->
<!-- var(site:lang)="pt" -->

Termos e condições
==================


<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
Última atualização:<!-- begin-var(last-update) -->2020-05-03 14:56:20<!-- end-var -->

Preâmbulo
---------

Este documento descreve os termos de compromisso com o uso de qualificações de caixas de telesserviço para os usuários. Faz parte do arcabouço legal:

- De[Regulamento Geral de Proteção de Dados](https://www.cnil.fr/pt/reglement-europeen-protection-donnees) ;
- [Diretiva 2005/36/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036) Parlamento Europeu;
- [Diretiva 2013/55/UE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32013L0055) Parlamento Europeu;
- De[Regulamento 910/2014](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32014R0910) Parlamento Europeu e o Conselho de 23 de Julho de 2014 (e-IDAS) sobre identificação eletrônica e serviços confiáveis para transações eletrônicas no mercado interno;
- do[Ordem de 8 de dezembro de 2005](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000636232) trocas eletrônicas entre usuários e autoridades administrativas e entre autoridades administrativas e os[Decreto nº 2010-112 de 2 de fevereiro de 2010](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021779444&categorieLien=id) tomadas para a aplicação dos artigos 9º, 10º e 12º da portaria;
- De[Ato 78-17 de 6 de janeiro de 1978 alterado](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460) ciência da computação, arquivos e liberdades;
- do[441-1 do Código Penal](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006418753&cidTexte=LEGITEXT000006070719&dateTexte=20020101) ;
- do[Artigo R. 123-30 do Código de Comércio](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006256107&dateTexte=&categorieLien=cid) ;
- do[decreto de 22 de abril de 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&categorieLien=id) criando um serviço nacionalmente competente chamado "janela de negócios".

O tema do documento
-------------------

O objetivo deste documento é definir os termos gerais e condições de uso das Qualificações da Caixa de Telesserviço, denominadas "Serviço" abaixo, entre o serviço nacional Guichet Enterprises e os usuários.

O serviço nacional Guichet Enterprises está sob a autoridade da Diretoria Corporativa do Ministério da Economia e Finanças.

Definição e propósito do Serviço
--------------------------------

O serviço implementado pelo serviço nacional Guichet Enterprises (doravante denominado "Business Box Service") ajuda a simplificar os procedimentos relacionados à criação, mudanças na situação e a cessação de um negócio de usuários franceses e europeus.

Este Serviço foi submetido à aprovação de segurança de acordo com a Instrução Interministerial ANSSI 901/SGDSN/ANSSI (NOR: PRMD1503279 28/01/2015 e o PGSSI dos Ministérios Econômicos e Financeiros (NOR: FCPP1622039A) de 1º de agosto de 2016.

O uso do Serviço é opcional e gratuito. No entanto, como parte das formalidades realizadas através deste Serviço, como start-up de negócios, os pagamentos online podem ser solicitados. Estes são seguros.

Todos os destinatários do Serviço são referidos como as agências parceiras abaixo.

De acordo com a Seção R.123-3 do Código de Comércio, os dados coletados são transmitidos para processamento para as seguintes organizações parceiras:

"1) Sujeitos às disposições do 2º e 3º, as câmaras territoriais de comércio e indústria criam e gerenciam os centros de formalidades das empresas responsáveis por:

# Comerciantes;
# Empresas comerciais.

(2) As câmaras de comércio e artesanato da região criam e gerenciam os centros competentes para pessoas físicas e jurídicas sujeitas a registro no diretório de comércioe para pessoas físicas beneficiadas pela isenção cadastral prevista no artigo 19 da Lei 96-603 de 5 de Julho de 1996 relativa ao desenvolvimento e promoção do comércio e do artesanato. , excluindo os mencionados no 3º deste artigo.

3. A Câmara Nacional de Embarcação Artesanal cria e gerencia o centro para pessoas físicas e jurídicas sujeitas ao registro no registro de empresas de barcos artesanais.

(4) Os registros comerciais ou comercialmente dominantes criam e gerenciam os centros responsáveis por:

# Empresas civis e não comerciais;
# Sociedades de exercício liberal;
# pessoas jurídicas sujeitas a registro no registro de comércio e empresas diferentes das mencionadas nos 1, 2 e 3
# Estabelecimentos públicos industriais e comerciais;
# Agentes comerciais;
# grupos de interesse econômico e grupos de interesse econômico europeus.

(5) Os sindicatos de arrecadação de seguridade social e de benefícios familiares (Urssaf) ou fundos gerais de seguridade social criam e gerenciam os centros responsáveis por:

# pessoas contratadas, como ocupação regular, em atividade regulamentada ou não comercial, artesanal ou independente agrícola;
# empregadores cujos negócios não estejam registrados no registro de comércios e empresas, no diretório comercial ou no registro de empresas de barcos artesanais, e que não se reportem aos centros mencionados no 6º.

(6) As Câmaras de Agricultura criam e gerenciam os centros competentes para pessoas físicas e jurídicas que realizam atividades agrícolas como principais[...]. »

Características
---------------

O Serviço permite ao usuário:

- acesso à documentação detalhando as obrigações dos Centros de Formalidade Empresarial (CFEs), bem como os blocos de construção do arquivo de declaração e do arquivo de solicitação;
- criar uma conta de usuário que forneça acesso ao espaço de armazenamento pessoal. Este espaço permite que o usuário gerencie e use seus dados pessoais, para manter as informações sobre ele e os documentos e documentos de suporte necessários para a conclusão dos procedimentos administrativos.

A partir de seu espaço, o usuário pode:

- construir seu arquivo único nos termos do artigo R. 123-1 do Código de Comércio, que inclui o arquivo de declaração e, se for o caso, pedidos de autorização;
- apresentar seu arquivo de declaração e, se necessário, pedidos de autorização ao centro de formalidades empresariais pertinentes;
- Acesse as informações de acompanhamento sobre o processamento de seu arquivo e, se necessário, seu arquivo de solicitação;
- permitir que os CFEs implementem os tratamentos necessários à exploração das informações recebidas da corporação abrangidas no último parágrafo do artigo R. 123-21 do Código de Comércio: recebimento do arquivo único nos termos do artigo 2º da Lei 94-126, de 11 de Fevereiro de 1994, relativo à iniciativa individual e aos negócios e encaminhados pela pessoa jurídica abrangida pelo artigo R. 123-21 do Código de Comércio; Recebimento de informações de acompanhamento sobre o processamento desses arquivos, conforme fornecido por agências e autoridades parceiras; transmissão de informações de acompanhamento desde o processamento de arquivos exclusivos até a pessoa jurídica abrangida pelo artigo R. 121-21 do Código de Comércio.

Como se cadastrar e usar o Serviço
----------------------------------

O acesso ao Serviço é aberto a qualquer pessoa e gratuito. É opcional e não é exclusivo de outros canais de acesso para permitir que o usuário complete suas formalidades.

Para o gerenciamento do acesso ao espaço pessoal do Serviço, o usuário compartilha as seguintes informações:

- endereço de e-mail do usuário.
- A senha escolhida pelo usuário

O uso do Serviço requer o fornecimento de dados pessoais para a criação da pasta única.
O processamento de dados pessoais é descrito na política de proteção de dados disponível[Aqui](protecao_politica_da.md).

O uso do Serviço requer uma conexão e um navegador de internet. O navegador deve ser configurado para permitir*Cookies* sessão de sessão.

Para garantir uma ótima experiência de navegação, recomendamos usar as seguintes versões do navegador:

- Firefox versão 45 e acima;
- Google Chrome versão 48 e acima.

De fato, outros navegadores podem não suportar certos recursos do Serviço.

O Serviço é otimizado para uma tela de 1024-768 pixels. Recomenda-se usar a versão mais recente do navegador e atualizá-lo regularmente para se beneficiar de patches de segurança e melhor desempenho.

Termos específicos de uso do serviço de assinatura
--------------------------------------------------

O serviço de assinatura eletrônica é acessível diretamente através do Serviço.

O artigo R. 123-24 do Código de Comércio do Regulamento 910/2014 do Parlamento Europeu e do Conselho de 23 de Julho de 2014 (e-IDAS) são as referências aplicáveis ao serviço de assinatura eletrônica do Serviço.

Papéis e compromisso
--------------------

### Compromisso de Bilheteria Corporativa

# O serviço enterprise box office implementa e opera o Serviço de acordo com o arcabouço legal existente estabelecido no preâmbulo.
# A Enterprise Box Office está comprometida em tomar todas as medidas necessárias para garantir a segurança e confidencialidade das informações fornecidas pelo usuário.
# A Business Box Office está comprometida em garantir a proteção dos dados coletados no âmbito do Serviço, inclusive evitando que sejam distorcidas, danificadas ou acessadas por terceiros não autorizados, de acordo com as medidas previstas na ordem de 8 de Dezembro de 2005 sobre trocas eletrônicas entre usuários e autoridades administrativas e entre autoridades administrativas, o Decreto nº 2010-112 de 2 de Fevereiro de 2010 para a aplicação dos artigos 9º, 10º e 12º desta portaria e regulamento 2016/679 do Parlamento Europeu e do Conselho de 27 de Abril de 2016 sobre o processamento de dados pessoais.
# A Bilheteria Empresarial e as agências parceiras garantem o acesso, a retificação e os direitos de oposição dos Usuários do Serviço nos termos da Lei 78-17 de 6 de Janeiro de 1978 relativas ao uso de arquivos e liberdades e regulamento nº 2016/679 do Parlamento Europeu e do Conselho de 27 de Abril de 2016 relativos ao processamento de dados pessoais.
Esse direito pode ser exercido de várias maneiras:
a Entrar em contato com o Centro de Formalidades Empresariais (CFE) que recebe o arquivo de declaração;
b- enviando um e-mail para o suporte
c- enviando uma carta para:

<blockquote><p>Service à compétence nationale Guichet Entreprises</p>
<p>120 rue de Bercy – Télédoc 766</p>
<p>75572 Paris cedex 12</blockquote># A Bilheteria Empresarial e as organizações parceiras comprometem-se a não comercializar as informações e documentos transmitidos pelo usuário através do Serviço, e não comunicá-los a terceiros, fora dos casos previstos em lei.
# A Bilheteria Empresarial e as organizações parceiras comprometem-se a não comercializar as informações e documentos transmitidos pelo usuário através do Serviço, e não comunicá-los a terceiros, fora dos casos previstos em lei.
# A Enterprise Box Office está comprometida em garantir a rastreabilidade de todas as ações realizadas por todos os usuários do Serviço, incluindo as de organizações parceiras e do usuário.
# O serviço Enterprise Box Office oferece suporte aos usuários em caso de incidente ou alerta de segurança definido.

Engajamento do usuário
----------------------

# O usuário preenche seu arquivo on-line e valida-o possivelmente anexando as peças necessárias para o tratamento do arquivo.
# Ao final da preparação do arquivo, um resumo das informações fornecidas pelo usuário é exibido na tela para que o usuário possa verificar e confirmá-las. Após a confirmação, o arquivo é encaminhado para agências parceiras. Vale a pena assinar o formulário e a confirmação do formulário pelo usuário.
# Estes termos e condições são exigidos de qualquer usuário que seja usuário do Serviço.

Disponibilidade e evolução do Serviço
-------------------------------------

O serviço está disponível 7 dias por semana, 24 horas por dia.

A Bilheteria Empresarial, no entanto, reserva a capacidade de evoluir, modificar ou suspender o Serviço sem aviso prévio por manutenção ou outras razões consideradas necessárias. A indisponibilidade do Serviço não lhe dá direito a qualquer compensação. No caso de indisponibilidade do Serviço, uma página de informações é então postada para o usuário mencionando essa indisponibilidade; ele é então convidado a dar seu passo em uma data posterior.

Os termos destes termos de uso podem ser alterados a qualquer momento, sem aviso prévio, dependendo de alterações na Lei, mudanças na legislação ou regulamentos, ou por qualquer outro motivo considerado necessário.

Responsabilidades
-----------------

# A responsabilidade pelo serviço de Bilheteria Corporativa não pode ser incorrida em caso de roubo de identidade ou qualquer uso fraudulento do Serviço.
# Os dados transmitidos aos serviços online das agências parceiras permanecem de responsabilidade do usuário, mesmo que sejam transmitidos pelos meios técnicos disponibilizados no Serviço.
O usuário pode modificá-los ou excluí-los de agências parceiras a qualquer momento.
Ele pode optar por excluir todas as informações de sua conta excluindo seus dados do Serviço.
# O usuário é lembrado de que qualquer pessoa que tenha uma declaração falsa para si mesmo ou para outros é responsável, em particular, pelas penalidades previstas no artigo 441-1 do Código Penal, prevendo penas de até três anos de prisão e multa de 45.000 euros.

