﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Politique de signature électronique" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-politique_signature_electronique" -->
<!-- var(last-update)="2020-05-03 14:58:09" -->
<!-- var(lang)="pt" -->
<!-- var(site:lang)="pt" -->

Política de assinatura eletrônica
=================================


<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
Última atualização:<!-- begin-var(last-update) -->2020-05-03 14:58:09<!-- end-var -->

| Versão | Data       | Objeto         |
|--------|------------|----------------|
| V1     | 27/09/2018 | Versão inicial |

O tema do documento
-------------------

A assinatura eletrônica afixada a um conjunto de dados garante a integridade dos dados transmitidos, o não repúdio aos dados assinados e a autenticidade de seu transmissor.

Esta política de assinatura eletrônica é um documento que descreve os requisitos de elegibilidade das agências receptoras, ou seja, as agências competentes, de um arquivo no qual uma assinatura eletrônica é afixada em conexão com as trocas eletrônicas abrangidas pelo artigo R. 123-30-10 do Código de Comércio.

Os órgãos competentes são os serviços responsáveis por ouvir pedidos de reconhecimento de qualificações profissionais.

Sob esta seção,*"Quando for necessária a assinatura, é permitida a utilização de assinatura eletrônica com as garantias estabelecidas na primeira frase do segundo parágrafo do artigo 1367 do Código Civil"*, e o segundo parágrafo do artigo 1367 do Código Civil,*"Quando é eletrônico, consiste no uso de um processo de identificação confiável garantindo sua conexão com o ato ao qual se prende. A confiabilidade desse processo é presumida, até que se prove o contrário, quando for criada a assinatura eletrônica, a identidade do segurado signatário e a integridade do ato garantido, em condições estabelecidas por decreto no Conselho de Estado."*,

Os regulamentos aplicáveis para a implementação da assinatura eletrônica das Qualificações da Caixa de Telesserviço são derivados dos seguintes textos:

- Regulamento 910/2014 do Parlamento Europeu e do Conselho de 23 de Julho de 2014 (e-IDAS) sobre identificação eletrônica e serviços confiáveis para transações eletrônicas no mercado interno;
- Código civil;
- Decreto nº 2017-1416, de 28 de setembro de 2017, relativo à assinatura eletrônica.

Caso a assinatura eletrônica exija o fornecimento de dados pessoais, as regulamentações aplicáveis são derivadas dos seguintes textos:

- Regulamento (UE) 2016/679 do Parlamento Europeu e do Conselho de 27 de Abril de 2016, regulamentação sobre a proteção de indivíduos no que diz respeito ao processamento de dados pessoais e à livre circulação desses dados, e revogação da Diretiva 95/46/CE (Regulamento Geral sobre Proteção de Dados);
- Lei 78-17 de 6 de janeiro de 1978 relativa a computadores, arquivos e liberdades.

Este documento, "Política eletrônica de assinatura de qualificações de caixas de telesserviço", descreve todas as regras e disposições que definem os requisitos aos quais cada um dos atores envolvidos nessas trocas desmaterializadas está em conformidade com a transmissão e recebimento de fluxos.

Este documento destina-se a:

- Os órgãos competentes;
- provedores potenciais que participam dessas trocas desmaterializadas em nome dessas agências receptoras.

No resto deste documento:

- As organizações receptoras acima mencionadas são referidas como "destinatários";
- as trocas acima aquecidas são referidas como "registros";
- o serviço nacional Business Bank é chamado de "Serviço de Bilheteria Empresarial".

Escopo
------

A assinatura eletrônica é necessária para qualquer formalidade relacionada a um pedido de reconhecimento de qualificação profissional no sentido do Parlamento Europeu e da Diretiva do Conselho 2005/36/CE de 7 de Setembro de 2005 relativas ao reconhecimento de qualificações profissionais ("Diretiva de Qualificação Profissional") e ao seu exercício, referido no artigo R. 123-30-8 do Código de Comércio.

O artigo 9º da Portaria nº 2016-1809, de 22 de dezembro de 2016, prevê os atingidos por essas formalidades,

*Qualquer cidadão de um Estado-Membro da União Europeia ou parte do acordo sobre a Área Econômica Europeia que adquiriu suas qualificações profissionais em um Estado-Membro da União Europeia ou parte do acordo sobre a Área Econômica Europeia, e desejando obter o reconhecimento destes para o exercício de uma profissão regulamentada dentro do significado da Diretiva 2005/36/CE do Parlamento Europeu e do Conselho de 2005,36/CE, pode concluir ou monitorar remota e eletronicamente, através de uma única janela, todos os requisitos, procedimentos ou formalidades relativos ao reconhecimento de suas qualificações profissionais em condições determinadas por decreto no Conselho de Estado.*

A assinatura eletrônica tem características diferentes dependendo da natureza dos arquivos submetidos.

**No caso de se aplicarem as disposições legais previstas no artigo 123-30-10 do Código de Comércio**, a assinatura eletrônica requerida atende ao disposto na primeira sentença do segundo parágrafo do artigo 1316-4 do Código Civil:

*Quando é eletrônico, consiste no uso de um processo de identificação confiável garantindo sua conexão com o ato ao qual se prende.*

**No caso de aplicação de disposições legais mais rigorosas do que as previstas no artigo 123-30-10 do Código de Comércio** (por exemplo, reconhecimento da qualificação profissional para a atividade de um clínico geral), a assinatura eletrônica atende ao disposto no artigo 1º do Decreto nº 2017-1416, de 28 de setembro de 2017 (referência ao regulamento "eIDAS" nº 910/2014):

*A confiabilidade de um processo de assinatura eletrônica é presumida, até que se prove o contrário, quando este processo implementa uma assinatura eletrônica qualificada.*

*Uma assinatura eletrônica é uma assinatura eletrônica avançada, de acordo com o artigo 26 do regulamento acima e criada utilizando um dispositivo de criação de assinatura eletrônica qualificado que atende aos requisitos do artigo 29 º desse regulamento, que se baseia em um certificado qualificado como assinatura eletrônica atendendo aos requisitos do artigo 28º desse regulamento.*

Artigo 26 do Regulamento "eIDAS" 910/2014:

*Uma assinatura eletrônica avançada atende aos seguintes requisitos:*

# *Estar vinculado ao signatário de uma forma inequívoca;*
# *Permitir que o signatário seja identificado;*
# *Ter sido criado usando dados de criação de assinaturas eletrônicas que o signatário pode usar sob seu controle exclusivo com um alto nível de confiança;*
# *Esteja vinculado aos dados associados a esta assinatura para que quaisquer alterações subseqüentes nos dados sejam detectáveis.*

Para informações, três níveis de garantia são fornecidos pelo regulamento "eIDAS" 910/2014:

- *Baixo* Neste nível, o objetivo é simplesmente reduzir o risco de uso indevido ou adulteração de identidade;
- *Substancial* Neste nível, o objetivo é reduzir substancialmente o risco de uso indevido ou adulteração de identidade;
- *Alta* Neste nível, o objetivo é evitar o uso indevido ou alteração de identidade.

### Identificação

A identificação desta política de assinatura eletrônica é significada pela presença do certificado de Serviço caixa da empresa utilizado para a assinatura eletrônica do documento.

O número de série do certificado é:

4B-51-5A-35-00-00-00-00-01-EF.

### Publicação do documento

Esta política de assinatura eletrônica é publicada após sua aprovação pelo Information Systems Security Manager (RSSI) da Enterprise Box Office.

### Processo de atualização

#### Circunstâncias fazendo uma atualização necessária

A atualização desta política de assinatura eletrônica pode incluir a evolução da lei em vigor (ver "Sujeito do Documento"), o surgimento de novas ameaças e novas medidas de segurança, e a consideração das observações dos diversos atores.

Esta política de assinatura eletrônica é revisada pelo menos a cada dois anos.

#### Levando em conta as observações

Todos os comentários, ou desejos de mudança, nesta política de assinatura e devem ser enviados por e-mail para:[Contato](mailto:contact.guichet-entreprises@finances.gouv.fr).

Esses comentários e desejos de desenvolvimento são revisados pelo RSSI do serviço Business Box Office, que, se necessário, inicia o processo de atualização desta política de assinatura eletrônica.

#### Informações dos atores

Informações sobre a versão atual desta política e versões anteriores estão disponíveis na tabela de versões no topo deste documento.

A publicação de uma nova versão da política de assinatura consiste em:

# Colocando a política de assinatura eletrônica on-line em formato HTML
# arquivar a versão anterior após o rótulo "obsoleto" ser afixado em cada página.

Uma nova versão entra em vigor e um período de validade
-------------------------------------------------------

Uma nova versão da política de assinatura só entra em vigor um (1) mês após ser postada on-line e permanece válida até que uma nova versão entre em vigor.

O atraso de um mês é usado pelos destinatários para levar em conta em seus aplicativos as alterações feitas pela nova política de assinatura eletrônica.

Atores
------

### O signatário do arquivo

O signatário do arquivo é o inscrito ou seu agente fazendo a formalidade de solicitar o reconhecimento da qualificação profissional

#### O papel do signatário

O papel do signatário é colocar sua assinatura eletrônica em seu arquivo incorporando a formalidade e documentos de apoio,

Para colocar uma assinatura eletrônica em seu arquivo, o signatário compromete-se a usar uma ferramenta de assinatura de acordo com esta política de assinatura eletrônica.

#### As obrigações do signatário

Essas obrigações estão descritas nos capítulos abaixo.

### Ferramenta de assinatura usada

O signatário deve verificar os dados que assinará antes de assinar sua assinatura eletrônica.

### Procedimento de assinatura eletrônica utilizado

O procedimento de assinatura eletrônica depende do tipo de formalidade (ver "Campo de Aplicação").

**No caso de se aplicarem as disposições legais previstas no artigo 123-30-10 do Código de Comércio**, o signatário deve verificar uma caixa no final da formalidade indicando que declara na honra a exatidão das informações da formalidade e assina esta declaração.

É então mencionado no espaço de assinatura do formulário (Cerfa), da conformidade desta assinatura com as expectativas do parágrafo 3º do artigo A. 123-4 do Código de Comércio.

Parágrafo 3º do artigo A. 123-4 do Código de Comércio:

*(3) Ao verificar a caixa de computador fornecida para este fim, o declarante declara na honra a exatidão dos itens declarados de acordo com a seguinte fórmula: "Declaro na honra a exatidão das informações da formalidade e assino esta declaração nº...., feita a..., a.... ».*

**No caso de aplicação de disposições legais mais rigorosas do que as previstas no artigo 123-30-10 do Código de Comércio**, o signatário deve anexar uma cópia de sua cópia declarada de acordo com o original, de acordo com o disposto no parágrafo 2º do artigo A. 123-4 do Código de Comércio:

Parágrafo 2º do artigo A. 123-4 do Código de Comércio:

*(2) Os documentos que o compõem foram digitalizados. A cópia do comprovante de identidade é digitalizada após ter sido previamente fornecida com um certificado escrito à mão da honra do cumprimento do original, uma data e a assinatura manuscrita da pessoa que faz a declaração.*

O documento a ser assinado eletronicamente (formulário Cerfa e cópia do Documento de Identidade) é apresentado a ele, bem como o comprovante de comprovação listando as condições e consequências da assinatura eletrônica do documento.

O signatário deve verificar uma caixa indicando que leu o acordo de prova e a aceita sem reservas.

Um código para validar a assinatura eletrônica é enviado a ele em seu celular, o número do qual ele indicou quando criou sua conta.

A inserção deste código desencadeia a vedação do documento (formulário/Cerfa e cópia do ID), ou seja, sua assinatura eletrônica com base em um certificado, seu hashing e o carimbo de tempo do conjunto.

O documento lacrado é arquivado e encaminhado aos destinatários envolvidos.

Os casos de erro mais comuns são:

**Caso 1 - Código Incorreto**

Caso funcional:

*O usuário inseriu um código incorreto (por exemplo, letras em vez de números).*

Exemplo de uma mensagem exibida:

*O código que você forneceu está incorreto. Verifique o código que foi enviado por SMS e insira-o novamente.*

**Caso 2 - Código expirado**

Caso funcional:

*O usuário inseriu o código recebido por SMS tarde demais (a vida útil de um Código OTP - One Time Password é de 20 minutos).*

Exemplo de uma mensagem exibida:

*A validade do seu código expirou. Você deve renovar seu pedido de assinatura eletrônica.*

**Caso 3 - Código bloqueado**

Caso funcional:

*O usuário fez n apreensões mal sucedidas de códigos OTP. Para evitar tentativas de invasão, o número de telefone do usuário é identificado como bloqueado no sistema.*

Exemplo de uma mensagem exibida:

*Devido a inúmeros erros de digitação de sua parte, seu número de telefone não está mais autorizado a assinar seu procedimento eletronicamente. Convidamos você a fornecer um novo número de telefone em sua conta de usuário ou entrar em contato com a Caixa de Negócios para desbloquear seu número de telefone atual.*

### Proteção e uso do arquivo assinado

Este capítulo trata apenas de arquivos que tratam de formalidades para as quais se aplicam disposições legais mais rigorosas do que as previstas no artigo R. 123-30-10 do Código de Comércio.

O documento assinado eletronicamente é armazenado em um cofre eletrônico com um arquivo legível com todos os vestígios de sua vedação para fins de auditoria.

O cofre eletrônico só é acessível pelo administrador e pelo RSSI do Business Box Service.

O acesso ao cofre eletrônico só é possível com a ajuda de um certificado de nome e qualquer ação nele é rastreada.

Para o RSSI do serviço Enterprise Box Office, somente é permitido o acesso para fins de auditoria por solicitação justificada (por exemplo, solicitação judicial).

O documento assinado é encaminhado às agências beneficiárias encarregadas de investigar o arquivo associado.

A proteção do arquivo transmitido eletronicamente é de responsabilidade do destinatário e deve cumprir as disposições dos Regulamentos Gerais de Segurança e dos Regulamentos Gerais de Proteção de Dados.

O RSSI do serviço Business Box Office garante o cumprimento dessas disposições.

### Arquivamento e exclusão de arquivo assinado

O arquivo é arquivado e excluído de acordo com as disposições da Seção R. 123-30-12 do Código de Comércio.

Artigo R. 123-30-12 do Código de Comércio:

*Se o registrante utilizar um serviço provisório de retenção de dados proposto pelo serviço de notificação em condições consistentes com a Lei 78-17 de 6 de janeiro de 1978 relativa a computadores, arquivos e liberdades, todos os dados e arquivos relativos ao registrante na mídia de computador em que estão listados são excluídos ao final do período de retenção provisória de até doze meses.*

Provedores de soluções de assinatura eletrônica
-----------------------------------------------

A solução deve incorporar as expectativas desta política de assinatura eletrônica na estrutura dos dados de assinatura.

A Bilheteria de Negócios
------------------------

O RSSI do serviço de Bilheteria de Negócios garante o cumprimento das disposições desta política de assinatura eletrônica para as Qualificações de Caixas de Telesserviço disponibilizadas aos usuários.

Verifica-se que os sistemas de informação de origem e destinatário dos feeds não representam um risco para a integridade e confidencialidade dos arquivos armazenados e transmitidos.

Destinatários
-------------

Os destinatários monitoram a integridade da assinatura eletrônica do arquivo e garantem que os procedimentos para armazenar e excluir os arquivos recebidos estejam de acordo com as normas.

### Dados de verificação

Para realizar as auditorias, a Enterprise Box Office utiliza as informações de vedação do arquivo assinado eletronicamente (ver "Proteção e uso do arquivo assinado").

### Proteger significa

A Bilheteria Empresarial garante que os meios necessários sejam implementados para proteger os equipamentos que fornecem os serviços de validação.

As medidas tomadas dizem respeito a ambos:

- proteger o acesso físico e lógico aos equipamentos a apenas pessoas autorizadas;
- Disponibilidade do serviço
- monitoramento e monitoramento do serviço.

### Assistência aos signatários

A assistência com o uso do procedimento de assinatura eletrônica é fornecida pelo suporte do serviço Business Box acessível por e-mail:[Apoio](mailto:support.guichet-entreprises@helpline.fr).

Assinatura e validação eletrônica
---------------------------------

### Dados assinados

Os dados assinados consistem no formulário/Cerfa e na cópia do ID na forma de um único arquivo PDF com uma área de assinatura eletrônica mutável.

Todo o arquivo PDF está assinado e, de fato, nenhuma modificação nele é possível depois de ter sido selado.

### Recursos de assinatura

A assinatura atende às expectativas da especificação "XML Signature Syntax and Processing (XMLDsig)" desenvolvida pela W3C (World Wide Web Consortium:<https://www.w3.org/>), bem como as extensões de formato de assinatura especificadas no padrão Europeu XML Advanced Electronic Signature (XADES) da ETSI (<https://www.etsi.org>).

A assinatura eletrônica implantada implementa um nível "substancial" de identificação (ISO/IEC 29115:2013), ou seja, de acordo com a regulamentação do eIDAS uma assinatura eletrônica avançada

#### Tipo de assinatura

A assinatura eletrônica é do tipo[Envolvido](https://fr.wikipedia.org/wiki/XML_Signature).

#### Padrão de assinatura

A assinatura eletrônica deve atender ao padrão[XMLDsig, revisão 1.1 de
fevereiro de 2002](https://www.w3.org/TR/2002/REC-xmldsig-core-20020212).

A assinatura eletrônica deve atender ao padrão XAdES-EPES (Explicit Policy based Electronic Signature),[ETSI TS 101 903 v1.3.2](http://uri.etsi.org/01903/v1.3.2).

De acordo com a norma XadES, as Propriedades assinadas (Propriedades assinadas/Propriedades de Assinatura Assinada) devem conter o seguinte:

- Certificação de assinatura
- assinando data e hora (SigningTime) no formato UTC.

Algoritmos que podem ser usados para assinatura
-----------------------------------------------

#### Algoritmo de condensação

O cálculo da iteração condensada da função de compressão nos demais blocos obtidos cortando a mensagem (esquema iterativo de Merkle-Damgord).

Este algoritmo está acessível no link:[Merkle-Damgord](https://fr.wikipedia.org/wiki/Construction_de_Merkle-Damgård)

#### Algoritmo de assinatura

O algoritmo de assinatura é baseado em RSA/[SHA 256](https://fr.wikipedia.org/wiki/SHA-2).

#### Algoritmo de canonização

O algoritmo de canonização é[c14N](https://www.w3.org/TR/xml-exc-c14n/).

### Condições para declarar arquivo assinado válido

As condições para declarar válido o arquivo assinado são:

- O signatário cumpriu suas obrigações:- baixando uma cópia de seu ID com uma cópia de acordo com o original e aceitando o acordo de prova,
  + inserindo o código enviado pelo método SMS ("One Time Password");
- A Bilheteria Empresarial cumpriu suas obrigações:- disponibilizando um certificado gerado por um CA certificando sua origem (provedor do serviço de assinatura eletrônica) e cujas características são:*Chave Pública - RSA 2048 bits, algoritmo de assinatura - RSA SHA 256* ;
- O provedor de serviços de assinatura e cumpriu suas obrigações:- assinando e selando o arquivo PDF de acordo com as características descritas nos capítulos anteriores.

A verificação da validade da assinatura pode ser feita através do Adobe Acrobat.

Disposições legais
------------------

### Dados nominais

A operadora tem o direito de acessar, modificar, corrigir e excluir dados sobre ele que ele pode exercer por e-mail no seguinte endereço:[Contato](mailto:contact.guichet-entreprises@finances.gouv.fr)

### Legislação aplicável - Resolução de litígios

Estas disposições estão sujeitas à lei francesa.

Qualquer disputa sobre a validade, interpretação ou execução dessas disposições será encaminhada à jurisdição do Tribunal Administrativo de Paris.

