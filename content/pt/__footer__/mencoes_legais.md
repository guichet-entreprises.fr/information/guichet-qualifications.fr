﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Mentions légales" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-mentions_legales" -->
<!-- var(last-update)="2020-05-03 14:56:36" -->
<!-- var(lang)="pt" -->
<!-- var(site:lang)="pt" -->

Legal
=====


<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
Última atualização:<!-- begin-var(last-update) -->2020-05-03 14:56:36<!-- end-var -->

Identificação do editor
-----------------------

```
Service à compétence nationale Guichet Entreprises
120 rue de Bercy – Télédoc 766
75572 Paris cedex 12


```
A Bilheteria de Negócios, criada pelo[decreto de 22 de abril de 2015](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=A1B17F7C2CD76682B8F5E3E5CE690458.tpdila13v_1?cidTexte=JORFTEXT000030517635&dateTexte=&oldAction=rechJO&categorieLien=id&idJO=JORFCONT000030517419), está sob a autoridade do[Diretoria Corporativa](https://www.entreprises.gouv.fr/) (DGE) dentro do[Ministério da Economia e Finanças](https://www.economie.gouv.fr/).

Projeto editorial, monitoramento, manutenção técnica e atualizações de sites[www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/) são fornecidos pela Bilheteria Empresarial.

Editor
------

Sr. Florent Tournois, Chefe da Bilheteria de Negócios.

Provedor de acomodação
----------------------

```
Cloud Temple
215 avenue Georges Clémenceau
92024 Nanterre
Tel : +33 1 41 91 77 77


```
Objeto do site
--------------

O www.guichet-qualifications.fr site permite que qualquer residente da União Europeia ou da Área Econômica Europeia tenha suas qualificações profissionais reconhecidas e pratique uma profissão regulamentada na França com vistas a um estabelecimento sustentável ([estabelecimento livre](https://www.guichet-entreprises.fr/pt/eugo/libre-etablissement-le/)) ou temporárias ([entrega de serviço gratuito](https://www.guichet-entreprises.fr/pt/eugo/libre-prestation-de-services-lps/)). Também permite que os residentes na França conheçam as possibilidades de acesso e exercício de uma profissão regulamentada em outro Estado-Membro.

As formalidades são objeto de assinatura eletrônica, os termos e condições que estão disponíveis no site no seguinte link:[Política de assinatura eletrônica](politica_de_assinatura_eletronica.md).

Além disso, o site www.guichet-qualifications.fr fornece aos contratantes um conjunto de informações sobre as formalidades, procedimentos e requisitos das atividades regulamentadas de acordo com o artigo R. 123-2 do Código de Comércio.

Processamento de dados pessoais
-------------------------------

Os dados pessoais contidos no arquivo único de acordo com o Regulamento Geral de Proteção de Dados estão sujeitos às disposições disponíveis neste site no seguinte link:[Política de proteção de dados](protecao_politica_da.md).

De acordo com este regulamento e a Lei de Computadores e Liberdades de 6 de janeiro de 1978, você tem o direito de acessar, corrigir, modificar e excluir os dados que lhe dizem respeito.

Há uma série de maneiras que você pode exercer este direito:

- Entrando em contato com o Centro de Formalidades Empresariais (CFE) que recebe o arquivo de declaração;
- Enviando um e-mail para o suporte
- enviando uma carta para:


```
  Service à compétence nationale Guichet Entreprises
  120 rue de Bercy – Télédoc 766
  75572 Paris cedex 12
```

Direitos reprodutivos
---------------------

O conteúdo deste site se enquadra na legislação francesa e internacional de direitos autorais e propriedade intelectual.

Todos os elementos gráficos do site são de propriedade do serviço Business Box Office. Qualquer reprodução ou adaptação das páginas do site que levariaos elementos gráficos é estritamente proibida.

Também é proibido qualquer uso de conteúdo para fins comerciais.

Qualquer citação ou reimpressão de conteúdo do site deve ter obtido a permissão do editor. A origem (www.guichet-qualifications.fr) e a data da cópia terão que ser indicadas, bem como o serviço "Business Box".

Links para as páginas do site
-----------------------------

Qualquer site público ou privado pode se vincular às páginas do site www.guichet-qualifications.fr. Não há necessidade de pedir autorização prévia. No entanto, a origem das informações precisará ser esclarecida, por exemplo, na forma de: "Reconhecimento de Qualificação Profissional (fonte: www.guichet-qualifications.fr, um site de bilheteria empresarial)". As páginas do site www.guichet-qualifications.fr não devem ser aninhadas nas páginas de outro site. Eles precisarão ser exibidos em uma nova janela ou guia.

Links para páginas de sites ao ar livre
---------------------------------------

Os links no site www.guichet-qualifications.fr podem direcionar o usuário para sites externos cujo conteúdo não pode de forma alguma envolver a responsabilidade da Bilheteria Corporativa.

Ambiente técnico
----------------

Alguns navegadores podem bloquear janelas neste site por padrão. Para permitir que você visualize certas páginas, você deve permitir que as janelas se abram quando o navegador lhe oferecer clicando na faixa de aviso exibida na parte superior da página.

Se o seu navegador não tiver uma mensagem de aviso, você precisa configurá-la para permitir que as janelas se abram para o site www.guichet-qualifications.fr.

