﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Utilisation des cookies sur guichet-qualifications.fr" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-cookies" -->
<!-- var(last-update)="2020-05-03 14:56:24" -->
<!-- var(lang)="pt" -->
<!-- var(site:lang)="pt" -->

Usando cookies em guichet-qualifications.fr
===========================================


<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
Última atualização:<!-- begin-var(last-update) -->2020-05-03 14:56:24<!-- end-var -->

O que são cookies e como você usa a Bilheteria de Negócios?
-----------------------------------------------------------

Bilheteria Pode usar cookies quando um usuário navega em seu site. Cookies são arquivos enviados ao navegador através de um servidor web para registrar as atividades do usuário durante seu tempo de navegação. O uso de cookies permite reconhecer o navegador da Web usado pelo usuário para facilitar sua navegação. Os cookies também são usados para medir o público do site e produzir estatísticas de consulta.

Os cookies usados pela Enterprise Box Office não fornecem referências para deduzir dados pessoais ou informações pessoais dos usuários para identificar um determinado usuário. Eles são de natureza temporária, com o único propósito de tornar a transmissão subsequente mais eficiente. Nenhum cookie usado no site terá um período de vigor de mais de dois anos.

Os usuários têm a opção de configurar seu navegador para serem notificados sobre o recebimento de cookies e recusar a instalação. Ao banir cookies ou desabilitá-los, o usuário pode não ser capaz de acessar certos recursos do site.

Em caso de recusa ou desativação de cookies, a sessão deve ser reiniciada.

Que tipos de cookies são usados por guichet-qualifications.fr?
--------------------------------------------------------------

### Cookies técnicos

Eles permitem que o usuário navegue pelo site e use alguns de seus recursos.

### Cookies de análise

O Box Office usa cookies de análise de audiência para quantificar o número de visitantes. Esses cookies são usados para medir e analisar como os usuários navegam no site.

