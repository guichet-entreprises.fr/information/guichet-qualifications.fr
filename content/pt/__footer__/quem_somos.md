﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Qui sommes-nous ?" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-qui_sommes_nous" -->
<!-- var(last-update)="2020-05-03 14:58:15" -->
<!-- var(lang)="pt" -->
<!-- var(site:lang)="pt" -->

Quem somos nós?
===============


<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
Última atualização:<!-- begin-var(last-update) -->2020-05-03 14:58:15<!-- end-var -->

O site www.guichet-qualifications.fr é projetado e desenvolvido pelo serviço nacionalmente responsável "Business Box Office", criado pelo[decreto de 22 de abril de 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&dateTexte=20200109) (JORF de 25 de abril de 2015). Está sob a autoridade do[Diretoria Corporativa](https://www.entreprises.gouv.fr/) dentro do[Ministério da Economia e Finanças](https://www.economie.gouv.fr/).

O serviço nacional "Business Box Office" gerencia sites[www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/) E[www.guichet-entreprises.fr](https://www.guichet-entreprises.fr/) [1] que juntos constituem a loja eletrônica de uma parada definida pelas diretivas europeias[2006/123/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32006L0123) E[2005/36](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036). Atms desse tipo existem em toda a Europa e são federados dentro do projeto " [Eugo](http://ec.europa.eu/internal_market/eu-go/index_fr.htm) Comissão Europeia.

Na Business Box Office, incentivamos a mobilidade profissional dos residentes da União Europeia ou da Área Econômica Europeia, fornecendo acesso a informações completas sobre o acesso e o exercício de profissões regulamentadas na França por meio de "registros profissionais".

O site[www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/) destina-se a todos os residentes da União Europeia ou da Área Econômica Europeia. Informa-os sobre as possibilidades de supor que suas qualificações profissionais são reconhecidas e que exercem uma profissão regulamentada na França com vistas a um estabelecimento sustentável ([estabelecimento livre](https://www.guichet-entreprises.fr/pt/eugo/libre-etablissement-le/)) ou temporárias ([entrega de serviço gratuito](https://www.guichet-entreprises.fr/pt/eugo/libre-prestation-de-services-lps/)). Também permite que os residentes na França conheçam as possibilidades de acesso e exercício de uma profissão regulamentada em outro Estado-Membro.

