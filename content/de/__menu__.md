﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="[Accueil](index.md)" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__menu__" -->
<!-- var(last-update)="2020-05-03 15:08:55" -->
<!-- var(lang)="de" -->
<!-- var(site:lang)="de" -->

[Startseite](index.md)
======================

Verstehe
========

[](willkommen.md)
----------------

[](verstehe/warum_ihre_berufliche_qualifikation_anerkannt_werden.md)
-----------------------------------------------------------------------------

[](verstehe/berufsqualifikation_in_frankreich.md)
---------------------------------------------------------

[](verstehe/diplom_mitgliedsstaat_ue_eee.md)
--------------------------------------------

[](verstehe/franzosisches_diplom.md)
----------------------------------

[](verstehe/reglementierte_berufe.md)
------------------------------------------

[](verstehe/europaische_visitenkarte.md)
--------------------------------------------------

Reglementierte Berufe
=====================

# <!-- include(../reference/de/directive-qualification-professionnelle/_list_menu.md) -->

[Dienstleistungen](https://welcome.guichet-qualifications.fr/)
==============================================================

[Meine Dateien](https://dashboard.guichet-qualifications.fr/) @authenticated
============================================================================

