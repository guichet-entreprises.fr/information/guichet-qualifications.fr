﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Mentions légales" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-mentions_legales" -->
<!-- var(last-update)="2020-05-03 15:10:44" -->
<!-- var(lang)="de" -->
<!-- var(site:lang)="de" -->

Rechtlich
=========


<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
Neueste Aktualisierung:<!-- begin-var(last-update) -->2020-05-03 15:10:44<!-- end-var -->

Editor-Identifikation
---------------------

```
Service à compétence nationale Guichet Entreprises
120 rue de Bercy – Télédoc 766
75572 Paris cedex 12


```
Das Business Box Office, das von der[Dekret vom 22. April 2015](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=A1B17F7C2CD76682B8F5E3E5CE690458.tpdila13v_1?cidTexte=JORFTEXT000030517635&dateTexte=&oldAction=rechJO&categorieLien=id&idJO=JORFCONT000030517419), untersteht der[Unternehmensdirektion](https://www.entreprises.gouv.fr/) (DGE) innerhalb der[Ministerium für Wirtschaft und Finanzen](https://www.economie.gouv.fr/).

Redaktionsdesign, Monitoring, technische Wartung und Website-Updates[www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/) werden von der Business Box Office bereitgestellt.

Editor
------

Florent Tournois, Leiter der Business Box Office.

Unterkunftsanbieter
-------------------

```
Cloud Temple
215 avenue Georges Clémenceau
92024 Nanterre
Tel : +33 1 41 91 77 77


```
Objekt der Website
------------------

Die www.guichet-qualifications.fr-Website ermöglicht es jedem Einwohner der Europäischen Union oder des Europäischen Wirtschaftsraums, seine beruflichen Qualifikationen anerkennen zu lassen und einen reglementierten Beruf in Frankreich auszuüben, um eine nachhaltige Niederlassung zu schaffen ([freie Niederlassung](https://www.guichet-entreprises.fr/de/eugo/libre-etablissement-le/)) oder vorübergehend ([kostenlose Service-Lieferung](https://www.guichet-entreprises.fr/de/eugo/libre-prestation-de-services-lps/)). Darüber hinaus können Einwohner Frankreichs die Möglichkeiten des Zugangs zu einem reglementierten Beruf in einem anderen Mitgliedstaat kennenlernen.

Die Formalitäten sind Gegenstand einer elektronischen Signatur, deren Geschäftsbedingungen auf der Website unter folgendem Link abrufbar sind:[Richtlinie für elektronisches Signieren](richtlinie_fur_elektronische_signaturen.md).

Darüber hinaus stellt die www.guichet-qualifications.fr Website den Auftragnehmern eine Reihe von Informationen über die Formalitäten, Verfahren und Anforderungen regulierter Tätigkeiten gemäß Artikel R. 123-2 des Handelskodex zur Verfügung.

Verarbeitung personenbezogener Daten
------------------------------------

Personenbezogene Daten, die in der einzelnen Datei gemäß den Datenschutz-Grundverordnungen enthalten sind, unterliegen den auf dieser Website unter folgendem Link verfügbaren Bestimmungen:[Datenschutzerklärung](politikschutz_gibt.md).

Gemäß dieser Verordnung und dem Computer and Freedoms Act vom 6. Januar 1978 haben Sie das Recht, auf die Daten zuzugreifen, sie zu korrigieren, zu ändern und zu löschen, die Sie betreffen.

Es gibt eine Reihe von Möglichkeiten, wie Sie dieses Recht ausüben können:

- Durch Kontaktaufnahme mit dem Business Formalities Centre (CFE), das die Deklarationsdatei erhält;
- Senden einer E-Mail an den Support
- indem Sie einen Brief an:


```
  Service à compétence nationale Guichet Entreprises
  120 rue de Bercy – Télédoc 766
  75572 Paris cedex 12
```

Reproduktionsrechte
-------------------

Der Inhalt dieser Website fällt unter das französische und internationale Urheberrecht und das Recht des geistigen Eigentums.

Alle grafischen Elemente der Website sind im Besitz des Business Box Office-Dienstes. Jegliche Vervielfältigung oder Anpassung der Seiten der Website, die die grafischen Elemente übernehmen würde, ist strengstens untersagt.

Jegliche Verwendung von Inhalten für kommerzielle Zwecke ist ebenfalls untersagt.

Jede Zitatierung oder Neudruckung von Inhalten von der Website muss die Erlaubnis des Herausgebers eingeholt haben. Die Quelle (www.guichet-qualifications.fr) und das Datum der Kopie müssen angegeben werden, ebenso wie der "Business Box"-Service.

Links zu den Seiten der Website
-------------------------------

Jede öffentliche oder private Website ist berechtigt, auf die Seiten der www.guichet-qualifications.fr Website zu verlinken. Es besteht keine Notwendigkeit, eine vorherige Genehmigung zu beantragen. Die Herkunft der Informationen muss jedoch geklärt werden, z. B. in Form von: "Professional Qualification Recognition (Quelle: www.guichet-qualifications.fr, a business box office site)". Die Seiten der www.guichet-qualifications.fr Website sollten nicht innerhalb der Seiten einer anderen Website verschachtelt werden. Sie müssen in einem neuen Fenster oder Tab angezeigt werden.

Links zu Außenseiten
--------------------

Links auf der www.guichet-qualifications.fr-Website können den Benutzer auf externe Websites weiterleiten, deren Inhalt in keiner Weise in die Verantwortung der Enterprise Box Office übergeht.

Technisches Umfeld
------------------

Einige Browser blockieren möglicherweise standardmäßig Fenster auf dieser Website. Damit Sie bestimmte Seiten anzeigen können, müssen Sie zulassen, dass sich die Fenster öffnen, wenn der Browser Ihnen das bietet, indem Sie auf das Warnkopfband klicken, das dann oben auf der Seite angezeigt wird.

Wenn Ihr Browser keine Warnmeldung hat, müssen Sie sie so einrichten, dass Fenster für die www.guichet-qualifications.fr Website geöffnet werden können.

