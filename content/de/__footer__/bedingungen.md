﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Conditions générales d’utilisation" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-cgu" -->
<!-- var(last-update)="2020-05-03 15:10:25" -->
<!-- var(lang)="de" -->
<!-- var(site:lang)="de" -->

Allgemeine Geschäftsbedingungen
===============================


<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
Neueste Aktualisierung:<!-- begin-var(last-update) -->2020-05-03 15:10:25<!-- end-var -->

Präambel
--------

In diesem Dokument werden die Bedingungen für die Verwendung von Teleservice-Box-Qualifikationen für Benutzer dargelegt. Sie ist Teil des Rechtsrahmens:

- von[Datenschutz-Grundverordnung](https://www.cnil.fr/de/reglement-europeen-protection-donnees) ;
- [Richtlinie 2005/36/EG](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036) Europäisches Parlament;
- [2013/55/EU-Richtlinie](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32013L0055) Europäisches Parlament;
- von[Verordnung 910/2014](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32014R0910) Europäisches Parlament und der Rat vom 23. Juli 2014 (e-IDAS) über elektronische Identifizierung und vertrauenswürdige Dienste für elektronische Transaktionen im Binnenmarkt;
- der[8. Dezember 2005 Bestellung](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000636232) elektronischen Austausch zwischen Nutzern und Verwaltungsbehörden sowie zwischen Verwaltungsbehörden und[Dekret Nr. 2010-112 vom 2. Februar 2010](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021779444&categorieLien=id) anwendung der Artikel 9, 10 und 12 dieser Verordnung;
- von[Gesetz 78-17 vom 6. Januar 1978 geändert](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460) Informatik, Akten und Freiheiten;
- der[Artikel 441-1 des Strafgesetzbuches](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006418753&cidTexte=LEGITEXT000006070719&dateTexte=20020101) ;
- der[Artikel R. 123-30 des Handelskodex](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006256107&dateTexte=&categorieLien=cid) ;
- der[Dekret vom 22. April 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&categorieLien=id) Schaffung eines national kompetenten Dienstes, der als "Geschäftsfenster" bezeichnet wird.

Gegenstand des Dokuments
------------------------

Zweck dieses Dokuments ist es, die allgemeinen Nutzungsbedingungen der Teleservice Box-Qualifikationen, die unten als "Dienst" bezeichnet werden, zwischen dem nationalen Dienst Guichet Enterprises und den Nutzern zu definieren.

Der nationale Dienst Guichet Enterprises untersteht der Direktion Unternehmen des Ministeriums für Wirtschaft und Finanzen.

Definition und Zweck des Dienstes
---------------------------------

Der dienstbetrieblich von Guichet Enterprises (im Folgenden "Business Box Service" genannt) trägt dazu bei, die Verfahren im Zusammenhang mit der Gründung, der Änderung der Situation und der Einstellung eines Geschäfts französischer und europäischer Nutzer zu vereinfachen.

Dieser Dienst unterliegt der Sicherheitsgenehmigung gemäß ANSSI Interministerial Instruction 901/SGDSN/ANSSI (NOR: PRMD1503279 28/01/2015) und dem PGSSI der Wirtschafts- und Finanzministerien (NOR: FCPP1622039A) vom 1. August 2016.

Die Nutzung des Dienstes ist optional und kostenlos. Im Rahmen der über diesen Dienst erbrachten Formalitäten, wie z. B. Unternehmensgründung, können jedoch Online-Zahlungen angefordert werden. Diese sind sicher.

Alle Empfänger des Dienstes werden unten als Partneragenturen bezeichnet.

Gemäß Abschnitt R.123-3 des Handelskodex werden die gesammelten Daten zur Verarbeitung an folgende Partnerorganisationen übermittelt:

"1) Vorbehaltlich der Bestimmungen des 2. und 3. Bezirksgewerbes schaffen und verwalten die Gebietskammern der Handels- und Handelskammern die Formalitätenzentren der Unternehmen, die für

# Händler;
# Handelsunternehmen.

(2) Die Handwerkskammern der Region schaffen und verwalten die zuständigen Zentren für Personen und Unternehmen, die im Handelsverzeichnis eingetragen sind, sowie für Personen, die von der Registrierungsbefreiung gemäß Artikel 19 des Gesetzes 96-603 vom 5. Juli 1996 über die Entwicklung und Förderung des Handels und des Handwerks profitieren. , mit Ausnahme der im 3. dieses Artikels genannten.

3. Die Nationale Handwerkskammer gründet und verwaltet das Zentrum für Personen und Unternehmen, die im Register der handwerklichen Bootsunternehmen eingetragen sind.

(4) Kommerzielle oder kommerziell regierende Register schaffen und verwalten die Zentren, die für

# Zivile und nichtkommerzielle Unternehmen;
# Liberale Sportverbände;
# juristische Personen, die in das Register der Gepontisten und Gesellschaften eingetragen sind, die nicht in den
# Gewerbliche und gewerbliche öffentliche Einrichtungen;
# Handelsvertreter;
# wirtschaftlichen Interessengruppen und europäischen wirtschaftlichen Interessengruppen.

(5) Gewerkschaften für die soziale Sicherheit und die Erhebung von Familienleistungen (Urssaf) oder allgemeine Sozialversicherungsfonds schaffen und verwalten die Zentren, die für folgende Bereiche zuständig sind:

# Personen, die als reguläre Randtätigkeit in einer geregelten oder nichtkommerziellen, handwerklichen oder landwirtschaftlichen selbständigen Tätigkeit tätig sind;
# Arbeitgeber, deren Unternehmen nicht im Handels- und Gewerberegister, im Handelsverzeichnis oder im Register der handwerklichen Bootsunternehmen eingetragen sind und die sich nicht bei den im 6. genannten Zentren melden.

(6) Die Landwirtschaftskammern schaffen und verwalten die zuständigen Zentren für Einzelpersonen und Körperschaften, die landwirtschaftliche Tätigkeiten als[...]. »

Funktionen
----------

Der Dienst ermöglicht dem Benutzer:

- Zugang zu Unterlagen, in denen die Verpflichtungen der Business Formality Centres (CFEs) sowie die Bausteine der Deklarationsakte und der Antragsakte detailliert beschrieben sind;
- erstellen Sie ein Benutzerkonto, das Zugriff auf persönlichen Speicherplatz bietet. Dieser Bereich ermöglicht es dem Benutzer, seine personenbezogenen Daten zu verwalten und zu nutzen, die Informationen über ihn und die Dokumente und Belege zu bewahren, die für den Abschluss der Verwaltungsverfahren erforderlich sind.

Von seinem Space aus kann der Benutzer:

- seine eindeutige Akte gemäß Artikel R. 123-1 des Handelskodex zu erstellen, die die Deklarationsdatei und gegebenenfalls Anträge auf Genehmigung enthält;
- ihre Erklärungsakte und erforderlichenfalls Anträge auf Genehmigung an das zuständige Zentrum für Geschäftsformalitäten zu übermitteln;
- Zugriff auf die Folgeinformationen über die Bearbeitung seiner Akte und gegebenenfalls seiner Bewerbungsakte;
- CFEs in die Lage zu versetzen, die Behandlungen durchzuführen, die erforderlich sind, um die in Artikel R. 123-21 des Artikels R. 123-21 des Handelskodex erfassten Informationen zu nutzen: Eingang der einzigen Akte nach Artikel 2 des Gesetzes 94-126 vom 11. Februar 1994 über individuelle Initiative und Geschäftstätigkeit, die von der in Artikel R. 123-21 des Handelskodex erfassten juristischen Person weitergeleitet wird; Erhalt von Folgeinformationen über die Bearbeitung dieser Dateien, wie sie von Partneragenturen und Behörden bereitgestellt werden; Übermittlung von Folgeinformationen aus der Bearbeitung eindeutiger Dateien an die juristische Person, die unter Artikel R. 121-21 des Handelskodex fällt.

So registrieren Sie sich und nutzen den Dienst
----------------------------------------------

Der Zugang zum Service ist für jedermann offen und kostenlos. Es ist optional und ist nicht exklusiv für andere Zugriffskanäle, damit der Benutzer seine Formalitäten erledigen kann.

Für die Verwaltung des Zugriffs auf den persönlichen Bereich des Dienstes gibt der Benutzer die folgenden Informationen weiter:

- E-Mail-Adresse des Benutzers.
- Das vom Benutzer gewählte Passwort

Die Nutzung des Dienstes erfordert die Bereitstellung personenbezogener Daten für die Erstellung des einzelnen Ordners.
Die Verarbeitung personenbezogener Daten ist in der vorliegenden Datenschutzerklärung[Hier](politikschutz_gibt.md).

Die Nutzung des Dienstes erfordert eine Verbindung und einen Internetbrowser. Der Browser muss so konfiguriert sein, dass*Cookies* Sitzungssitzung.

Um ein optimales Surferlebnis zu gewährleisten, empfehlen wir die Verwendung der folgenden Browserversionen:

- Firefox Version 45 und mehr;
- Google Chrome Version 48 und mehr.

Tatsächlich unterstützen andere Browser möglicherweise bestimmte Funktionen des Dienstes möglicherweise nicht.

Der Service ist für ein 1024-768 Pixel Display optimiert. Es wird empfohlen, die neueste Version des Browsers zu verwenden und ihn regelmäßig zu aktualisieren, um von Sicherheitspatches und bester Leistung zu profitieren.

Spezifische Nutzungsbedingungen des Signaturdienstes
----------------------------------------------------

Der elektronische Signaturdienst ist direkt über den Dienst zugänglich.

Artikel R. 123-24 des Handelskodex gemäß der Verordnung 910/2014 des Europäischen Parlaments und des Rates vom 23. Juli 2014 (e-IDAS) sind die Verweise, die für den elektronischen Signaturdienst des Dienstes gelten.

Rollen und Engagement
---------------------

### Corporate Box Office-Verpflichtung

# Der Enterprise Box Office-Dienst implementiert und betreibt den Dienst in Übereinstimmung mit dem in der Präambel dargelegten Rechtsrahmen.
# Die Enterprise Box Office verpflichtet sich, alle erforderlichen Maßnahmen zu ergreifen, um die Sicherheit und Vertraulichkeit der vom Benutzer bereitgestellten Informationen zu gewährleisten.
# Die Business Box Office verpflichtet sich, den Schutz der im Rahmen des Dienstes gesammelten Daten zu gewährleisten, einschließlich der Verhinderung von Verfälschungen, beschädigt oder von unbefugten Dritten zugegriffen, in Übereinstimmung mit den Maßnahmen der Verordnung vom 8. Dezember 2005 über den elektronischen Austausch zwischen Nutzern und Verwaltungsbehörden und zwischen Verwaltungsbehörden, Dekret Nr. 2010-112 vom 2. Februar 2010 über die Anwendung der Artikel 9, 10 und 12 dieser Verordnung und Verordnung 2016/679 des Europäischen Parlaments und des Rates vom 27. April 2016 über die Verarbeitung personenbezogener Daten.
# Die Business Box Office und Partneragenturen garantieren den Zugang, die Berichtigung und die Widerspruchsrechte der Nutzer des Dienstes gemäß dem Gesetz 78-17 vom 6. Januar 1978 über die Computernutzung von Dateien und Freiheiten sowie die Verordnung Nr. 2016/679 des Europäischen Parlaments und des Rates vom 27. April 2016 über die Verarbeitung personenbezogener Daten.
Dieses Recht kann auf verschiedene Weise ausgeübt werden:
a) durch Kontaktaufnahme mit dem Business Formalities Centre (CFE), das die Erklärungsakte erhält;
b- durch Senden einer E-Mail an den Support-
c- durch Zusendung eines Schreibens an:

<blockquote><p>Service à compétence nationale Guichet Entreprises</p>
<p>120 rue de Bercy – Télédoc 766</p>
<p>75572 Paris cedex 12</blockquote># Die Business Box Office und Partnerorganisationen verpflichten sich, die vom Nutzer über den Dienst übermittelten Informationen und Dokumente nicht zu kommerzialisieren und nicht außerhalb der gesetzlich vorgesehenen Fälle an Dritte weiterzugeben.
# Die Business Box Office und Partnerorganisationen verpflichten sich, die vom Nutzer über den Dienst übermittelten Informationen und Dokumente nicht zu kommerzialisieren und nicht außerhalb der gesetzlich vorgesehenen Fälle an Dritte weiterzugeben.
# Die Enterprise Box Office verpflichtet sich, die Rückverfolgbarkeit aller Aktionen sicherzustellen, die von allen Benutzern des Dienstes, einschließlich der Von benutzern, durchgeführt werden.
# Der Enterprise Box Office-Dienst bietet Benutzern Unterstützung im Falle eines Vorfalls oder einer definierten Sicherheitswarnung.

Benutzerbindung
---------------

# Der Benutzer füllt seine Datei online aus und validiert sie, indem er möglicherweise die notwendigen Teile für die Behandlung der Datei anhängt.
# Am Ende der Erstellung der Datei wird eine Zusammenfassung der vom Benutzer bereitgestellten Informationen auf dem Bildschirm angezeigt, damit der Benutzer sie überprüfen und bestätigen kann. Nach der Bestätigung wird die Datei an Partneragenturen weitergeleitet. Die Bestätigung und Übermittlung des Formulars durch den Benutzer lohnt sich.
# Diese Geschäftsbedingungen sind von jedem Benutzer verlangt, der ein Benutzer des Dienstes ist.

Verfügbarkeit und Weiterentwicklung des Dienstes
------------------------------------------------

Der Service ist 7 Tage die Woche, 24 Stunden am Tag verfügbar.

Das Enterprise Box Office behält sich jedoch die Möglichkeit vor, den Dienst ohne vorherige Ankündigung aus Wartungsgründen oder aus anderen für notwendig erachteten Gründen weiterzuentwickeln, zu modifizieren oder auszusetzen. Die Nichtverfügbarkeit des Dienstes berechtigt Sie nicht zu einer Entschädigung. Im Falle der Nichtverfügbarkeit des Dienstes wird dem Benutzer dann eine Informationsseite mit hinweiserung dieser Nichtverfügbarkeit angezeigt. er wird dann zu einem späteren Zeitpunkt zu seinem Schritt eingeladen.

Die Nutzungsbedingungen dieser Nutzungsbedingungen können jederzeit ohne vorherige Ankündigung geändert werden, abhängig von Änderungen des Dienstes, Änderungen der Rechtsvorschriften oder Vorschriften oder aus einem anderen Grund, der als notwendig erachtet wird.

Verantwortung
-------------

# Die Verantwortung für den Enterprise Box Office-Dienst kann nicht im Falle eines Identitätsdiebstahls oder einer betrügerischen Nutzung des Dienstes übernommen werden.
# Die an die Online-Dienste der Partneragenturen übermittelten Daten bleiben in der Verantwortung des Nutzers, auch wenn sie mit den im Service zur Verfügung gestellten technischen Mitteln übermittelt werden.
Der Benutzer kann sie jederzeit von Partneragenturen ändern oder löschen.
Er kann alle Informationen aus seinem Konto löschen, indem er seine Daten aus dem Dienst löscht.
# Der Nutzer wird daran erinnert, dass jeder, der eine falsche Aussage für sich selbst oder andere macht, insbesondere für die Strafen nach Artikel 441-1 des Strafgesetzbuches haftbar gemacht wird, die Strafen von bis zu drei Jahren Haft und eine Geldstrafe von 45.000 Euro vorsehen.

