﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Politique de protection des données personnelles" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-politique_protection_donnees" -->
<!-- var(last-update)="2020-05-03 15:11:23" -->
<!-- var(lang)="de" -->
<!-- var(site:lang)="de" -->

Datenschutzerklärung
====================


<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
Neueste Aktualisierung:<!-- begin-var(last-update) -->2020-05-03 15:11:23<!-- end-var -->

| Version | Datum      | Objekt                                                  |
|---------|------------|---------------------------------------------------------|
| V1      | 25/05/2018 | Erste Version                                           |
| V2      | 01/09/2018 | Aktualisierte Informationen zum Datenschutzbeauftragten |

Die Datenschutzrichtlinie (die "Richtlinie") informiert Sie darüber, wie Ihre Daten vom Dienst der National Business Bank gesammelt und verarbeitet werden, welche Sicherheitsmaßnahmen zur Gewährleistung ihrer Integrität und Vertraulichkeit implementiert werden und welche Rechte Sie haben, um ihre Nutzung zu kontrollieren.

Diese Politik ergänzt die[Nutzungsbedingungen](bedingungen.md) (CGU) sowie, falls erforderlich,[Rechtlich](rechtliche_hinweise.md). Als solche gilt diese Politik als in diese Dokumente aufgenommen.

Im Rahmen der Entwicklung unserer Dienstleistungen und der Umsetzung neuer Regulierungsstandards müssen wir diese Politik möglicherweise ändern. Wir laden Sie ein, es regelmäßig zu lesen.

Wer sind wir?
-------------

Das Business Box Office, das von der[Dekret vom 22. April 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&categorieLien=id) unter der Aufsicht der[Unternehmensdirektion](https://www.entreprises.gouv.fr/) (DGE) innerhalb der[Ministerium für Wirtschaft und Finanzen](https://www.economie.gouv.fr/).

Der Business Box Office-Dienst implementiert einen Teleservice-guichet-qualifcations.fr (nachfolgend "Teleservice Box Office Professional Qualifications").

Die Berufsqualifikationen der Teleservice Box Office richten sich an alle Einwohner der Europäischen Union oder des Europäischen Wirtschaftsraums. Sie informiert sie über die Möglichkeiten, ihre beruflichen Qualifikationen anerkannt zu lassen und in Frankreich einen reglementierten Beruf auszuüben, um eine nachhaltige (freie Niederlassung) oder vorübergehende (kostenlose Erbringung von Dienstleistungen) zu erhalten.

Darüber hinaus können Einwohner Frankreichs die Möglichkeiten des Zugangs zu einem reglementierten Beruf in einem anderen Mitgliedstaat kennenlernen.

Die Teleservice Box Office Professional Qualifications ist Teil des rechtlichen Rahmens:

- von[Datenschutz-Grundverordnung](https://www.cnil.fr/de/reglement-europeen-protection-donnees) ;
- [Richtlinie 2005/36/EG](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036) Europäisches Parlament;
- [2013/55/EU-Richtlinie](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32013L0055) Europäisches Parlament;
- von[Verordnung 910/2014](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32014R0910) Europäisches Parlament und der Rat vom 23. Juli 2014 (e-IDAS) über elektronische Identifizierung und vertrauenswürdige Dienste für elektronische Transaktionen im Binnenmarkt;
- der[8. Dezember 2005 Bestellung](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000636232) elektronischen Austausch zwischen Nutzern und Verwaltungsbehörden sowie zwischen Verwaltungsbehörden und[Dekret Nr. 2010-112 vom 2. Februar 2010](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021779444&categorieLien=id) anwendung der Artikel 9, 10 und 12 dieser Verordnung;
- von[Gesetz 78-17 vom 6. Januar 1978 geändert](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460) Informatik, Akten und Freiheiten;
- der[Artikel 441-1 des Strafgesetzbuches](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006418753&cidTexte=LEGITEXT000006070719&dateTexte=20020101) ;
- der[Artikel R. 123-30 des Handelskodex](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006256107&dateTexte=&categorieLien=cid) ;
- der[Dekret vom 22. April 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&categorieLien=id) Schaffung eines national kompetenten Dienstes, der als "Geschäftsfenster" bezeichnet wird.

Welche Organisation implementieren wir zum Schutz Ihrer Daten?
--------------------------------------------------------------

Die Einhaltung und Umsetzung der geltenden Datenschutzbestimmungen wird vom Datenschutzbeauftragten des Business Bank Service überwacht.

Innerhalb des Ministeriums für Wirtschaft und Finanzen, von dem die Generaldirektion Unternehmen abhängt, sind die Dienststellen des leitenden Verteidigungsbeamten für die Überprüfung der Umsetzung und Einhaltung der Bestimmungen der Datenschutz-Grundverordnung zuständig.

Wie kann ich mit uns in Kontakt treten?
---------------------------------------

Wenn Sie Fragen zur Verwendung Ihrer personenbezogenen Daten durch die Enterprise Box Office haben, lesen Sie bitte diese Richtlinie und wenden Sie sich über die folgenden Kontaktinformationen an unseren Datenschutzbeauftragten:

Per Post an:

```
Le Délégué à la protection des données des ministères économique et financier
Délégation aux Systèmes d’Information
139, rue de Bercy – Télédoc 322
75572 PARIS CEDEX 12


```
Per E-Mail wird das Objekt "Datenschutz" angegeben, um:[Kontakt](kontakt.md).

Was sind personenbezogene Daten?
--------------------------------

Personenbezogene Daten sind Informationen, die Sie direkt oder indirekt identifizieren. Dazu gehören beispielsweise Ihr Name, Vorname, Adresse, Telefonnummer oder E-Mail-Adresse sowie die IP-Adresse Ihres Computers oder Informationen zur Nutzung von Teleservice.

Wann erheben wir personenbezogene Daten?
----------------------------------------

Die Business Box Office sammelt personenbezogene Daten, wenn Sie:

- Erstellen Sie Ihren persönlichen Raum
- formalitätüber die Teleservice Business Box.

Welche personenbezogenen Daten verarbeiten wir?
-----------------------------------------------

Die personenbezogenen Daten, mit denen wir uns befassen, beziehen sich nur auf die Formalitäten der Anerkennung von Berufsqualifikationen gemäß den Bestimmungen der Artikel R. 123-30-10 bis R. 123-30-14 des Handelskodex.

Die gesammelten Daten ermöglichen es, die Datei wie erwartet unter Abschnitt R. 123-30-10 des Handelskodex zu kompilieren.

Die gesammelten Daten ermöglichen es insbesondere den Cerfa-Dokumenten, die mit den Formalitäten und Verfahren verbunden sind, die erforderlich sind, um sich für eine Berufsqualifikation oder einen europäischen Berufsausweis zu qualifizieren.

Die folgende Liste fasst die von uns verarbeiteten personenbezogenen Daten zusammen.

Für die Verwaltung des Zugriffs auf den persönlichen Bereich des Dienstes gibt der Benutzer die folgenden Informationen weiter:

- Die vom Benutzer gewählte Login-ID
- Das vom Benutzer gewählte Passwort
- Die E-Mail-Adresse des Benutzers
- Die Mobiltelefonnummer
- IP-Anmeldeadresse.

Für die Nutzung des persönlichen Speicherplatzes gibt der Benutzer die folgenden Informationen mit:

- Nachname;
- Verwenden Sie den Namen/die Name der Frau;
- Übungsname
- Vorname;
- Geburtsdatum;
- Landgeburt;
- Staatsangehörigkeit;
- Sozialversicherungsnummer;
- Wohnadresse (einschließlich Land);
- Telefon
- E‑mail
- Diplom, Urkunde, Titel;
- Berufserfahrung
- ein professionelles Projekt;
- Qualität oder Funktion;
- Aktuelle Beschäftigung
- Arbeitsort (Ort, Land);
- auf die besetzte Position betitelt.

Mit welchen persönlichen Belegen beschäftigen wir uns?
------------------------------------------------------

Die persönlichen Belege, mit denen wir uns befassen, beziehen sich nur auf die Formalitäten der Anerkennung von Berufsqualifikationen oder die Erlangung des Europäischen Berufsausweises gemäß den Vorschriften im Zusammenhang mit der regulierten Tätigkeit.

Die Belege werden verwendet, um die Datei wie erwartet nach Artikel R. 123-30-10 des Handelskodex zu erstellen.

Die Liste der Belege wird von der zuständigen Behörde genehmigt, die für die Untersuchung des Falles zuständig ist.

Zu den persönlichen Belegen gehören Dokumente, die die Identität oder den Aufenthalt rechtfertigen. Diese Belege werden zu den gleichen Bedingungen wie personenbezogene Daten aufbewahrt.

Ihre Speicherung erfolgt in Räumen, die von außen nicht zugänglich sind (Internet) und deren Download oder Übertragung ist gemäß den Bestimmungen im Kapitel "Wie werden Ihre personenbezogenen Daten geschützt?" verschlüsselt.

Was ist unser Cookie-Management?
--------------------------------

Die Cookie-Verwaltung wird auf der Seite beschrieben:[https://www.guichet-entreprises.fr/de/cookies/](https://www.guichet-entreprises.fr/de/cookies/).

Alle verwalteten Cookies verwenden den sicheren Cookie-Modus.

Sichere Cookies sind eine Art von Cookies, die ausschließlich über verschlüsselte Verbindungen (https) übertragen werden.

Technische Cookies enthalten die folgenden personenbezogenen Daten:

- Name;
- Vorname;
- E‑mail
- Telefon.

Analyse-Cookies enthalten die Login-IP-Adresse.

Welche Rechte haben Sie, um Ihre personenbezogenen Daten zu schützen?
---------------------------------------------------------------------

Sie können auf Ihre Daten zugreifen, sie korrigieren, löschen, ihre Verwendung einschränken oder sich derVerarbeitung widersetzen.

Wenn Sie der Meinung sind, dass die Verwendung Ihrer personenbezogenen Daten gegen die Datenschutzbestimmungen verstößt, haben Sie die Möglichkeit,[Cnil](https://www.CNIL.fr/).

Sie haben auch das Recht, Richtlinien für den Umgang mit Ihren personenbezogenen Daten im Todesfall festzulegen. Spezifische Richtlinien können beim Behandlungsleiter registriert werden. Allgemeine Richtlinien können bei einem von der CNIL zertifizierten digitalen Trust eines Drittanbieters registriert werden. Sie haben die Möglichkeit, diese Richtlinien jederzeit zu ändern oder zu entfernen.

Wenn ja, können Sie uns auch bitten, Ihnen die personenbezogenen Daten zur Verfügung zu stellen, die Sie uns in einem lesbaren Format zur Verfügung gestellt haben.

Sie können Ihre verschiedenen Anfragen an senden an:

```
Service à compétence nationale Guichet Entreprises
120 rue de Bercy – Télédoc 766
75572 Paris cedex 12


```
Ihre Bewerbungen müssen mit einer Kopie eines Ausweises versehen sein und werden von unseren Dienststellen geprüft.

Wie werden Ihre personenbezogenen Daten geschützt?
--------------------------------------------------

Die Enterprise Box Office implementiert alle branchenüblichen Praktiken und Sicherheitsverfahren, um eine Verletzung Ihrer personenbezogenen Daten zu verhindern.

Alle bereitgestellten Informationen werden auf sicheren Servern gespeichert, die von unseren technischen Anbietern gehostet werden.

Wenn die Datenübertragung erforderlich und autorisiert ist, stellt die Enterprise Box Office sicher, dass diese Dritten ausreichende Garantien bieten, um ein angemessenes Schutzniveau zu gewährleisten. Die Austausche werden verschlüsselt und die Server durch gegenseitige Anerkennung authentifiziert.

In Übereinstimmung mit den Datenschutzbestimmungen verpflichtet sich der Business Guichet-Dienst im Falle eines Verstoßes, diese Verletzung der zuständigen Aufsichtsbehörde und gegebenenfalls den betroffenen Personen mitzuteilen.

#### Persönlicher Raum

Alle an Ihren persönlichen Bereich übermittelten Daten werden verschlüsselt und der Zugriff durch die Verwendung eines persönlichen Passworts gesichert. Sie sind verpflichtet, dieses Passwort vertraulich zu behandeln und nicht offenzulegen.

#### Konservierungszeit

Personenbezogene Daten, die von der Enterprise Box Office verarbeitet werden, werden gemäß Artikel R. 123-30-14 des Handelskodex für einen Zeitraum von zwölf Monaten aufbewahrt.

#### Zu Kreditkartendaten

Der Enterprise Checkout-Dienst speichert oder speichert keine Bankdaten von Benutzern, die zum Zeitpunkt ihrer Formalitäten zahlungspflichtiger Benutzer sind. Unser Anbieter verwaltet Transaktionsdaten im Auftrag der Enterprise Box Office in Übereinstimmung mit den strengsten Sicherheitsregeln, die in der Online-Zahlungsbranche durch den Einsatz von Verschlüsselungsprozessen gelten.

An wen werden Ihre Daten übermittelt?
-------------------------------------

Die gesammelten Daten ermöglichen die Erstellung der Datei wie erwartet gemäß Artikel R.123-30-10 des Handelskodex.

Gemäß Artikel R.123-30-12 des Handelskodex werden die erhobenen Daten zur Verarbeitung an die zuständigen Behörden übermittelt.

