﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Qui sommes-nous ?" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-qui_sommes_nous" -->
<!-- var(last-update)="2020-05-03 15:12:43" -->
<!-- var(lang)="de" -->
<!-- var(site:lang)="de" -->

Wer sind wir?
=============


<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
Neueste Aktualisierung:<!-- begin-var(last-update) -->2020-05-03 15:12:43<!-- end-var -->

Die www.guichet-qualifications.fr Website wird von dem national zuständigen Service "Business Box Office" konzipiert und entwickelt, der von der[Dekret vom 22. April 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&dateTexte=20200109) (JORF vom 25. April 2015). Sie steht unter der Aufsicht der[Unternehmensdirektion](https://www.entreprises.gouv.fr/) innerhalb der[Ministerium für Wirtschaft und Finanzen](https://www.economie.gouv.fr/).

Der nationale Dienst "Business Box Office" verwaltet Websites[www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/) Und[www.guichet-entreprises.fr](https://www.guichet-entreprises.fr/) [1] die zusammen die in den europäischen Richtlinien definierte elektronische zentrale Anlaufstelle bilden[2006/123/EG](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32006L0123) Und[2005/36](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036). Solche Arten gibt es in ganz Europa und sind im Rahmen des Projekts " [Eugo](http://ec.europa.eu/internal_market/eu-go/index_fr.htm) Europäische Kommission.

Am Business Box Office fördern wir die berufliche Mobilität von Einwohnern der Europäischen Union oder des Europäischen Wirtschaftsraums, indem wir durch "Berufsunterlagen" Zugang zu vollständigen Informationen über den Zugang und die Ausübung reglementierter Berufe in Frankreich gewähren.

Die Website[www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/) richtet sich an alle Einwohner der Europäischen Union oder des Europäischen Wirtschaftsraums. Sie informiert sie über die Möglichkeiten, ihre beruflichen Qualifikationen anzuerkennen und in Frankreich einen reglementierten Beruf auszuüben, um eine nachhaltige Niederlassung zu ermöglichen ([freie Niederlassung](https://www.guichet-entreprises.fr/de/eugo/libre-etablissement-le/)) oder vorübergehend ([kostenlose Service-Lieferung](https://www.guichet-entreprises.fr/de/eugo/libre-prestation-de-services-lps/)). Darüber hinaus können Einwohner Frankreichs die Möglichkeiten des Zugangs zu einem reglementierten Beruf in einem anderen Mitgliedstaat kennenlernen.

