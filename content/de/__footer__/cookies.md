﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Utilisation des cookies sur guichet-qualifications.fr" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-cookies" -->
<!-- var(last-update)="2020-05-03 15:10:30" -->
<!-- var(lang)="de" -->
<!-- var(site:lang)="de" -->

Verwenden von Cookies auf guichet-qualifications.fr
===================================================


<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
Neueste Aktualisierung:<!-- begin-var(last-update) -->2020-05-03 15:10:30<!-- end-var -->

Was sind Cookies und wie verwenden Sie die Business Box Office?
---------------------------------------------------------------

Box Office Kann Cookies verwenden, wenn ein Benutzer seine Website durchsucht. Cookies sind Dateien, die über einen Webserver an den Browser gesendet werden, um die Aktivitäten des Benutzers während der Browserzeit aufzuzeichnen. Die Verwendung von Cookies ermöglicht es, den vom Benutzer verwendeten Webbrowser zu erkennen, um die Navigation zu erleichtern. Cookies werden auch verwendet, um das Publikum der Website zu messen und Konsultationsstatistiken zu erstellen.

Cookies, die von Enterprise Box Office verwendet werden, enthalten keine Verweise, um die persönlichen Daten oder persönlichen Informationen der Nutzer abzuleiten, um einen bestimmten Benutzer zu identifizieren. Sie sind vorübergehender Natur, mit dem alleinigen Zweck, die nachträgliche Übertragung effizienter zu gestalten. Kein Auf der Website verwendetes Cookie hat eine Nutzungsdauer von mehr als zwei Jahren.

Benutzer haben die Möglichkeit, ihren Browser so einzurichten, dass er über den Erhalt von Cookies informiert wird und die Installation ablehnt. Durch das Verbot von Cookies oder deren Deaktivierung kann der Benutzer möglicherweise nicht auf bestimmte Funktionen der Website zugreifen.

Im Falle einer Cookie-Verweigerung oder Deaktivierung sollte die Sitzung neu gestartet werden.

Welche Arten von Cookies werden von guichet-qualifications.fr verwendet?
------------------------------------------------------------------------

### Technische Cookies

Sie ermöglichen es dem Benutzer, die Website zu durchsuchen und einige seiner Funktionen zu verwenden.

### Analyse-Cookies

Box Office verwendet Zielgruppenanalyse-Cookies, um die Anzahl der Besucher zu quantifizieren. Diese Cookies werden verwendet, um zu messen und zu analysieren, wie Benutzer auf der Website navigieren.

