﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Politique de signature électronique" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-politique_signature_electronique" -->
<!-- var(last-update)="2020-05-03 15:12:36" -->
<!-- var(lang)="de" -->
<!-- var(site:lang)="de" -->

Richtlinie für elektronisches Signieren
=======================================


<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
Neueste Aktualisierung:<!-- begin-var(last-update) -->2020-05-03 15:12:36<!-- end-var -->

| Version | Datum      | Objekt        |
|---------|------------|---------------|
| V1      | 27/09/2018 | Erste Version |

Gegenstand des Dokuments
------------------------

Die elektronische Signatur, die an einem Datensatz angebracht ist, gewährleistet die Integrität der übermittelten Daten, die Nichtabstreitbarkeit der signierten Daten und die Authentizität des Senders.

Diese Richtlinie über die elektronische Signatur ist ein Dokument, das die Berechtigungsanforderungen der Empfangsstellen, d. h. der zuständigen Agenturen, einer Datei beschreibt, auf der eine elektronische Signatur im Zusammenhang mit dem elektronischen Austausch gemäß Artikel R. 123-30-10 des Handelskodex angebracht wird.

Die zuständigen Stellen sind die für die Anhörung von Anträgen auf Anerkennung von Berufsqualifikationen zuständigen Stellen.

In diesem Abschnitt*"Ist eine Unterschrift erforderlich, ist die Verwendung einer elektronischen Signatur mit den Garantien nach Art. 1367 Abs. 2 des Bürgerlichen Gesetzbuches zulässig."*, und Art. 1367 Abs. 2 des Bürgerlichen Gesetzbuches,*"Wenn es elektronisch ist, besteht es aus der Verwendung eines zuverlässigen Identifizierungsverfahrens, das seine Verbindung zu der Handlung garantiert, an die es sich anhängt. Die Zuverlässigkeit dieses Verfahrens wird, solange nichts anderes nachgewiesen wird, bei der Erstellung der elektronischen Signatur, der Identität des versicherten Unterzeichners und der Integrität des garantierten Rechtsakts unter den im Staatsrat festgelegten Bedingungen vorausgesetzt."*,

Die geltenden Vorschriften für die Umsetzung der elektronischen Signatur der Teleservice Box Qualifications sind aus folgenden Texten abgeleitet:

- Verordnung 910/2014 des Europäischen Parlaments und des Rates vom 23. Juli 2014 (e-IDAS) über elektronische Identifizierung und vertrauenswürdige Dienste für elektronische Transaktionen im Binnenmarkt;
- Zivilgesetzbuch;
- Dekret Nr. 2017-1416 vom 28. September 2017 über die elektronische Signatur.

Für den Fall, dass die elektronische Signatur die Bereitstellung personenbezogener Daten erfordert, werden die anwendbaren Vorschriften aus den folgenden Texten abgeleitet:

- Verordnung (EU) 2016/679 des Europäischen Parlaments und des Rates vom 27. April 2016, Verordnung über den Schutz natürlicher Personen bei der Verarbeitung personenbezogener Daten und des freien Verkehrs dieser Daten und Aufhebung der Richtlinie 95/46/EG (Allgemeine Verordnung über den Datenschutz);
- Gesetz 78-17 vom 6. Januar 1978 über Computer, Dateien und Freiheiten.

Dieses Dokument mit dem Artikel "Electronic Signing Policy of Teleservice Box Qualifications" beschreibt alle Regeln und Bestimmungen, die die Anforderungen definieren, an die jeder der an diesem entmaterialisierten Austausch beteiligten Akteure die Übertragung und den Empfang von Streams erfüllt.

Dieses Dokument ist für:

- Die zuständigen Stellen;
- potenziellen Anbietern, die im Auftrag dieser Empfängeragenturen an diesen entmaterialisierten Austauschen teilnehmen.

Im Rest dieses Dokuments:

- Die oben genannten Empfängerorganisationen werden als "Empfänger" bezeichnet;
- die oben erhitzten Börsen werden als "Datensätze" bezeichnet;
- der nationale Dienst Business Bank wird als "Business Box Office-Dienst" bezeichnet.

Umfang
------

Für jede Formalität im Zusammenhang mit einem Antrag auf Anerkennung der Berufsqualifikation im Sinne der Richtlinie 2005/36/EG des Rates vom 7. September 2005 über die Anerkennung von Berufsqualifikationen ("Richtlinie über Berufsqualifikationen") und ihre Ausübung im Sinne von Artikel R. 123-30-8 des Handelskodex ist eine elektronische Unterschrift erforderlich.

Artikel 9 der Verordnung Nr. 2016-1809 vom 22. Dezember 2016 sieht für die von diesen Formalitäten Betroffenen vor,

*Staatsangehörige eines Mitgliedstaats der Europäischen Union oder Vertragspartei des Abkommens über den Europäischen Wirtschaftsraum, die ihre beruflichen Qualifikationen in einem Mitgliedstaat der Europäischen Union erworben haben oder Vertragspartei des Abkommens über den Europäischen Wirtschaftsraum sind, und die Anerkennung dieser für die Ausübung eines reglementierten Berufes im Sinne der Richtlinie 2005 des Europäischen Parlaments und des Rates von 2005/EG zu erlangen, können alle Anforderungen, Verfahren oder Formalitäten im Zusammenhang mit der Anerkennung der Berufsqualifikationen unter im Staatsrat festgelegten Bedingungen aus der Ferne und elektronisch ausfüllen oder überwachen.*

Die elektronische Signatur weist je nach Art der eingereichten Dateien unterschiedliche Merkmale auf.

**Für den Fall, dass die rechtsvorschriften Bestimmungen des Artikels R. 123-30-10 des Handelskodex gelten,**erfüllt die erforderliche elektronische Signatur die Bestimmungen des Artikels 1316-4 Satz 1 des Bürgerlichen Gesetzbuches:

*Wenn es elektronisch ist, besteht es aus der Verwendung eines zuverlässigen Identifizierungsverfahrens, das seine Verbindung zu der Handlung garantiert, an die es sich selbst anhängt.*

**Für den Fall, dass strengere Rechtsvorschriften als die in Art. R. 123-30-10 des Handelskodex** (z. B. Anerkennung der Berufsqualifikation für die Tätigkeit eines Allgemeinmediziners) entspricht die elektronische Unterschrift den Bestimmungen des Artikels 1 des Dekrets Nr. 2017-1416 vom 28. September 2017 (Bezug auf die Verordnung "eIDAS" Nr.910/2014):

*Die Zuverlässigkeit eines elektronischen Signaturprozesses wird, bis zum Beweis anders, vorausgesetzt, wenn dieser Prozess eine qualifizierte elektronische Signatur implementiert.*

*Bei einer elektronischen Signatur handelt es sich um eine elektronische Signatur gemäß Artikel 26 der genannten Verordnung, die mit einem qualifizierten elektronischen Signaturbilderstellthergestellten erstellt wird, das den Anforderungen von Art. 29 dieser Verordnung entspricht und auf einer Bescheinigung beruht, die als elektronische Signatur qualifiziert ist, die den Anforderungen von Art. 28 dieser Verordnung entspricht.*

Artikel 26 der Verordnung "eIDAS" 910/2014:

*Eine erweiterte elektronische Signatur erfüllt die folgenden Anforderungen:*

# *Seien Sie eindeutig an den Unterzeichner gebunden;*
# *Den Unterzeichner identifizieren lassen;*
# *Mit elektronischen Signaturerstellungsdaten erstellt worden zu sein, die der Unterzeichner unter seiner ausschließlichen Kontrolle mit einem hohen Maß an Vertrauen verwenden kann;*
# *Mit den Daten verknüpft werden, die dieser Signatur zugeordnet sind, sodass alle nachfolgenden Änderungen an den Daten erkennbar sind.*

Für Informationen werden durch die Verordnung "eIDAS" 910/2014 drei Garantiestufen bereitgestellt:

- *Niedrig* Auf dieser Ebene besteht das Ziel einfach darin, das Risiko von Missbrauch oder Identitätsmanipulation zu verringern;
- *Erhebliche* Auf dieser Ebene soll das Risiko von Missbrauch oder Identitätsmanipulation erheblich verringert werden;
- *Hoch* Auf dieser Ebene soll Missbrauch oder Identitätsänderung verhindert werden.

### Identifizierung

Die Identifizierung dieser Richtlinie für elektronische Signaturen wird durch das Vorhandensein des Firmen-Box-Service-Zertifikats angezeigt, das für die elektronische Signatur des Dokuments verwendet wird.

Die Seriennummer des Zertifikats lautet:

4B-51-5A-35-00-00-00-00-01-EF.

### Veröffentlichung des Dokuments

Diese Richtlinie für elektronische Signaturen wird nach ihrer Genehmigung durch den Information Systems Security Manager (RSSI) der Enterprise Box Office veröffentlicht.

### Aktualisierungsprozess

#### Umstände, die eine Aktualisierung erforderlich machen

Die Aktualisierung dieser Politik der elektronischen Signatur kann die Weiterentwicklung des geltenden Gesetzes (siehe "Thema des Dokuments"), das Entstehen neuer Bedrohungen und neuer Sicherheitsmaßnahmen sowie die Berücksichtigung der Beobachtungen der verschiedenen Akteure umfassen.

Diese Richtlinie über die elektronische Signatur wird mindestens alle zwei Jahre überprüft.

#### Unter Berücksichtigung der Bemerkungen

Alle Kommentare oder Änderungswünsche zu dieser E-Signatur-Richtlinie sind per E-Mail an:[Kontakt](mailto:contact.guichet-entreprises@finances.gouv.fr).

Diese Kommentare und Entwicklungswünsche werden vom RSSI des Business Box Office-Dienstes überprüft, der erforderlichenfalls den Prozess der Aktualisierung dieser Richtlinie für elektronische Signaturen einleitet.

#### Informationen der Akteure

Informationen zur aktuellen Version dieser Richtlinie und früheren Versionen finden Sie in der Tabelle der Versionen oben in diesem Dokument.

Die Veröffentlichung einer neuen Version der Signaturrichtlinie besteht aus:

# Die Richtlinie für elektronische Signaturen im HTML-Format online stellen
# archivieren Sie die vorherige Version, nachdem auf jeder Seite das Etikett "veraltet" angebracht wurde.

Eine neue Fassung tritt in Kraft und eine Gültigkeitsdauer
----------------------------------------------------------

Eine neue Version der Signaturrichtlinie tritt erst einen (1) Kalendermonat nach ihrer Online-Veröffentlichung in Kraft und bleibt gültig, bis eine neue Version in Kraft tritt.

Die Verzögerung von einem Monat wird von den Empfängern genutzt, um in ihren Anträgen die Änderungen zu berücksichtigen, die durch die neue Richtlinie für elektronische Signaturen vorgenommen wurden.

Akteure
-------

### Der Unterzeichner der Datei

Unterzeichner der Akte ist der Registrant oder sein Bevollmächtigter, der die Formalität der Beantragung der Anerkennung einer Berufsqualifikation

#### Die Rolle des Unterzeichners

Aufgabe des Unterzeichners ist es, seine elektronische Unterschrift in seine Akte mit den Formalitäten und Belegen aufzunehmen.

Um eine elektronische Signatur in seine Akte zu legen, verpflichtet sich der Unterzeichner, ein Signaturwerkzeug in Übereinstimmung mit dieser elektronischen Signaturrichtlinie zu verwenden.

#### Die Verpflichtungen des Unterzeichners

Diese Verpflichtungen werden in den nachstehenden Kapiteln beschrieben.

### Signaturwerkzeug verwendet

Der Unterzeichner muss die Daten, die er unterzeichnen wird, überprüfen, bevor er seine elektronische Unterschrift unterzeichnet.

### Elektronisches Signaturverfahren verwendet

Das Verfahren der elektronischen Signatur hängt von der Art der Formalität ab (siehe "Anwendungsgebiet").

**Für den Fall, dass die rechtsvorschriften Bestimmungen des Artikels R. 123-30-10 des Handelskodex gelten,**hat der Unterzeichner am Ende der Formalität ein Kästchen anzuchecken, das angibt, dass er die Richtigkeit der Informationen über die Formalität zu Ehren erklärt und diese Erklärung unterschreibt.

Es wird dann im Unterschriftsraum des Formblatts (Cerfa) der Übereinstimmung dieser Unterschrift mit den Erwartungen von Artikel A. 123-4 Absatz 3 des Handelskodex erwähnt.

Artikel A. 123-4 Absatz 3 des Handelskodex:

*(3) Durch die Überprüfung der zu diesem Zweck vorgesehenen Computerbox erklärt der Anmelder die Richtigkeit der nach folgender Formel deklarierten Gegenstände: "Ich erkläre auf der Ehrenseite die Richtigkeit der Informationen über die Formalität und unterschreibe diese Erklärung Nr...., gemacht an..., die..., die.... ».*

**Für den Fall, dass strengere Rechtsvorschriften als die in Art. R. 123-30-10 des Handelskodex**hat der Unterzeichner eine Kopie seiner deklarierten Abschrift gemäß dem Original gemäß Artikel A. 123-4 Absatz 2 des Handelskodex beizufügen:

Artikel A. 123-4 Absatz 2 des Handelskodex:

*(2) Die Dokumente, aus denen sie besteht, wurden digitalisiert. Die Kopie des Identitätsnachweises wird gescannt, nachdem ihr zuvor eine handschriftliche Bescheinigung über die Ehre der Originalübereinstimmung, ein Datum und die handschriftliche Unterschrift der Person, die die Erklärung abgibt, vorgelegt wurde.*

Das elektronisch zu unterzeichnende Dokument (Cerfa-Formular und Kopie des Ausweises) wird ihm sowie die Nachweisvereinbarung vorgelegt, in der die Bedingungen und Folgen der elektronischen Signatur des Dokuments aufgeführt sind.

Der Unterzeichner muss ein Kästchen anaktivieren, das angibt, dass er die Beweisvereinbarung gelesen hat und diese vorbehaltlos akzeptiert.

Ein Code zur Bestätigung der elektronischen Signatur wird ihm auf seinem Mobiltelefon zugesandt, dessen Nummer er bei der Erstellung seines Kontos angegeben hat.

Die Eingabe dieses Codes löst die Versiegelung des Dokuments (Formular/Cerfa und Kopie der ID) aus, d.h. seine elektronische Signatur auf der Grundlage eines Zertifikats, sein Hashing und die Zeitstempelung des Sets.

Das versiegelte Dokument wird archiviert und an die betreffenden Empfänger weitergeleitet.

Die häufigsten Fehlerfälle sind:

**Fall 1 - Falscher Code**

Funktionsfall:

*Der Benutzer hat einen falschen Code eingegeben (z. B. Buchstaben statt Zahlen).*

Beispiel für eine angezeigte Meldung:

*Der von Ihnen angegebene Code ist falsch. Bitte überprüfen Sie den Code, der Ihnen per SMS zugesandt wurde, und geben Sie ihn erneut ein.*

**Fall 2 - Code abgelaufen**

Funktionsfall:

*Der Benutzer hat den per SMS empfangenen Code zu spät eingegeben (die Lebensdauer eines OTP - One Time Password Codes beträgt 20 Minuten).*

Beispiel für eine angezeigte Meldung:

*Die Gültigkeit Ihres Codes ist abgelaufen. Sie müssen Ihren Antrag auf elektronische Signatur erneuern.*

**Fall 3 - Blockierter Code**

Funktionsfall:

*Der Benutzer machte N erfolglose Beschlagnahmen von OTP-Codes. Um Hacking-Versuche zu vermeiden, wird die Telefonnummer des Benutzers im System als blockiert identifiziert.*

Beispiel für eine angezeigte Meldung:

*Aufgrund zahlreicher Tippfehler Ihrerseits ist es Ihrer Telefonnummer nicht mehr gestattet, Ihr Verfahren elektronisch zu unterschreiben. Wir laden Sie ein, eine neue Telefonnummer in Ihrem Benutzerkonto einzugeben oder sich an die Business Box zu wenden, um Ihre aktuelle Telefonnummer zu entsperren.*

### Schutz und Verwendung der signierten Datei

Dieses Kapitel behandelt nur Akten, die sich mit Formalitäten befassen, für die strengere Rechtsvorschriften gelten als die in Artikel R. 123-30-10 des Handelskodex.

Das elektronisch signierte Dokument wird in einem elektronischen Tresor mit einer lesbaren Datei aufbewahrt, die alle Spuren seiner Versiegelung zu Prüfungszwecken enthält.

Der elektronische Safe ist nur für seinen Administrator und den RSSI des Business Box Service zugänglich.

Der Zugang zum elektronischen Safe ist nur mit Hilfe eines Namenszertifikats möglich und jede darauf folgende Aktion wird nachverfolgt.

Für den RSSI des Enterprise Box Office-Dienstes ist nur der Zugriff zu Prüfungszwecken auf begründeten Antrag (z. B. Gerichtsantrag) zulässig.

Das signierte Dokument wird an die Empfängeragenturen weitergeleitet, die für die Untersuchung der zugehörigen Datei zuständig sind.

Der Schutz der elektronisch übermittelten Datei liegt in der Verantwortung des Empfängers und muss den Bestimmungen der Allgemeinen Sicherheitsbestimmungen und der Datenschutz-Grundverordnung entsprechen.

Der RSSI des Business Box Office-Dienstes stellt die Einhaltung dieser Bestimmungen sicher.

### Archivieren und Löschen der signierten Datei

Die Datei wird gemäß den Bestimmungen des Abschnitts R. 123-30-12 des Handelskodex archiviert und gelöscht.

Artikel R. 123-30-12 des Handelskodex:

*Nutzt der Registrant einen vom Meldedienst vorgeschlagenen vorläufigen Datenaufbewahrungsdienst unter Bedingungen, die mit dem Gesetz 78-17 vom 6. Januar 1978 über Computer, Dateien und Freiheiten im Einklang stehen, so werden alle Daten und Dateien, die sich auf den Computermedien, in denen sie aufgeführt sind, über den Registranten beziehen, gelöscht, wenn die vorläufige Aufbewahrungsfrist von bis zu zwölf Monaten endet.*

Anbieter elektronischer Signaturlösungen
----------------------------------------

Die Lösung muss die Erwartungen dieser Politik der elektronischen Signatur in die Struktur der Signaturdaten einbeziehen.

Die Business Box Office
-----------------------

Der RSSI des Business Box Office-Dienstes stellt sicher, dass die Bestimmungen dieser Richtlinie für die elektronische Signatur für die Den Benutzern zur Verfügung gestellten Teleservice Box Qualifications eingehalten werden.

Es wird überprüft, ob die Quell- und Empfängerinformationssysteme der Feeds kein Risiko für die Integrität und Vertraulichkeit der gespeicherten und übertragenen Dateien darstellen.

Empfänger
---------

Die Empfänger überwachen die Integrität der elektronischen Signatur der Datei und stellen sicher, dass die Verfahren zum Speichern und Löschen der empfangenen Dateien den Vorschriften entsprechen.

### Verifizierungsdaten

Zur Durchführung der Audits verwendet das Enterprise Box Office die Siegelungsinformationen der elektronisch signierten Datei (siehe "Schutz und Verwendung der signierten Datei").

### Schutz der Mittel

Das Enterprise Box Office stellt sicher, dass die erforderlichen Mittel zum Schutz der Geräte, die die Validierungsdienste bereitstellen, implementiert werden.

Die getroffenen Maßnahmen betreffen beide:

- Schutz des physischen und logischen Zugangs zu Geräten nur für autorisierte Personen;
- Verfügbarkeit des Dienstes
- Überwachung und Überwachung des Dienstes.

### Unterstützung der Unterzeichner

Unterstützung bei der Nutzung des elektronischen Signaturverfahrens wird durch die Unterstützung des Business Box-Dienstes geleistet, der per E-Mail zugänglich ist unter:[Unterstützung](mailto:support.guichet-entreprises@helpline.fr).

Elektronische Signatur und Validierung
--------------------------------------

### Signierte Daten

Die signierten Daten bestehen aus dem Formular/Cerfa und der Kopie der ID in Form einer einzigen PDF-Datei mit einem veränderbaren elektronischen Signaturbereich.

Die gesamte PDF-Datei ist signiert und in der Tat sind keine Änderungen daran möglich, nachdem sie versiegelt wurde.

### Signatur-Funktionen

Die Signatur erfüllt die Erwartungen der von W3C (World Wide Web Consortium:<https://www.w3.org/>) sowie die Signaturformaterweiterungen, die im ETSI-Standard Für die europäische XML Advanced Electronic Signature (XADES) festgelegt sind (<https://www.etsi.org>).

Die eingesetzte elektronische Signatur implementiert eine "erhebliche" Identifizierungsebene (ISO/IEC 29115:2013), d. h. gemäß der eIDAS-Verordnung eine erweiterte elektronische Signatur

#### Art der Signatur

Die elektronische Signatur ist vom Typ[Gewickelt](https://fr.wikipedia.org/wiki/XML_Signature).

#### Signaturstandard

Die elektronische Signatur muss den Standard-[XMLDsig, Revision 1.1 von
Februar 2002](https://www.w3.org/TR/2002/REC-xmldsig-core-20020212).

Die elektronische Signatur muss dem XAdES-EPES-Standard (Explicit Policy based Electronic Signature) entsprechen,[ETSI TS 101 903 v1.3.2](http://uri.etsi.org/01903/v1.3.2).

Gemäß dem XadES-Standard muss SignedProperties (SignedProperties/SignedSignatureProperties) Folgendes enthalten:

- SigningCertificate
- Datum und Uhrzeit (SigningTime) im UTC-Format.

Algorithmen, die für die Signatur verwendet werden können
---------------------------------------------------------

#### Kondensationsalgorithmus

Die Berechnung der Iteration, die der Komprimierungsfunktion auf den übrigen Blöcken durch Ausschneiden der Nachricht (iteratives Schema von Merkle-Damgord) kondensiert wurde.

Dieser Algorithmus ist unter dem Link zugänglich:[Merkle-Damgord](https://fr.wikipedia.org/wiki/Construction_de_Merkle-Damgård)

#### Signaturalgorithmus

Der Signaturalgorithmus basiert auf RSA/[SHA 256](https://fr.wikipedia.org/wiki/SHA-2).

#### Kanonisierungsalgorithmus

Der Kanonisierungsalgorithmus ist[C14n](https://www.w3.org/TR/xml-exc-c14n/).

### Bedingungen für die Deklaration der signierten Datei gültig

Die Bedingungen für die Gültigkeit der signierten Datei sind:

- Der Unterzeichner hat seinen Verpflichtungen nachgekommen:- herunterladen eine Kopie seines Personalausweises mit einer Kopie in Übereinstimmung mit dem Original und die Annahme der Beweisvereinbarung,
  + Eingabe des per SMS gesendeten Codes ("One Time Password")-Methode;
- Die Business Box Office hat ihren Verpflichtungen nachgekommen:- Bereitstellung eines zertifikats, das von einem AC erstellt wird, der seinen Ursprung bescheinigt (Anbieter des elektronischen Signaturdienstes) und dessen Merkmale sind:*Öffentlicher Schlüssel - RSA 2048 Bits, Signaturalgorithmus - RSA SHA 256* ;
- Der E-Signatur-Dienstleister hat seinen Verpflichtungen nachgekommen:- Signieren und Versiegeln der PDF-Datei gemäß den in den vorstehenden Kapiteln beschriebenen Merkmalen.

Die Überprüfung der Gültigkeit der Signatur kann über Adobe Acrobat erfolgen.

Rechtsvorschriften
------------------

### Nominale Daten

Der Beförderer hat das Recht, auf Daten über ihn zuzugreifen, diese zu ändern, zu korrigieren und zu löschen, die er per E-Mail unter folgender Adresse ausüben kann:[Kontakt](mailto:contact.guichet-entreprises@finances.gouv.fr)

### Anwendbares Recht - Streitbeilegung

Diese Bestimmungen unterliegen französischem Recht.

Streitigkeiten über die Gültigkeit, Auslegung oder Durchsetzung dieser Bestimmungen werden an das Verwaltungsgericht Paris verwiesen.

