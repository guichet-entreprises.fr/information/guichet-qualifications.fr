﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(site:package)="https://gitlab.com/|13561843|latest|reference_md|reference/configuration.yml" -->
<!-- var(site:home)="Accueil" -->
<!-- var(site:menu)="__menu__.md" -->
<!-- var(site:footer)="__footer__/__links__.md" -->
<!-- var(site:lang)="de" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Guichet Qualifications  " -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:font_sizer_off)="off" -->
<!-- var(page:breadcrumb_off)="off" -->
<!-- var(key)="main" -->
<!-- var(translation)="Auto" -->
<!-- var(last-update)="2020-05-03 15:08:54" -->
<!-- var(lang)="de" -->

Box Office-Qualifikationen<!-- section-banner:surf.jpg --><!-- color:dark -->
=============================================================================

Eine Adresse für die Anerkennung Ihrer Qualifikationen

[Starten Sie meinen Spaziergang<!-- link-model:box-trans -->](https://profiler.guichet-qualifications.fr/start/)

Eine Adresse für die Anerkennung Ihrer Qualifikationen<!-- section-information:-120px -->
=========================================================================================

Willkommen
----------

Auf dieser Seite finden Sie alle Informationen zur Anerkennung der Berufsqualifikation.

[Erster Besuch<!-- link-model:box -->](willkommen.md)

Entdecken
---------

Sie haben einen Abschluss in einem Mitgliedstaat der Europäischen Union oder des Europäischen Wirtschaftsraums

[Ihre Möglichkeiten<!-- link-model:box -->](verstehe/berufsqualifikation_in_frankreich.md)

Online-Schritte
---------------

Vervollständigen Sie Ihre Online-Formalitäten im Zusammenhang mit der Anerkennung Ihrer beruflichen Qualifikationen in Frankreich!

[Starten<!-- link-model:box -->](https://profiler.guichet-qualifications.fr/start/)

Warum haben Sie Ihre beruflichen Qualifikationen anerkannt?<!-- section:courses -->
===================================================================================

Ein Beruf wird als "reguliert" bezeichnet, wenn es notwendig ist, ein bestimmtes Diplom zu besitzen, bestimmte Prüfungen bestanden zu haben, eine bestimmte Genehmigung erhalten zu haben oder bei einer Berufsorganisation registriert zu sein, um es durchzuführen.

Die Berufsqualifikationen zu vermitteln ist unerlässlich, wenn man in einem anderen Land der Europäischen Union arbeiten möchte, in dem der Beruf geregelt ist. Dies bedeutet, dass seine Ausbildung und/oder Berufserfahrung vom Gastland offiziell anerkannt wird.[Lesen Sie mehr...](verstehe/warum_ihre_berufliche_qualifikation_anerkannt_werden.md)

Eine einzige Adresse für die Anerkennung Ihrer beruflichen Qualifikationen<!-- section:welcome --><!-- color:grey -->
=====================================================================================================================

Der Online-Dienst guichet-qualifications.fr fördert die berufliche Mobilität von Einwohnern der Europäischen Union und des Europäischen Wirtschaftsraums, indem er umfassende Informationen über den Zugang und die Ausübung reglementierter Berufe in Frankreich bereitstellt, um die Anerkennung einer beruflichen Qualifikation zu erreichen.

Dieser Dienst ist die Initiative des Ministeriums für Wirtschaft und Finanzen.

Geschäfte in Frankreich und Europa machen<!-- section-stories:drapeaux.jpg --><!-- color:dark -->
=================================================================================================

Sind Sie Staatsangehöriger der Europäischen Union oder des Europäischen Wirtschaftsraums? Wir fördern die Unternehmensgründung und die berufliche Mobilität in Frankreich und Europa. Guichet-qualifications.fr ist als französisches Einzelfenster für die Anerkennung Ihrer beruflichen Qualifikationen anerkannt, ein Mitglied des von der Europäischen Kommission geschaffenen Eugo-Netzwerks.

Nachdem Sie die Anerkennung Ihrer Qualifikation erlangt haben, wenn Sie sich dauerhaft oder vorübergehend niederlassen möchten, um in Frankreich zu arbeiten, gehen Sie zu:

[guichet-entreprises.fr](https://guichet-entreprises.fr/)

