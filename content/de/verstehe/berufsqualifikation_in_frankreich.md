﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt) -->
<!-- include-file(generated.txt) -->

<!-- var(collapsable)="close" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Meine berufliche Qualifikation in Frankreich anerkannt" -->
<!-- var(key)="fr-comprendre-qualification_professionnelle_en_france" -->
<!-- var(last-update)="2020-07-15 14:48:40" -->
<!-- var(lang)="de" -->
<!-- var(site:lang)="de" -->

Meine berufliche Qualifikation in Frankreich anerkannt
======================================================


<!-- begin-include(disclaimer-trans-de) -->
<!-- end-include -->
Neueste Aktualisierung:<!-- begin-var(last-update) -->2020-05-03 11:32:08<!-- end-var -->

Sie sind Staatsangehöriger der Europäischen Union oder des Europäischen Wirtschaftsraums und möchten, dass Ihre beruflichen Qualifikationen in Ihrem Herkunftsland anerkannt werden?<!-- collapsable:off -->
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Die Website guichet-qualifications.fr fördert die berufliche Mobilität der Einwohner der Europäischen Union und des Europäischen Wirtschaftsraums. Online-Dienste guichet-entreprises.fr und guichet-qualifications.fr sind die von der Europäischen Kommission anerkannte zentrale Anlaufstelle. Sie ermöglichen es Ihnen, neue Möglichkeiten zu erkunden und eine Aktivität auf französischem Territorium zu entwickeln, indem Sie Ihre Online-Formalitäten abschließen.

Auf dieser Website können Sie Ihre berufliche Qualifikation bei der zuständigen Behörde anerkennen:

- Ich habe einen Abschluss in einem EU- oder EWR-Mitgliedstaat und möchte[meine berufliche Qualifikation in Frankreich anerkennen zu lassen](diplom_mitgliedsstaat_ue_eee.md)
- Ich habe ein französisches Diplom und möchte[meine berufliche Qualifikation in einem EU- oder EWR-Mitgliedstaat anerkennen zu lassen](franzosisches_diplom.md)

Erstellen Sie eine Aktivität in Frankreich mit guichet-entreprises.fr
---------------------------------------------------------------------

Sobald Sie Ihre berufliche Qualifikation anerkannt haben, haben Sie die Möglichkeit, Ihr Online-Geschäft auf[guichet-entreprises.fr](https://www.guichet-entreprises.fr) ! Sie finden auch nützliche Informationen über regulierte Aktivitäten in Frankreich und Installationsbedingungen.

Regulierte Tätigkeiten<!-- collapsable:close -->
------------------------------------------------

Finden Sie heraus, welche Bedingungen Sie befolgen müssen, um eine regulierte Tätigkeit in Frankreich durchzuführen.

[Schauen Sie sich die Factsheets an](https://www.guichet-entreprises.fr/de/activites-reglementees/)

Freie Niederlassung<!-- collapsable:close -->
---------------------------------------------

Sie können sich jetzt in Frankreich niederlassen und Ihr Geschäft frei, und das, dauerhaft durchführen.

[Erfahren Sie mehr über die freie Niederlassung](https://www.guichet-entreprises.fr/de/eugo/libre-etablissement-le/)

Kostenlose Erbringung von Dienstleistungen<!-- collapsable:close -->
--------------------------------------------------------------------

Vorübergehende oder gelegentliche Dienstleistungen in Frankreich waren noch nie so einfach !

[Erfahren Sie mehr über kostenlose Service-Bereitstellung](https://www.guichet-entreprises.fr/de/eugo/libre-prestation-de-services-lps/)

