﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="La carte professionnelle européenne (CPE)" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(key)="fr-comprendre-carte_professionnelle_europeenne" -->
<!-- var(last-update)="2020-05-03 15:09:07" -->
<!-- var(lang)="de" -->
<!-- var(site:lang)="de" -->

Der Europäische Berufsausweis (CPE)
===================================


<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
Neueste Aktualisierung:<!-- begin-var(last-update) -->2020-05-03 15:09:07<!-- end-var -->

Einige reglementierte Berufe profitieren vom europäischen Berufsausweissystem. In diesem Artikel finden Sie alle Informationen über seine Funktionsweise, die Formalitäten, um es zu erhalten und die reglementierten Berufe, die es betrifft.

Was ist die europäische Visitenkarte?
-------------------------------------

Der Europäische Berufsausweis (ECC) ist ein elektronisches Verfahren zur Anerkennung von Berufsqualifikationen zwischen EU-Mitgliedstaaten, das in den Artikeln 4a bis 4 der[Richtlinie 2005/36/EG](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036). Ziel ist es, die Ausübung eines reglementierten Berufes für europäische Bürger in einem anderen EU-Mitgliedstaat zu erleichtern. Dieses Verfahren ist Teil der beruflichen Mobilität der europäischen Bürgerinnen und Bürger.

###### Gut zu wissen

Dabei handelt es sich nicht um eine physische Karte, sondern um einen elektronischen Nachweis, dass die beruflichen Qualifikationen des Bürgers überprüft wurden und von den zuständigen Behörden des Aufnahmelandes anerkannt werden.

Wer kann davon profitieren?
---------------------------

Dieses Gerät kann von jedem europäischen Fachmann sowie in der Umgebung im Gastland verwendet werden, um dort zu üben ([freie Niederlassung](https://www.guichet-entreprises.fr/de/eugo/libre-etablissement-le/)), nur im Rahmen der vorübergehenden Erbringung von Dienstleistungen ([kostenlose Service-Lieferung](https://www.guichet-entreprises.fr/de/eugo/libre-prestation-de-services-lps/)).

Bis heute ist das CPE für fünf Berufe verfügbar:

# Immobilienmakler;
# Bergführer;
# Krankenschwester;
# Physiotherapeut;
# Apotheker.

Diese Berufe wurden ausgewählt, um vom CPE-System nach drei Kriterien zu profitieren:

- erhebliche Mobilität oder erhebliches Mobilitätspotenzial in dem betreffenden Beruf;
- ausreichendes Interesse von Parteien mit berechtigtem Interesse;
- Regulierung des Berufs oder der damit verbundenen Ausbildung in einer erheblichen Anzahl von EU-Mitgliedstaaten.

Es wird erwartet, dass diese Zahl von Berufen zunehmen wird, da einige, wie Ingenieure oder spezialisierte Krankenschwestern, derzeit evaluiert werden.

Warum fragen?
-------------

Das CPE ist ein elektronisches Zertifikat. Sie zielt darauf ab, die Anerkennung von Berufsqualifikationen zu vereinfachen und ein zusätzliches Maß an Effizienz sowohl für die Bürger als auch für die zuständigen Behörden einzuführen, die für ihre Erteilung zuständig sind.

Wenn es um**kostenlose Service-Lieferung**soll die CPE die Erklärung vor der ersten Leistung im Aufnahmemitgliedstaat ersetzen, die alle 18 Monate erneuert werden muss.

Wenn es um**Einrichtung**ist die CPE die Entscheidung, die Berufsqualifikationen des Aufnahmemitgliedstaats anzuerkennen.

###### Gut zu wissen

Von der CPE betroffene Berufsangehörige haben die Wahl zwischen ihrem Gerät oder den traditionellen Verfahren für die Vorberichterstattung oder Anerkennung von Berufsqualifikationen.

###### Um weiter zu gehen

[Europäische Berufskarte](https://europa.eu/youreurope/citizens/work/professional-qualifications/european-professional-card/index_fr.htm) (Europäische Kommission)

