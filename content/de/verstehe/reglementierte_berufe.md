﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Professions réglementées" -->
<!-- var(key)="fr-comprendre-professions_reglementees" -->
<!-- var(last-update)="2020-05-03 15:09:36" -->
<!-- var(lang)="de" -->
<!-- var(site:lang)="de" -->

Reglementierte Berufe
=====================


<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
Neueste Aktualisierung:<!-- begin-var(last-update) -->2020-05-03 15:09:36<!-- end-var -->

Architekt, Krankenschwester, Tierarzt, Bäcker, etc., bevor Sie Ihre berufliche Qualifikation anerkannt haben, informieren Sie sich über die Bedingungen für den Zugang zu einem reglementierten Beruf in Frankreich.

Schauen Sie sich unsere Factsheets an, kategorisiert nach Berufsgruppen.

Spezifische Vorschriften in Frankreich
--------------------------------------

In Frankreich erfordern einige Berufe eine Berufsbezeichnung, eine berufliche Qualifikation oder sogar eine vorherige Genehmigung für ihre Praxis, wodurch ein bestimmtes Ausbildungs- oder Erfahrungsniveau sanktioniert wird. Die Berufsunterlagen enthalten eine Aktualisierung der Anerkennung von Berufsqualifikationen und der für diese verschiedenen Berufe geltenden Rechtsvorschriften. Bis heute sind fast 250 Aktivitäten im Einsatz und gegenstandswirb sind unsere beruflichen Aufzeichnungen.

**Wussten Sie das?** Die Berufskarten werden online gestellt, sobald sie von der Business Box Office in Zusammenarbeit mit den zuständigen Verwaltungen geschrieben werden.

Wenn das Merkblatt zu Ihrer beruflichen Qualifikation nicht verfügbar ist, gehen Sie[Europäische Datenbank reglementierter Berufe](https://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=homepage).

