﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Je suis titulaire d'un diplôme français..." -->
<!-- var(key)="fr-comprendre-diplome_francais" -->
<!-- var(last-update)="2020-05-03 15:09:27" -->
<!-- var(lang)="de" -->
<!-- var(site:lang)="de" -->

Ich habe einen französischen Abschluss...
=========================================


<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
Neueste Aktualisierung:<!-- begin-var(last-update) -->2020-05-03 15:09:27<!-- end-var -->

... und ich möchte, dass meine berufliche Qualifikation in einem Mitgliedstaat der Europäischen Union oder im Europäischen Wirtschaftsraum anerkannt wird
---------------------------------------------------------------------------------------------------------------------------------------------------------

Wenn Sie Staatsangehöriger eines Mitgliedstaats der Europäischen Union (EU) oder des Europäischen Wirtschaftsraums (EWR) sind, ein französisches Diplom oder ein von einem Drittland ausgestelltes Diplom besitzen und von Frankreich anerkannt sind, müssen Sie Ihre berufliche Qualifikation anerkennen lassen, bevor Sie in einem EU- oder EWR-Mitgliedstaat mit der Ausübung beginnen.

Ich möchte in ein EU- oder EWR-Land ziehen, in dem mein Beruf
-------------------------------------------------------------

Bevor Sie in einem Land arbeiten, informieren Sie sich über die reglementierten Berufe in diesem Land. Wenn Ihr Beruf reguliert wird, müssen Sie Ihre berufliche Qualifikation anerkennen lassen. Es ist die[zuständige Behörde](https://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=homepage) das Land, in dem Sie sich befinden, um Ihre Tätigkeit durchzuführen, was der Hinweis auf die zu befolgenden Schritte ist.

Sobald Sie die Anerkennung erhalten haben, können Sie Ihren Beruf als die Staatsangehörigen ausüben, die dort ihren Abschluss gemacht haben.

Ich möchte in einem Mitgliedstaat, in dem mein Beruf reglementiert ist, eine einmalige Leistung erbringen.
----------------------------------------------------------------------------------------------------------

Um eine vorübergehende Dienstleistung im Ausland zu erbringen, müssen Sie in Ihrem Herkunftsland niedergelassen sein, aber ohne die Verpflichtung zur Ausübung und Anerkennung von Berufsqualifikationen ist nicht notwendig, nur eine schriftliche Voranmeldung ist ausreichend. Je nach Grad der Auswirkungen des Berufs auf Gesundheit oder Sicherheit kann das Mitgliedsland um eine Überprüfung Ihrer beruflichen Qualifikation bitten. Eine Verlängerung der Erklärung kann jährlich erforderlich sein, wenn Sie vorübergehend in diesem Land arbeiten.

**Gut zu wissen**

Wenn der Beruf im Aufnahmestaat nicht geregelt ist, gehört die Beurteilung des Diploms und der beruflichen Ebene dem Arbeitgeber.

Schritte
--------

Die Behörden des Landes, in dem Sie die Anerkennung Ihrer Beruflichen Qualifikationen beantragen, haben einen Monat Zeit, um Ihre Bewerbung zu bestätigen und weitere Belege anzufordern.

Sobald die vollständige Akte eingegangen ist, entscheiden die Behörden innerhalb eines Zeitrahmens:

- 3 Monate, wenn Sie Arzt, Krankenschwester, Hebamme, Tierarzt, Zahnarzt, Apotheker oder Architekt sind und Sie von einem[Automatische Erkennung](https://europa.eu/youreurope/citizens/work/professional-qualifications/european-professional-card-documents/index_fr.htm#103720) ;
- 4 Monate für alle anderen Berufe.

Lassen Sie daher 5 Monate, in denen die Anerkennung der Qualifikation erfolgt, eine wesentliche Voraussetzung ein, bevor Sie Ihren Beruf rechtmäßig in dem Mitgliedstaat ausüben, in dem Sie Ihre Dienstleistung erbringen. Nach dieser Zeit[Wenden Sie sich an die Helpline](https://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=yec_pq) oder sprechen Sie mit[nationale Informationsstellen zur Anerkennung von Berufsqualifikationen](https://ec.europa.eu/growth/single-market/services/free-movement-professionals_fr/#contacts).

Weiter gehen
------------

Nützliche Links (Website der Europäischen Kommission):

- [Regulierte Berufe Datenbank listet Berufe auf, die von EU-Ländern und Behörden reguliert werden](https://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=homepage)
- [Liste nationaler Websites für reglementierte Berufe](https://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=sites)
- Um die zuständige nationale Behörde zu authentifizieren, können Sie sich an eine[Nationaler Informationspunkt zur Anerkennung von Berufsqualifikationen](https://ec.europa.eu/growth/single-market/services/free-movement-professionals_fr/#contacts).

Liste der EU-Mitgliedstaaten
----------------------------

Deutschland, Belgien, Bulgarien, Zypern, Dänemark, Spanien, Estland, Finnland, Frankreich, Griechenland, Ungarn, Irland, Italien, Lettland, Litauen, Luxemburg, Malta, Niederlande, Polen, Portugal, Rumänien, Vereinigtes Königreich, Slowakei, Slowenien, Schweden, Tschechische Republik.

Liste der Staaten des Europäischen Wirtschaftsraums
---------------------------------------------------

Dies sind die oben aufgeführten 28 Mitgliedstaaten der Europäischen Union (EU) und die folgenden drei Länder: Norwegen, Island, Liechtenstein.

