﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Je suis titulaire d’un diplôme d’un État membre de l’Union européenne ou de l’Espace économique européen..." -->
<!-- var(key)="fr-comprendre-diplome_etat_membre_ue_eee" -->
<!-- var(last-update)="2020-05-03 15:09:18" -->
<!-- var(lang)="de" -->
<!-- var(site:lang)="de" -->

Ich habe einen Abschluss aus einem Mitgliedstaat der Europäischen Union oder dem Europäischen Wirtschaftsraum...
================================================================================================================


<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
Neueste Aktualisierung:<!-- begin-var(last-update) -->2020-05-03 15:09:18<!-- end-var -->

... und ich möchte, dass meine berufliche Qualifikation in Frankreich anerkannt wird.
-------------------------------------------------------------------------------------

In Frankreich gibt es fast 250 reglementierte Berufe, die in zwei Kategorien unterteilt werden können:

- Die freien Berufe;
- Handwerks- und Handelsberufe, für die die Anerkennung der Berufsqualifikation bei der Handwerkskammer erfolgt, auf die Sie angewiesen sind.

Ich möchte nach Frankreich ziehen
---------------------------------

Zwei Schritte sind erforderlich, um in Frankreich einen reglementierten Beruf ausüben zu können:

# Anerkennung der Berufsqualifikation für Ausländer der Europäischen Union (EU) oder des Europäischen Wirtschaftsraums (EWR);
# Genehmigung zur Ausübung bei der zuständigen Behörde des betreffenden Berufs (zusätzliche Zulassung je nach Beruf).

das[Merkblätter zu reglementierten Berufen](reglementierte_berufe.md) diese Schritte zu identifizieren und Sie durch bestehende Vorschriften zu leiten.

Beachten Sie, dass Berufe, die von der[Europäische Berufskarte](europaische_visitenkarte.md) (CPE) kann somit vom herkömmlichen Verfahren abweichen:

- Krankenschwestern und Krankenschwestern und Krankenschwestern;
- Apotheker;
- Physiotherapeuten;
- Bergführer;
- Immobilienhändler.

Ich möchte eine vorübergehende Leistung in Frankreich ausstellen
----------------------------------------------------------------

Eine Berufsqualifikationsanerkennung ist nicht erforderlich, sie muss jedoch in Ihrem Heimatland niedergelassen sein. In einigen Fällen müssen Sie eine Genehmigung vor Ihrer Leistung beantragen.

Schritte
--------

Der Kontakt mit der zuständigen Behörde ist in den meisten Bereichen, in denen Sie sich entscheiden, sich niederzulassen, zu nehmen. Es sind eine Reihe von Belegen vorzulegen, von denen einige manchmal eine beglaubigte Übersetzung erfordern.

**Gut zu wissen**

Wenn Ihr Beruf in Ihrem Herkunftsland, aber nicht im Gastland (Frankreich) geregelt ist, können Sie ohne berufsqualifizierende Qualifikationen, wie z. B. Staatsangehörige des Gastlandes, praktizieren.

### Liste der EU-Mitgliedstaaten

Deutschland, Belgien, Bulgarien, Zypern, Dänemark, Spanien, Estland, Finnland, Frankreich, Griechenland, Ungarn, Irland, Italien, Lettland, Litauen, Luxemburg, Malta, Niederlande, Polen, Portugal, Rumänien, Vereinigtes Königreich, Slowakei, Slowenien, Schweden, Tschechische Republik.

### Liste der Staaten des Europäischen Wirtschaftsraums

Dies sind die oben aufgeführten 28 Mitgliedstaaten der Europäischen Union (EU) und die folgenden drei Länder: Norwegen, Island, Liechtenstein.

