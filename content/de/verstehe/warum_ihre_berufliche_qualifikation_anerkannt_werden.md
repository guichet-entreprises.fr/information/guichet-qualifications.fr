﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Pourquoi faire reconnaître sa qualification professionnelle ?" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(key)="fr-comprendre-pourquoi_faire_reconnaitre_sa_qualification_professionnelle" -->
<!-- var(last-update)="2020-05-03 15:09:33" -->
<!-- var(lang)="de" -->
<!-- var(site:lang)="de" -->

Warum haben Sie Ihre beruflichen Qualifikationen anerkannt?
===========================================================


<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
Neueste Aktualisierung:<!-- begin-var(last-update) -->2020-05-03 15:09:33<!-- end-var -->

Ein Beruf wird als "reguliert" bezeichnet, wenn es notwendig ist, ein bestimmtes Diplom zu besitzen, bestimmte Prüfungen bestanden zu haben, eine bestimmte Genehmigung erhalten zu haben oder bei einer Berufsorganisation registriert zu sein, um es durchzuführen.

**Anerkennung Ihrer beruflichen Qualifikationen** ist unerlässlich, wenn Sie in einem anderen EU-Land arbeiten möchten, in dem Ihr Beruf reglementiert ist. Dies bedeutet, dass seine Ausbildung und/oder Berufserfahrung vom Gastland offiziell anerkannt wird.

- Wenn es darum geht, in ein anderes Land zu ziehen, um seinen Beruf auszuüben,[freie Niederlassung](https://www.guichet-entreprises.fr/de/eugo/libre-etablissement-le/) und es ist wichtig, die Anerkennung seiner Qualifikationen zu machen.
- Wenn es darum geht, Dienstleistungen vorübergehend in einem anderen Land zu erbringen,[kostenlose Service-Lieferung](https://www.guichet-entreprises.fr/de/eugo/libre-prestation-de-services-lps/) und eine Voraberklärung genügt. Für Berufe mit schwerwiegenden Auswirkungen auf Gesundheit oder Sicherheit gibt es eine Ausnahme, da der Aufnahmemitgliedstaat möglicherweise eine Vorqualifizierungsprüfung verlangen kann.

Um die Regeln zu erfahren, die für Ihre Situation gelten, wenden Sie sich bitte an die nationale Behörde, die für den Zugang zu Ihrem Beruf im Gastland zuständig ist.

Sie können auch die[Europäische Datenbank reglementierter Berufe](http://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=homepage) die alle reglementierten Berufe in den EU-Ländern und ihre zuständigen Behörden auflistet.

- Wenn Ihr Beruf in Ihrem Heimatland geregelt ist, können Sie ihn in Ihrer eigenen Sprache suchen und die englische Übersetzung in der Beschreibung einsehen; Sie müssen dann nach dem englischen Namen suchen, um eine Liste der anderen Länder zu erhalten, in denen diese Besetzung reguliert ist. Wenn das Land, in dem Sie sich niederlassen möchten, nicht erscheint, kann dies bedeuten, dass der Beruf nicht reguliert ist.
- Wenn Sie Ihren Beruf nicht in der Datenbank finden, können Sie sich an die nationalen Informationsstellen zur Anerkennung von Berufsqualifikationen in dem Land wenden, in dem Sie arbeiten möchten. Sie können Ihnen helfen, die entsprechende Behörde zu finden und die erforderlichen Dokumente zu ermitteln.

**Lesen Sie auch:** [Europäische Berufskarte (CPE)](europaische_visitenkarte.md)

