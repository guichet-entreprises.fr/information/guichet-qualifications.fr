﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Bienvenue" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(key)="welcome" -->
<!-- var(translation)="Auto" -->
<!-- var(last-update)="2020-05-03 15:08:21" -->
<!-- var(lang)="de" -->
<!-- var(site:lang)="de" -->

Willkommen
==========


<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
Neueste Aktualisierung:<!-- begin-var(last-update) -->2020-05-03 15:08:21<!-- end-var -->

Auf dieser Seite finden Sie alle Informationen zum Verfahren zur Anerkennung der Berufsqualifikation.<!-- alert-start:info -->
<!-- alert-end:info -->

Erster Besuch in www.guichet-qualifications.fr
----------------------------------------------

Guichet-qualifications.fr ist die zentrale Anlaufstelle für die Anerkennung von Berufsqualifikationen. Der Online-Service ist kostenlos und für alle zugänglich und ermöglicht es Ihnen, einen Ordner zu erstellen, ohne reisen zu müssen. Sie finden auf der Website[die Factsheets der betroffenen Berufe](verstehe/reglementierte_berufe.md) Anerkennung der Qualifikation.

**Wussten Sie das?** Die Berufskarten werden online gestellt, sobald sie von der Business Box Office in Zusammenarbeit mit den zuständigen Verwaltungen geschrieben werden. Wir zeigen immer das Datum ihrer neuesten Updates.

Anerkennung der Berufsqualifikation: Formalitäten
-------------------------------------------------

Die Berufsqualifikationen zu vermitteln ist unerlässlich, wenn man in einem anderen Land der Europäischen Union arbeiten möchte, in dem der Beruf geregelt ist. Um die Regeln zu erfahren, die für Ihre Situation gelten, müssen Sie sich an die nationale Behörde wenden, die für den Zugang zu Ihrem Beruf im Gastland zuständig ist. Dank der www.guichet-qualifications.fr Website können Sie diese Formalitäten online erledigen, ohne reisen zu müssen.

**Lesen Sie auch** :[Warum haben Sie Ihre beruflichen Qualifikationen anerkannt?](https://www.guichet-qualifications.fr/de/pourquoi-faire-reconnaitre-sa-qualification-professionnelle/)

Erstellen Sie ein Konto und identifizieren Sie sich
---------------------------------------------------

Um den Online-Dienst nutzen zu können, sind Sie eingeladen,[Schaffung eines persönlichen Raums](https://account.guichet-qualifications.fr/session/new) am www.guichet-qualifications.fr. Es ermöglicht Ihnen, Ihre Datei zu erstellen und zu verwalten und Ihre persönlichen Daten zu ändern. Sie werden zu[www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/de/) Ihre Schritte bleiben jedoch auf der Registerkarte "Meine Schritte auf guichet-qualifications.fr" auf Ihrem Dashboard sichtbar.

Mit Authentifizierung[FranceConnect](https://franceconnect.gouv.fr/), wenn Sie bereits über ein Konto auf Impots.gouv.fr, Ameli.fr oder der Post Office-Website verfügen, können Sie sich mit einem dieser drei Konten bei www.guichet-qualifications.fr anmelden.

Vervollständigen und validieren Sie Ihre Datei online
-----------------------------------------------------

Die Website www.guichet-qualifications.fr ermöglicht es Ihnen, eine Online-Datei zu erstellen, alle Belege anzufügen und alle Kosten im Zusammenhang mit der Formalität zu bezahlen. Nachdem die Datei kompiliert und validiert wurde, wird sie zur Verarbeitung an die entsprechende Stelle gesendet. Um Ihre Datei nachzuverfolgen, können Sie sich jederzeit an die zuständige Behörde wenden, die Ihre Datei erhält.

**Wussten Sie das?** In Frankreich gibt es 250 reglementierte Berufe (gemäß der Qualifikationsrichtlinie), die in der[Reglementierte Berufe](verstehe/reglementierte_berufe.md). Wenn Sie jedoch Ihren Beruf nicht finden, gehen Sie zum[Europäische Datenbank reglementierter Berufe](http://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=sites).

