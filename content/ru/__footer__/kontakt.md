﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Contact" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-contact" -->
<!-- var(last-update)="2020-05-03 15:26:35" -->
<!-- var(lang)="ru" -->
<!-- var(site:lang)="ru" -->

Контакт
=======


<!-- begin-include(disclaimer-trans-ru) -->

**Уведомление о качестве машинного перевода**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Эта страница была переведена с помощью инструмента машинного перевода и может содержать ошибки. Пользователям рекомендуется проверить точность информации, представленной на этой странице, перед выполнением любых действий.

Банк Сервис Предприятие не может нести ответственность за эксплуатацию информации, которая будет неточным из-за неавтоматического перевод верному к оригиналу.<!-- alert-end:warning -->

<!-- end-include -->
Последнее обновление:<!-- begin-var(last-update) -->2020-05-03 15:26:35<!-- end-var -->

Команда www.guichet-qualifications.fr сайта предлагает вам возможность отправить вам по электронной почте следующий адрес:[contact.guichet-entreprises@finances.gouv.fr](mailto:contact.guichet-entreprises@finances.gouv.fr).

