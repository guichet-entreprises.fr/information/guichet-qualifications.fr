﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Qui sommes-nous ?" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-qui_sommes_nous" -->
<!-- var(last-update)="2020-05-03 15:29:30" -->
<!-- var(lang)="ru" -->
<!-- var(site:lang)="ru" -->

Кто мы такие?
=============


<!-- begin-include(disclaimer-trans-ru) -->

**Уведомление о качестве машинного перевода**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Эта страница была переведена с помощью инструмента машинного перевода и может содержать ошибки. Пользователям рекомендуется проверить точность информации, представленной на этой странице, перед выполнением любых действий.

Банк Сервис Предприятие не может нести ответственность за эксплуатацию информации, которая будет неточным из-за неавтоматического перевод верному к оригиналу.<!-- alert-end:warning -->

<!-- end-include -->
Последнее обновление:<!-- begin-var(last-update) -->2020-05-03 15:29:30<!-- end-var -->

Сайт www.guichet-qualifications.fr разработан и разработан общенациональной службой «Бизнес Бокс Офис», созданной[Постановление от 22 апреля 2015 года](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&dateTexte=20200109) (JORF от 25 апреля 2015 г.). Он находится под властью[Корпоративная дирекция](https://www.entreprises.gouv.fr/) в пределах[Министерство экономики и финансов](https://www.economie.gouv.fr/).

Национальный сервис "Бизнес Бокс Офис" управляет веб-сайтами[www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/) И[www.guichet-entreprises.fr](https://www.guichet-entreprises.fr/) которые в совокупности представляют собой электронный единый магазин, определяемый европейскими директивами[2006/123/EC](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32006L0123) И[2005/36](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036). Атмы такого рода существуют по всей Европе и федеративное в рамках проекта " [Эуго](http://ec.europa.eu/internal_market/eu-go/index_fr.htm) Европейской комиссии.

В Business Box Office мы поощряем профессиональную мобильность жителей Европейского Союза или Европейской экономической зоны, предоставляя доступ к полной информации о доступе и практике регулируемых профессий во Франции через "профессиональные записи".

Сайт[www.guichet-qualifications.fr](https://www.guichet-qualifications.fr/) направлена на всех жителей Европейского Союза или Европейской экономической зоны. Оно сообщает им о возможностях suring что их профессиональная квалификация узнана и что они практикуют отрегулированную профессию в Франции с целью устойчивого установки ([бесплатное создание](https://www.guichet-entreprises.fr/ru/eugo/libre-etablissement-le/)) или временно ([бесплатная доставка услуг](https://www.guichet-entreprises.fr/ru/eugo/libre-prestation-de-services-lps/)). Он также позволяет жителям Франции узнать о возможностях доступа к регулируемой профессии и ее практики в другом государстве-члене.

