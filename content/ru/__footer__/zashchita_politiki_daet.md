﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Politique de protection des données personnelles" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="fr-__footer__-politique_protection_donnees" -->
<!-- var(last-update)="2020-05-03 15:27:48" -->
<!-- var(lang)="ru" -->
<!-- var(site:lang)="ru" -->

Политика защиты персональных данных
===================================


<!-- begin-include(disclaimer-trans-ru) -->

**Уведомление о качестве машинного перевода**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Эта страница была переведена с помощью инструмента машинного перевода и может содержать ошибки. Пользователям рекомендуется проверить точность информации, представленной на этой странице, перед выполнением любых действий.

Банк Сервис Предприятие не может нести ответственность за эксплуатацию информации, которая будет неточным из-за неавтоматического перевод верному к оригиналу.<!-- alert-end:warning -->

<!-- end-include -->
Последнее обновление:<!-- begin-var(last-update) -->2020-05-03 15:27:48<!-- end-var -->

| Версия | Дата       | Объекта                                       |
|--------|------------|-----------------------------------------------|
| V1     | 25/05/2018 | Первоначальная версия                         |
| V2     | 01/09/2018 | Обновленная информация о делеге защиты данных |

Политика конфиденциальности («политика») информирует вас о том, как ваши данные собираются и обрабатываются службой Национального Делового Банка, о мерах безопасности, принятых для обеспечения их целостности и конфиденциальности, а также о правах, которые вы должны контролировать их использование.

Эта политика дополняет[условия использования](usloviia.md) (CGU), а также, при необходимости,[Юридических](iuridicheskie_upominaniia.md). Таким образом, считается, что эта политика включена в эти документы.

В рамках развития наших услуг и внедрения новых нормативных стандартов, возможно, нам придется изменить эту политику. Мы приглашаем вас регулярно читать его.

Кто мы такие?
-------------

Бизнес Box Office, созданный[Постановление от 22 апреля 2015 года](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&categorieLien=id) находится под властью[Корпоративная дирекция](https://www.entreprises.gouv.fr/) (DGE) в рамках[Министерство экономики и финансов](https://www.economie.gouv.fr/).

Бизнес Box Office службы реализует телесервис guichet-qualifcations.fr (в дальнейшем "Телесервис Box Office Профессиональные квалификации").

Профессиональная квалификация Teleservice Box Office предназначена для всех жителей Европейского Союза или Европейской экономической зоны. Он информирует их о возможностях оставить свою профессиональную квалификацию признанной и практикующей регулируемую профессию во Франции с целью устойчивого (бесплатного учреждения) или временного (бесплатного предоставления услуг).

Он также позволяет жителям Франции узнать о возможностях доступа к регулируемой профессии и ее практики в другом государстве-члене.

Профессиональная квалификация Teleservice Box Office является частью правовой базы:

- Из[Общий регламент по защите данных](https://www.cnil.fr/ru/reglement-europeen-protection-donnees) ;
- [Директива 2005/36/EC](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32005L0036) Европейский парламент;
- [Директива 2013/55/ЕС](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32013L0055) Европейский парламент;
- Из[Постановление 910/2014](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32014R0910) Европейский парламент и Совет от 23 июля 2014 года (e-IDAS) по электронной идентификации и доверенным услугам для электронных транзакций на внутреннем рынке;
- из[8 декабря 2005 г. Заказ](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000636232) электронных обменов между пользователями и административными органами, а также между административными органами и[Указ No 2010-112 от 2 февраля 2010 года](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021779444&categorieLien=id) для применения статей 9, 10 и 12 настоящего постановления;
- Из[Закон 78-17 от 6 января 1978 года с поправками](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460) компьютерные науки, файлы и свободы;
- из[Статья 441-1 Уголовного кодекса](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006418753&cidTexte=LEGITEXT000006070719&dateTexte=20020101) ;
- из[Статья R. 123-30 Торгового кодекса](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006256107&dateTexte=&categorieLien=cid) ;
- из[Постановление от 22 апреля 2015 года](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030517635&categorieLien=id) создание национально-компетентной службы под названием «окно бизнеса».

Какую организацию мы реализуем для защиты ваших данных?
-------------------------------------------------------

Соблюдение и выполнение применимых положений о защите данных контролируется представителем по защите данных Службы Бизнес Банка.

В министерстве экономики и финансов, от которого зависит Главное корпоративное управление, за проверку выполнения и соблюдения положений Общего регламента по защите данных отвечают службы старшего должностного лица оборонного ведомства.

Как я могу связаться с нами?
----------------------------

Если у вас есть какие-либо вопросы об использовании ваших персональных данных в Box Office Enterprise, пожалуйста, ознакомьтесь с этой политикой и свяжитесь с нашим представителем по защите данных, используя следующие контактные данные:

По почте:

```
Le Délégué à la protection des données des ministères économique et financier
Délégation aux Systèmes d’Information
139, rue de Bercy – Télédoc 322
75572 PARIS CEDEX 12


```
По электронной почте, указывая "Защита данных" объект:[Контакт](kontakt.md).

Что такое персональные данные?
------------------------------

Личные данные — это информация, которая идентифицирует вас, прямо или косвенно. Они могут включать ваше имя, имя, адрес, номер телефона или адрес электронной почты, а также IP-адрес вашего компьютера, например, или информацию, связанную с использованием Teleservice.

Когда мы собираем персональные данные?
--------------------------------------

Офис Business Box собирает персональные данные при:

- Создайте свое личное пространство
- завершить формальность через Teleservice Business Box.

Какие персональные данные мы обрабатываем?
------------------------------------------

Личные данные, с которой мы имеем дело, относятся только к формальностям признания профессиональной квалификации в соответствии с положениями статей R. 123-30-10 до R. 123-30-14 Кодекса о торговле.

Собранные данные позволяют собирать файл, как и ожидалось, в соответствии с разделом R. 123-30-10 Кодекса торговли.

В частности, собранные данные позволяют документам Cerfa, связанным с формальностями и процедурами, необходимыми для получения профессиональной квалификации или получения европейской профессиональной карты.

В следующем списке суммируются персональные данные, которые мы обрабатываем.

Для управления доступом к личному пространству сервиса пользователь делится следующей информацией:

- Идентификатор входа, выбранный пользователем
- Пароль, выбранный пользователем
- Адрес электронной почты пользователя
- Номер мобильного телефона
- IP-адрес входа.

Для использования личного места хранения пользователь делится следующей информацией:

- фамилия;
- Используйте имя/имя жены;
- Название упражнения
- Имя;
- дата рождения;
- Роды в стране;
- Национальность;
- Номер социального страхования;
- адрес проживания (включая страну);
- Телефон
- Электронной почты
- диплом, сертификат, звания;
- Опыт работы
- профессиональный проект;
- качество или функция;
- Текущая занятость
- место работы (местность, страна);
- под названием на занимаемую должность.

С какими личными подтверждающими документами мы имеем дело?
-----------------------------------------------------------

Личные подтверждающие документы, которыми мы имеем дело, относятся только к формальностям признания профессиональной квалификации или получения европейской профессиональной карты в соответствии с правилами, связанными с осуществляемой регулируемой деятельностью.

Подтверждающие документы используются для создания файла, как и ожидалось, в соответствии со статьей R. 123-30-10 Торгового кодекса.

Перечень подтверждающих документов утверждается компетентным органом, ответственным за расследование этого дела.

Личные подтверждающие документы включают документы, оправдывающие личность или место жительства. Эти подтверждающие документы хранятся на тех же условиях, что и персональные данные.

Их хранение предоставляется в помещениях, недоступных извне (Интернет), а их загрузка или передача обеспечивается шифрованием в соответствии с положениями, указанными в главе "Как защищены ваши персональные данные?".

Каково наше управление файлами cookie?
--------------------------------------

Управление cookie описано на странице:[https://www.guichet-entreprises.fr/ru/cookies/](https://www.guichet-entreprises.fr/ru/cookies/).

Все управляемые файлы cookie используют безопасный режим cookie.

Безопасные файлы cookie — это тип cookie, передаваемый исключительно через зашифрованные соединения (https).

Технические файлы cookie содержат следующие персональные данные:

- Имя;
- Имя;
- Электронной почты
- Телефон.

Аналитические файлы содержат IP-адрес входа.

Каковы ваши права на защиту ваших персональных данных?
------------------------------------------------------

Вы можете получить доступ к своим данным, исправить их, удалить его, ограничить его использование или против обработки.

Если вы считаете, что использование ваших персональных данных нарушает правила конфиденциальности, у вас есть возможность[Cnil](https://www.CNIL.fr/).

Вы также имеете право устанавливать правила обработки ваших персональных данных в случае смерти. Конкретные рекомендации могут быть зарегистрированы у лечебного менеджера. Общие руководящие принципы могут быть зарегистрированы в сторонней цифровой траст, сертифицированный CNIL. У вас есть возможность изменить или удалить эти руководящие принципы в любое время.

Если это так, вы также можете попросить нас предоставить вам личные данные, которые вы предоставили нам в читаемом формате.

Вы можете отправить различные запросы:

```
Service à compétence nationale Guichet Entreprises
120 rue de Bercy – Télédoc 766
75572 Paris cedex 12


```
Ваши заявления должны сопровождаться копией документа, удостоверяющего личность, и будут рассмотрены нашими службами.

Как защищаются ваши персональные данные?
----------------------------------------

В Box Office Enterprise внедряется все отраслевые стандартные методы и процедуры безопасности для предотвращения любых нарушений ваших персональных данных.

Вся предоставленная информация хранится на защищенных серверах, размещенных нашими техническими поставщиками.

В тех случаях, когда передача данных требуется и санкционирована, Касса предприятия обеспечивает, чтобы эти третьи стороны обеспечивали достаточные гарантии для обеспечения надлежащего уровня защиты. Обмены шифруются, а серверы проверены на основе взаимного признания.

В соответствии с правилами защиты данных, в случае нарушения служба "Деловой гуишет" обязуется сообщить об этом нарушении соответствующим надзорным органам и в случае необходимости соответствующим лицам.

#### Личное пространство

Все данные, передаваемые в ваше личное пространство, шифруются, а доступ обеспечивается с помощью личного пароля. Вы обязаны сохранять этот пароль конфиденциальным и не разглашать его.

#### Время консервации

Личные данные, обрабатываемые Кассой предприятия, хранятся в течение двенадцати месяцев в соответствии с положениями статьи 123-30-14 Торгового кодекса.

#### Что касается данных кредитных карт

Служба Enterprise Checkout не хранит и не хранит банковские данные пользователей, которые подлежат оплате на момент их формальности. Наш поставщик управляет данными о транзакциях от имени офиса Enterprise Box в соответствии с самыми строгими правилами безопасности, применимыми в индустрии онлайн-платежей с помощью процессов шифрования.

Кому передаются ваши данные?
----------------------------

Собранные данные позволяют собирать файл, как и ожидалось, в соответствии со статьей R.123-30-10 Торгового кодекса.

В соответствии со статьей Р.123-30-12 Кодекса торговли собранные данные передаются для обработки в соответствующие органы.

