﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(translation)="Auto" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Professions réglementées" -->
<!-- var(key)="fr-comprendre-professions_reglementees" -->
<!-- var(last-update)="2020-05-03 15:25:27" -->
<!-- var(lang)="ru" -->
<!-- var(site:lang)="ru" -->

Регулируемые профессии
======================


<!-- begin-include(disclaimer-trans-ru) -->

**Уведомление о качестве машинного перевода**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Эта страница была переведена с помощью инструмента машинного перевода и может содержать ошибки. Пользователям рекомендуется проверить точность информации, представленной на этой странице, перед выполнением любых действий.

Банк Сервис Предприятие не может нести ответственность за эксплуатацию информации, которая будет неточным из-за неавтоматического перевод верному к оригиналу.<!-- alert-end:warning -->

<!-- end-include -->
Последнее обновление:<!-- begin-var(last-update) -->2020-05-03 15:25:27<!-- end-var -->

Архитектор, медсестра, ветеринар, пекарь и т.д., прежде чем у вас есть профессиональная квалификация признана, узнать об условиях доступа к регулируемой профессии во Франции.

Ознакомьтесь с нашими информационными бюллетенями, классифицированными по профессиональным группам.

Конкретные правила во Франции
-----------------------------

Во Франции некоторые профессии требуют профессионального назначения, профессиональной квалификации или даже предварительного разрешения на их практику, что является санкционированием определенного уровня подготовки или опыта. Профессиональные отчеты предоставляют обновленную информацию о том, как распознать профессиональную квалификацию и законодательство, применимое к этим различным профессиям. На сегодняшний день, около 250 мероприятий участвуют и являются предметом наших профессиональных записей.

**Ты знала об этом?** Профессиональные карты выкладываются в интернет, как только они написаны Business Box Office в сотрудничестве с соответствующими администрациями.

Если информационный бюллетень о вашей профессиональной квалификации отсутствует,[Европейская база данных регулируемых профессий](https://ec.europa.eu/growth/tools-databases/regprof/index.cfm?action=homepage).

