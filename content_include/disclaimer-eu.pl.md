﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="pl" -->

Zawiadomienie o jakość tłumaczenia
==================================

<!-- begin-ref(disclaimer-eu-pl) -->

**Zawiadomienie o jakość tłumaczenia**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Ta strona została przetłumaczona przez profesjonalny, ale może zawierać błędy. Użytkownicy proszeni są o sprawdzenie poprawności informacji zawartych na tej stronie przed podjęciem jakichkolwiek działań.
Enterprise Banku Serwis nie ponosi odpowiedzialności za działanie informacji, które będą niedokładne ze względu na tłumaczenie non-wierny oryginału.<!-- alert-end:warning -->

<!-- end-ref -->